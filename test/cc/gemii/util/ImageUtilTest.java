package cc.gemii.util;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.junit.Test;

public class ImageUtilTest {
	
	public static final String S3_DIR_ROBOT_IMG1 = "accounts/qrcode";
	public static final String S3_DIR_ROBOT_IMG2 = "accounts/source_qrcode";
	

	@Test
	public void crop_test() throws IOException{
		int x = 0;
	    int y = 0;
	    int w = 1064-x;
	    int h = 744-y;
	    String path = "/Users/yk/Desktop/WX20170823-140831@2x.png";
	    String path_croped = "/Users/yk/Desktop/WX20170823-140831@2x_croped.png";
	    BufferedImage image =  ImageIO.read(new File(path));
	    BufferedImage out = image.getSubimage(x, y, w, h);
	    ImageIO.write(out, "png", new File(path_croped));
	}
	
	@Test
	public void resjze_test() throws IOException{
	 	System.out.println("resizing...");
	 	Image originalImage = new ImageIcon("/Users/yk/Desktop/WX20170823-140831@2x_croped.png").getImage();
        int imageType = BufferedImage.TYPE_INT_RGB;
        BufferedImage scaledBI = new BufferedImage(1064, 744, imageType);
        Graphics2D g = scaledBI.createGraphics();
        g.setComposite(AlphaComposite.Src);
        g.drawImage(originalImage, 0, 0, 1064, 744, null); 
        g.dispose();
	    ImageIO.write(scaledBI, "jpg", new File("/Users/yk/Desktop/WX20170823-140831@2x_resize.jpg"));
	}
	
	
	@Test
	public void crop_resjze_test() throws IOException{
		new FileUtil("AKIAOTDCELIZTI4INNVA","A3baxBnGJSVknuuYYfGS8xImwUF4r3xeKnd/3NR2");
		String filePath = "/Users/yk/Desktop/";
		String fileName = "福利小助手 小宝_crop_resize.jpg";
	    FileInputStream fin = new FileInputStream("/Users/yk/Desktop/福利小助手 小宝.png");
	    
	    String fileUrl = FileUtil.uploadFile(S3_DIR_ROBOT_IMG2, fileName,fin);
	    System.out.println(fileUrl);
		int handelResult = cropAndResjzeQRCode(fin,filePath,fileName);
	    if(1 == handelResult){
		    fin = new FileInputStream(filePath+fileName);
		    fileUrl = FileUtil.uploadFile(S3_DIR_ROBOT_IMG1, fileName,fin);
		    System.out.println(fileUrl);
	    }
	}
	
	private int cropAndResjzeQRCode(InputStream fin,String filePath,String fileName) throws FileNotFoundException{
		int xImage = 4000;
		int yImage = 3889;
		int x = 800;
	    int y = 1350;
	    int wResultImage = 430;
	    int hResultImage = 430;
	    
	    int w = xImage-2*x;
	    int h = yImage-y;
		String file = filePath+fileName;
		FileOutputStream fout = new FileOutputStream(file);
		try {
			BufferedImage image = ImageIO.read(fin);
			BufferedImage out = image.getSubimage(x, y, w, h);
	        BufferedImage scaledBI = new BufferedImage(wResultImage, hResultImage, BufferedImage.TYPE_INT_RGB);
	        Graphics2D g = scaledBI.createGraphics();
	        g.setComposite(AlphaComposite.Src);
	        g.drawImage(out, 0, 0, wResultImage, hResultImage, null); 
	        g.dispose();
		    ImageIO.write(scaledBI, "jpg", fout);
		    fout.close();
		    return 1;
		} catch (IOException e) {
			e.printStackTrace();
			return -2;
		}
	}
}
