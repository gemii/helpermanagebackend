package cc.gemii.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

public class WechatCardTest {

	private static String token = "jdPyLric2Ag0lC0J0dcMktT3Z87GWYKaR0rgkTZ8ix9IvzE_c8LecuaVp0f6oNKXBiaNzv24ApyuJokCIc5QgxGt3tbFHNhnwvkU-_uqT4ofU54T7cNZRl46jdyZQccCRMMaABAGBB";
	private static String DEFAULT_CHARSET = "UTF-8";

	private static enum HTTPMehtod {
		GET, POST
	}

	/*
	 * 图片上传 curl https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=
	 * O41Vh8MpzD9WizxndYBxT3pDLxNaALrIL9nEQnabg0Smi9oGE65Zs7dmidUpPpgxKd2QixadBJHT7dRmSYEtmWYCHJfREhX3iEy3ueRAWV4Y7GhwuLm0BejFpQSe
	 * -3OOUAVhADANTO -F buffer=@123.jpg
	 */

	/*
	 * 创建会员卡
	 */
	// @Test
	// public void create(){
	// String url = "https://api.weixin.qq.com/card/create?access_token="+token;
	// StringBuffer sb = new StringBuffer();
	// sb.append("{");
	// sb.append(" \"card\": {");
	// sb.append(" \"card_type\": \"MEMBER_CARD\",");
	// sb.append(" \"member_card\": {");
	// sb.append(" \"background_pic_url\":
	// \"http://mmbiz.qpic.cn/mmbiz_png/bVZHjI8R7o4mKdbBgA3icdCiaORfxKoAHPPeiaEOBFTD70tEMCAaBHj4Fhyot7BwfkpiaY8E63TS7EHrNUfOe9QpOw/0\",");
	// sb.append(" \"base_info\": {");
	// sb.append(" \"logo_url\":
	// \"http://mmbiz.qpic.cn/mmbiz_png/bVZHjI8R7o5GWESdj4ibOdgBgUmI750vGomUSOuxOFXmnAibmmuqKSGp02JT787N3HvrCX8x3akJ1uTKrmDoPGng/0?wx_fmt=png\",");
	// sb.append(" \"brand_name\": \"启爱空间\",");
	// sb.append(" \"code_type\": \"CODE_TYPE_TEXT\",");
	// sb.append(" \"title\": \"启爱空间妈妈群\",");
	// sb.append(" \"color\": \"Color010\",");
	// sb.append(" \"notice\": \"...\",");
	// sb.append(" \"service_phone\": \"\",");
	// sb.append(" \"description\": \"这里将是您的：\r\n"
	// + "知识站：专业医生定期微课堂解答\\r\\n"
	// + "加油站：孕期妈妈间的亲密互动畅聊烦恼\\r\\n"
	// + "囤货站：社群妈妈共享的大牌福利与超值优惠\",");
	// sb.append(" \"date_info\": {");
	// sb.append(" \"type\": \"DATE_TYPE_PERMANENT\"");
	// sb.append(" },");
	// sb.append(" \"sku\": {");
	// sb.append(" \"quantity\": 50000000");
	// sb.append(" },");
	//// sb.append(" \"get_limit\": 5,");
	//// sb.append(" \"use_custom_code\": false,");
	// sb.append(" \"can_give_friend\": false,");
	//// sb.append(" \"location_id_list\": [");
	//// sb.append(" 123,");
	//// sb.append(" 12321");
	//// sb.append(" ],");
	//// sb.append(" \"custom_url_name\": \"立即使用\",");
	//// sb.append(" \"custom_url\": \"http://weixin.qq.com\",");
	//// sb.append(" \"custom_url_sub_title\": \"6个汉字tips\",");
	//// sb.append(" \"promotion_url_name\": \"营销入口1\",");
	//// sb.append(" \"promotion_url\": \"http://www.qq.com\",");
	// sb.append(" \"need_push_on_view\": false");
	// sb.append(" },");
	//// sb.append(" \"advanced_info\": {");
	//// sb.append(" \"use_condition\": {");
	//// sb.append(" \"accept_category\": \"鞋类\",");
	//// sb.append(" \"reject_category\": \"阿迪达斯\",");
	//// sb.append(" \"can_use_with_other_discount\": true");
	//// sb.append(" },");
	//// sb.append(" \"abstract\": {");
	//// sb.append(" \"abstract\": \"微信餐厅推出多种新季菜品，期待您的光临\",");
	//// sb.append(" \"icon_url_list\": [");
	//// sb.append("
	// \"http://mmbiz.qpic.cn/mmbiz/p98FjXy8LacgHxp3sJ3vn97bGLz0ib0Sfz1bjiaoOYA027iasqSG0sj
	// piby4vce3AtaPu6cIhBHkt6IjlkY9YnDsfw/0\"");
	//// sb.append(" ]");
	//// sb.append(" },");
	//// sb.append(" \"text_image_list\": [");
	//// sb.append(" {");
	//// sb.append(" \"image_url\":
	// \"http://mmbiz.qpic.cn/mmbiz/p98FjXy8LacgHxp3sJ3vn97bGLz0ib0Sfz1bjiaoOYA027iasqSG0sjpiby4vce3AtaPu6cIhBHkt6IjlkY9YnDsfw/0\",");
	//// sb.append(" \"text\": \"此菜品精选食材，以独特的烹饪方法，最大程度地刺激食 客的味蕾\"");
	//// sb.append(" },");
	//// sb.append(" {");
	//// sb.append(" \"image_url\":
	// \"http://mmbiz.qpic.cn/mmbiz/p98FjXy8LacgHxp3sJ3vn97bGLz0ib0Sfz1bjiaoOYA027iasqSG0sj
	// piby4vce3AtaPu6cIhBHkt6IjlkY9YnDsfw/0\",");
	//// sb.append(" \"text\": \"此菜品迎合大众口味，老少皆宜，营养均衡\"");
	//// sb.append(" }");
	//// sb.append(" ],");
	//// sb.append(" \"time_limit\": [");
	//// sb.append(" {");
	//// sb.append(" \"type\": \"MONDAY\",");
	//// sb.append(" \"begin_hour\":0,");
	//// sb.append(" \"end_hour\":10,");
	//// sb.append(" \"begin_minute\":10,");
	//// sb.append(" \"end_minute\":59");
	//// sb.append(" },");
	//// sb.append(" {");
	//// sb.append(" \"type\": \"HOLIDAY\"");
	//// sb.append(" }");
	//// sb.append(" ],");
	//// sb.append(" \"business_service\": [");
	//// sb.append(" \"BIZ_SERVICE_FREE_WIFI\",");
	//// sb.append(" \"BIZ_SERVICE_WITH_PET\",");
	//// sb.append(" \"BIZ_SERVICE_FREE_PARK\",");
	//// sb.append(" \"BIZ_SERVICE_DELIVER\"");
	//// sb.append(" ]");
	//// sb.append(" },");
	// sb.append(" \"supply_bonus\": false,");
	// sb.append(" \"supply_balance\": false,");
	// sb.append(" \"prerogative\": \"宝妈微社群\",");
	//// sb.append(" \"auto_activate\": true,");
	//// sb.append(" \"custom_field1\": {");
	//// sb.append(" \"name_type\": \"FIELD_NAME_TYPE_LEVEL\",");
	//// sb.append(" \"url\": \"http://www.qq.com\"");
	//// sb.append(" },");
	//// sb.append(" \"activate_url\": \"http://www.qq.com\",");
	//// sb.append(" \"custom_cell1\": {");
	//// sb.append(" \"name\": \"使用入口2\",");
	//// sb.append(" \"tips\": \"激活后显示\",");
	//// sb.append(" \"url\": \"http://www.xxx.com\"");
	//// sb.append(" },");
	//// sb.append(" \"bonus_rule\": {");
	//// sb.append(" \"cost_money_unit\": 100,");
	//// sb.append(" \"increase_bonus\": 1,");
	//// sb.append(" \"max_increase_bonus\": 200,");
	//// sb.append(" \"init_increase_bonus\": 10,");
	//// sb.append(" \"cost_bonus_unit\": 5,");
	//// sb.append(" \"reduce_money\": 100,");
	//// sb.append(" \"least_money_to_use_bonus\": 1000,");
	//// sb.append(" \"max_reduce_bonus\": 50");
	//// sb.append(" },");
	// sb.append(" \"wx_activate\": true,");
	// sb.append(" \"wx_activate_after_submit\": true,");
	// sb.append(" \"wx_activate_after_submit_url\":\"https://www.baidu.com\"");
	//// sb.append(" \"discount\": 10");
	// sb.append(" }");
	// sb.append(" }");
	// sb.append("}");
	// String json = sb.toString();
	// String result = WechatCardTest.request(url, json, HTTPMehtod.POST,
	// DEFAULT_CHARSET, true);
	// System.out.println(result);
	// }
	//
	// /*
	// * 设置开卡字段
	// * */
	// @Test
	// public void set(){
	// String url =
	// "https://api.weixin.qq.com/card/membercard/activateuserform/set?access_token="+token;
	// String json = "{"
	// + "\"card_id\": \"pNPcuvzIf0QhlaOarm1_yBV37eNw\","
	// + "\"required_form\": {"
	// + "\"can_modify\":false,"
	// +"\"common_field_id_list\":["
	// + "\"USER_FORM_INFO_FLAG_MOBILE\""
	// + "]"
	// + "},"
	// +"\"optional_form\":{},"
	// +"\"service_statement\": {}"
	// +"}";
	// String result = WechatCardTest.request(url, json, HTTPMehtod.POST,
	// DEFAULT_CHARSET, true);
	// System.out.println(result);
	// }
	//
	// /*
	// * 活动卡券url
	// * */
	// @Test
	// public void geturl(){
	// String url =
	// "https://api.weixin.qq.com/card/membercard/activate/geturl?access_token="+token;
	// String json = "{"
	// +"\"card_id\" : \"pNPcuvzIf0QhlaOarm1_yBV37eNw\","
	// +"\"outer_str\" : \"123\""
	// +"}";
	// String result = WechatCardTest.request(url, json, HTTPMehtod.POST,
	// DEFAULT_CHARSET, true);
	// System.out.println(result);
	// }
	//
	// @Test
	// public void tempTest(){
	// String url = "http://wyeth.gemii.cc/HelperManage/enter/receive/match";
	// String json = "{"
	// +"\"openid\" : \"oNPcuv6IH2E1pRxyVVu7LNZ8SOdM\","
	// +"\"city\" : \"上海\","
	// +"\"hospital\" : \"上海市浦东新区妇幼保健院\","
	// +"\"phone\" : \"18516513048\","
	// +"\"edc\" : \"2017-05-06\","
	// +"}";
	// String result = WechatCardTest.request(url, json, HTTPMehtod.POST,
	// DEFAULT_CHARSET, false);
	// System.out.println(result);
	// }
	//
	// @Test
	// public void tempTest1(){
	// String url = "http://wyeth.gemii.cc/HelperManage/enter/select/robotid";
	// String json = "{"
	// +"\"openid\" : \"oNPcuv6IH2E1pRxyVVu7LNZ8SOdM\""
	// +"}";
	// String result = WechatCardTest.request(url, json, HTTPMehtod.POST,
	// DEFAULT_CHARSET, false);
	// System.out.println(result);
	// }

	public static String request(String urlStr, String paramStr, HTTPMehtod method, String charset, boolean isHttps,
			Map<String, String> header) throws Exception {
		StringBuffer result = new StringBuffer("");

		HttpURLConnection urlConn = null;
		PrintWriter pw = null;
		BufferedReader br = null;
		try {
			URL url = new URL(urlStr);
			if (isHttps) {
				trustAllHttpsCertificates();
				HostnameVerifier hv = new HostnameVerifier() {
					@Override
					public boolean verify(String arg0, SSLSession arg1) {
						return false;
					}
				};
				HttpsURLConnection.setDefaultHostnameVerifier(hv);
				urlConn = (HttpsURLConnection) url.openConnection();
			} else {
				urlConn = (HttpURLConnection) url.openConnection();
			}
			if (HTTPMehtod.POST == method) {
				urlConn.setRequestMethod("POST");
			}
			if (header != null) {
				for (String key : header.keySet()) {
					urlConn.setRequestProperty(key, header.get(key));
				}
			}
			urlConn.setDoOutput(true);
			urlConn.setDoInput(true);
			urlConn.connect();

			pw = new PrintWriter(urlConn.getOutputStream());
			if (paramStr != null) {
				pw.write(paramStr);
			}
			pw.flush();

			br = new BufferedReader(new InputStreamReader(urlConn.getInputStream(), charset));

			for (String str = br.readLine(); str != null; str = br.readLine()) {
				result.append(str);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (urlConn != null) {
				urlConn.disconnect();
			}
			if (pw != null) {
				pw.close();
			}
			if (br != null) {
				pw.close();
			}
		}
		return result.toString();
	}

	private static void trustAllHttpsCertificates() throws Exception {
		javax.net.ssl.TrustManager[] trustAllCerts = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc = javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	static class miTM implements javax.net.ssl.TrustManager, javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	@Test
	public void testPost() throws Exception {
		String url = "https://gwdev.rfc-china.com:8888/api/sso/access_token";
		// String json = "";
		// Map<String,String> header = new HashMap<>();
		String request = WechatCardTest.request(url, "appid=crm.wedo&appsecret=crm.wedo", HTTPMehtod.GET,
				DEFAULT_CHARSET, true, null);
		Map<String, Object> token = JSON.parseObject(request);
		String access_token = (String) token.get("data");
		System.out.println(access_token);
	}

}
