package cc.gemii.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;
import cc.gemii.data.otd.ErrorInfoToFriso;
import cc.gemii.pojo.MatchGroup;

public class EnterGroupMapperTest extends AbstractBaseTest{

	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Test
	public void getMatchGroup_test(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("country", "%2017%");
		map.put("robotID", "467439024");
		//map.put("city", this.handleCity(user.getCity()));
		map.put("city", "上海");
		map.put("edc", "2016-12-01");
		map.put("hospital", "上海虹口妇幼");
		map.put("code", "1234");
		map.put("uid", "@1234");
		map.put("phone", "18721317074");
		map.put("srcode", "");
		map.put("openid", "test");
		MatchGroup sendGroup = enterGroupMapper.getMatchGroup(map);
		System.out.println(sendGroup);
	}
	
	@Test
	public void getCityGroup_test(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("country", "%2017%");
		map.put("robotID", "467439024");
		//map.put("city", this.handleCity(user.getCity()));
		map.put("city", "上海");
		map.put("edc", "2016-12-01");
		map.put("hospital", "上海虹口妇幼");
		map.put("code", "1234");
		map.put("uid", "@1234");
		map.put("phone", "18721317074");
		map.put("srcode", "");
		map.put("openid", "test");
		MatchGroup cityGroup = enterGroupMapper.getCityGroup(map);
		System.out.println(cityGroup);
	}
	
	public void getCountryGroup_test(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("country", "%2017%");
		map.put("robotID", "467439024");
		//map.put("city", this.handleCity(user.getCity()));
		map.put("city", "上海");
		map.put("edc", "2016-12-01");
		map.put("hospital", "上海虹口妇幼");
		map.put("code", "1234");
		map.put("uid", "@1234");
		map.put("phone", "18721317074");
		map.put("srcode", "");
		map.put("openid", "test");
		MatchGroup countryGroup = enterGroupMapper.getCountryGroup(map);
		System.out.println(countryGroup);
	}
	
	@Test
	public void insertIntoFriso(){
		
		ArrayList<ErrorInfoToFriso> data = new ArrayList<>();
		ErrorInfoToFriso errorInfoToFriso = new ErrorInfoToFriso();
		errorInfoToFriso.setType(1);
		errorInfoToFriso.setOpenId("1111");
		errorInfoToFriso.setGroupId("11111");
		errorInfoToFriso.setGroupName("111111");
		data.add(errorInfoToFriso);
		enterGroupMapper.insertErrorInfo(data.get(0));
	}
	
	@Test
	public void selectMemberInfoByOpenid(){
		String openID = "oNPcuv9b3ahEPJaGW5TB4CCzjgzk";
		List<String> selectMemberInfoByOpenid = enterGroupMapper.selectMemberInfoByOpenid(openID);
		System.out.println(selectMemberInfoByOpenid.toString());
	}
}
