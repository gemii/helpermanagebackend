package cc.gemii.component.matchGroup;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import net.sf.json.JSONObject;

public class MatchGroupContextTest extends AbstractBaseTest{

	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Test
	public void matchGroup_test(){
		Map<String, String> matchingconds = new HashMap<String,String>();
		matchingconds.put("phone","15639951556");
//		matchingconds.put("openid","oNPcuv6IH2E1pRxyVVu7LNZ8SOdM");
		matchingconds.put("openid","oNPcuv_t1Wo26yZ1kjArci_Cs42sdfsdfU");
		matchingconds.put("city","苏州");
		MatchGroupResult matchResult = matchGroupContext.matchGroup(MatchGroupStrategy.HUGGIES_MATCHGROUP_STRATEGY, matchingconds);
		System.out.println(JSONObject.fromObject(matchResult));
	}
}
