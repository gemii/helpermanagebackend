package cc.gemii.component.matchGroup;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;

public class HuggiesMatchGroupStrategyTest extends AbstractBaseTest{

	@Autowired
	private IMatchGroupStrategy huggiesMatchGroupStrategy;
	
	@Test
	public void matchGroup_test(){
		Map<String,String> _matchingconds = new HashMap<String,String>();
		_matchingconds.put("edc", "2017-02-02");
		_matchingconds.put("city", "上海");
		_matchingconds.put("hospital", "其他");
		_matchingconds.put("phone", "18516513049");
		huggiesMatchGroupStrategy.matchGroup(_matchingconds);
	}
	
}
