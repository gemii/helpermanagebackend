package cc.gemii.component.sendMsg;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;
import cc.gemii.data.ShortMessage;

public class MontnetsSendSMSTest extends AbstractBaseTest{

	
	@Autowired
	private ISendSMS montnetsSendSMS;
	
	@Test
	public void sendSMS_test(){
		ShortMessage _shortMessage = new ShortMessage();
		_shortMessage.setContent("欢迎加入社群，您可以获得孕期健康干货，专家入群讲课，周末福利大奖，电话回访关爱，服务热线020-89285847，回复T退订");
		_shortMessage.setReciverPhone("18516513048,15212147570");
		montnetsSendSMS.sendSMS(_shortMessage);
	}
}
