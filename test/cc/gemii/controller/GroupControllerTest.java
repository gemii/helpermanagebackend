package cc.gemii.controller;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import cc.gemii.AbstractBaseControllerTest;

public class GroupControllerTest extends AbstractBaseControllerTest{
	
	@Test
	public void getAllGroup_test(){
		MockHttpServletRequestBuilder req = MockMvcRequestBuilders.get("/group/getGroup")
				.param("page", "3")
				.param("clerkID","12034")
				.param("pageSize","20");
		try {
			MvcResult result = mockMvc.perform(req).andReturn();
			MockHttpServletResponse response = result.getResponse();
			System.out.println(response.getContentAsString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
