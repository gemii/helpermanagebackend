package cc.gemii.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mysql.jdbc.TimeUtil;

import cc.gemii.AbstractBaseTest;
import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.data.otd.EnterGroupInfo;
import cc.gemii.data.otd.ErrorInfoToFriso;
import cc.gemii.data.otd.FrisoUserInfo;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.TimeUtils;

public class FrisoEnterGroupServiceTest extends AbstractBaseTest{

	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Autowired
	private FrisoEnterGroupService frisoEnterGroupService;
	
	/**
	 * 美素佳儿入群测试 
	 * type : 5
	 */
	@Test
	public void beautyGoodSon_test(){
		UserFriendEnterGroup member=new UserFriendEnterGroup();
		member.setEdc("2017-11-19");
		member.setPhone("15212147570");
		member.setUsername("请叫我小稳稳");
		member.setCity("南京");
		member.setName("稳稳");
		member.setOpenid("oNPcuv67ncH2mvL5A4eWz75F3Gns");
		CommonResult result = frisoEnterGroupService.beautyGoodSonMatchGroup(member);
		
	}
	
	/**
	 * 新美素佳儿入群测试 
	 * type : 51
	 */
	@Test
	public void newBeautyGoodSon_test(){
		UserFriendEnterGroup member=new UserFriendEnterGroup();
		member.setHospital("爱婴室");
		member.setEdc("2016-11-19");
		member.setPhone("15212147570");
		member.setUsername("请叫我小稳稳");
		member.setName("稳稳");
		member.setOpenid("oNPcuv67ncH2mvL5A4eWz75F3Gns");
		CommonResult result = frisoEnterGroupService.newBeautyGoodSonMatchGroup(member);
		
	}
	
	/**
	 * 对接美素佳儿入群测试 
	 * type : 52
	 */
	@Test
	public void matchGroupForFriso(){
		FrisoUserInfo userInfo = new FrisoUserInfo();
		userInfo.setProvince("上海");
		userInfo.setCity("太原");
		userInfo.setStore("爱婴室");
		userInfo.setName("小稳稳");
		userInfo.setEdc("2018/03/11");
		userInfo.setChannel(1);
		userInfo.setOpenid("oNPcuv67ncH2mvL5A4eWz75F3Gn");
		userInfo.setCmdInfo("this is cmdInfo");
		userInfo.setPhone("15212147570");
		CommonResult matchGroupForFriso = frisoEnterGroupService.matchGroupForFriso(userInfo);
		System.out.println(matchGroupForFriso.toString());
	}
	
	@Test
	public void returnInfoToFriso(){
		
//		String json = "{\"openID\":\"oNPcuv67ncH2mvL5A4eWz75F3Gns\",\"uRoomID\":\"78851ACE419C61B6966B1B3118E84516\",\"userID\":\"515194\"}";

//		CommonResult returnInfoToFriso = frisoEnterGroupService.returnInfoToFriso(json);
		
	}
	
	@Test
	public void getAccessToken(){
		EnterGroupInfo enterGroupInfo = new EnterGroupInfo();
		enterGroupInfo.setuRoomID("MS0001");
		enterGroupInfo.setOpenID("oNPcuv67ncH2mvL5A4eWz75F3Gns");
		enterGroupInfo.setTime(TimeUtils.getTime());
		CommonResult returnEnterGroupInfoToFriso = frisoEnterGroupService.returnEnterOrQuitGroupInfoToFriso(enterGroupInfo);
	}
	
	@Test
	public void getToken(){
		String url = "";
		String json = "";
		Map<String, String> headers = new HashMap<>();
//		headers.put("Authorization ","bearer "+ access_token);
		HttpClientUtil.doPostJsonSetHeader(url, json, headers);
	}
	
	@Test
	public void returnInfo(){
		EnterGroupInfo enterGroupInfo = new EnterGroupInfo();
		enterGroupInfo.setOpenID("oNPcuv67ncH2mvL5A4eWz75F3Gns");
		enterGroupInfo.setTime("2017-08-14T18:02:43");
		enterGroupInfo.setType("2");
		enterGroupInfo.setuRoomID("48E597D2A57B8154F8F8C9BE5418F593");
		CommonResult returnEnterOrQuitGroupInfoToFriso = frisoEnterGroupService.returnEnterOrQuitGroupInfoToFriso(enterGroupInfo);
		System.out.println(returnEnterOrQuitGroupInfoToFriso);
	}
	
	@Test
	public void updateErrorInfo(){
		List<ErrorInfoToFriso> selectErrorInfo = frisoEnterGroupService.selectErrorInfo();
		frisoEnterGroupService.updateErrorInfo(selectErrorInfo);
	}
	
	@Test
	public void tokenTime(){
		EnterGroupInfo enterGroupInfo = new EnterGroupInfo();
		//test
//		enterGroupInfo.setOpenID("oNPcuv67ncH2mvL5A4eWz75F3Gns");
//		enterGroupInfo.setuRoomID("48E597D2A57B8154F8F8C9BE5418F593");
		//pro
		enterGroupInfo.setOpenID("oNPcuv67ncH2mvL5A4eWz75F3Gns");
		enterGroupInfo.setuRoomID("BC85F41DCCB7BB673287D63C14F2271C");
		enterGroupInfo.setType("1");
		enterGroupInfo.setTime("2017-08-15 22:36:04.426586");
		CommonResult returnEnterOrQuitGroupInfoToFriso = frisoEnterGroupService.returnEnterOrQuitGroupInfoToFriso(enterGroupInfo);
	}
	
}
