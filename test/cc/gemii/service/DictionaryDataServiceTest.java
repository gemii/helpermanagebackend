package cc.gemii.service;

import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.AbstractBaseTest;
import cc.gemii.data.otd.DicNodeData;

public class DictionaryDataServiceTest extends AbstractBaseTest{

	@Autowired
	private DictionaryDataService dictionaryDataService ;
	
	@Test
	public void searchAllProvince_test(){
		List<String> provinces = dictionaryDataService.searchAllProvince();
		System.out.println(JSONObject.toJSONString(provinces));
	}
	
	@Test
	public void searchAllCityByProvince_test(){
		List<String> citys = dictionaryDataService.searchAllCityByProvince(null);
		System.out.println(JSONObject.toJSONString(citys));
		List<String> citys1 = dictionaryDataService.searchAllCityByProvince("河北");
		System.out.println(JSONObject.toJSONString(citys1));
	}   
	
	@Test
	public void searchAllHospitalByCity_test(){
		List<String> hospitals = dictionaryDataService.searchAllHospitalByCity("上海","");
		System.out.println(JSONObject.toJSONString(hospitals));
	}
	
	@Test
	public void searchQRcodePageData_test(){
		Collection<DicNodeData> dicNodeDatas = dictionaryDataService.searchQRcodePageData("","");
		System.out.println(JSONObject.toJSONString(dicNodeDatas));
	}
	
}
