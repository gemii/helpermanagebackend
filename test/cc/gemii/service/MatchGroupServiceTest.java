package cc.gemii.service;

import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.AbstractBaseTest;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.otd.UserInfoTroubledMum;

public class MatchGroupServiceTest  extends AbstractBaseTest{

	@Autowired
	private MatchGroupService matchGroupService;
	
	@Test
	public void matchByUserInfo_test(){
		UserInfoTroubledMum userInfoBase = new UserInfoTroubledMum();
		userInfoBase.setOpenid("openID"+System.currentTimeMillis());
		userInfoBase.setChannel(163);
		userInfoBase.setPhone("18516513048");
		userInfoBase.setCity("上海");
		GeneralContentResult<Map<String, String>> result = matchGroupService.matchByUserInfo(userInfoBase, MatchGroupStrategy.WYETH_TROBLEDMUM_MATCHGROUP_STRATEGY,"2");
		System.out.println(JSONObject.toJSON(result));
	}
	
}
