package cc.gemii.service;

import java.text.ParseException;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.AbstractBaseTest;


public class SysAccessServiceTest extends AbstractBaseTest{

	@Autowired
	private SysAccessService sysAccessService;
	
	@Test
	public void countMatchTimesByMember_test() throws ParseException{
		String startTime = "2017-05-01";
		String endTime = "2017-06-01";
		List<String> data = sysAccessService.countMatchTimesByMember(startTime,endTime);
		System.out.println(JSONObject.toJSONString(data));
	}
	
	@Test
	public void countMatchTimesByGroup_test() throws ParseException{
		String startTime = "2017-05-01";
		String endTime = "2017-06-01";
		List<String> data = sysAccessService.countMatchTimesByGroup(startTime, endTime);
		System.out.println(JSONObject.toJSONString(data));
	}
	
}
