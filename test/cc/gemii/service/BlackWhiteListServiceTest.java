package cc.gemii.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cc.gemii.AbstractBaseTest;
import cc.gemii.po.BlackWhiteListDO.BlackWhiteListType;
import cc.gemii.util.RedisUtil;

public class BlackWhiteListServiceTest extends AbstractBaseTest{
	
	@Autowired
	private BlackWhiteListService blackWhiteListService;
	@Autowired
	private RedisUtil redisUtil;
	
	@Test
	public void getUserType_test(){
		BlackWhiteListType blackWhiteListType = blackWhiteListService.getUserType("18516513048", "haoqi");
		System.out.println(blackWhiteListType);
	}
}
