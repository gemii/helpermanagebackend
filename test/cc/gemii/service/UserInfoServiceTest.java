package cc.gemii.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;

import cc.gemii.AbstractBaseTest;
import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserStatusCardVO;

public class UserInfoServiceTest extends AbstractBaseTest{

	@Autowired
	private UserInfoService userInfoService;
	
	@Autowired
	private UserStatusService userstatusservice;
	@Test
	public void searchUserInfoByCardIn_test(){
//		UserInfoCardProfile cardProfile= new UserInfoCardProfile();
//		cardProfile.setActiveStatus((byte) 1);
//		cardProfile.setProvince("上海");
//		cardProfile.setPhoneNumber("13234569876");
//		cardProfile.setCity("上海");
//		cardProfile.setHospital("这是一个医院名");
//		cardProfile.setMatchStatus((byte)1);
//		cardProfile.setRobotId("34234");
//		cardProfile.setUserName("这里是昵称");
//		System.out.println(JSONObject.toJSONString(cardProfile));
//		PageInfo<UserInfoCardVO> pageinfo = userInfoService.searchUserInfoByCardIn(1,10,cardProfile);
//		System.out.println(JSONObject.toJSONString(pageinfo));
		
		UserStatusCardProfile ucr=new UserStatusCardProfile();
		ucr.setEnterGroupStatus((byte)1);
		ucr.setMatchStatus((byte)1);
		ucr.setPhoneNumber("13122051173");
		ucr.setRobotId("467439024");
		ucr.setUserMatchStatus((byte)1);
		ucr.setUserName("cpyder");
		PageInfo<UserStatusCardVO> usclist=userstatusservice.searchUserStatusByCardIn(1, 10, ucr);
		System.out.println(JSONObject.toJSONString(usclist));
	}
}
