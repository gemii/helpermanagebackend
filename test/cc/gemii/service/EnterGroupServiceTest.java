package cc.gemii.service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.pagehelper.PageInfo;

import cc.gemii.AbstractBaseTest;
import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserStatusCardVO;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;

public class EnterGroupServiceTest extends AbstractBaseTest{
	
	@Autowired
	private EnterGroupService enterGroupService;
	
	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Autowired
	private UserStatusService userStatusService;
	@Autowired
	private GemiiMatchGroupService gemii;
	
	@Autowired
	private GroupService groupService;
	@Autowired
	private FrisoEnterGroupService frisoEnterGroupService;
	
	@Test
	public void createHuggiesMember_test(){
		
//		#第一组测试数据
//		oNPcuvww4U4qvQ7B2v8s1c0G_QdU
//		沧州
//		其他
//		18330782203
//		UserFriendEnterGroup memberReq = new UserFriendEnterGroup();
//		memberReq.setCity("沧州");
//		memberReq.setEdc("2017-05-04 12:00:00");
//		memberReq.setPhone("18330782203");
//		memberReq.setFullName("李女士");
//		memberReq.setNickname("人气王");
//		memberReq.setOpenid("oNPcuvww4U4qvQ7B2v8s1c0G_QdU");
//		memberReq.setHospital("其他");
//		CommonResult commonResult = enterGroupService.createHuggiesMember(memberReq);
//		System.out.println(JSONObject.fromObject(commonResult));
		
//		UserFriendEnterGroup memberReq = new UserFriendEnterGroup();
//		memberReq.setCity("重庆");
//		memberReq.setEdc("2017-05-04 12:00:00");
//		memberReq.setPhone("15935846619");
//		memberReq.setFullName("李女士");
//		memberReq.setNickname("人气王");
//		memberReq.setOpenid("oNPcuv0xjuNfFa44fyK9x33AqKp8");
//		memberReq.setHospital("其他");
//		CommonResult commonResult = enterGroupService.createHuggiesMember(memberReq);
//		System.out.println(JSONObject.fromObject(commonResult));
		
//		UserFriendEnterGroup memberReq = new UserFriendEnterGroup();
//		memberReq.setCity("石家庄");
//		memberReq.setEdc("2017-05-04 12:00:00");
//		memberReq.setPhone("15767053745");
//		memberReq.setFullName("李女士");
//		memberReq.setNickname("人气王");
//		memberReq.setOpenid("oNPcuvx8Vu8yXuUYvjeMnrgA19io");
//		memberReq.setHospital("其他");
//		CommonResult commonResult = enterGroupService.createHuggiesMember(memberReq);
//		System.out.println(JSONObject.fromObject(commonResult));
		
//		UserFriendEnterGroup memberReq = new UserFriendEnterGroup();
//		memberReq.setCity("济南");
//		memberReq.setEdc("2017-05-04 12:00:00");
//		memberReq.setPhone("15981117875");
//		memberReq.setFullName("李女士");
//		memberReq.setNickname("人气王");
//		memberReq.setOpenid("oNPcuvwLESJ-zxPmiUWvfrqqduz0");
//		memberReq.setHospital("其他");
//		CommonResult commonResult = enterGroupService.createHuggiesMember(memberReq);
//		System.out.println(JSONObject.fromObject(commonResult));
////		
		UserFriendEnterGroup member=new UserFriendEnterGroup();
		member.setCity("其他");
		member.setPhone("13519106981");
		member.setOpenid("oXPbIjt-YXFqo8vjXOSXL8apIoow");
		member.setHospital("其他‬‬‬‬");
		member.setEdc("2017-07-28");
		member.setProvince("其他");
		member.setUsername("iein");
//		CommonResult comm=enterGroupService.receiveInfoMatchGroup(member);
//		CommonResult comm=enterGroupService.goodParentMatchGroup(member);
//		CommonResult comm=enterGroupService.pharmacyMatchGroup(member);
		CommonResult comm=enterGroupService.cooperationMatchGroup(member);
//		CommonResult comm=enterGroupService.loveBabyMatchGroup(member);
//		System.out.println(JSONObject.fromObject(comm));
		
//		System.out.println((int)(Math.random() * ((999999 - 1) + 1)));
		
//		System.out.println(RegularUtil.isDate("2012-12-12"));
		
		
		
		
		
//		UserFriendEnterGroup mb=new UserFriendEnterGroup();
//		mb.setOpenid("oYOUU1LCzvYQ40mH6zDm597JEwO8");
//		mb.setType("3");
//		CommonResult cm=enterGroupService.selectRobotUrl(mb);
//		CommonResult cm=enterGroupService.selectRobotId(mb);
//		System.out.println(cm);
////		System.out.println(JSONObject.fromObject(cm));
		
	
	}
	@Test
	public void wyeth_test(){
		Map<String,String> map =new HashMap<>();
		map.put("city", "沈阳");
		map.put("hospital", "沈阳市妇婴医院啊啊啊");
		map.put("edc", "2017-11-19");
		map.put("type", "2");
		map.put("robotID", "570464520");
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(MatchGroupStrategy.WYETH_THREE_MATCHGROUP_STRATEGY, map);
	}
	
	
	@Test
	public void robotowner(){
		Map<String, String> map=new HashMap<>();
		// 判断预产期
//		map.put("country", this.validateEdc(user.getEdc()));
		map.put("robotID", "3158395410");
		// map.put("city", this.handleCity(user.getCity()));
		map.put("city", "福州");
		map.put("edc", "2018-01-27");
		map.put("hospital", "南京军区福州总医院");
		map.put("code", "");
		map.put("uid", "");
		map.put("phone", "13519106981");
		map.put("srcode", "");
		map.put("openid", "");
		enterGroupService.matchGroup(map);
	}
	
	
	@Test
	public void h5Info(){
		Userinfo user=new Userinfo();
		user.setCity("重庆");
		user.setEdc("2017-10-17");
		user.setHospitalkeyword("新桥医院");
		user.setOpenid("oXPbIjt-YXFqo8vjXOSXL8apIoow");
		user.setPhonenumber("13519106981");
		enterGroupService.submitUserInfo(user, "1951972778", "8523", "@5629062718c6171f1f7b6bf721bed1bb8ff7630e6cc2ea3148fea8168335d364");
	}
	
	
	@Test
	public void userStatus(){
		UserStatusCardProfile user=new UserStatusCardProfile();
		user.setRobotId("532082555");
		PageInfo<UserStatusCardVO> size=userStatusService.searchUserStatusByCardIn(1, 20, user);
		System.out.println(size);
	}
	
	
	
	@Test
	public void robot(){
		groupService.getGroupByHelper("1","",1,20,"all", "huishi");
	}
	
	
	public static void main(String[] args) {
		System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(System.currentTimeMillis());
	}
	
	@Test
	public void leyin_test(){
		UserFriendEnterGroup member=new UserFriendEnterGroup();
		member.setArea("江阴一区");
		member.setEdc("2017-11-19");
		member.setPhone("15212147570");
		member.setUsername("请叫我小稳稳");
		member.setOpenid("oNPcuv67ncH2mvL5A4eWz75F3Gns");
		CommonResult leYinMatchGroup = enterGroupService.leYinMatchGroup(member);
	}
	
	@Test
	public void submitUserInfo_test(){
		String robotID = "3484657888";
		String code = "";
		String uid ="";
		Userinfo user = new Userinfo();
		user.setCity("上海");
		user.setHospitalkeyword("上海市第八人民医院");
		user.setProvince("上海");
		user.setEdc("2017-11-22");
		
		Map<String, String> map = new HashMap<String, String>();
		map.put("robotID", robotID);
		map.put("city", user.getCity());
		map.put("edc", user.getEdc());
		map.put("hospital", user.getHospitalkeyword());
		enterGroupService.matchGroup(map);
	}
	
	@Test
	public void gemii(){
		UserFriendEnterGroup member=new UserFriendEnterGroup();
		member.setCity("佛山");
		member.setPhone("13519106981");
		member.setOpenid("oXPbIjt-YXFqo8vjXOSXL8apIoow");
		member.setEdc("2017-07-28");
		member.setProvince("广东");
		member.setType("8");
		CommonResult comm=gemii.selectGemiiMatchGroup(member);
	}
	
}
