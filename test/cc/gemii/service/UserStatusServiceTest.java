package cc.gemii.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;

import cc.gemii.AbstractBaseTest;
import cc.gemii.data.dto.UserInfoCardProfile;
import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserInfoCardVO;
import cc.gemii.data.otd.UserStatusCardVO;
import cc.gemii.po.Userinfo;

public class UserStatusServiceTest extends AbstractBaseTest{

	@Autowired
	private UserStatusService userStatusService;
	
	@Autowired
	private UserInfoService userinfo;
	
	@Test
	public void searchUserInfoByCardIn_test(){
		UserStatusCardProfile cardProfile = new UserStatusCardProfile();
		cardProfile.setEnterGroupStatus((byte)1);
		cardProfile.setMatchStatus((byte)1);
		cardProfile.setUserMatchStatus((byte)1);
		cardProfile.setPhoneNumber("1233ß");
		cardProfile.setRobotId("34234");
		cardProfile.setUserName("萨达省");
		System.out.println(JSONObject.toJSONString(cardProfile));
		PageInfo<UserStatusCardVO> pageInfo = userStatusService.searchUserStatusByCardIn(1, 10, cardProfile);
		System.out.println(JSONObject.toJSONString(pageInfo));
	}
//	
//	@Test
//	public void matchuserinfo_test(){
//		Userinfo userinfo = userStatusService.matchuserinfo(523467, "18516513043");
//		System.out.println(JSONObject.toJSONString(userinfo));

	@Test
	public void searchuserinfo(){
		UserInfoCardProfile cardProfile = new UserInfoCardProfile();
		cardProfile.setMatchStatus((byte)0);
		System.out.println(JSONObject.toJSONString(cardProfile));
		PageInfo<UserInfoCardVO> pageInfo = userinfo.searchUserInfoByCardIn(1, 10, cardProfile);
		System.out.println(JSONObject.toJSONString(pageInfo));
	}
}
