package cc.gemii.data.dto;

import java.io.Serializable;

public class UserStatusCardProfile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1269066126774817913L;
	
	private String robotId;
	/**0 未入群 1 已邀请 2 手动邀请*/
	private Byte enterGroupStatus;
	/**0 未匹配 1 已匹配 2 匹配到城市群 3 匹配到全国群*/
	private Byte matchStatus;
	private String userName;
	private String phoneNumber;
	/**1 确认好友 2 匹配成功*/
	private Byte userMatchStatus;
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRobotId() {
		return robotId;
	}
	public void setRobotId(String robotId) {
		this.robotId = robotId;
	}
	public Byte getEnterGroupStatus() {
		return enterGroupStatus;
	}
	public void setEnterGroupStatus(Byte enterGroupStatus) {
		this.enterGroupStatus = enterGroupStatus;
	}
	public Byte getMatchStatus() {
		return matchStatus;
	}
	public void setMatchStatus(Byte matchStatus) {
		this.matchStatus = matchStatus;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public Byte getUserMatchStatus() {
		return userMatchStatus;
	}
	public void setUserMatchStatus(Byte userMatchStatus) {
		this.userMatchStatus = userMatchStatus;
	}
	
}
