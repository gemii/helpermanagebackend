package cc.gemii.data.dto;

import java.io.Serializable;

public class UserInfoCardProfile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1269066126774817913L;
	
	private Byte activeStatus;
	private String province;
	private String phoneNumber;
	private String city;
	private String hospital;
	private Byte matchStatus;
	private String robotId;
	private String userName;
	private int page;
	private int size;
	private String type;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Byte getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(Byte activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public Byte getMatchStatus() {
		return matchStatus;
	}
	public void setMatchStatus(Byte matchStatus) {
		this.matchStatus = matchStatus;
	}

	public String getRobotId() {
		return robotId;
	}
	public void setRobotId(String robotId) {
		this.robotId = robotId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}

	
}
