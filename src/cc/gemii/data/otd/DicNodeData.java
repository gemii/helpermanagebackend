package cc.gemii.data.otd;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DicNodeData implements Serializable{

	private String value;
	private List<DicNodeData> child =null;
	
	public DicNodeData(){}
	public DicNodeData(String value) {
		super();
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public List<DicNodeData> getChild() {
		if(child == null)
			child = new ArrayList<DicNodeData>();
		return child;
	}
	public void setChild(List<DicNodeData> child) {
		this.child = child;
	}
}
