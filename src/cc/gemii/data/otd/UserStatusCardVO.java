package cc.gemii.data.otd;

import java.io.Serializable;

public class UserStatusCardVO implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 121953407979255759L;
	private Integer id;
	private String userName;
	/**0 未入群 1 已邀请 2 手动邀请*/
	private Byte enterGroupStatus;
	private String phoneNumber;
	private String province;
	private String city;
	private String hospital;
	private String edc;
	private String remindMessage;
	/**0 未匹配 1 已匹配 2 匹配到城市群 3 匹配到全国群*/
	private Byte matchStatus;
	/**1 确认好友 2 匹配成功*/
	private Byte userMatchStatus;
	private String roomName;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Byte getEnterGroupStatus() {
		return enterGroupStatus;
	}
	public void setEnterGroupStatus(Byte enterGroupStatus) {
		this.enterGroupStatus = enterGroupStatus;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getEdc() {
		return edc;
	}
	public void setEdc(String edc) {
		this.edc = edc;
	}
	public String getRemindMessage() {
		return remindMessage;
	}
	public void setRemindMessage(String remindMessage) {
		this.remindMessage = remindMessage;
	}
	public Byte getMatchStatus() {
		return matchStatus;
	}
	public void setMatchStatus(Byte matchStatus) {
		this.matchStatus = matchStatus;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public Byte getUserMatchStatus() {
		return userMatchStatus;
	}
	public void setUserMatchStatus(Byte userMatchStatus) {
		this.userMatchStatus = userMatchStatus;
	}
	
	
}
