package cc.gemii.data.otd;

import java.io.Serializable;

public class UserInfoCardVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2235776961132722558L;
	
	private Byte activeStatus;
	private String phoneNumber;
	private String province;
	private String city;
	private String hospital;
	private String edc;
	/**0 未匹配 1 已匹配 2 匹配到城市群 3 匹配到全国群*/
	private Byte matchStatus;
	private String robotName;
	private String roomName;
	private String formTime;
	
	
	public Byte getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(Byte activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getEdc() {
		return edc;
	}
	public void setEdc(String edc) {
		this.edc = edc;
	}
	public Byte getMatchStatus() {
		return matchStatus;
	}
	public void setMatchStatus(Byte matchStatus) {
		this.matchStatus = matchStatus;
	}
	public String getRobotName() {
		return robotName;
	}
	public void setRobotName(String robotName) {
		this.robotName = robotName;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getFormTime() {
		return formTime;
	}
	public void setFormTime(String formTime) {
		this.formTime = formTime;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	
}
