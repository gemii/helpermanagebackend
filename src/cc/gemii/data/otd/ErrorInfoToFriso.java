package cc.gemii.data.otd;

import com.alibaba.fastjson.annotation.JSONField;

public class ErrorInfoToFriso {

	private int Type;
	
	private String OpenId;
	
	private String EventTime;//入群或退群时间
	
	private String GroupId;
	
	private String GroupName;
	
	private int status;//状态 1未发送 2已发送

	@JSONField(name = "Type") 
	public int getType() {
		return Type;
	}

	public void setType(int type) {
		Type = type;
	}

	@JSONField(name = "OpenId") 
	public String getOpenId() {
		return OpenId;
	}

	public void setOpenId(String openId) {
		OpenId = openId;
	}

	@JSONField(name = "EventTime") 
	public String getEventTime() {
		return EventTime;
	}

	public void setEventTime(String eventTime) {
		EventTime = eventTime;
	}

	@JSONField(name = "GroupId") 
	public String getGroupId() {
		return GroupId;
	}

	public void setGroupId(String groupId) {
		GroupId = groupId;
	}

	@JSONField(name = "GroupName") 
	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}

	@JSONField(name = "status") 
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}



	
	
	
}
