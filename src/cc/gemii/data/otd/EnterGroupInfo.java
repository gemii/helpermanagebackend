package cc.gemii.data.otd;

public class EnterGroupInfo {

	private String openID;
	
	private String uRoomID;
	
	private String time;//入群或退群时间
	
	private String type;//入群1，退群2
	
	public String getOpenID() {
		return openID;
	}

	public void setOpenID(String openID) {
		this.openID = openID;
	}

	public String getuRoomID() {
		return uRoomID;
	}

	public void setuRoomID(String uRoomID) {
		this.uRoomID = uRoomID;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "EnterGroupInfo [openID=" + openID + ", uRoomID=" + uRoomID + ", time=" + time + ", type=" + type + "]";
	}
	
}
