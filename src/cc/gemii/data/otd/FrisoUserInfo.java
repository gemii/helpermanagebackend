package cc.gemii.data.otd;

public class FrisoUserInfo extends UserInfoBase{
	
	private String cmdInfo;//cmd信息
	private String name;
	
	public String getCmdInfo() {
		return cmdInfo;
	}

	public void setCmdInfo(String cmdInfo) {
		this.cmdInfo = cmdInfo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
