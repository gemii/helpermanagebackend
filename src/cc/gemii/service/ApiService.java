package cc.gemii.service;

import java.util.Map;

import cc.gemii.pojo.CommonResult;
import cc.gemii.util.InterfaceUtils;

public interface ApiService {

	/**
	 * 
	 * @param msg 机器人消息json
	 * @return
	 * TODO 解析消息json,通过websocket与前端通信
	 */
	CommonResult handleMessage(String msg);

	/**
	 * 
	* @param start 记录开始时间 必选
	 * @param end 记录结束时间 默认当前时间
	 * @param string
	 * @return
	 * TODO
	 */
	Map<String, Object> getFile(String start, String end_string, String string) throws Exception;

	/**
	 * 
	 * @param start 记录开始时间
	 * @param end 记录结束时间 默认当前时间 
	 * @return
	 * TODO
	 */
	InterfaceUtils H5Record(String start, String end);

}
