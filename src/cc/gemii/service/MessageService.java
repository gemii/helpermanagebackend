package cc.gemii.service;

import cc.gemii.po.Userstatus;
import cc.gemii.po.WechatuserentergroupstatusWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SelectMessage;
import cc.gemii.pojo.SelectMessageAB;


public interface MessageService {

	/**
	 * 
	 * @param selectMessage 搜索条件包装类
	 * @return
	 * TODO 根据搜索条件  查询用户上行消息
	 * 
	 */
	CommonResult selectMessage(SelectMessage selectMessage);

	/**
	 * 第二套方案
	 * @param selectMessageAB 搜索条件包装类
	 * @return
	 * TODO 根据搜索条件 查询用户进群信息
	 */
	CommonResult selectMessage2(SelectMessageAB selectMessageAB);

	/**
	 * 第三套方案
	 * @param selectMessageAB 搜索条件包装类
	 * @return
	 * TODO
	 */
	CommonResult selectMessage3(SelectMessageAB selectMessageAB);

	/**
	 * 
	 * @param userstatus 用户H5状态包装类
	 * @return
	 * TODO 管理员手动修改状态
	 */
	CommonResult modifyMessage2(Userstatus userstatus);

	/**
	 * 
	 * @param enterStatus 上行关键字包装类
	 * @return
	 * TODO 管理员手动添加上关键字逻辑
	 */
	CommonResult insertMessage(WechatuserentergroupstatusWithBLOBs enterStatus);

	/**
	 * 
	 * @param enterStatus
	 * @return
	 * TODO 管理员手动修改关键字逻辑
	 */
	CommonResult modifyMessage(WechatuserentergroupstatusWithBLOBs enterStatus);

}
