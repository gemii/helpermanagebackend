package cc.gemii.service;

import cc.gemii.po.BlackWhiteListDO.BlackWhiteListType;

public interface BlackWhiteListService {

	/**
	 * 根据用户标示和功能码，获取用户类型
	 * */
	public BlackWhiteListType getUserType(String userKey,String funCode);

}
