package cc.gemii.service;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserInfoCustom;

public interface HelperService {

	/**
	 * 
	 * @return
	 * TODO 获取所有小助手
	 */
	CommonResult getAllHelper();

	/**
	 * 
	 * @param province 省份
	 * @param city 城市
	 * @return
	 * TODO
	 */
	CommonResult getHosByCity(String province, String city);

	/**
	 * 
	 * @param user 用户信息包装类
	 * @return
	 * TODO 用户提交表单，并且返回根据用户信息匹配的群对应的小助手的二维码
	 */
	CommonResult insertUser(UserInfoCustom user);

	/**
	 * 
	 * @param robotID 机器人id
	 * @return
	 * TODO 根据机器人id获得二维码图片流
	 */
	byte[] getQrcode(String robotID);

}
