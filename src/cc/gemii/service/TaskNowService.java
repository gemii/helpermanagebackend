package cc.gemii.service;

import java.util.List;
import java.util.Map;

import cc.gemii.po.TaskNow;
import cc.gemii.pojo.SendMsgTask;

public interface TaskNowService {
	
	List<SendMsgTask> getPhones();
	
	void insertTaskInfo(List<TaskNow>  map);

}
