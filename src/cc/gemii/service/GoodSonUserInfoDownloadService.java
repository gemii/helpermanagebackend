package cc.gemii.service;

import java.util.List;

import cc.gemii.pojo.GoodSonUserInfo;

public interface GoodSonUserInfoDownloadService {

	public List<GoodSonUserInfo> GoodSonUserInfoQuery(String startTime,String endTime);
	
}
