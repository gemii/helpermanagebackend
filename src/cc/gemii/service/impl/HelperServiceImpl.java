package cc.gemii.service.impl;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.mapper.WechatrobotinfoMapper;
import cc.gemii.mapper.WechatrobotqrcodeCustomMapper;
import cc.gemii.mapper.WechatroomCustomMapper;
import cc.gemii.mapper.WechatroominfoCustomMapper;
import cc.gemii.po.WechatrobotinfoExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserInfoCustom;
import cc.gemii.service.HelperService;

@Service
public class HelperServiceImpl implements HelperService{

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private WechatrobotinfoMapper robotMapper;
	
	@Autowired
	private WechatroominfoCustomMapper wriCustomMapper;
	
	@Autowired
	private WechatroomCustomMapper wrCustomMapper;
	
	@Autowired
	private UserinfoMapper userinfoMapper;
	
	@Autowired
	private UserstatusMapper userStatusMapper;
	
	@Autowired
	private WechatrobotqrcodeCustomMapper qrCustomMapper;
	
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Override
	public CommonResult getAllHelper() {
		try {
			WechatrobotinfoExample example = new WechatrobotinfoExample();
			return CommonResult.ok(robotMapper.selectByExampleWithBLOBs(example));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, e.toString());
		}
	}

	@Override
	public CommonResult getHosByCity(String province,String city) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("province", province);
		map.put("city", city);
		return CommonResult.ok(wriCustomMapper.getHosByCity(map));
	}

	@Override
	public CommonResult insertUser(UserInfoCustom user) {
		//验证码逻辑
//		CommonResult validate = this.validate(user.getPhonenumber(), user.getCode());
//		if(validate.getStatus() == -1){
//			return validate;
//		}
//		
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		user.setFormtime(sdf.format(new Date()));
//		//群匹配状态
//		String matchGroupStatus = "";
//		//经过匹配选出要发送的小助手、群信息
//		MatchGroup sendGroup = null;
//		
//		Map<String, String> map = new HashMap<String, String>();
//		map.put("city", user.getCity());
//		map.put("keyword", user.getHospitalkeyword());
//		map.put("edc", user.getEdc());
//		//匹配群包括机器人未在线的群
//		String RoomID = enterGroupMapper.MatchGroup(map);
//		
//		if(RoomID == null || "".equals(RoomID)){   									//根据用户信息没有匹配到群,返回全国群
//			matchGroupStatus = ConfigUtil.MATCH_GROUP_FAILED_COUNTRY;				//群匹配状态设为失败
//			Map<String, String> paramMap = new HashMap<String, String>();
//			sendGroup = enterGroupMapper.getCountryGroup(paramMap);
//		}else{						   												//已匹配到群
//			matchGroupStatus = ConfigUtil.MATCH_GROUP_SUCCESS;						//群匹配状态设为成功
//			List<MatchGroup> matchGroups = wrCustomMapper.getGroupByRoomID(RoomID);   //根据群ID匹配到的机器人
//			List<MatchGroup> onlineGroups = new ArrayList<MatchGroup>();			  //匹配到的机器人中在线的机器人列表
//			for (MatchGroup g : matchGroups) {
//				if (g.getStatus().equals(ConfigUtil.ROBOT_ONLINE)) {
//					onlineGroups.add(g);
//				}
//			}
//			
//			if (onlineGroups.size() == 0) {    // 如果匹配到的群对应的机器人都offline则随机挑选
//				sendGroup = matchGroups.get((int) (Math.random() * matchGroups.size()));
//			} else { // 在online的机器人中随机挑选
//				sendGroup = onlineGroups.get((int) (Math.random() * onlineGroups.size()));
//			}
//		}
//		
//		if(sendGroup == null){  			  //如果此时sendGroup为空,1、进入匹配全国群逻辑,但是对应的机器人都offline
//			sendGroup = new MatchGroup();
//		}
//		
//		//插入用户信息记录	
//		userinfoMapper.insert(user);							
//		//插入用户状态
//		int uid = user.getId();
//		String time = sdf.format(new Date());
//		return CommonResult.ok(sendGroup);
		return CommonResult.ok();
	}

	@Override
	public byte[] getQrcode(String robotID) {
		return qrCustomMapper.getQrcodeByRid(robotID).getQrcodeImg();
	}
	
}
