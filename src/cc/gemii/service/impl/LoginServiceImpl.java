package cc.gemii.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.WechatclerkMapper;
import cc.gemii.po.Wechatclerk;
import cc.gemii.po.WechatclerkExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.LoginService;
import cc.gemii.util.AESCurrencyUtil;
import cc.gemii.util.MD5Utils;
@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private WechatclerkMapper clerkMapper;
	
	@Override
	public CommonResult login(String username, String password) {
		password = MD5Utils.md5Encode(password);
		WechatclerkExample example = new WechatclerkExample();
		example.createCriteria()
			.andUsernameEqualTo(username)
			.andPasswordEqualTo(password);
		List<Wechatclerk> list = clerkMapper.selectByExample(example);
		if(list.size() == 0){
			return CommonResult.build(-1, "账户名或密码错误");
		}
		Wechatclerk user = list.get(0);
		user.setPassword("");
		user.setPhonenumber(AESCurrencyUtil.decrypt2Str(user.getPhonenumber()));
		return CommonResult.ok(user);
	}

}
