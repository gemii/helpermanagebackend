package cc.gemii.service.impl;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.mapper.custom.OtherMapper;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.GetH5Record;
import cc.gemii.service.ApiService;
import cc.gemii.util.ExportExcel;
import cc.gemii.util.InterfaceUtils;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.TimeUtils;
@Service
public class ApiServiceImpl implements ApiService{

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private OtherMapper otherMapper;
	
	@Autowired
	private UserinfoMapper userInfoMapper;
	
	@Override
	public CommonResult handleMessage(String msg) {
		Map<String, Object> msgMap = (Map<String, Object>) JSON.parse(msg);
		if(msgMap.get("MsgType").equals(ParamUtil.SCAN_QRCODE)){
			return this.scanQrcode(msg);
		}
		return null;
	}

	
	private CommonResult scanQrcode(String msg){
		return CommonResult.ok();
	}


	@Override
	public Map<String, Object> getFile(String start, String end, String path) throws Exception {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		    Date start_time = sdf.parse(start);
		    String end_string = "";
		    if(end == null || "".equals(end)){
		    	end_string = TimeUtils.getTime();
		    }else{
		    	Date end_time = sdf.parse(end);
		    	end_string = end;
		    }
		    String filename = "H5Record_" + start + "-" + end_string + ".xls";
		    String filepath = path + filename;
		    
			Map<String, String> map = new HashMap<String, String>();
			map.put("start", TimeUtils.patternConvert(start));
			map.put("end", TimeUtils.patternConvert(end_string));
			List<GetH5Record> records = otherMapper.getH5Record(map);
			ExportExcel<GetH5Record> export = new ExportExcel<GetH5Record>();
			OutputStream out = new BufferedOutputStream(new FileOutputStream(filepath));
			String title = "record";
			String[] headers = {"openid","用户昵称","城市", "关键词","预产期","手机号码","srcode", "提交时间", "群名", "大区" ,"省份", "所在城市", "群ID", "群属性", "部门", "关联城市", "医院", "医院代码"};
			export.exportExcel(title,headers, records, out);  //title是excel表中底部显示的表格名，如Sheet
			out.close();
			
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("filename", filename);
			response.put("file", new File(filepath));
			return response;
		}
		catch(ParseException e){
			logger.error(e.getMessage(),e);
			return null;
		}
		catch (Exception e) {
			logger.error(e.getMessage(),e);
			return null;
		}
		
	}


	@Override
	public InterfaceUtils H5Record(String start, String end) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		    Date start_time = sdf.parse(start);
		    String end_string = "";
		    if(end == null || "".equals(end)){
		    	end_string = TimeUtils.getTime();
		    }else{
		    	Date end_time = sdf.parse(end);
		    	end_string = end;
		    }
			Map<String, Object> res = new HashMap<String, Object>();
			
			Map<String, String> map = new HashMap<String, String>();
			map.put("start", TimeUtils.patternConvert(start));
			map.put("end", TimeUtils.patternConvert(end_string));
			List<GetH5Record> records = otherMapper.getH5Record(map);
			
			
			InterfaceUtils response = new InterfaceUtils();
			response.setError_code(0);
			response.setReason("success");
			res.put("data", records);
			res.put("total", records.size());
			response.setResult(res);
			return response;
		} 
		catch (ParseException e){
			InterfaceUtils response = new InterfaceUtils();
			response.setError_code(-1);
			response.setReason("date parseException");
			return response;
		}
		catch (Exception e) {
			InterfaceUtils response = new InterfaceUtils();
			response.setError_code(500);
			response.setReason("server error");
			return response;
		}
	}
}
