package cc.gemii.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cc.gemii.data.dto.UserInfoCardProfile;
import cc.gemii.data.otd.UserInfoCardVO;
import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.service.UserInfoService;

@Service
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	private UserinfoMapper userinfoMapper;
	
	@Override
	public PageInfo<UserInfoCardVO> searchUserInfoByCardIn(int page, int size, UserInfoCardProfile searchInfo) {
		PageHelper.startPage(page, size);
		Page<UserInfoCardVO> pageData = userinfoMapper.selectCardUserInfos(searchInfo);
		PageInfo<UserInfoCardVO> pageInfo = pageData.toPageInfo();
		return pageInfo;
	}

}
