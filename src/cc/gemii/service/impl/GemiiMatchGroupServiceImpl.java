package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.data.otd.DicNodeData;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.GetRobotIdByU;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.pojo.SelectUser;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.GemiiMatchGroupService;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.RedisUtil;

@Service
public class GemiiMatchGroupServiceImpl implements GemiiMatchGroupService{
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Autowired
	private SendMsgContext sendMsgContext;
	@Autowired
	private Properties propertyConfigurer;
	@Override
	public CommonResult selectGemiiMatchGroup(UserFriendEnterGroup receiveInfo) {
		Map<String, String> param = new HashMap<>();
		param.put("city", receiveInfo.getCity());
		param.put("type", receiveInfo.getType());
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(MatchGroupStrategy.GEMII_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveInfo);
		insert.put("type",receiveInfo.getType());//景栗grouptag=8
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			enterGroupMapper.insertUserInfo(insert);
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		GetRobotIdByU getRobotIdByU = enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveInfo.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		logger.info("传给python的参数"+"taskid "+getRobotIdByU.getTaskId()+" u创id "+getRobotIdByU.getuRoomId()+" openid  "+receiveInfo.getOpenid()+" 群id "+matchGroupResult.getMatchGroupId()+" 群name "+getRobotIdByU.getRoomName());
		String resultByPython = HttpClientUtil.doPost(propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		logger.info("接收python的值" + resultByPython);
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);

		Map<String, Object> getDataInfo = (Map<String, Object>) robotInfo.get("data");
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		insert.put("sendGroup", null);
		enterGroupMapper.insertUserInfo(insert);
		Map<String, Object> result = new HashMap<>();
		result.put("robotQr", getDataInfo.get("qr_url")); // 二维码
		result.put("code", getDataInfo.get("verify_code"));// 验证码
		RedisUtil.set(receiveInfo.getOpenid(), getDataInfo.get("qr_url").toString(), 10 * 60);
		RedisUtil.set(receiveInfo.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 10 * 60);
		logger.info("机器人二维码url : " +getDataInfo.get("qr_url").toString());
		logger.info("验证码  : " +getDataInfo.get("verify_code").toString());
		sendMsg(receiveInfo.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	@Override
	public CommonResult selectUserInfo(String phone,String type) {
		List<UserFriendEnterGroup> citys=enterGroupMapper.selectUserCity(phone,type);
		Map<String, String> param=new HashMap<>();
		if(citys.size()==1){
			param.put("province", citys.get(0).getProvince());
			param.put("city", citys.get(0).getCity());
			param.put("edc", citys.get(0).getEdc());
			return CommonResult.ok(param);
		}
		if(citys.size() ==0 ){
			return CommonResult.build(1, "没有该用户信息");
		}
		if(citys.size()>1){
			return CommonResult.build(2, "有多条用户信息");
		}
		return CommonResult.build(-1, "出错了");
	}
	
	
	/**
	 * 发送短信
	 * 
	 * @param phone
	 * @param code
	 * @return
	 */
	private GeneralContentResult<String> sendMsg(String phone, String code) {
		ShortMessage _shortMessage = new ShortMessage();
		_shortMessage.setReciverPhone(phone);
		Formatter fm = new Formatter();
		String firstTemplate = propertyConfigurer.getProperty("send_msg_prompt");
		String first = String.valueOf(fm.format(firstTemplate, code));
		_shortMessage.setContent(first);
		_shortMessage.setCode(code);
		return sendMsgContext.sendSMS(_shortMessage);
	}

	@Override
	public Collection<DicNodeData> searchProvinceByGemii(String type) {
		Map<String,DicNodeData> result = new HashMap<String,DicNodeData> ();
		List<Map<String,String>> rs =null;
		if("8".equals(type)){
			rs=enterGroupMapper.selectProvinceByGemii();
		}else if("15".equals(type)){
			rs=enterGroupMapper.selectProvinceByWyeth();
		}
		for (Map<String, String> map : rs) {
			String province = map.get("province");
			if(result.get(province) == null){
				result.put(province,new DicNodeData(province));
			}
			DicNodeData dicNodeData = (DicNodeData)result.get(province);
			dicNodeData.getChild().add(new DicNodeData(map.get("city")));
		}
		List<DicNodeData> list = new ArrayList<>(result.values());
		return list;
	}

//	@Override
//	public CommonResult selectRepeat(UserFriendEnterGroup receiveinfomatch) {
//		Map<String, Object> param = new HashMap<>();
//		Map<String, String> paramuser = new HashMap<>();
//		param.put("openid", receiveinfomatch.getOpenid());
//		if (receiveinfomatch.getType() == null) {
//			param.put("type", 1);
//		} else {
//			param.put("type", receiveinfomatch.getType());
//		}
//		List<Userinfo> info = enterGroupMapper.selectUser(param);
//		logger.info("Userinfo里面的信息" + info.size());
//		if (info.size() > 0) {
//			List<SelectUser> selectuser = enterGroupMapper.selectStatus(receiveinfomatch.getOpenid(),
//					receiveinfomatch.getType());// 获取返回的入群状态
//			logger.info("Userstatus里面的信息" + selectuser.size());
//			Map<String, String> redisInfo = new HashMap<>();
//			if (selectuser != null && selectuser.size() > 0) {
//				paramuser.put("roomId", selectuser.get(0).getMatchGroup());
//				paramuser.put("userName", selectuser.get(0).getUserName());
//				String resultlist = enterGroupMapper.selectRoomInfo(paramuser);
//				if (resultlist != null && !"".equals(resultlist)) {
//					return CommonResult.build(2, "已发送入群邀请");
//				}
//				return CommonResult.build(-1, "确认好友没入群");
//			}
//			return CommonResult.build(-2, "未确认好友");
//			}
//		return CommonResult.build(-3, "请重新匹配");
//
//		} 


}
