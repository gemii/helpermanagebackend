package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.Result;

import cc.gemii.constant.ResultCode;
import cc.gemii.mapper.RobotStatusMapper;
import cc.gemii.mapper.RobotroleMapper;
import cc.gemii.mapper.WechatclerkMapper;
import cc.gemii.mapper.WechatclerkrobotMapper;
import cc.gemii.mapper.custom.OtherMapper;
import cc.gemii.po.Robotrole;
import cc.gemii.po.Wechatclerk;
import cc.gemii.po.WechatclerkExample;
import cc.gemii.po.Wechatclerkrobot;
import cc.gemii.po.WechatclerkrobotExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.RobotResult;
import cc.gemii.service.ClerkService;
import cc.gemii.util.AESCurrencyUtil;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;
import cc.gemii.util.TagUtil;
@Service
public class ClerkServiceImpl implements ClerkService {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private WechatclerkMapper clerkMapper;
	
	@Autowired
	private WechatclerkrobotMapper crMapper;

	@Autowired
	private RobotroleMapper roleMapper;
	
	@Autowired
	private OtherMapper otherMapper;
	
	@Autowired
	private Properties propertyConfigurer;
	
	@Override
	public CommonResult judgeName(String username) {
		WechatclerkExample example = new WechatclerkExample();
		example.createCriteria()
			.andUsernameEqualTo(username);
		List<Wechatclerk> list = clerkMapper.selectByExample(example);
		if(list.size() == 0){
			return CommonResult.ok();
		}
		return CommonResult.build(-1, "用户名已存在");
	}

	@Override
	public CommonResult insertBind(Wechatclerkrobot clerkrobot) {
		crMapper.insert(clerkrobot);
		return CommonResult.ok();
	}

	@Override
	public CommonResult updatePhone(Wechatclerk clerk) {
		clerk.setPhonenumber(AESCurrencyUtil.encrypt2Str(clerk.getPhonenumber()));
		clerkMapper.updateByPrimaryKeySelective(clerk);
		return CommonResult.ok();
	}

//	public CommonResult getNextTag() {
//		String nextTag = RedisUtil.get("NextTag");
//		if(nextTag == null){
//			Integer tag = otherMapper.getTag();
//			RedisUtil.set(ParamUtil.ROBOT_NEXT_TAG, String.valueOf(tag));
//		}
//		return CommonResult.ok(RedisUtil.incr(ParamUtil.ROBOT_NEXT_TAG));
//	}
	
	@Override
	public CommonResult addRobot(Integer id,String type) {
		try {
			String tag = String.valueOf(TagUtil.getTag());
			//插入机器人规则
			Robotrole role = new Robotrole();
			role.setBottag(tag);
			role.setRole(type);
			roleMapper.insert(role);
			
			
			//发送请求
			String url = propertyConfigurer.getProperty("START_ROBOT_URL").replace("BOT", tag).replace("NEW", "true");
			logger.info("添加拉人小助手：url="+url );
			String responseJson = HttpClientUtil.doGet(url);
			logger.info("responseJson:"+responseJson);
			Map<String, String> responseMap = (Map<String, String>) JSON.parse(responseJson);
			//请求发送成功之后查询redis
			if(ParamUtil.START_ROBOT_SUCCESS.equals(String.valueOf(responseMap.get("code")))){
				for (int i = 0; i < 3; i++) {
					String urlJson = RedisUtil.hget(ParamUtil.ITEM_LOGIN, tag);
					logger.info("redis里面的信息="+JSON.toJSONString(urlJson));
					if(urlJson != null){
						Map<String, String> urlMap = (Map<String, String>) JSON.parse(urlJson);
						//删除redis记录
						RedisUtil.hdel(ParamUtil.ITEM_LOGIN, tag);
						//插入用户机器人关联记录
						Wechatclerkrobot clerkRobot = new Wechatclerkrobot();
						clerkRobot.setClerkid(id);
						clerkRobot.setRobottag(tag);
						crMapper.insert(clerkRobot);
						//temp 增加管理员与新添加机器人关系
						Wechatclerkrobot adminRobot = new Wechatclerkrobot();
						adminRobot.setClerkid(5);
						adminRobot.setRobottag(tag);
						crMapper.insert(adminRobot);
						
						
						Map<String, String> result=new HashMap<>();
						result.put("url", urlMap.get("url"));
						result.put("robotTag", tag);
						
						return CommonResult.ok(result);
					}
					Thread.sleep(2000);
				}
			}
			return CommonResult.build(-1, JSON.toJSONString(responseMap));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, "server error");
		}
		
	}
	
	/**
	 * 重启robot
	 */
	@Override
	public CommonResult restartRobot(Integer clerkid, String robotTag,String type) {
		try {
			Robotrole robotrole=new Robotrole();
			robotrole.setBottag(robotTag);
			robotrole.setRole(type);
			roleMapper.updateByPrimaryKey(robotrole);
			//发送请求
			String url = propertyConfigurer.getProperty("START_ROBOT_URL").replace("BOT", robotTag).replace("NEW", "");
			logger.info("重启拉人小助手：url="+url);
			String responseJson = HttpClientUtil.doGet(url);
			logger.info("responseJson="+responseJson);
			Map<String, String> responseMap = (Map<String, String>) JSON.parse(responseJson);
			if(ParamUtil.UNKOWN_ROBOT_TAG.equals(String.valueOf(responseMap.get("code")))){
				return this.addRobot(clerkid,type);
			}
			//请求发送成功之后查询redis
			if(ParamUtil.START_ROBOT_SUCCESS.equals(String.valueOf(responseMap.get("code")))){
				for (int i = 0; i < 3; i++) {
					String urlJson = RedisUtil.hget(ParamUtil.ITEM_LOGIN, robotTag);
					logger.info("redis里面的信息:=="+urlJson);
					if(urlJson != null){
						//删除redis记录
						Map<String, String> urlMap = (Map<String, String>) JSON.parse(urlJson);
						RedisUtil.hdel(ParamUtil.ITEM_LOGIN, robotTag);
						Map<String, String> result=new HashMap<>();
						result.put("url", urlMap.get("url"));
						result.put("robotTag", "");
						return CommonResult.ok(result);
					}
					Thread.sleep(2500);
				}
			}
			return CommonResult.build(-1, JSON.toJSONString(responseMap));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(-1, "server error");
		}
		
	}

	@Override
	public CommonResult deleteRobot(Wechatclerkrobot clerkrobot) {
		WechatclerkrobotExample example = new WechatclerkrobotExample();
		example.createCriteria()
			.andRobottagEqualTo(clerkrobot.getRobottag());
		crMapper.deleteByExample(example);
		return CommonResult.ok();
	}

	@Override
	public CommonResult selectStatus(String robotTag) {
		RobotResult status =clerkMapper.selectStatus(robotTag);
		if(status !=null && !"".equals(status)){
			if("online".equals(status.getStatus())){
				return CommonResult.ok(status.getRobotId());
			}else{
				return CommonResult.build(ResultCode.RESULT_OTHER, "小助手不在线");
			}
		}
		return CommonResult.build(ResultCode.RESULT_FAIL, "没有小助手信息");
	}
	
}
