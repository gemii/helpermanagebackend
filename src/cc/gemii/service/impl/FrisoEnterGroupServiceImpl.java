package cc.gemii.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.data.otd.EnterGroupInfo;
import cc.gemii.data.otd.ErrorInfoToFriso;
import cc.gemii.data.otd.FrisoUserInfo;
import cc.gemii.fuckemoji.EmojiUtil;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.po.Userinfo;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.GetRobotIdByU;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.FrisoEnterGroupService;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.RedisUtil;

@SuppressWarnings("all")
@Service
public class FrisoEnterGroupServiceImpl implements FrisoEnterGroupService {

	private Logger logger = Logger.getLogger(this.getClass());
	
	private static String DEFAULT_CHARSET = "UTF-8";
	
	private static String access_token;

	@Autowired
	private MatchGroupContext matchGroupContext;

	@Autowired
	private EnterGroupMapper enterGroupMapper;

	@Autowired
	private Properties propertyConfigurer;

	@Autowired
	private SendMsgContext sendMsgContext;

	/**
	 * 发送短信
	 * 
	 * @param phone
	 * @param code
	 * @return
	 */
	private GeneralContentResult<String> sendMsg(String phone, String code) {
		ShortMessage _shortMessage = new ShortMessage();
		_shortMessage.setReciverPhone(phone);
		Formatter fm = new Formatter();
		String firstTemplate = propertyConfigurer.getProperty("send_msg_prompt");
		String first = String.valueOf(fm.format(firstTemplate, code));
		_shortMessage.setContent(first);
		_shortMessage.setCode(code);
		return sendMsgContext.sendSMS(_shortMessage);
	}

	public CommonResult beautyGoodSonMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> param = new HashMap<>();
		param.put("city", receiveinfomatch.getCity());
		param.put("edc", receiveinfomatch.getEdc());
		param.put("phone", receiveinfomatch.getPhone());
		param.put("type", receiveinfomatch.getType());
		receiveinfomatch.setNickname(EmojiUtil.handleEmoji(receiveinfomatch.getNickname()));

		MatchGroupResult matchGroupResult = this.matchGroupContext
				.matchGroup(MatchGroupContext.MatchGroupStrategy.BEAUTY_GOODSON_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", receiveinfomatch.getType());
		//未匹配到群
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(insert);
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		//重复入群
		if (matchGroupResult.getResultCode() == 2) {
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		insert.put("sendGroup", null);
		insertUserInfo(insert);
		GetRobotIdByU getRobotIdByU = this.enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveinfomatch.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		logger.info("用户首次扫码调用U创接口之前的时间："
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		String resultByPython = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		logger.info("用户首次扫码调用U创接口之后的时间："
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		this.logger.info("接受python的值" + JSON.parse(resultByPython));
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		Map<String, Object> getDataInfo = (Map) robotInfo.get("data");
		Map<String, Object> result = new HashMap<>();
		result.put("robotQr", getDataInfo.get("qr_url"));
		result.put("code", getDataInfo.get("verify_code"));
		RedisUtil.set(receiveinfomatch.getOpenid(), getDataInfo.get("qr_url").toString(), 600);
		RedisUtil.set(receiveinfomatch.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 600);
		sendMsg(receiveinfomatch.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	/**
	 * 插入userinfo信息
	 * 
	 * @param param
	 */
	public void insertUserInfo(Map<String, Object> param) {
		enterGroupMapper.insertUserInfo(param);
	}

	@Override
	public CommonResult newBeautyGoodSonMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> param = new HashMap<>();
		param.put("city", receiveinfomatch.getCity());
		param.put("edc", receiveinfomatch.getEdc());
		param.put("hospital", receiveinfomatch.getHospital());
		param.put("phone", receiveinfomatch.getPhone());
		param.put("type", receiveinfomatch.getType());
		receiveinfomatch.setNickname(EmojiUtil.handleEmoji(receiveinfomatch.getNickname()));

		MatchGroupResult matchGroupResult = this.matchGroupContext
				.matchGroup(MatchGroupContext.MatchGroupStrategy.NEW_BEAUTY_GOODSON_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", receiveinfomatch.getType());
		//未匹配到群
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(insert);
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		//重复入群
		if (matchGroupResult.getResultCode() == 2) {
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		insert.put("sendGroup", null);
		insertUserInfo(insert);
		GetRobotIdByU getRobotIdByU = this.enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveinfomatch.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		logger.info("用户首次扫码调用U创接口之前的时间："
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		String resultByPython = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		logger.info("用户首次扫码调用U创接口之后的时间："
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		this.logger.info("接受python的值" + JSON.parse(resultByPython));
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		Map<String, Object> getDataInfo = (Map) robotInfo.get("data");
		Map<String, Object> result = new HashMap<>();
		result.put("robotQr", getDataInfo.get("qr_url"));
		result.put("code", getDataInfo.get("verify_code"));
		RedisUtil.set(receiveinfomatch.getOpenid(), getDataInfo.get("qr_url").toString(), 600);
		RedisUtil.set(receiveinfomatch.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 600);
		sendMsg(receiveinfomatch.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	@Override
	public CommonResult matchGroupForFriso(FrisoUserInfo receiveinfomatch) {
		if (null == receiveinfomatch) {
			return CommonResult.build(-3, "传递的参数为空");// TODO
		}
		logger.info("美素佳儿请求入群");
		Map<String, String> param = new HashMap<>();
		param.put("province", receiveinfomatch.getProvince());
		param.put("city", receiveinfomatch.getCity());
		param.put("edc", receiveinfomatch.getEdc());
		param.put("store", receiveinfomatch.getStore());
		MatchGroupResult matchGroupResult = this.matchGroupContext
				.matchGroup(MatchGroupContext.MatchGroupStrategy.MATCHGROUP_FOR_FRISO_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", "52");//TODO 此处groupTag 为52
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			enterGroupMapper.insertMSUserInfo(insert);
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		insert.put("sendGroup", null);
		enterGroupMapper.insertMSUserInfo(insert);
		// 根据roomID获取U_roomID,taskID,roomName，组装参数请求二维码
		GetRobotIdByU getRobotIdByU = this.enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveinfomatch.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		logger.info("用户首次扫码调用U创接口之前的时间："
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		String resultByPython = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		logger.info("用户首次扫码调用U创接口之后的时间："
				+ new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-2, "获取机器人二维码失败");
		}
		this.logger.info("接受python的值" + JSON.parse(resultByPython));
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-2, "获取机器人二维码失败");
		}
		Map<String, Object> getDataInfo = (Map) robotInfo.get("data");
		Map<String, Object> result = new HashMap<>();
		result.put("robotQr", getDataInfo.get("qr_url"));
		result.put("code", getDataInfo.get("verify_code"));
		RedisUtil.set(receiveinfomatch.getOpenid(), getDataInfo.get("qr_url").toString(), 600);
		RedisUtil.set(receiveinfomatch.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 600);
		sendMsg(receiveinfomatch.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	@Override
	public CommonResult returnEnterOrQuitGroupInfoToFriso(EnterGroupInfo enterGroupInfo) {
		if (enterGroupInfo == null) {
			return CommonResult.build(-1, "传过来的信息为Null");
		}
		String openID = enterGroupInfo.getOpenID();
		String uRoomID = enterGroupInfo.getuRoomID();
		//根据uroomid查找群信息
		List<Wechatroominfo> wechatRoomInfo = enterGroupMapper.selectRoomInfoById(uRoomID);
		if (wechatRoomInfo == null || wechatRoomInfo.size() == 0) {
			logger.info("根据U_RoomID查不到相应的群");
			return CommonResult.build(-1, "根据U_RoomID查不到相应的群");
		}
		//根据openid去查询userInfo
		List<Userinfo> userInfo = enterGroupMapper.selectUserInfoByOpenid(openID);
		if(userInfo == null || userInfo.size() == 0){
			logger.info("根据openid查不到相应的user");
			return CommonResult.build(-1, "根据openid查不到相应的user");
		}
		String groupTag = wechatRoomInfo.get(0).getGroupTag();
		// 美素佳儿用户入群，返回信息给美素
		//判断其groupTag5 且userinfo type52
		if(groupTag.substring(0,1).equals("5") && userInfo.get(0).getType().equals("52")){
			String type = enterGroupInfo.getType();
			if(type.equals("1")){
				logger.info("用户入群开始返回信息给美素佳儿"  + enterGroupInfo.toString());
			}else{
				logger.info("用户退群开始返回信息给美素佳儿" + enterGroupInfo.toString());
			}
			ArrayList<ErrorInfoToFriso> data = new ArrayList<>();
			ErrorInfoToFriso errorInfoToFriso = new ErrorInfoToFriso();
			errorInfoToFriso.setType(Integer.valueOf(type));
			errorInfoToFriso.setEventTime(enterGroupInfo.getTime());
			errorInfoToFriso.setOpenId(openID);
			errorInfoToFriso.setGroupId(wechatRoomInfo.get(0).getRoomid());
			errorInfoToFriso.setGroupName(wechatRoomInfo.get(0).getRoomname());
			data.add(errorInfoToFriso);
			String json = JSON.toJSON(data).toString();
			logger.info("返还给美素佳儿入群或退群信息如下 : " + json);
			try {
				//首次去请求美素  携带access_token为null
				if(null == access_token || access_token.equals("")){
					access_token = getAccessToken();
				}
				String url = this.propertyConfigurer.getProperty("RETURN_INFO_TO_FRISO");
				Map<String,String> header = new HashMap<>();
				header.put("Authorization","bearer "+ access_token);
				header.put("Content-Type", "application/json");
				//调用美素接口(https、POST请求)
				String resultByFriso = HttpClientUtil.request(url, json, "POST", DEFAULT_CHARSET, true, header);
				logger.info("调用美素返还的信息 : " + resultByFriso);
				//access_token过期
				if("".equals(resultByFriso)){
					logger.info("access_token过期，去请求新的access_token");
					//去获取access_token
					access_token = getAccessToken();
					//将access_token置于请求头
					header.put("Authorization","bearer "+ access_token);
					header.put("Content-Type", "application/json");
					//调用美素接口(https、POST请求)
					String resultByFriso2 = HttpClientUtil.request(url, json, "POST", DEFAULT_CHARSET, true, header);
					//如果403失败，存库
					if("".equals(resultByFriso2)){
						logger.info("拿到新的token,即时请求出错！");
						enterGroupMapper.insertErrorInfo(data.get(0));
					}
					logger.info("调用美素返还的信息 : " + resultByFriso2);
					return CommonResult.ok();
				}
				Map<String,Object> result = JSON.parseObject(resultByFriso);
				//其他未知错误
				if(!result.get("errcode").equals(0)){
					//将返还信息存入数据库
					enterGroupMapper.insertErrorInfo(data.get(0));
					logger.info("调用美素接口出现未知错误，错误码 ：" +result.get("errcode") + "已将此条记录存库");
					return CommonResult.build(-1, "调用美素出现未知异常");
				}
			} catch (Exception e) {
				logger.info("调用美素接口出现未知异常" + e);
				return CommonResult.build(-1, "调用美素出现未知异常");
			}
		}else{
			return CommonResult.build(-1, "此用户不是美素佳儿用户");	
		}
		return CommonResult.ok();
	}

	
	/**
	 * 获取accessToken
	 * @return 
	 * @throws Exception 
	 */
	private String getAccessToken() throws Exception {
		logger.info("获取access_token");
//		String param = "appid=crm.wedo&appsecret=crm.wedo";
//		String param = "appid=web.jl&appsecret=7a5f7a58f904";
		String param = this.propertyConfigurer.getProperty("APPID_APPSECRET");
		String url = this.propertyConfigurer.getProperty("GET_ACCESS_TOKEN");
		String accessResult = HttpClientUtil.request(url, param,"GET", DEFAULT_CHARSET, true, null);
		Map<String,Object> token = JSON.parseObject(accessResult);
		access_token = (String) token.get("data");
		logger.info("获取到的accessToken的值为 ：" + access_token);
		return access_token;
	}
	
	

//	@Override
//	public CommonResult returnQuitGroupInfoToFriso(EnterGroupInfo enterGroupInfo) {
//		logger.info("用户入群开始调用此接口");
//		if (enterGroupInfo == null) {
//			return CommonResult.build(-1, "传过来的信息为Null");
//		}
//		String openID = enterGroupInfo.getOpenID();
//		String uRoomID = enterGroupInfo.getuRoomID();
//		List<Wechatroominfo> wechatRoomInfo = enterGroupMapper.selectRoomInfoById(uRoomID);
//		HashMap<String, Object> data = new HashMap<>();
//		data.put("Type", 2);// 1代表入群，2代表退群
//		data.put("OpenId", openID);
//		data.put("EventTime", enterGroupInfo.getTime());// 退群时间
//		if (wechatRoomInfo == null) {
//			return CommonResult.build(-1, "根据U_RoomID查不到相应的群");
//		}
//		data.put("GroupName", wechatRoomInfo.get(0).getRoomname());
//		data.put("GroupId", wechatRoomInfo.get(0).getRoomid());
//		String groupTag = wechatRoomInfo.get(0).getGroupTag();
//		// 美素佳儿用户退群，返回信息给美素
//		if (null != groupTag && !groupTag.equals("") && groupTag.equals("52")) {
//			logger.info("返还给美素佳儿入群信息 : " + JSONObject.toJSONString(data));
//			try {
//				String json = JSONObject.toJSONString(data);
//				//获取access_token
//				Map<String,String> param = new HashMap<>();
//				param.put("appid", "crm.wedo");
//				param.put("appsecret", "crm.wedo");
//				String accessResult = HttpClientUtil.doGet(this.propertyConfigurer.getProperty("GET_ACCESS_TOKEN"), param);
//				Map<String,Object> token = JSON.parseObject(accessResult);
//				String access_token = (String) token.get("data");
//				//将access_token置于请求头
//				Map<String,String> header = new HashMap<>();
//				header.put("Authorization ","bearer "+ access_token);
//				// TODO 调用美素接口
//				String url = this.propertyConfigurer.getProperty("RETURN_INFO_TO_FRISO");
//				String resultByFriso = HttpClientUtil.request(url, null,"POST", DEFAULT_CHARSET, true, header);
////				String resultByFriso = HttpClientUtil.doPostJsonSetHeader(this.propertyConfigurer.getProperty("RETURN_INFO_TO_FRISO"), json,header);// TODO暂未验证
//				logger.info("美素返还的信息 : " + JSON.parse(resultByFriso));
//			} catch (Exception e) {
//				logger.info("调用美素接口出现未知异常" + e);
//				return CommonResult.build(-1, "调用美素出现未知异常");
//			}
//			return CommonResult.ok();
//		}
//		return CommonResult.build(-1, "此用户不是美素用户");
//	}

	@Override
	public List<ErrorInfoToFriso> selectErrorInfo() {
		return enterGroupMapper.selectErrorInfo();
	}

	@Override
	public void updateErrorInfo(List<ErrorInfoToFriso> selectErrorInfo) {
		enterGroupMapper.updateErrorInfo(selectErrorInfo);
	}

	@Override
	public int countErrorInfo() {
		return enterGroupMapper.countErrorInfo();
	}
	

}
