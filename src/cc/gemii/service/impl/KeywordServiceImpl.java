package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.WechatkeywordCustomMapper;
import cc.gemii.mapper.WechatkeywordMapper;
import cc.gemii.po.Wechatkeyword;
import cc.gemii.po.WechatkeywordExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SelectKeyword;
import cc.gemii.service.KeywordService;
import cc.gemii.util.ComputePages;

@Service
public class KeywordServiceImpl implements KeywordService {

	@Autowired
	private WechatkeywordMapper wkMapper;
	
	@Autowired
	private WechatkeywordCustomMapper wkCustomMapper;
	
	@Override
	public CommonResult getKeywordByGid(String id) {
		WechatkeywordExample example = new WechatkeywordExample();
		example.createCriteria()
			.andRoomidEqualTo(id);
		return CommonResult.ok(wkMapper.selectByExample(example));
	}

	@Override
	public CommonResult insertKeyword(Wechatkeyword keyword) {
		wkMapper.insert(keyword);
		return CommonResult.ok();
	}

	@Override
	public CommonResult updateKeyword(Wechatkeyword keyword) {
		wkMapper.updateByPrimaryKeySelective(keyword);
		return CommonResult.ok();
	}

	@Override
	public CommonResult selectKeyword(SelectKeyword selectKeyword) {
		if("".equals(selectKeyword.getEdc())){
			selectKeyword.setEdc(null);
		}
		selectKeyword.setCity(selectKeyword.getCity());
		selectKeyword.setKeyword(selectKeyword.getKeyword());
		int pageSize = selectKeyword.getPageSize();
		selectKeyword.setStart((selectKeyword.getPage() - 1) * pageSize);
		if("all".equals(selectKeyword.getType())){
			int total = wkCustomMapper.selectKeywordByCidCount(selectKeyword);
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("total", total);
			response.put("count", ComputePages.computePages(total, pageSize));
			response.put("keywords", wkCustomMapper.selectKeywordByCid(selectKeyword));
			return CommonResult.ok(response);
		}else {
			int total = wkCustomMapper.selectKeywordCount(selectKeyword);
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("total", total);
			response.put("count", ComputePages.computePages(total, pageSize));
			response.put("keywords", wkCustomMapper.selectKeyword(selectKeyword));
			return CommonResult.ok(response);
		}
		
	}
}
