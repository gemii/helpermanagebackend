package cc.gemii.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.data.otd.UserInfoBase;
import cc.gemii.fuckemoji.EmojiUtil;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.po.UserInfoPO;
import cc.gemii.po.Userinfo;
import cc.gemii.po.UserinfoExample;
import cc.gemii.po.Userstatus;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.WyethMatchGroupService;
import cc.gemii.util.ConfigUtil;
import cc.gemii.util.TimeUtils;

@Service
public class WyethMatchGroupServiceImpl implements WyethMatchGroupService{

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private UserstatusMapper userstatusMapper;
	@Autowired
	private UserinfoMapper userinfoMapper;
	@Autowired
	private SendMsgContext sendMsgContext;
	@Autowired
	private Properties propertyConfigurer;
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	@Autowired
	private MatchGroupContext matchGroupContext;
	
	@Override
	public CommonResult commitUserInfo(Userinfo user, String robotID, String code, String uid, String type) {
		try {
			String matchstatus = "";
			// 最终匹配的到的群
			Map<String, String> map = new HashMap<String, String>();

			// 判断手机号是否入群
			if (this.phoneExists(user)) {
				return CommonResult.build(-1, "用户已入群");
			}

			map.put("robotID", robotID);
			map.put("city", user.getCity());
			map.put("edc", user.getEdc());
			map.put("hospital", user.getHospitalkeyword());
			map.put("code", code);
			map.put("uid", uid);
			map.put("phone", user.getPhonenumber());
			map.put("srcode", user.getSrcode());
			map.put("openid", user.getOpenid());
			map.put("type", type);
			if ("all".equals(uid)) {
				return this.submitUserInfoAll(map);
			}

			Userstatus userStatus = enterGroupMapper.getLastRecord(map);
			if (userStatus == null) {
				return CommonResult.build(-3, "未匹配到此用户");
			}
			if (ConfigUtil.ENTER_ROOM_STATUS_SUCCESS.equals(userStatus.getEntergroupstatus())
					|| ConfigUtil.ENTER_ROOM_STATUS_INVITE.equals(userStatus.getEntergroupstatus())
					|| ConfigUtil.ENTER_ROOM_STATUS_HM.equals(userStatus.getEntergroupstatus())) {
				// 如果此用户状态为 已入群 已邀请 手动邀请则返回
				return CommonResult.build(-1, "用户已入群");
			}
			MatchGroupResult matchGroupResult = matchGroupContext
					.matchGroup(MatchGroupStrategy.WYETH_THREE_MATCHGROUP_STRATEGY, map);
			if (matchGroupResult.getResultCode() == 1) {
				matchstatus="已匹配";
			}

			if (matchGroupResult.getResultCode() == 2) {
				matchstatus="已匹配城市群";
			}
			if (matchGroupResult.getResultCode() == 3) {
				matchstatus="已匹配全国群";
			}
			// 更新用户状态
			userStatus.setMatchstatus(matchstatus);
			userStatus.setMatchgroup(matchGroupResult.getMatchGroupId());
			// 发送短信
			GeneralContentResult<String> result = sendMsg(user.getPhonenumber(), code);
			if (ResultCode.OPERATION_SUCCESS.equals(result.getResultCode())) {
				userStatus.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_SUCCESS);
			} else {
				userStatus.setPhonecodesendstatus(
						ConfigUtil.PHONE_CODE_SEND_FAILED + "(" + result.getResultContent() + ")");
			}
			userstatusMapper.updateByPrimaryKeySelective(userStatus);

			// 插入用户信息
			UserinfoExample example = new UserinfoExample();
			example.createCriteria().andUserstatusidEqualTo(userStatus.getId());
			userinfoMapper.deleteByExample(example);
			user.setFormtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
			user.setUserstatusid(userStatus.getId());
			user.setSrcode(user.getSrcode());
			userinfoMapper.insert(user);
			return CommonResult.ok(code);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "服务器错误");
		}
	}
	
	private CommonResult submitUserInfoAll(Map<String, String> map) {
		String matchstatus="";
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(MatchGroupStrategy.WYETH_THREE_MATCHGROUP_STRATEGY, map);
		if (matchGroupResult.getResultCode() == 1) {
			matchstatus="已匹配";
		}

		if (matchGroupResult.getResultCode() == 2) {
			matchstatus="已匹配城市群";
		}
		if (matchGroupResult.getResultCode() == 3) {
			matchstatus="已匹配全国群";
		}
		Userstatus userstatus = new Userstatus();
		userstatus.setH5status(ConfigUtil.SEND_H5_CLICK);
		userstatus.setEntergroupstatus(ConfigUtil.NOT_INTO_ROOM);
		userstatus.setMatchgroup(matchGroupResult.getMatchGroupId());
		userstatus.setMatchstatus(matchstatus);
		userstatus.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_SUCCESS);
		// 发送短信
		String code = String.valueOf((int) (Math.random() * 9000 + 1000));
		GeneralContentResult<String> result = sendMsg(map.get("phone"), code);
		if (ResultCode.OPERATION_SUCCESS.equals(result.getResultCode())) {
			userstatus.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_SUCCESS);
		} else {
			userstatus
					.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_FAILED + "(" + result.getResultContent() + ")");
		}
		userstatus.setPhonecode(code);
		userstatus.setRobotid(map.get("robotID"));
		userstatus.setType("A");
		userstatus.setUserid("all");
		userstatus.setFriendtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		// 插入 UserStatus
		userstatusMapper.insert(userstatus);

		Userinfo userinfo = new Userinfo();
		userinfo.setCity(map.get("city"));
		userinfo.setEdc(map.get("edc"));
		userinfo.setFormtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		userinfo.setHospitalkeyword(map.get("hospital"));
		userinfo.setOpenid(map.get("openid"));
		userinfo.setPhonenumber(map.get("phone"));
		userinfo.setSrcode(map.get("srcode"));
		userinfo.setUserstatusid(userstatus.getId());
		userinfoMapper.insert(userinfo);
		return CommonResult.ok(code);
	}
	
	private boolean phoneExists(Userinfo user) {
		Integer id = enterGroupMapper.phoneExists(user.getPhonenumber());
		if (id == null || id == 0) {
			return false;
		}

		user.setProvince("重复入群");
		userinfoMapper.insert(user);
		return true;
	}
	
	private String validateEdc(String edc) {
		// 验证预产期,根据edc与当前日期进行比较,edc早于当前日期365天入 %新妈%群,晚于当前日期280天入 %孕妈%群
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long day;
		try {
			day = TimeUtils.dateDiff(sdf.parse(edc));
			if (day < -365 || day > 280) {
				return "无";
			} else if (day >= -365 && day <= 0) {
				return "%新妈%";
			} else {
				return "%孕妈%";
			}
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			return "无";
		}

	}
	
	/**
	 * 发送短信
	 * 
	 * @param phone
	 * @param code
	 * @return
	 */
	private GeneralContentResult<String> sendMsg(String phone, String code) {
		ShortMessage _shortMessage = new ShortMessage();
		_shortMessage.setReciverPhone(phone);
		Formatter fm = new Formatter();
		String firstTemplate = propertyConfigurer.getProperty("send_msg_prompt");
		String first = String.valueOf(fm.format(firstTemplate, code));
		_shortMessage.setContent(first);
		_shortMessage.setCode(code);
		return sendMsgContext.sendSMS(_shortMessage);
	}

	@Override
	public CommonResult selectRobotOwner(String robotId) {
		Map<String, String> result=new HashMap<>();
		String robotOwner =enterGroupMapper.selectRobotOwner(robotId);
		if(!"".equals(robotOwner) && robotOwner!=null){
			result.put("robotOwner", robotOwner);
			return CommonResult.ok(result);
		}
		return CommonResult.build(-1, "获取信息失败");
	}
	
}
