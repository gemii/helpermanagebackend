package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.WechatroomCustomMapper;
import cc.gemii.mapper.WechatroominfoMapper;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.GroupService;
import cc.gemii.util.ComputePages;
@Service
public class GroupServiceImpl implements GroupService {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private WechatroomCustomMapper wcrCustomMapper;
	
	@Autowired
	private WechatroominfoMapper wriMapper;
	
	@Override
	public CommonResult getAllGroup(Integer page, Integer pageSize, String groupName, Integer clerkID) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("start", (page - 1) * pageSize);
			map.put("pageSize", pageSize);
			map.put("groupName", "%" + groupName + "%");
			map.put("clerkID", clerkID);
			
			Map<String, Object> response = new HashMap<String, Object>();
			response.put("groups", wcrCustomMapper.getAllGroup(map));
			int total = wcrCustomMapper.getCount(map);
			response.put("count", ComputePages.computePages(total, pageSize));
			response.put("total", total);
			return CommonResult.ok(response);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, e.toString());
		}
	}
	@Override
	public CommonResult getGroupByName(String groupName) {
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("group_name", groupName);
			return CommonResult.ok(wcrCustomMapper.getGroupByName(map));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, e.toString());
		}
	}
	@Override
	public CommonResult updateGroupInfo(Wechatroominfo roomInfo) {
		try {
			wriMapper.updateByPrimaryKeySelective(roomInfo);
			return CommonResult.ok();
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, e.toString());
		}
	}
	@Override
	public CommonResult getGroupByHelper(String helper_id,String groupName,Integer page,Integer pageSize,String type,String groupTag) {
		try {
			Map<String, Object> response = new HashMap<String, Object>();
			if("all".equals(type)){
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("uid", helper_id);
				param.put("start", (page - 1) * pageSize);
				param.put("pageSize", pageSize);
				param.put("roomName", groupName);
				param.put("groupType", groupTag);
				int total = wcrCustomMapper.getGroupByUidCount(param);
				response.put("total", total);
				response.put("count", ComputePages.computePages(total, pageSize));
				response.put("groups", wcrCustomMapper.getGroupByUid(param));
				return CommonResult.ok(response);
			}else{
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("robotID", helper_id);
				param.put("start", (page - 1) * pageSize);
				param.put("pageSize", pageSize);
				param.put("roomName", groupName);
				param.put("groupType", groupTag);
				int total = wcrCustomMapper.getGroupByHelperCount(param);
				response.put("total", total);
				response.put("count", ComputePages.computePages(total, pageSize));
				response.put("groups", wcrCustomMapper.getGroupByHelper(param));
				return CommonResult.ok(response);
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			return CommonResult.build(500, e.toString());
		}
	}
}
