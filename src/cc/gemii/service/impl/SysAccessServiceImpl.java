package cc.gemii.service.impl;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.SysAccessMapper;
import cc.gemii.po.SysAccessDO;
import cc.gemii.service.SysAccessService;

@Service
public class SysAccessServiceImpl implements SysAccessService {

	@Autowired
	private SysAccessMapper sysAccessMapper;
	
	@Override
	public int insertSysAccess(SysAccessDO sysAccessDO) {
		return sysAccessMapper.insertSysAccess(sysAccessDO);
	}

	@Override
	public List<String> countMatchTimesByMember(String startTime,String endTime) {
		return sysAccessMapper.countMatchTimesByMember( startTime, endTime);
	}

	@Override
	public List<String> countMatchTimesByGroup(String startTime,String endTime) {
		return sysAccessMapper.countMatchTimesByGroup( startTime, endTime);
	}

}
