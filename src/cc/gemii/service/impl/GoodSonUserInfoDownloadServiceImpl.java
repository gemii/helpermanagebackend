package cc.gemii.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.GoodSonUserInfoDownloadMapper;
import cc.gemii.pojo.GoodSonUserInfo;
import cc.gemii.service.GoodSonUserInfoDownloadService;

@Service
public class GoodSonUserInfoDownloadServiceImpl implements GoodSonUserInfoDownloadService{

	@Autowired
	private GoodSonUserInfoDownloadMapper goodSonUserInfoDownloadMapper;
	
	@Override
	public List<GoodSonUserInfo> GoodSonUserInfoQuery(String startTime, String endTime) {
		return goodSonUserInfoDownloadMapper.goodSonUserInfoQuery(startTime, endTime);
	}

}
