package cc.gemii.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.TaskNowMapper;
import cc.gemii.po.TaskNow;
import cc.gemii.pojo.SendMsgTask;
import cc.gemii.service.TaskNowService;

@Service
public class TaskNowImpl implements TaskNowService {

	@Autowired
	private TaskNowMapper sendMessageMapper;
	
	@Override
	public List<SendMsgTask> getPhones() {
		return sendMessageMapper.getPhoneNums();
	}

	@Override
	public void insertTaskInfo(List<TaskNow> map) {
		sendMessageMapper.insertTaskInfo(map);
		
	}

}
