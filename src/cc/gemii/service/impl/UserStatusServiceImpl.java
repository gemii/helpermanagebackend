package cc.gemii.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserStatusCardVO;
import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.UserStatusService;

@Service
public class UserStatusServiceImpl implements UserStatusService {

	@Autowired
	private UserstatusMapper userstatusMapper;
	@Autowired
	private UserinfoMapper userinfoMapper;
	
	@Override
	public PageInfo<UserStatusCardVO> searchUserStatusByCardIn(int page, int size, UserStatusCardProfile searchInfo) {
		PageHelper.startPage(page, size);
		Page<UserStatusCardVO> pageData = userstatusMapper.selectCardUserStatus(searchInfo);
		PageInfo<UserStatusCardVO> pageInfo = pageData.toPageInfo();
		return pageInfo;
	}

	@Override
	public CommonResult matchuserinfo(int id, String phoneNumber) {
		Userinfo userinfo = userinfoMapper.selectByPhoneNumber(phoneNumber);
		CommonResult cr=new CommonResult();
		if(userinfo == null){
			return null;
		}
		List<String> username=userstatusMapper.selectInfo(userinfo.getId());
		if(userinfo != null && username.size() !=0){
			cr.setData(userinfo);
			cr.setStatus(2);
			return cr;
		}
		userstatusMapper.updateFKUserinfoId(id,userinfo.getId());
		cr.setData(userinfo);
		cr.setStatus(1);
		return cr;
	}


}
