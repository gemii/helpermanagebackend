package cc.gemii.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.fuckemoji.EmojiUtil;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.po.Userinfo;
import cc.gemii.po.UserinfoExample;
import cc.gemii.po.Userstatus;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.GetAreaHosNameByCode;
import cc.gemii.pojo.GetRobotIdByU;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.pojo.SelectUser;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.EnterGroupService;
import cc.gemii.util.ConfigUtil;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.ParamUtil;
import cc.gemii.util.RedisUtil;
import cc.gemii.util.TimeUtils;

@Service
public class EnterGroupServiceImpl implements EnterGroupService {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private EnterGroupMapper enterGroupMapper;

	@Autowired
	private UserinfoMapper userinfoMapper;

	@Autowired
	private UserstatusMapper userstatusMapper;

	@Autowired
	private SendMsgContext sendMsgContext;

	@Autowired
	private MatchGroupContext matchGroupContext;

	@Autowired
	private Properties propertyConfigurer;
	@Value("${HANDLE_NICKNAME_URL}")
	private String HANDLE_NICKNAME_URL;

	@Override
	public CommonResult getCity(String robotID) {
		List<String> citys = enterGroupMapper.getCity(robotID);
		return CommonResult.ok(citys);
	}

	@Override
	public CommonResult getAllCity() {
		List<String> citys = enterGroupMapper.getAllCity();
		return CommonResult.ok(citys);
	}

	/**
	 * 发送短信
	 * 
	 * @param phone
	 * @param code
	 * @return
	 */
	private GeneralContentResult<String> sendMsg(String phone, String code) {
		ShortMessage _shortMessage = new ShortMessage();
		_shortMessage.setReciverPhone(phone);
		Formatter fm = new Formatter();
		String firstTemplate = propertyConfigurer.getProperty("send_msg_prompt");
		String first = String.valueOf(fm.format(firstTemplate, code));
		_shortMessage.setContent(first);
		_shortMessage.setCode(code);
		return sendMsgContext.sendSMS(_shortMessage);
	}

	@Override
	public CommonResult getHosByCity(String city, String robotID) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("city", city);
		map.put("robotID", robotID);
		List<String> hospitals = enterGroupMapper.getHosByCity(map);
		return CommonResult.ok(hospitals);
	}

	@Override
	public CommonResult submitUserInfo(Userinfo user, String robotID, String code, String uid) {
		try {
			String matchstatus = "";
			// 最终匹配的到的群
			MatchGroup sendGroup = new MatchGroup();

			Map<String, String> map = new HashMap<String, String>();

			// 判断手机号是否入群
			if (this.phoneExists(user)) {
				return CommonResult.build(-1, "用户已入群");
			}

			// 判断预产期
			map.put("country", this.validateEdc(user.getEdc()));
			map.put("robotID", robotID);
			// map.put("city", this.handleCity(user.getCity()));
			map.put("city", user.getCity());
			map.put("edc", user.getEdc());
			map.put("hospital", user.getHospitalkeyword());
			map.put("code", code);
			map.put("uid", uid);
			map.put("phone", user.getPhonenumber());
			map.put("srcode", user.getSrcode());
			map.put("openid", user.getOpenid());

			if ("all".equals(uid)) {
				return this.submitUserInfoAll(map);
			}

			Userstatus userStatus = enterGroupMapper.getLastRecord(map);
			if (userStatus == null) {
				return CommonResult.build(-3, "未匹配到此用户");
			}
			if (ConfigUtil.ENTER_ROOM_STATUS_SUCCESS.equals(userStatus.getEntergroupstatus())
					|| ConfigUtil.ENTER_ROOM_STATUS_INVITE.equals(userStatus.getEntergroupstatus())
					|| ConfigUtil.ENTER_ROOM_STATUS_HM.equals(userStatus.getEntergroupstatus())) {
				// 如果此用户状态为 已入群 已邀请 手动邀请则返回
				return CommonResult.build(-1, "用户已入群");
			}
			Map<String, Object> matchResult = this.matchGroup(map);
			sendGroup = (MatchGroup) matchResult.get("sendGroup");
			matchstatus = String.valueOf(matchResult.get("matchstatus"));

			// 更新用户状态
			userStatus.setMatchstatus(matchstatus);
			userStatus.setMatchgroup(sendGroup.getRoomID());
			// 发送短信
			GeneralContentResult<String> result = sendMsg(user.getPhonenumber(), code);
			if (ResultCode.OPERATION_SUCCESS.equals(result.getResultCode())) {
				userStatus.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_SUCCESS);
			} else {
				userStatus.setPhonecodesendstatus(
						ConfigUtil.PHONE_CODE_SEND_FAILED + "(" + result.getResultContent() + ")");
			}
			userstatusMapper.updateByPrimaryKeySelective(userStatus);

			// 插入用户信息
			UserinfoExample example = new UserinfoExample();
			example.createCriteria().andUserstatusidEqualTo(userStatus.getId());
			userinfoMapper.deleteByExample(example);
			user.setFormtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
			user.setUserstatusid(userStatus.getId());
			user.setSrcode(user.getSrcode());
			userinfoMapper.insert(user);
			return CommonResult.ok(code);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "服务器错误");
		}
	}

	/*
	 * temp 重庆万和药店入群 临时修改
	 */
	private String handleCity(String city) {
		if (ParamUtil.get_chong_qing_wanhe().contains(city)) {
			return "重庆";
		}
		return city;
	}

	@Override
	public CommonResult getCode(String phone, String code) {
		GeneralContentResult<String> result = sendMsg(phone, code);
		Map<String, String> param = new HashMap<String, String>();
		param.put("phone", phone);
		param.put("code", code);
		param.put("status", ConfigUtil.PHONE_CODE_RESEND + "(" + result.getResultContent() + ")");
		enterGroupMapper.updatePhoneCodeSendStatus(param);
		if (!ResultCode.OPERATION_SUCCESS.equals(result.getResultCode()))
			return CommonResult.build(-1, "短信发送失败");
		return CommonResult.ok();
	}

	@Override
	public CommonResult modifyH5Status(String robotID, String code, String uid) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("uid", uid);
		map.put("robotID", robotID);
		map.put("code", code);
		Userstatus userStatus = enterGroupMapper.getLastRecord(map);
		if (userStatus == null) {
			return CommonResult.build(500, "未匹配到数据");
		}
		userStatus.setH5status(ConfigUtil.SEND_H5_CLICK);
		userstatusMapper.updateByPrimaryKeySelective(userStatus);
		return CommonResult.ok();
	}

	private CommonResult submitUserInfoAll(Map<String, String> map) {
		Map<String, Object> matchResult = this.matchGroup(map);
		MatchGroup sendGroup = (MatchGroup) matchResult.get("sendGroup");
		String matchstatus = String.valueOf(matchResult.get("matchstatus"));
		Userstatus userstatus = new Userstatus();
		userstatus.setH5status(ConfigUtil.SEND_H5_CLICK);
		userstatus.setEntergroupstatus(ConfigUtil.NOT_INTO_ROOM);
		userstatus.setMatchgroup(sendGroup.getRoomID());
		userstatus.setMatchstatus(matchstatus);
		userstatus.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_SUCCESS);
		// 发送短信
		String code = String.valueOf((int) (Math.random() * 9000 + 1000));
		GeneralContentResult<String> result = sendMsg(map.get("phone"), code);
		if (ResultCode.OPERATION_SUCCESS.equals(result.getResultCode())) {
			userstatus.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_SUCCESS);
		} else {
			userstatus
					.setPhonecodesendstatus(ConfigUtil.PHONE_CODE_SEND_FAILED + "(" + result.getResultContent() + ")");
		}
		userstatus.setPhonecode(code);
		userstatus.setRobotid(map.get("robotID"));
		userstatus.setType("A");
		userstatus.setUserid("all");
		userstatus.setFriendtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		// 插入 UserStatus
		userstatusMapper.insert(userstatus);

		Userinfo userinfo = new Userinfo();
		userinfo.setCity(map.get("city"));
		userinfo.setEdc(map.get("edc"));
		userinfo.setFormtime(TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		userinfo.setHospitalkeyword(map.get("hospital"));
		userinfo.setOpenid(map.get("openid"));
		userinfo.setPhonenumber(map.get("phone"));
		userinfo.setSrcode(map.get("srcode"));
		userinfo.setUserstatusid(userstatus.getId());
		userinfoMapper.insert(userinfo);
		return CommonResult.ok(code);
	}

	/*
	 * 匹配群逻辑 param include city、hospital、edc、robotID(非必要)
	 */
	public Map<String, Object> matchGroup(Map<String, String> param) {
		String matchstatus = "";
		MatchGroup sendGroup = null;
		if (ConfigUtil.OTHERS.equals(param.get("city"))) { // 如果城市为其他, 则直接进入全国群
			matchstatus = ConfigUtil.MATCH_GROUP_FAILED_COUNTRY;
			sendGroup = enterGroupMapper.getCountryGroup(param);
		} else if (ConfigUtil.OTHERS.equals(param.get("hospital"))) { // 如果医院为其他,
																		// 则直接进入城市群
			matchstatus = ConfigUtil.MATCH_GROUP_FAILED_CITY;
			sendGroup = enterGroupMapper.getCityGroup(param);
			if (sendGroup == null) { // 如果没有城市群, 匹配全国群
				matchstatus = ConfigUtil.MATCH_GROUP_FAILED_COUNTRY;
				sendGroup = enterGroupMapper.getCountryGroup(param);
			} else if (sendGroup.getUserCount() > 480) { // 如果匹配到了城市群成员但人数 >
															// 480, 匹配全国群
				matchstatus = ConfigUtil.MATCH_GROUP_FAILED_FULL;
				sendGroup = enterGroupMapper.getCountryGroup(param);
			}
		} else { // 城市和医院都不为其他, 匹配医院群
			sendGroup = enterGroupMapper.getMatchGroup(param);
			if (sendGroup == null || sendGroup.getUserCount() > 480) { // 如果医院群人数
																		// >
																		// 480,
																		// 匹配城市群
				matchstatus = ConfigUtil.MATCH_GROUP_FAILED_CITY;
				sendGroup = enterGroupMapper.getCityGroup(param);
				if (sendGroup == null || sendGroup.getUserCount() > 480) { // 如果匹配不到城市群或城市群人数
																			// >
																			// 480,
																			// 匹配全国群
					matchstatus = ConfigUtil.MATCH_GROUP_FAILED_COUNTRY;
					sendGroup = enterGroupMapper.getCountryGroup(param);
				}
			} else {
				matchstatus = ConfigUtil.MATCH_GROUP_SUCCESS; // 群匹配成功
			}
		}

		if (sendGroup == null) { // 此时sendGroup为null时,没有匹配到群
			matchstatus = ConfigUtil.MATCH_GROUP_FAILED_NONE;
			sendGroup = new MatchGroup();
		}

		Map<String, Object> response = new HashMap<String, Object>();
		// 匹配群状态
		response.put("matchstatus", matchstatus);
		// 匹配的群信息
		response.put("sendGroup", sendGroup);
		return response;
	}

	@Override
	public CommonResult getCityAndHost(String robotID) {
		List<Map<String, Object>> response = new ArrayList<Map<String, Object>>();
		Map<String, Object> each = null;
		;
		List<String> citys = enterGroupMapper.getCity(robotID);
		Map<String, String> param = null;
		for (String city : citys) {
			param = new HashMap<String, String>();
			param.put("city", city);
			param.put("robotID", robotID);
			each = new HashMap<String, Object>();
			each.put(city, enterGroupMapper.getHosByCity(param));
			response.add(each);
		}
		return CommonResult.ok(response);
	}

	private boolean phoneExists(Userinfo user) {
		Integer id = enterGroupMapper.phoneExists(user.getPhonenumber());
		if (id == null || id == 0) {
			return false;
		}

		user.setProvince("重复入群");
		userinfoMapper.insert(user);
		return true;
	}

	@Override
	public CommonResult createMember(UserFriendEnterGroup memberCardInfo) {
		Map<String, String> selectGroupParam = new HashMap<String, String>();
		selectGroupParam.put("city", memberCardInfo.getCity());
		selectGroupParam.put("hospital", memberCardInfo.getHospital());
		selectGroupParam.put("edc", memberCardInfo.getEdc());
		selectGroupParam.put("country", this.validateEdc(memberCardInfo.getEdc()));
		Map<String, Object> matchResult = this.matchGroup(selectGroupParam);
		String matchStatus = String.valueOf(matchResult.get("matchstatus"));
		MatchGroup sendGroup = (MatchGroup) matchResult.get("sendGroup");
		// 从公众号获取用户信息
		String userInfoJson = HttpClientUtil
				.doGet(ParamUtil.GET_USERINFO_BY_OPENID.replace("OPEN_ID", memberCardInfo.getOpenid()));
		Map<String, Object> userInfoMap = (Map<String, Object>) JSON.parse(userInfoJson);
		// 处理emoji
		memberCardInfo.setNickname(EmojiUtil.handleEmoji(String.valueOf(userInfoMap.get("nickname"))));
		memberCardInfo.setHeadimgurl(String.valueOf(userInfoMap.get("headimgurl")));
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("userInfo", memberCardInfo);
		int length = memberCardInfo.getPhone().length();
		param.put("phoneCode", memberCardInfo.getPhone().substring(length - 4, length));
		param.put("matchStatus", matchStatus);
		param.put("sendGroup", sendGroup);
		enterGroupMapper.insertMemberCard(param);
		return CommonResult.ok(sendGroup);
	}

	private String validateEdc(String edc) {
		// 验证预产期,根据edc与当前日期进行比较,edc早于当前日期365天入 %新妈%群,晚于当前日期280天入 %孕妈%群
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long day;
		try {
			day = TimeUtils.dateDiff(sdf.parse(edc));
			if (day < -365 || day > 280) {
				return "无";
			} else if (day >= -365 && day <= 0) {
				return "%新妈%";
			} else {
				return "%孕妈%";
			}
		} catch (ParseException e) {
			logger.error(e.getMessage(), e);
			return "无";
		}

	}

	@Override
	public CommonResult createHuggiesMember(UserFriendEnterGroup memberReq) {

		Map<String, String> matchingconds = new HashMap<String, String>();
		matchingconds.put("phone", memberReq.getPhone());
		matchingconds.put("openid", memberReq.getOpenid());
		matchingconds.put("city", memberReq.getCity());
		MatchGroupResult matchGroupResult = matchGroupContext.matchGroup(MatchGroupStrategy.HUGGIES_MATCHGROUP_STRATEGY,
				matchingconds);
		if (matchGroupResult.getResultCode() != 1 && matchGroupResult.getResultCode() != -4) {
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}

		Map<String, Object> param = new HashMap<String, Object>();
		// 处理emoji
		memberReq.setNickname(EmojiUtil.handleEmoji(memberReq.getNickname()));
		param.put("userInfo", memberReq);
		int length = memberReq.getPhone().length();
		param.put("phoneCode", memberReq.getPhone().substring(length - 4, length));
		if (matchGroupResult.getResultCode() == -4) {
			param.put("matchStatus", "群匹配失败");
			param.put("sendGroup", new MatchGroup());
			enterGroupMapper.insertHuggiesMember(param);
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}

		MatchGroup matchResult = enterGroupMapper.getMatchGroupByHuggies(matchGroupResult.getMatchGroupId());
		param.put("matchStatus", matchGroupResult.getMsg());
		param.put("sendGroup", matchResult);
		enterGroupMapper.insertHuggiesMember(param);
		return CommonResult.ok(matchResult);
	}

	@Override
	public CommonResult receiveInfoMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		return wyethMatchGroup(receiveinfomatch,MatchGroupStrategy.WYETH_CARD_MATCHGROUP_STRATEGY);
	}

	@Override
	public CommonResult selectRobotId(UserFriendEnterGroup receiveinfomatch) {
		Map<String, Object> param = new HashMap<>();
		Map<String, String> paramuser = new HashMap<>();
		param.put("openid", receiveinfomatch.getOpenid());
		if (receiveinfomatch.getType() == null) {
			param.put("type", 1);
		} else if("9".equals(receiveinfomatch.getType())){
			param.put("type", 1);
			receiveinfomatch.setType("1");
		}else if("31".equals(receiveinfomatch.getType())){
			param.put("type", 3);
			receiveinfomatch.setType("3");
		}else {
			param.put("type", receiveinfomatch.getType());
		}
		Userinfo info = enterGroupMapper.selectUserInfo(param);
		Map<String, Object> result=new HashMap<>();
		if (info!= null && !"".equals(info)) {
			
			List<SelectUser> selectuser = enterGroupMapper.selectStatus(receiveinfomatch.getOpenid(),
					receiveinfomatch.getType());// 获取返回的入群状态
			logger.info("Userinfo里面的信息"+info);
			if (selectuser != null && selectuser.size() > 0) {
				paramuser.put("roomId", selectuser.get(0).getMatchGroup());
				paramuser.put("userName", selectuser.get(0).getUserName());
				String resultlist = enterGroupMapper.selectRoomInfo(paramuser);
				if (resultlist != null && !"".equals(resultlist)) {
					return CommonResult.build(2, "已发送入群邀请");
				}
				result.put("robotId", info.getMatchrobot());
				result.put("phone", info.getPhonenumber());
				userinfoMapper.refreshVcodeStartTiem(info.getId());
				return CommonResult.ok(result);
			}
			if ("已匹配".equals(info.getMatchGroupStatus()) || "已匹配城市群".equals(info.getMatchGroupStatus()) || "已匹配全国群".equals(info.getMatchGroupStatus())) {
				if("".equals(info.getMatchrobot())  || info.getMatchrobot()==null ){
					logger.info("查询到的群信息 "+info.getMatchGroup()+"  type "+receiveinfomatch.getType());
					List<MatchGroup> receives = enterGroupMapper.getMatchGroupByUserInfo(info.getMatchGroup(), receiveinfomatch.getType());
					if(receives.size() == 0){
						return CommonResult.build(-1, "请重新匹配");
					}
					MatchGroup receive = receives.get(new Random().nextInt(receives.size()));
					logger.info("查询到的机器人信息  "+receive.getRobotID());
					if(!"".equals(receive.getRobotID())  && receive.getRobotID()!=null){
						info.setMatchrobot(receive.getRobotID());
						Map<String, Object> par=new HashMap<>();
						par.put("matchRobot", receive.getRobotID());
						par.put("id", info.getId());
						enterGroupMapper.updateUserRobot(par);
					}
				}
				result.put("robotId", info.getMatchrobot());
				result.put("phone", info.getPhonenumber());
				userinfoMapper.refreshVcodeStartTiem(info.getId());
				logger.info("返回参数  "+info.getMatchrobot()+ "手机号 "+info.getPhonenumber());
				return CommonResult.ok(result);
			}
			return CommonResult.build(-1, "请重新匹配");

		} else {
			return CommonResult.build(-1, "请重新匹配");
		}

	}

	@Override
	public CommonResult goodParentMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> receiveinfo = new HashMap<>();
		receiveinfo.put("phone", receiveinfomatch.getPhone());
		receiveinfo.put("city", receiveinfomatch.getCity());
		receiveinfo.put("openid", receiveinfomatch.getOpenid());
		receiveinfo.put("hospital", receiveinfomatch.getHospital());
		receiveinfo.put("edc", receiveinfomatch.getEdc());
		receiveinfo.put("province", receiveinfomatch.getProvince());
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(MatchGroupStrategy.GOOD_PARENT_MATCHGROUP_STRATEGY, receiveinfo);
		Map<String, Object> param = new HashMap<String, Object>();
		receiveinfomatch.setNickname(EmojiUtil.handleEmoji(receiveinfomatch.getNickname()));
		param.put("receiveinfo", receiveinfomatch);
		param.put("type", "2");// 好育儿标识
		if (matchGroupResult.getResultCode() == -1) {
			param.put("matchStatus", "未匹配");
			param.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(param);
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}
		param.put("matchStatus", "已匹配");
		param.put("matchGroup", matchGroupResult.getMatchGroupId());
		List<MatchGroup> receives = enterGroupMapper.getMatchGroupByUserInfo(matchGroupResult.getMatchGroupId(), "2");
		if (receives.size() == 0) {
			insertUserInfo(param);
			return CommonResult.build(-2, "没匹配到机器人信息");
		}
		MatchGroup receive = receives.get(new Random().nextInt(receives.size()));

		param.put("sendGroup", receive.getRobotID());
		insertUserInfo(param);
		return CommonResult.ok(receive.getRobotID());
	}

	@Override
	public CommonResult pharmacyMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> param = new HashMap<>();
		param.put("phone", receiveinfomatch.getPhone());
		param.put("city", receiveinfomatch.getCity());
		param.put("openid", receiveinfomatch.getOpenid());
		param.put("hospital", receiveinfomatch.getHospital());
		param.put("edc", receiveinfomatch.getEdc());
		param.put("province", receiveinfomatch.getProvince());
		SelectUser selectUser=enterGroupMapper.selectRepeatInfo(receiveinfomatch.getPhone(), receiveinfomatch.getType());
		if(selectUser!=null && !"".equals(selectUser)){
			Map<String, String> paramuser=new HashMap<>();
			paramuser.put("roomId", selectUser.getMatchGroup());
			paramuser.put("userName", selectUser.getUserName());
			String resultlist = enterGroupMapper.selectRoomInfo(paramuser);
			if (resultlist != null && !"".equals(resultlist)) {
				return CommonResult.build(2, "已发送入群邀请");
			}
		}
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(MatchGroupStrategy.WYETH_PHARMACY_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", receiveinfomatch.getType());// 卡券入群标识
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			enterGroupMapper.insertUserInfoParam(insert);
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		
		List<MatchGroup> receives = enterGroupMapper.getMatchGroupByUserInfo(matchGroupResult.getMatchGroupId(), receiveinfomatch.getType());
		if (receives.size() == 0) {
			enterGroupMapper.insertUserInfoParam(insert);
			return CommonResult.build(-2, "没匹配到机器人信息");
		}
		MatchGroup receive = receives.get(new Random().nextInt(receives.size()));	

		insert.put("sendGroup", receive.getRobotID());
		enterGroupMapper.insertUserInfoParam(insert);
		return CommonResult.ok(receive.getRobotID());
	}

	@Override
	public CommonResult loveBabyMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> param = new HashMap<>();
		if(null != receiveinfomatch.getCity()){
			param.put("city", receiveinfomatch.getCity().trim());
		}else{
			param.put("city", receiveinfomatch.getCity());
		}
		param.put("hospital", receiveinfomatch.getHospital());
		param.put("edc", receiveinfomatch.getEdc());
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(MatchGroupStrategy.LOVE_BABY_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", receiveinfomatch.getType());// 爱婴岛入群标识/4为爱婴岛41为爱婴通
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(insert);
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		GetRobotIdByU getRobotIdByU = enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveinfomatch.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		String resultByPython = HttpClientUtil.doPost(propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		logger.info("接收python的值" + resultByPython);
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);

		Map<String, Object> getDataInfo = (Map<String, Object>) robotInfo.get("data");
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		insert.put("sendGroup", null);
		insertUserInfo(insert);
		Map<String, Object> result = new HashMap<>();
		// result.put("robotId", receive.getRobotID());
		result.put("robotQr", getDataInfo.get("qr_url")); // 二维码
		result.put("code", getDataInfo.get("verify_code"));// 验证码
		RedisUtil.set(receiveinfomatch.getOpenid(), getDataInfo.get("qr_url").toString(), 10 * 60);
		RedisUtil.set(receiveinfomatch.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 10 * 60);
		logger.info("机器人二维码url : " +getDataInfo.get("qr_url").toString());
		logger.info("验证码  : " +getDataInfo.get("verify_code").toString());
		sendMsg(receiveinfomatch.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	/**
	 * 插入userinfo信息
	 * 
	 * @param param
	 */
	public void insertUserInfo(Map<String, Object> param) {
		enterGroupMapper.insertUserInfo(param);
	}

	@Override
	public CommonResult fixedMatchGroup(UserFriendEnterGroup paramUserFriendEnterGroup) {
		Map<String, Object> param = new HashMap();
		param.put("receiveinfo", paramUserFriendEnterGroup);
		param.put("matchStatus", "已匹配");
		param.put("matchGroup", "");
		param.put("sendGroup", "");
		param.put("type", "1");
		insertUserInfo(param);
		return CommonResult.ok("");
	}

	@Override
	public CommonResult selectRobotUrl(UserFriendEnterGroup receiveinfomatch) {
		Map<String, Object> param = new HashMap<>();
		Map<String, String> paramuser = new HashMap<>();
		param.put("openid", receiveinfomatch.getOpenid());
		if (receiveinfomatch.getType() == null) {
			param.put("type", 1);
		} else {
			param.put("type", receiveinfomatch.getType());
		}
		List<Userinfo> info = enterGroupMapper.selectUser(param);
		logger.info("Userinfo里面的信息" + info.size());
		if (info.size() > 0) {
			GetRobotIdByU getRobotIdByU = enterGroupMapper.matchGroupByLoveBaby(info.get(0).getMatchGroup());
			List<SelectUser> selectuser = enterGroupMapper.selectStatus(receiveinfomatch.getOpenid(),
					receiveinfomatch.getType());// 获取返回的入群状态
			logger.info("Userstatus里面的信息" + selectuser.size());
			Map<String, String> redisInfo = new HashMap<>();
			if (selectuser != null && selectuser.size() > 0) {
				paramuser.put("roomId", selectuser.get(0).getMatchGroup());
				paramuser.put("userName", selectuser.get(0).getUserName());
				String resultlist = enterGroupMapper.selectRoomInfo(paramuser);
				if (resultlist != null && !"".equals(resultlist)) {
					return CommonResult.build(2, "已发送入群邀请");
				}
				String url = RedisUtil.get(receiveinfomatch.getOpenid());
				String code = RedisUtil.get(receiveinfomatch.getOpenid() + "type");
				if(null != getRobotIdByU){
					if (("".equals(url) || null == url) || ("".equals(code) || null == code)) {
						return getRobotUrl(getRobotIdByU.getTaskId(), getRobotIdByU.getuRoomId(),
								receiveinfomatch.getOpenid());
					}
				}
				redisInfo.put("qr_url", url);
				redisInfo.put("verify_code", code);
				return CommonResult.ok(redisInfo);
			}
			logger.info("Userinfo里面的群状态" + info.get(0).getMatchGroupStatus());
			if ("已匹配".equals(info.get(0).getMatchGroupStatus())) {
				String url = RedisUtil.get(receiveinfomatch.getOpenid());
				String code = RedisUtil.get(receiveinfomatch.getOpenid() + "type");
				logger.info("redis里面的信息" + url + "验证码" + code);
				if (("".equals(url) || null == url) || ("".equals(code) || null == code)) {
					return getRobotUrl(getRobotIdByU.getTaskId(), getRobotIdByU.getuRoomId(),
							receiveinfomatch.getOpenid());
				}
				redisInfo.put("qr_url", url);
				redisInfo.put("verify_code", code);
				return CommonResult.ok(redisInfo);
			}
			return CommonResult.build(-1, "请重新匹配");

		} else {
			return CommonResult.build(-1, "请重新匹配");
		}
	}

	private CommonResult getRobotUrl(String taskId, String uRoomId, String openid) {
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", taskId);
		pythonInfo.put("chat_room_id", uRoomId);
		pythonInfo.put("open_id", openid);
		pythonInfo.put("group_id", "");
		pythonInfo.put("group_name", "");
		logger.info("用户重复扫码调用U创接口之前的时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		String resultByPython = HttpClientUtil.doPost(propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		logger.info("用户重复扫码调用U创接口之后的时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		logger.info("返回机器人的信息" + JSON.parse(resultByPython));
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		Map<String, String> data = (Map<String, String>) robotInfo.get("data");
		//将请求由创的二维码和验证码缓存入redis十分钟
		RedisUtil.set(openid, data.get("qr_url").toString(), 600);
		RedisUtil.set(openid + "type", data.get("verify_code").toString(), 600);
		return CommonResult.ok(data);
	}

	@Override
	public CommonResult selectHospitalInfo(String code, String type) {
		String hospitalName = enterGroupMapper.selectHospitalInfo(code, type);
		if (!"".equals(hospitalName) && null != hospitalName) {
			return CommonResult.ok(hospitalName);
		}
		return CommonResult.build(-1, "获取信息失败");
	}


	@Override
	public CommonResult leYinMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> param = new HashMap<>();
		param.put("edc", receiveinfomatch.getEdc());
		param.put("area", receiveinfomatch.getArea());
		receiveinfomatch.setNickname(EmojiUtil.handleEmoji(receiveinfomatch.getNickname()));
		
		MatchGroupResult matchGroupResult = this.matchGroupContext
				.matchGroup(MatchGroupContext.MatchGroupStrategy.LE_YIN_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", "6");
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(insert);
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		GetRobotIdByU getRobotIdByU = this.enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveinfomatch.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		logger.info("用户首次扫码调用U创接口之前的时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		String resultByPython = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		logger.info("用户首次扫码调用U创接口之后的时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		this.logger.info("接受python的值" + JSON.parse(resultByPython));
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		Map<String, Object> getDataInfo = (Map) robotInfo.get("data");
		insert.put("sendGroup", null);
		insertUserInfo(insert);
		Map<String, Object> result = new HashMap<>();
		result.put("robotQr", getDataInfo.get("qr_url"));
		result.put("code", getDataInfo.get("verify_code"));
		RedisUtil.set(receiveinfomatch.getOpenid(), getDataInfo.get("qr_url").toString(), 600);
		RedisUtil.set(receiveinfomatch.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 600);
		sendMsg(receiveinfomatch.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	@Override
	public CommonResult selectLyHospitalInfo(String code, String level) {
		List<GetAreaHosNameByCode> selectLyHospitalInfo = enterGroupMapper.selectLyHospitalInfo(code, level);
		if(null != selectLyHospitalInfo && selectLyHospitalInfo.size() > 0){
			Map<String, Object> result = new HashMap<>();
			result.put("hospitalName", selectLyHospitalInfo.get(0).getHospitalName());
			result.put("areaName", selectLyHospitalInfo.get(0).getAreaName());
			if(null != result && result.size() > 0){
				return CommonResult.ok(result);
			}
		}
		return CommonResult.build(-1, "获取信息失败");
	}

	@Override
	public CommonResult yiBeiMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		Map<String, String> param = new HashMap<>();
		receiveinfomatch.setNickname(EmojiUtil.handleEmoji(receiveinfomatch.getNickname()));
		
		MatchGroupResult matchGroupResult = this.matchGroupContext
				.matchGroup(MatchGroupContext.MatchGroupStrategy.YI_BEI_MATCHGROUP_STRATEGY, param);
		Map<String, Object> insert = new HashMap<>();
		insert.put("receiveinfo", receiveinfomatch);
		insert.put("type", "7");
		if (matchGroupResult.getResultCode() == -1) {
			insert.put("matchStatus", "未匹配");
			insert.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(insert);
			return CommonResult.build(Integer.valueOf(matchGroupResult.getResultCode()), matchGroupResult.getMsg());
		}
		insert.put("matchStatus", "已匹配");
		insert.put("matchGroup", matchGroupResult.getMatchGroupId());
		GetRobotIdByU getRobotIdByU = this.enterGroupMapper.matchGroupByLoveBaby(matchGroupResult.getMatchGroupId());
		// 设置u创接口需要的参数获取u创机器人二维码
		Map<String, String> pythonInfo = new HashMap<>();
		pythonInfo.put("task_id", getRobotIdByU.getTaskId());
		pythonInfo.put("chat_room_id", getRobotIdByU.getuRoomId());
		pythonInfo.put("open_id", receiveinfomatch.getOpenid());
		pythonInfo.put("group_id", matchGroupResult.getMatchGroupId());
		pythonInfo.put("group_name", getRobotIdByU.getRoomName());
		logger.info("用户首次扫码调用U创接口之前的时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		String resultByPython = HttpClientUtil.doPost(this.propertyConfigurer.getProperty("GET_ROBOT_QR"), pythonInfo);
		logger.info("用户首次扫码调用U创接口之后的时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS").format(System.currentTimeMillis()));
		if ("".equals(resultByPython) || null == resultByPython) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		this.logger.info("接受python的值" + JSON.parse(resultByPython));
		Map<String, Object> robotInfo = JSON.parseObject(resultByPython);
		if ((int) robotInfo.get("code") == 1) {
			return CommonResult.build(-1, "获取机器人二维码失败");
		}
		Map<String, Object> getDataInfo = (Map) robotInfo.get("data");
		insert.put("sendGroup", null);
		insertUserInfo(insert);
		Map<String, Object> result = new HashMap<>();
		result.put("robotQr", getDataInfo.get("qr_url"));
		result.put("code", getDataInfo.get("verify_code"));
		RedisUtil.set(receiveinfomatch.getOpenid(), getDataInfo.get("qr_url").toString(), 600);
		RedisUtil.set(receiveinfomatch.getOpenid() + "type", getDataInfo.get("verify_code").toString(), 600);
		sendMsg(receiveinfomatch.getPhone(), String.valueOf(getDataInfo.get("verify_code")));
		return CommonResult.ok(result);
	}

	@Override
	public CommonResult cooperationMatchGroup(UserFriendEnterGroup receiveinfomatch) {
		return wyethMatchGroup(receiveinfomatch,MatchGroupStrategy.COOPERATION_MATCHGROUP_STRATEGY);
	}
	
	/**
	 * 关于惠氏卡券入群的逻辑共用部分
	 * @param receiveinfomatch
	 * @param menuType
	 * @return
	 */
	public CommonResult wyethMatchGroup(UserFriendEnterGroup receiveinfomatch,MatchGroupStrategy menuType){
		Map<String, String> receiveinfo = new HashMap<>();
		receiveinfo.put("phone", receiveinfomatch.getPhone());
		receiveinfo.put("city", receiveinfomatch.getCity().trim());
		receiveinfo.put("openid", receiveinfomatch.getOpenid());
		receiveinfo.put("hospital", receiveinfomatch.getHospital());
		receiveinfo.put("edc", receiveinfomatch.getEdc());
		receiveinfo.put("province", receiveinfomatch.getProvince());
		SelectUser selectUser=enterGroupMapper.selectRepeatInfo(receiveinfomatch.getPhone(), receiveinfomatch.getType());
		if(selectUser!=null && !"".equals(selectUser)){
			Map<String, String> paramuser=new HashMap<>();
			paramuser.put("roomId", selectUser.getMatchGroup());
			paramuser.put("userName", selectUser.getUserName());
			String resultlist = enterGroupMapper.selectRoomInfo(paramuser);
			if (resultlist != null && !"".equals(resultlist)) {
				return CommonResult.build(2, "已发送入群邀请");
			}
		}
		MatchGroupResult matchGroupResult = matchGroupContext
				.matchGroup(menuType, receiveinfo);
		Map<String, Object> param = new HashMap<String, Object>();

		receiveinfomatch.setNickname(EmojiUtil.handleEmoji(receiveinfomatch.getNickname()));
		param.put("type", "1");// 卡券入群标识
		param.put("receiveinfo", receiveinfomatch);
		if (matchGroupResult.getResultCode() == -1) {
			param.put("matchStatus", "未匹配");
			param.put("sendGroup", new MatchGroup().getRobotID());
			insertUserInfo(param);
			return CommonResult.build(matchGroupResult.getResultCode(), matchGroupResult.getMsg());
		}
		if (matchGroupResult.getResultCode() == 1) {
			param.put("matchStatus", "已匹配");
		}

		if (matchGroupResult.getResultCode() == 2) {
			param.put("matchStatus", "已匹配城市群");
		}
		if (matchGroupResult.getResultCode() == 3) {
			param.put("matchStatus", "已匹配全国群");
		}
		param.put("matchGroup", matchGroupResult.getMatchGroupId());
		List<MatchGroup> receives = enterGroupMapper.getMatchGroupByUserInfo(matchGroupResult.getMatchGroupId(), "1");
		if (receives.size() == 0) {
			insertUserInfo(param);
			return CommonResult.build(-2, "没匹配到机器人信息");
		}
		MatchGroup receive = receives.get(new Random().nextInt(receives.size()));
		param.put("sendGroup", receive.getRobotID());
		insertUserInfo(param);
		return CommonResult.ok(receive.getRobotID());
	}




	
	
}
