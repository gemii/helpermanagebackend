package cc.gemii.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.mapper.WechatuserentergroupstatusCustomMapper;
import cc.gemii.mapper.WechatuserentergroupstatusMapper;
import cc.gemii.po.Userstatus;
import cc.gemii.po.Wechatuserentergroupstatus;
import cc.gemii.po.WechatuserentergroupstatusWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SelectMessage;
import cc.gemii.pojo.SelectMessageAB;
import cc.gemii.pojo.SelectMessageAResult;
import cc.gemii.pojo.SelectMessageBResult;
import cc.gemii.service.MessageService;
import cc.gemii.util.ComputePages;
import cc.gemii.util.ConfigUtil;
import cc.gemii.util.TimeUtils;
import net.sf.json.JSONObject;

@Service
public class MessageServiceImpl implements MessageService {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private WechatuserentergroupstatusCustomMapper uEnterStatusCustomMapper;
	
	@Autowired
	private WechatuserentergroupstatusMapper uEnterStatusMapper;
	
	@Autowired
	private UserstatusMapper userStatusMapper;
	
	@Override
	public CommonResult selectMessage(SelectMessage selectMessage) {
		if(selectMessage.getPage() == null || selectMessage.getPageSize() == null 
				|| "".equals(selectMessage.getPage()) || "".equals(selectMessage.getPageSize())){
			return CommonResult.ok();
		}
		selectMessage.setStatus("%" + selectMessage.getStatus() + "%");
		selectMessage.setUpMessage("%" + selectMessage.getUpMessage() + "%");
		selectMessage.setUsername("%" + selectMessage.getUsername() + "%");
		selectMessage.setStart((selectMessage.getPage() - 1) * selectMessage.getPageSize()); 
		List<Wechatuserentergroupstatus> list = uEnterStatusCustomMapper.selectMessage(selectMessage);
		int total = uEnterStatusCustomMapper.selectMessageCount(selectMessage);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", list);
		map.put("total", total);
		map.put("count", ComputePages.computePages(total, selectMessage.getPageSize()));
		return CommonResult.ok(map);
	}
	
	@Override
	public CommonResult selectMessage2(SelectMessageAB selectMessage) {
		if(selectMessage.getPage() == null || selectMessage.getPageSize() == null 
				|| "".equals(selectMessage.getPage()) || "".equals(selectMessage.getPageSize())){
			return CommonResult.ok();
		}
		selectMessage.setRoomName("%" + selectMessage.getRoomName() + "%");
		selectMessage.setUsername("%" + selectMessage.getUsername() + "%");
		selectMessage.setUrlStatus("%" + selectMessage.getUrlStatus() + "%");
		selectMessage.setEnterRoomStatus("%" + selectMessage.getEnterRoomStatus() + "%");
		selectMessage.setCode("%" + selectMessage.getCode() + "%");
		selectMessage.setStart((selectMessage.getPage() - 1) * selectMessage.getPageSize());
		List<SelectMessageAResult> list = uEnterStatusCustomMapper.selectMessageA(selectMessage);
		Map<String, Object> response = new HashMap<String, Object>();
		
		int total = uEnterStatusCustomMapper.selectMessageACount(selectMessage);
		response.put("total", total);
		response.put("count", ComputePages.computePages(total, selectMessage.getPageSize()));
		response.put("messages", list);
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult selectMessage3(SelectMessageAB selectMessageAB) {
		if(selectMessageAB.getPage() == null || selectMessageAB.getPageSize() == null 
				|| "".equals(selectMessageAB.getPage()) || "".equals(selectMessageAB.getPageSize())){
			return CommonResult.ok();
		}
		selectMessageAB.setRoomName("%" + selectMessageAB.getRoomName() + "%");
		selectMessageAB.setUsername("%" + selectMessageAB.getUsername() + "%");
		selectMessageAB.setUrlStatus("%" + selectMessageAB.getUrlStatus() + "%");
		selectMessageAB.setEnterRoomStatus("%" + selectMessageAB.getEnterRoomStatus() + "%");
		selectMessageAB.setStart((selectMessageAB.getPage() - 1) * selectMessageAB.getPageSize());
		List<SelectMessageBResult> list = uEnterStatusCustomMapper.selectMessageB(selectMessageAB);
		Map<String, Object> response = new HashMap<String, Object>();
		
		int total = uEnterStatusCustomMapper.selectMessageBCount(selectMessageAB);
		response.put("total", total);
		response.put("count", ComputePages.computePages(total, selectMessageAB.getPageSize()));
		response.put("messages", list);
		return CommonResult.ok(response);
	}

	@Override
	public CommonResult modifyMessage2(Userstatus userstatus) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		userstatus.setGrouptime(sdf.format(new Date()));
		userStatusMapper.updateByPrimaryKeySelective(userstatus);
		return CommonResult.ok();
	}

	@Override
	public CommonResult insertMessage(WechatuserentergroupstatusWithBLOBs enterStatus) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		enterStatus.setGrouptime(sdf.format(new Date()));
		enterStatus.setConfirmstatus(ConfigUtil.CONFIRM_STATUS);
		uEnterStatusMapper.insert(enterStatus);
		return CommonResult.ok();
	}

	@Override
	public CommonResult modifyMessage(WechatuserentergroupstatusWithBLOBs enterStatus) {
		WechatuserentergroupstatusWithBLOBs upKeyword = uEnterStatusMapper.selectByPrimaryKey(enterStatus.getId());
		Map<String, String> param = new HashMap<String, String>();
		param.put("robotID", upKeyword.getRobotid());
		param.put("nickname", upKeyword.getUsername());
		param.put("time", TimeUtils.getTime("yyyy-MM-dd HH:mm:ss"));
		param.put("messagestatus", enterStatus.getMessagestatus());
		logger.info("modifyMessage:"+JSONObject.fromObject(param).toString());
		uEnterStatusCustomMapper.updateMessageStatus(param);
		return CommonResult.ok();
	}

}
