package cc.gemii.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.otd.UserInfoBase;
import cc.gemii.data.otd.UserInfoTroubledMum;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.mapper.UserinfoMapper;
import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.po.UserInfoPO;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.service.MatchGroupService;

@Service
public class MatchGroupServiceImpl implements MatchGroupService {

	private Logger log = Logger.getLogger(this.getClass());
	
	private final static String QRCODE_URL_PREFIX = "http://nfs.gemii.cc/accounts/qrcode/";
	private final static String IMG_SUFFIX = ".jpg";
	@Autowired
	private Properties propertyConfigurer;
	@Autowired
	private MatchGroupContext matchGroupContext;
	@Autowired
	private UserstatusMapper userstatusMapper;
	@Autowired
	private UserinfoMapper userinfoMapper;
	@Autowired
	private SendMsgContext sendMsgContext;
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Override
	public GeneralContentResult<Map<String, String>> matchByUserInfo(UserInfoBase userInfoBase,
			MatchGroupStrategy matchGroupStrategy, String source) {
		log.info("接收到参数：userInfoBase="+JSONObject.toJSONString(userInfoBase)+"-matchGroupStrategy="+matchGroupStrategy+"-source="+source);
		GeneralContentResult<Map<String,String>> result = new GeneralContentResult<Map<String,String>>();
		try {
			GeneralContentResult<Map<String,String>> matchResult = matchGroup(userInfoBase,matchGroupStrategy);
			String matchStatus = matchResult.getResultContent().get("matchStatus");
			String matchGroupId = matchResult.getResultContent().get("groupId");
			if(!ResultCode.OPERATION_SUCCESS.equals(matchResult.getResultCode())){
				saveUserInfo(userInfoBase,matchStatus,source,null);
				log.info("返回参数：matchResult="+JSONObject.toJSONString(matchResult));
				return matchResult;
			}
			MatchGroup matchGroup = new MatchGroup();
			matchGroup.setRoomID(matchGroupId);
			GeneralContentResult<Map<String,String>> robotResult = matchRobotByGroup(matchGroupId,matchGroupStrategy,source);
			if(!ResultCode.OPERATION_SUCCESS.equals(robotResult.getResultCode())){
				saveUserInfo(userInfoBase,matchStatus,source,matchGroup);
				log.info("返回参数：matchResult="+JSONObject.toJSONString(matchResult));
				return robotResult;
			}
			String robotId = robotResult.getResultContent().get("robotId");
			String qrCode = QRCODE_URL_PREFIX+robotId+IMG_SUFFIX;
			String vCode = userInfoBase.getPhone().substring(userInfoBase.getPhone().length()-4,userInfoBase.getPhone().length());
			matchGroup.setRobotID(robotId);
			saveUserInfo(userInfoBase,matchStatus,source,matchGroup);
			Map<String,String> data = new HashMap<String,String>();
			data.put("robotId",robotId);
			data.put("vCode",vCode);
			data.put("qrCode",qrCode);
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			result.setDetailDescription("匹配成功");
			result.setResultContent(data);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("系统异常");
		}
		log.info("返回参数：result="+JSONObject.toJSONString(result));
		return result;
	}
	
	private GeneralContentResult<Map<String, String>> matchRobotByGroup(String matchGroupId, MatchGroupStrategy matchGroupStrategy, String source) {
		GeneralContentResult<Map<String, String>> result = new GeneralContentResult<Map<String, String>>();
		Map<String, String> resultData = new HashMap<String, String>();
		//"1" 表示匹配 惠氏的福利小助手
		String robotType = "1";
		if(matchGroupStrategy == MatchGroupStrategy.GOOD_PARENT_MATCHGROUP_STRATEGY){
			robotType = "2";
		}
		List<MatchGroup> receives = enterGroupMapper.getMatchGroupByUserInfo(matchGroupId,robotType);
		if (receives == null || receives.size() == 0) {
			result.setResultCode(ResultCode.ENTER_GROUP_NOT_MATCH_ROBOT);
			result.setDetailDescription("未找到合适的机器人");
			return result;
		}
		MatchGroup receive = receives.get(new Random().nextInt(receives.size()));
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		resultData.put("robotId",receive.getRobotID());
		result.setResultContent(resultData);
		return result;
	}

	private GeneralContentResult<Map<String, String>> matchGroup(UserInfoBase userInfoBase,MatchGroupStrategy matchGroupStrategy){
		GeneralContentResult<Map<String, String>> result = new GeneralContentResult<Map<String, String>>();
		Map<String, String> resultData  = new HashMap<String, String>();
		Map<String, String> matchReqMap = new HashMap<>();
		matchReqMap.put("phone", userInfoBase.getPhone());
		matchReqMap.put("city", userInfoBase.getCity()==null?null:userInfoBase.getCity().trim());
		matchReqMap.put("openid", userInfoBase.getOpenid());
		matchReqMap.put("hospital", userInfoBase.getStore());
		matchReqMap.put("edc", userInfoBase.getEdc());
		matchReqMap.put("province", userInfoBase.getProvince());
		MatchGroupResult matchGroupResult = matchGroupContext.matchGroup(matchGroupStrategy, matchReqMap);
		String matchStatus = "未匹配";
		if (matchGroupResult.getResultCode() == 1) {
			matchStatus = "已匹配";
		}
		if (matchGroupResult.getResultCode() == 2) {
			matchStatus = "已匹配城市群";
		}
		if (matchGroupResult.getResultCode() == 3) {
			matchStatus = "已匹配全国群";
		}
		if (matchGroupResult.getResultCode() == -1) {
			result.setResultCode(ResultCode.ENTER_GROUP_NOT_MATCH_ROOM);
			result.setDetailDescription("未找到合适的群");
			return result;
		}
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		resultData.put("matchStatus",matchStatus);
		resultData.put("groupId",matchGroupResult.getMatchGroupId());
		result.setResultContent(resultData);
		return result;
	}

	private void saveUserInfo(UserInfoBase userInfoBase,String matchStatus,String source,MatchGroup matchGroup){
		UserInfoPO userInfoPO = new UserInfoPO();
		userInfoPO.setMatchStatus(matchStatus);
		userInfoPO.setPhoneNumber(userInfoBase.getPhone());
		userInfoPO.setCity(userInfoBase.getCity() == null?null:userInfoBase.getCity().trim());
		userInfoPO.setOpenId(userInfoBase.getOpenid());
		userInfoPO.setHospitalKeyword(userInfoBase.getStore());
		userInfoPO.setEdc(userInfoBase.getEdc());
		userInfoPO.setProvince(userInfoBase.getProvince());
		userInfoPO.setMatchGroup(matchGroup.getRoomID());
		userInfoPO.setMatchRobot(matchGroup.getRobotID());
		if(userInfoBase instanceof UserInfoTroubledMum){
			UserInfoTroubledMum userInfoTroubledMum = (UserInfoTroubledMum)userInfoBase;
			userInfoPO.setUserName(userInfoTroubledMum.getNickName());
			userInfoPO.setName(userInfoTroubledMum.getName());
			userInfoPO.setType(String.valueOf(userInfoTroubledMum.getChannel()));
		}
		userinfoMapper.insertUserInfo(userInfoPO);
	}
	
}
