package cc.gemii.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.RobotStatusMapper;
import cc.gemii.mapper.UserstatusMapper;
import cc.gemii.mapper.WechatclerkrobotMapper;
import cc.gemii.mapper.WechatrobotinfoMapper;
import cc.gemii.po.Userstatus;
import cc.gemii.po.WechatclerkrobotExample;
import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.po.WechatrobotinfoExample;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.RepeatRecord;
import cc.gemii.service.StatusService;
@Service
public class StatusServiceImpl implements StatusService {

	@Autowired
	private WechatrobotinfoMapper wriMapper;
	
	@Autowired
	private RobotStatusMapper robotStatusMapper;
	
	@Autowired
	private UserstatusMapper userStatusMapper;
	
	@Autowired
	private WechatclerkrobotMapper clerkRobotMapper;
	
	@Override
	public List<Wechatrobotinfo> getRobotStatus() {
		WechatrobotinfoExample example = new WechatrobotinfoExample();
		List<Wechatrobotinfo> list = wriMapper.selectByExampleWithBLOBs(example);
		return list;
	}

	@Override
	public CommonResult getRobot(Integer id) {
		//查找重复记录
		List<RepeatRecord> records = robotStatusMapper.getRepeatRecord(id);
		WechatclerkrobotExample crExample = null;
		List<Integer> ids = null;
		for (RepeatRecord record : records) {
			ids = robotStatusMapper.getRepeatID(record);
			if(ids.size() != 0){
				crExample = new WechatclerkrobotExample();
				crExample.createCriteria()
				.andIdIn(ids);
				clerkRobotMapper.deleteByExample(crExample);
			}
			
		}
		return CommonResult.ok(robotStatusMapper.getRobot(id));
	}

	@Override
	public CommonResult updateH5(Userstatus userstatus) {
		userStatusMapper.updateByPrimaryKeySelective(userstatus);
		return CommonResult.ok();
	}

}
