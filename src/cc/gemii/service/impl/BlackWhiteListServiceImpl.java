package cc.gemii.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.mapper.BlackWhiteListMapper;
import cc.gemii.po.BlackWhiteListDO;
import cc.gemii.po.BlackWhiteListDO.BlackWhiteListType;
import cc.gemii.service.BlackWhiteListService;

@Service
public class BlackWhiteListServiceImpl implements BlackWhiteListService{

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private BlackWhiteListMapper blackWhiteListMapper;
	
	@Override
	public BlackWhiteListType getUserType(String userKey, String funCode) {
		BlackWhiteListDO blackWhiteListDO = null;
		try {
			blackWhiteListDO = blackWhiteListMapper.selectEntityByUserKeyAndFunCode(userKey, funCode);
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
		}
		if(blackWhiteListDO == null){
			return BlackWhiteListType.UNKOWN;
		}
		return blackWhiteListDO.getType();
	}
	
}
