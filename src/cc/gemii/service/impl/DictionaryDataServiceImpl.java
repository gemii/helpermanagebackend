package cc.gemii.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cc.gemii.data.otd.DicNodeData;
import cc.gemii.mapper.WechatroominfoMapper;
import cc.gemii.service.DictionaryDataService;

@Service
public class DictionaryDataServiceImpl implements DictionaryDataService {

	@Autowired
	private WechatroominfoMapper wechatroominfoMapper;
	
	@Override
	public List<String> searchAllProvince() {
		return wechatroominfoMapper.selectProvince();
	}

	@Override
	public List<String> searchAllCityByProvince(String provinceName) {
		return wechatroominfoMapper.selectCity(provinceName);
	}

	@Override
	public List<String> searchAllHospitalByCity(String cityName,String type) {
		return wechatroominfoMapper.selectHospitalByCity(cityName,type);
	}

	@Override
	public List<String> searchAllCityByRegion(String regionName) {
		return wechatroominfoMapper.selectCityByArea(regionName);
	}
	
	@Override
	public Collection<DicNodeData> searchQRcodePageData(String areaType,String type) {
		Map<String,DicNodeData> result = new HashMap<String,DicNodeData> ();
		Map<String,String> param=new HashMap<>();
		param.put("areaType", areaType);
		param.put("type", type);
		List<Map<String,String>> rs = wechatroominfoMapper.selectQRcodePageData(param);
		for (Map<String, String> map : rs) {
			String province = map.get("province");
			if(result.get(province) == null){
				result.put(province,new DicNodeData(province));
			}
			DicNodeData dicNodeData = (DicNodeData)result.get(province);
			dicNodeData.getChild().add(new DicNodeData(map.get("city")));
		}
		List<DicNodeData> list = new ArrayList<>(result.values());
	    if ("1".equals(type) || "9".equals(type))
	    {
	      for (DicNodeData dicNodeData : list) {
	        dicNodeData.getChild().add(new DicNodeData("其他"));
	      }
	      DicNodeData otherNode = new DicNodeData("其他");
	      otherNode.getChild().add(new DicNodeData("其他"));
	      list.add(otherNode);
	    }
		return list;
	}

	@Override
	public List<String> selectPharmacyName(String cityName) {
		// TODO Auto-generated method stub
		return wechatroominfoMapper.selectPharmacyByCity(cityName);
	}

}
