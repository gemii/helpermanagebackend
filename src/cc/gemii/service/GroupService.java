package cc.gemii.service;

import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.CommonResult;

public interface GroupService {

	/**
	 * 
	 * @param page 页码
	 * @param pageSize 每页的大小
	 * @param groupName 群名
	 * @param clerkID 用户ID
	 * @return 根据页码和每页大小获取到的群信息
	 * TODO
	 */
	CommonResult getAllGroup(Integer page, Integer pageSize, String groupName, Integer clerkID);

	/**
	 * 
	 * @param groupName 群名称
	 * @return 根据群名获取指定群
	 * TODO
	 */
	CommonResult getGroupByName(String groupName);

	/**
	 * 
	 * @param roomInfo
	 * @return
	 * TODO 更新群信息
	 */
	CommonResult updateGroupInfo(Wechatroominfo roomInfo);

	/**
	 * 
	 * @param helper_id 小助手id 
	 * @param groupName 群名
	 * @param pageSize 页大小
	 * @param page 页码
	 * @param type 是否用户选择all
	 * @return
	 * TODO 根据小助手id获取此小助手所在群
	 */
	CommonResult getGroupByHelper(String helper_id, String groupName, Integer page, Integer pageSize, String type,String groupTag);

}
