package cc.gemii.service;

import cc.gemii.pojo.CommonResult;

public interface LoginService {

	/**
	 * 
	 * @param username 用户名
	 * @param password 密码
	 * @return
	 * TODO
	 */
	CommonResult login(String username, String password);

}
