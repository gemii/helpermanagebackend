package cc.gemii.service;

import java.util.Collection;
import java.util.List;

import cc.gemii.data.otd.DicNodeData;

public interface DictionaryDataService {

	public List<String> searchAllProvince();
	
	public List<String> searchAllCityByProvince(String provinceName);
	
	public List<String> searchAllHospitalByCity(String cityName,String type);

	public List<String> searchAllCityByRegion(String regionName);

	Collection<DicNodeData> searchQRcodePageData(String areaType,String type);
	
	
	List<String> selectPharmacyName(String cityName);
}
