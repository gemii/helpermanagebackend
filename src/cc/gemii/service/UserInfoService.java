package cc.gemii.service;

import com.github.pagehelper.PageInfo;

import cc.gemii.data.dto.UserInfoCardProfile;
import cc.gemii.data.otd.UserInfoCardVO;

public interface UserInfoService {


	public PageInfo<UserInfoCardVO> searchUserInfoByCardIn(int page, int size, UserInfoCardProfile searchInfo);
	
}
