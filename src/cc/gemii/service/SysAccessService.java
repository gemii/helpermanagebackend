package cc.gemii.service;

import java.util.List;

import cc.gemii.po.SysAccessDO;

public interface SysAccessService {
	
	public int insertSysAccess(SysAccessDO sysAccessDO);
	
	
	/*
	 * count operate
	 * */
	public List<String> countMatchTimesByMember(String startTime,String endTime);
	
	public List<String> countMatchTimesByGroup(String startTime,String endTime);

}
