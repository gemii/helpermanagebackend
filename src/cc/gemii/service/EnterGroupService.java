package cc.gemii.service;

import java.util.Map;

import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;

public interface EnterGroupService {

	/**
	 * 
	 * @param robotID 机器人id，如果为空，则返回所有城市
	 * @return
	 * TODO
	 */
	CommonResult getCity(String robotID);

	/**
	 * 
	 * @return 取得所有城市
	 * TODO
	 */
	CommonResult getAllCity();

	/**
	 * 
	 * @param city 城市 
	 * @param robotID 机器人id
	 * @return
	 * TODO 根据城市来获取医院
	 */
	CommonResult getHosByCity(String city, String robotID);

	/**
	 * 
	 * @param user 用户信息包装类
	 * @param robotID 机器人id
	 * @param code 验证码
	 * @param uid 用户临时ID 
	 * @return
	 * TODO 用户提交信息
	 */
	CommonResult submitUserInfo(Userinfo user, String robotID, String code, String uid);

	/**
	 * 
	 * @param phone 手机号
	 * @param code 验证码
	 * @return
	 * TODO
	 */
	CommonResult getCode(String phone, String code);

	/**
	 * 
	 * @param uid 用户临时ID
	 * @param robotID 机器人ID 
	 * @param code 验证码
	 * @return
	 * TODO
	 */
	CommonResult modifyH5Status(String uid, String code, String robotID);

	/**
	 * 
	 * @param robotID 小助手ID
	 * @return
	 * TODO
	 */
	CommonResult getCityAndHost(String robotID);

	CommonResult createMember(UserFriendEnterGroup memberCardInfo);

	/**
	 * 
	 * @param memberReq 创建会员请求包装类
	 * @return
	 * TODO 好奇会员入惠氏群流程
	 */
	CommonResult createHuggiesMember(UserFriendEnterGroup memberReq);
	
	
	/**
	 * 接收信息入群
	 * @param receiveinfomatch
	 * @return
	 */
	CommonResult receiveInfoMatchGroup(UserFriendEnterGroup receiveinfomatch);
	
	/**
	 * 项目合作群卡券入群
	 * @param receiveinfomatch
	 * @return
	 */
	CommonResult cooperationMatchGroup(UserFriendEnterGroup receiveinfomatch);
	
	
	CommonResult fixedMatchGroup(UserFriendEnterGroup paramUserFriendEnterGroup);
	
	/**
	 * 根据openid查询robotid
	 * @return
	 */
	CommonResult selectRobotId(UserFriendEnterGroup receiveinfomatch);
	
	/**
	 * 根据openid查询roboturl
	 * @return
	 */
	CommonResult selectRobotUrl(UserFriendEnterGroup receiveinfomatch);
	
	
	/**
	 * 好育儿入群
	 * @param receiveinfomatch
	 * @return
	 */
	CommonResult goodParentMatchGroup(UserFriendEnterGroup receiveinfomatch);
	
	
	/**
	 * 药店入群
	 * @param receiveinfimatch
	 * @return
	 */
	CommonResult pharmacyMatchGroup(UserFriendEnterGroup receiveinfomatch);
	
	/**
	 * 爱婴岛入群
	 * @return
	 */
	CommonResult loveBabyMatchGroup(UserFriendEnterGroup receiveinfomatch);
	
	/**
	 * 根据院code查询院信息
	 * @param code
	 * @return
	 */
	CommonResult selectHospitalInfo(String code,String type);

	/**
	 * 乐茵入群
	 * @return
	 */
	CommonResult leYinMatchGroup(UserFriendEnterGroup receiveInfo);

	/**
	 * 根据门店code查询门店信息
	 * @param code
	 * @return
	 */
	CommonResult selectLyHospitalInfo(String code, String level);

	/**
	 * 亿贝入群
	 * @return
	 */
	CommonResult yiBeiMatchGroup(UserFriendEnterGroup receiveInfo);

	
//	/**
//	 * 惠氏匹配合作群，自建群，网红群
//	 * @param receiveinfomatch
//	 * @return
//	 */
//	CommonResult wyethThreeMatch(UserFriendEnterGroup receiveinfomatch);
	
	public Map<String, Object> matchGroup(Map<String, String> param);
	
}
