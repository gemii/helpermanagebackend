package cc.gemii.service;

import cc.gemii.po.Wechatkeyword;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SelectKeyword;


public interface KeywordService {

	/**
	 * 
	 * @param i 群id
	 * @return
	 * TODO
	 */
	CommonResult getKeywordByGid(String i);

	/**
	 * 
	 * @param keyword 关键字包装类
	 * @return
	 * TODO 插入新的关键字
	 */
	CommonResult insertKeyword(Wechatkeyword keyword);

	/**
	 * 
	 * @param keyword 关键字包装类
	 * @return
	 * TODO 更新关键字
	 */
	CommonResult updateKeyword(Wechatkeyword keyword);

	/**
	 * 
	 * @param selectKeyword 搜索条件包装类
	 * @return
	 * TODO 根据条件搜索群
	 */
	CommonResult selectKeyword(SelectKeyword selectKeyword);

}
