package cc.gemii.service;

import cc.gemii.po.Wechatclerk;
import cc.gemii.po.Wechatclerkrobot;
import cc.gemii.pojo.CommonResult;

public interface ClerkService {

	/**
	 * 
	 * @param username 用户名
	 * @return
	 * TODO 判断用户名是否存在
	 */
	CommonResult judgeName(String username);

	/**
	 * 
	 * @param clerkRobot 用户机器人包装类
	 * @return
	 * TODO 用户添加机器人绑定
	 */
	CommonResult insertBind(Wechatclerkrobot clerkRobot);

	/**
	 * 
	 * @param clerk 用户信息包装类
	 * @return
	 * TODO 用户修改手机号
	 */
	CommonResult updatePhone(Wechatclerk clerk);

	/**
	 * 
	 * @param id 用户id
	 * @return
	 * TODO 用户添加小助手
	 */
	CommonResult addRobot(Integer id,String type);

	/**
	 * 
	 * @param clerkid 
	 * @param robotTag 机器人标签
	 * @return
	 * TODO 重启机器人
	 */
	CommonResult restartRobot(Integer clerkid, String robotTag,String type);

	/**
	 * 
	 * @param clerkrobot clerkid : 用户ID,robottag : 机器人标签
	 * @return
	 * TODO 用户取消机器人管理
	 */
	CommonResult deleteRobot(Wechatclerkrobot clerkrobot);
	
	CommonResult selectStatus(String robotTag);
}
