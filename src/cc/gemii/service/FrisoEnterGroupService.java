package cc.gemii.service;

import java.util.List;

import cc.gemii.data.otd.EnterGroupInfo;
import cc.gemii.data.otd.ErrorInfoToFriso;
import cc.gemii.data.otd.FrisoUserInfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;

public interface FrisoEnterGroupService {
	
	/**
	 * 美素佳儿入群
	 * @return
	 */
	CommonResult beautyGoodSonMatchGroup(UserFriendEnterGroup receiveInfo);

	/**
	 * 新美素佳儿入群
	 * @return
	 */
	CommonResult newBeautyGoodSonMatchGroup(UserFriendEnterGroup receiveInfo);

	/**
	 * 美素佳儿对接匹配群
	 * @return
	 */
	CommonResult matchGroupForFriso(FrisoUserInfo receiveInfo);

	/**
	 * 返还美素佳儿入群信息
	 * @return
	 */
	CommonResult returnEnterOrQuitGroupInfoToFriso(EnterGroupInfo enterGroupInfo);

	/**
	 * 返还美素佳儿退群信息
	 * @return
	 */
//	CommonResult returnQuitGroupInfoToFriso(EnterGroupInfo enterGroupInfo);

	/**
	 * 查询推送失败信息
	 */
	List<ErrorInfoToFriso> selectErrorInfo();

	/**
	 * 更新status为2
	 */
	void updateErrorInfo(List<ErrorInfoToFriso> selectErrorInfo);

	/**
	 * 查询失败信息条数
	 */
	int countErrorInfo();
	
}
