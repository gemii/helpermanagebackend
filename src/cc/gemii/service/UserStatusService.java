package cc.gemii.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserStatusCardVO;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;

public interface UserStatusService {

	public PageInfo<UserStatusCardVO> searchUserStatusByCardIn(int page, int size, UserStatusCardProfile searchInfo);

	public CommonResult matchuserinfo(int id, String phoneNumber);
	

}
