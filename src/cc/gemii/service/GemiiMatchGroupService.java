package cc.gemii.service;

import java.util.Collection;
import java.util.List;

import cc.gemii.data.otd.DicNodeData;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;

public interface GemiiMatchGroupService {

	/**
	 * 景栗匹配群
	 * @param receiveInfo
	 * @return
	 */
	CommonResult selectGemiiMatchGroup(UserFriendEnterGroup receiveInfo);
	
	/**
	 * 根据手机号查询userinfo里面的城市信息
	 * @param phone
	 * @return
	 */
	CommonResult selectUserInfo(String phone,String type);
	
	
	Collection<DicNodeData> searchProvinceByGemii(String type);
	
//	CommonResult selectRepeat(UserFriendEnterGroup receiveInfo);
	
}
