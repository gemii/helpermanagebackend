package cc.gemii.service;

import java.util.List;

import cc.gemii.po.Userstatus;
import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.pojo.CommonResult;

public interface StatusService {

	/**
	 * 
	 * @return
	 * TODO 取得所有robot状态
	 */
	List<Wechatrobotinfo> getRobotStatus();

	/**
	 * 
	 * @param id 用户id
	 * @return
	 * TODO 根据用户id获取对应的robot
	 */
	CommonResult getRobot(Integer id);

	/**
	 * 
	 * @param userstatus 手动发送H5包装类
	 * @return
	 * TODO
	 */
	CommonResult updateH5(Userstatus userstatus);
}
