package cc.gemii.service;

import java.util.Map;

import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.otd.UserInfoBase;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;

public interface WyethMatchGroupService {
	
	/**
	 * h5入群新增匹配规则
	 * @param user
	 * @param robotID
	 * @param code
	 * @param uid
	 * @param type
	 * @return
	 */
	CommonResult commitUserInfo(Userinfo user,String robotID,String code,String uid,String type);
	
	/**
	 * 根据机器人id查询机器人归属
	 * @param robotId
	 * @return
	 */
	CommonResult selectRobotOwner(String robotId);

}
