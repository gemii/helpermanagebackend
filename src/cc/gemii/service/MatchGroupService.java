package cc.gemii.service;

import java.util.Map;

import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.otd.UserInfoBase;

/**
 * 入群信息匹配群的核心服务
 * 逐渐使用该接口
 * */
public interface MatchGroupService {

	/**
	 * 根据UserInfoBase 及下相关匹配策略，
	 * 匹配机器人及群
	 *
	 *@param userInfoBase 用于匹配群的用户信息
	 *@param matchGroupStrategy 匹配策略的选择
	 *@param source 请求来源 1 页面，2 接口
	 *@return 匹配到的接口 包括小助手二维码的url和验证码
	 * */
	GeneralContentResult<Map<String, String>> matchByUserInfo(UserInfoBase userInfoBase,MatchGroupStrategy matchGroupStrategy, String source);
	
}
