package cc.gemii.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserInfoCustom;
import cc.gemii.service.HelperService;

@Controller
@RequestMapping("/helper")
public class HelperController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private HelperService helperService;
	
	@RequestMapping("/getAllHelper")
	@ResponseBody
	public CommonResult getAllHelper(HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return helperService.getAllHelper();
	}
	
	@RequestMapping("/getHosByCity")
	@ResponseBody
	public CommonResult geHosByCity(@RequestParam String province,@RequestParam String city,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return helperService.getHosByCity(province,city);
	}
	
	@RequestMapping("/insertUser")
	@ResponseBody
	public CommonResult insertUser(UserInfoCustom user,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return helperService.insertUser(user);
	}
	
	@RequestMapping("/getQrcode")
	public void getQrcode(@RequestParam String robotID,HttpServletResponse response){
		try {
			//设置header
			response.setHeader("Access-Control-Allow-Origin", "*");
		    response.setHeader("Pragma", "No-cache");   
		    response.setHeader("Cache-Control", "no-cache");   
		    response.setDateHeader("Expires", 0L);
			OutputStream os = response.getOutputStream();
			os.write(helperService.getQrcode(robotID));
			os.close();
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
		}
	}

}
