package cc.gemii.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.StatusService;

@Controller
public class PageController {

	@Autowired
	private StatusService robotStatusService;
	
	@RequestMapping("/toStatusPage")
	public String toStatusPage(Model model){
		List<Wechatrobotinfo> list = robotStatusService.getRobotStatus();
		model.addAttribute("list", list);
		return "robot/status";
	}
	
	@RequestMapping("/sendRobotStatus")
	@ResponseBody
	public CommonResult sendRobotStatus(@RequestParam String msg){
		//socket.sendMsg(msg);
		return CommonResult.ok();
	}
}
