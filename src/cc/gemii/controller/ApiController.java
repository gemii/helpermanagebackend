package cc.gemii.controller;

import java.io.File;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ApiService;
import cc.gemii.util.InterfaceUtils;

@Controller
@RequestMapping("/api")
public class ApiController {

	private Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private ApiService apiService;

	@RequestMapping("/message")
	@ResponseBody
	public CommonResult handleMessage(@RequestParam String msg) {
		return apiService.handleMessage(msg);
	}

	@RequestMapping("/downloadExcel")
	@ResponseBody
	public Object getH5Record(@RequestParam String start,
			@RequestParam(required=false) String end,
			HttpServletRequest request,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		logger.info("getH5Record--remoteId:"+request.getRemoteAddr());
		try {
//			if(!ParamUtil.getAcceptIP().containsKey(request.getRemoteAddr())){
//				InterfaceUtils res = new InterfaceUtils();
//				res.setError_code(-2);
//				res.setReason("forbidden");
//				return res;
//			}
			HttpHeaders headers = new HttpHeaders();  
		    headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		    String path = request.getSession().getServletContext().getRealPath("") + "file/";
		    Map<String, Object> result = apiService.getFile(start,end,path);
		    
		    headers.setContentDispositionFormData("attachment", URLEncoder.encode(String.valueOf(result.get("filename")),"UTF-8")); 
		    return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray((File) result.get("file")),  headers, HttpStatus.CREATED);
		} catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping("/H5Record")
	@ResponseBody
	public InterfaceUtils H5Record(@RequestParam String start,
			@RequestParam(required=false) String end,
			HttpServletRequest request,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
//		if(!ParamUtil.getAcceptIP().containsKey(request.getRemoteAddr())){
//			InterfaceUtils res = new InterfaceUtils();
//			res.setError_code(-2);
//			res.setReason("forbidden");
//			return res;
//		}
		return apiService.H5Record(start, end);
	}
	
}
