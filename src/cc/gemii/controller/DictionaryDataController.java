package cc.gemii.controller;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.data.otd.DicNodeData;
import cc.gemii.service.DictionaryDataService;

@Controller
public class DictionaryDataController {

	@Autowired
	private DictionaryDataService dictionaryDataService;

	@RequestMapping(value = "/dicdata/provinces", method = RequestMethod.GET)
	@ResponseBody
	public List<String> searchAllProvince(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return dictionaryDataService.searchAllProvince();
	}

	@RequestMapping(value = "/dicdata/province/{provinceName}/citys", method = RequestMethod.GET)
	@ResponseBody
	public List<String> searchAllCityByProvince(@PathVariable("provinceName") String provinceName,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if ("all".equals(provinceName)) {
			return dictionaryDataService.searchAllCityByProvince(null);
		} else {
			return dictionaryDataService.searchAllCityByProvince(provinceName);
		}
	}

	/*
	 * 返回数据格式为：["province-city","province-city","province-city"...]
	 */
	@RequestMapping(value = "/dicdata/region/{regionName}/citys", method = RequestMethod.GET)
	@ResponseBody
	public List<String> searchAllCityByRegion(@PathVariable("regionName") String regionName,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if ("all".equals(regionName)) {
			return dictionaryDataService.searchAllCityByRegion(null);
		} else {
			return dictionaryDataService.searchAllCityByRegion(regionName);
		}
	}

	@RequestMapping(value = "/dicdata/city/{cityName}/hospitals", method = RequestMethod.GET)
	@ResponseBody
	public List<String> searchAllHospitalByCity(@PathVariable("cityName") String cityName,
			@RequestParam("type") String type, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if ('1' == type.charAt(0)) {
			type = "1";
		}
		if("3".equals(type)){
			return dictionaryDataService.selectPharmacyName(cityName);
		}
		return dictionaryDataService.searchAllHospitalByCity(cityName, type);
	}

	@RequestMapping(value = "/dicdata/citys/QRcode", method = RequestMethod.GET)
	@ResponseBody
	public Collection<DicNodeData> searchQRcodeData(@RequestParam("areaType") String areaType,
			@RequestParam("type") String type, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if ('1' == type.charAt(0)) {
			type = "1";
		}
		return dictionaryDataService.searchQRcodePageData(areaType, type);
	}

}
