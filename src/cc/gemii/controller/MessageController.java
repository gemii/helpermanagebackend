package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Userstatus;
import cc.gemii.po.WechatuserentergroupstatusWithBLOBs;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SelectMessage;
import cc.gemii.pojo.SelectMessageAB;
import cc.gemii.service.MessageService;

@Controller
@RequestMapping("/message")
public class MessageController {

	@Autowired
	private MessageService messageService;
	
	@RequestMapping("/selectMessage")
	@ResponseBody
	public CommonResult selectMessage(SelectMessage selectMessage,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.selectMessage(selectMessage);
	}
	
	@RequestMapping("/selectMessage2")
	@ResponseBody
	public CommonResult selectMessage2(SelectMessageAB selectMessageAB,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.selectMessage2(selectMessageAB);
	}
	
	@RequestMapping("/selectMessage3")
	@ResponseBody
	public CommonResult selectMessage3(SelectMessageAB selectMessageAB,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.selectMessage3(selectMessageAB);
	}
	
	@RequestMapping("/modifyMessage2")
	@ResponseBody
	public CommonResult modifyMessage2(Userstatus userstatus,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.modifyMessage2(userstatus);
	}
	
	@RequestMapping("/insertMessage")
	@ResponseBody
	public CommonResult insertMessage(WechatuserentergroupstatusWithBLOBs enterStatus,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.insertMessage(enterStatus);
	}
	
	@RequestMapping("/modifyMessage")
	@ResponseBody
	public CommonResult modifyMessage(WechatuserentergroupstatusWithBLOBs enterStatus,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return messageService.modifyMessage(enterStatus);
	}
}
