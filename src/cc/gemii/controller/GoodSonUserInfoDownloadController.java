package cc.gemii.controller;

import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cc.gemii.pojo.GoodSonUserInfo;
import cc.gemii.service.GoodSonUserInfoDownloadService;
import cc.gemii.util.POIExportUtil;


@Controller
public class GoodSonUserInfoDownloadController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private GoodSonUserInfoDownloadService goodSonUserInfoDownloadService;
	
	@SuppressWarnings("resource")
	@RequestMapping(value="/goodSonUserInfo/download",method=RequestMethod.GET)
	public void goodSonUserInfo(HttpServletResponse response, 
			@RequestParam(value="startTime",required=true) String startTime,
			@RequestParam(value="endTime",required=true) String endTime) throws Exception{
		
		 List<GoodSonUserInfo> userInfoQueryList = goodSonUserInfoDownloadService.GoodSonUserInfoQuery(startTime, endTime);
		 
		 String[] headers = {"微信昵称","手机号码","群名称","群ID","省","市","门店名称"};
		 String title = "好育儿用户注册";
		 String filename = "好育儿用户注册信息"+startTime+"~"+endTime+".xls";
		 response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
		 POIExportUtil.exportExcel(title, headers, userInfoQueryList, response.getOutputStream());
		 
	}
	
}
