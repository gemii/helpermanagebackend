package cc.gemii.controller;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.util.FileUtil;

@Controller
public class RobotController {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	public static final String S3_DIR_ROBOT_IMG1 = "accounts/source_qrcode";
	public static final String S3_DIR_ROBOT_IMG2 = "accounts/qrcode";
	public static final String TEMP_IMAGE_PATH = "/var/helper/upload/qrcode/";
	public static final String IMAGE_SUFFIX = ".jpg";
	public static final String IMAGE_SUFFIXS = "#.png#.jpg";
	public static final int QRCODE_ORIGINAL_WEIGHT = 4000;
	public static final int QRCODE_ORIGINAL_HIGHT = 3889;
	public static final int UPPER_LEFT_CORNER_X = 800;
	public static final int UPPER_LEFT_CORNER_Y = 1350;
	public static final int QRCODE_HANDLED_WEIGHT = 430;
	public static final int QRCODE_HANDLED_HIGHT = 430;
	
	@RequestMapping(value = "/robot/qrcode/upload", method = RequestMethod.POST)
	@ResponseBody
	public GeneralContentResult<Map<String,Object>> uploadRobotQRcode(MultipartFile file,@RequestParam String robotId){
		log.info("接收到参数 file="+file.getOriginalFilename()+"-robotId="+robotId);
		GeneralContentResult<Map<String,Object>> result = new GeneralContentResult<Map<String,Object>>();
		try {
			//图片格式验证
			if(!checkImage(file)){
				log.info("uploadRobotQRcode 上传图片格式非法");
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("上传图片 格式非法！");
				return result;
			}
			//上传原图
			String originalFileurl = FileUtil.uploadFile(S3_DIR_ROBOT_IMG1, robotId+IMAGE_SUFFIX, file.getInputStream());
			if(StringUtils.isBlank(originalFileurl)){
				log.info("uploadRobotQRcode 上传原图片失败");
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("上传原图片失败！");
				return result;
			}
			//图片裁剪与压缩
			String localFileName = UUID.randomUUID().toString();
			String handleResult = cropAndResjzeQRCode(file.getInputStream(),TEMP_IMAGE_PATH,localFileName);
			if(!ResultCode.OPERATION_SUCCESS.equals(handleResult)){
				log.info("uploadRobotQRcode 图片处理失败");
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("图片处理失败！");
				return result;
			}
			//处理后图片上传
			File localFile = new File(TEMP_IMAGE_PATH+localFileName);
			FileInputStream localFis = new FileInputStream(localFile);
			String handledFileUrl = FileUtil.uploadFile(S3_DIR_ROBOT_IMG2, robotId+IMAGE_SUFFIX, localFis);
			if(StringUtils.isBlank(handledFileUrl)){
				log.info("uploadRobotQRcode 上传处理后图片失败");
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("上传处理后图片失败！");
				return result;
			}
			if(localFile.exists()){
				localFile.delete();
			}
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			result.setDetailDescription("上传图片成功！");
			Map<String,Object> data = new HashMap<String,Object>();
			data.put("fileName", robotId);
			data.put("originalFileurl", originalFileurl);
			data.put("handledFileUrl", handledFileUrl);
			result.setResultContent(data);
			return result;
		} catch (IOException e) {
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("上传图片失败！");
			return result;
		}
	}
	
	private boolean checkImage(MultipartFile file) throws IOException{
		String fileName = file.getOriginalFilename();
		String prefix = fileName.substring(fileName.lastIndexOf(".")+1);	
		if(!IMAGE_SUFFIXS.contains(prefix)){
			return false;
		}
		BufferedImage image = ImageIO.read(file.getInputStream());
		if(image.getHeight() != QRCODE_ORIGINAL_HIGHT){
			return false;
		}
		if(image.getWidth() != QRCODE_ORIGINAL_WEIGHT){
			return false;
		}
		return true;
	}
	/**
	 * 图片裁剪与压缩
	 * */
	private String cropAndResjzeQRCode(InputStream fin,String filePath,String fileName) throws FileNotFoundException{
		int xImage = QRCODE_ORIGINAL_WEIGHT;
		int yImage = QRCODE_ORIGINAL_HIGHT;
		int x = UPPER_LEFT_CORNER_X;
	    int y = UPPER_LEFT_CORNER_Y;
	    int wResultImage = QRCODE_HANDLED_WEIGHT;
	    int hResultImage = QRCODE_HANDLED_HIGHT;
	    
	    int w = xImage-2*x;
	    int h = yImage-y;
		String file = filePath+fileName;
		log.info("路径+  ："+file);
		FileOutputStream fout = new FileOutputStream(file);
		try {
			BufferedImage image = ImageIO.read(fin);
			BufferedImage out = image.getSubimage(x, y, w, h);
	        BufferedImage scaledBI = new BufferedImage(wResultImage, hResultImage, BufferedImage.TYPE_INT_RGB);
	        Graphics2D g = scaledBI.createGraphics();
	        g.setComposite(AlphaComposite.Src);
	        g.drawImage(out, 0, 0, wResultImage, hResultImage, null); 
	        g.dispose();
		    ImageIO.write(scaledBI, "jpg", fout);
		    fout.close();
		    return ResultCode.OPERATION_SUCCESS;
		} catch (IOException e) {
			e.printStackTrace();
		    return ResultCode.OPERATION_ERROR;
		}
	}
}
