package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Wechatclerk;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.LoginService;

@Controller
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/login")
	@ResponseBody
	public CommonResult login(@RequestParam String username,
			@RequestParam String password,
			HttpSession session,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		CommonResult result = loginService.login(username,password);
		if(result.getStatus() == 200){
			session.setAttribute("user", result.getData());
		}
		return result;
	}
	
	@RequestMapping("/isLogin")
	@ResponseBody
	public CommonResult isLogin(HttpSession session,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		Wechatclerk user = (Wechatclerk)session.getAttribute("user");
		if(user == null){
			return CommonResult.build(-1, "未登录");
		}else{
			return CommonResult.ok(user);
		}
	}
	
	@RequestMapping("/logout")
	@ResponseBody
	public CommonResult logout(HttpSession session,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		session.removeAttribute("user");
		return CommonResult.ok();
	}
}
