package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Wechatclerk;
import cc.gemii.po.Wechatclerkrobot;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.ClerkService;
import cc.gemii.util.TagUtil;

@Controller
@RequestMapping("/user")
public class ClerkController {

	@Autowired
	private ClerkService clerkService;
	
	@RequestMapping("/judgeName")
	@ResponseBody
	public CommonResult judgeName(@RequestParam String username,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.judgeName(username.trim());
	}
	
	@RequestMapping("/insertBind")
	@ResponseBody
	public CommonResult insertBind(Wechatclerkrobot clerkRobot,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.insertBind(clerkRobot);
	}
	
	@RequestMapping("/updatePhone")
	@ResponseBody
	public CommonResult updatePhone(Wechatclerk clerk,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.updatePhone(clerk);
	}
	
	@RequestMapping("/addRobot")
	@ResponseBody
	public CommonResult addRobot(@RequestParam Integer clerkid,@RequestParam String type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.addRobot(clerkid,type);
	}
	
	@RequestMapping("/restartRobot")
	@ResponseBody
	public CommonResult restartRobot(@RequestParam String robotTag,
			@RequestParam Integer clerkid,@RequestParam String type,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.restartRobot(clerkid, robotTag,type);
	}
	
	@RequestMapping("/deleteRobot")
	@ResponseBody
	public CommonResult deleteRobot(Wechatclerkrobot clerkrobot,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.deleteRobot(clerkrobot);
	}
	
	@RequestMapping("/getTag")
	@ResponseBody
	public CommonResult getTag(HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return CommonResult.ok(TagUtil.getTag());
	}
	
	@RequestMapping("/selectStatus")
	@ResponseBody
	public CommonResult selectStatus(@RequestParam String robotTag,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return clerkService.selectStatus(robotTag);
	}
}
