package cc.gemii.controller;

import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.SysAccessDO;
import cc.gemii.pojo.PagePointVO;
import cc.gemii.service.SysAccessService;
import net.sf.json.JSONObject;

@Controller
public class SysAccessController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private SysAccessService sysAccessService;

	@RequestMapping("/savepoint")
	public void savePoint(PagePointVO pagePoint,HttpServletRequest request){
		try {
			SysAccessDO sysAccessDO = buildSysAccessDO(pagePoint,request);
			sysAccessService.insertSysAccess(sysAccessDO);
		} catch (Exception e) {
			logger.error(JSONObject.fromObject(pagePoint).toString());
			logger.error(e.getMessage(),e);
		}
		
	}
	
	private SysAccessDO buildSysAccessDO(PagePointVO pagePoint,HttpServletRequest request){
		SysAccessDO sysAccessDO = new SysAccessDO();
		sysAccessDO.setContent(pagePoint.getContent());
		sysAccessDO.setPageName(pagePoint.getPageName());
		sysAccessDO.setPointName(pagePoint.getPointName());
		sysAccessDO.setUserKey(pagePoint.getUserKey());
		sysAccessDO.setIp(request.getRemoteAddr());
		sysAccessDO.setUserAgent(request.getHeader("User-Agent"));
		return sysAccessDO;
	}
	
	@RequestMapping(value="/countSavepointData/download",method=RequestMethod.GET)
	@ResponseBody
	public void countSavepointData(HttpServletResponse response, 
			@RequestParam(value="startTime",required=true) String startTime,
			@RequestParam(value="endTime",required=true) String endTime){
        String filename = "金牌妈妈埋点数据统计_"+startTime+"~"+endTime+".csv";
        List<String> memberData = sysAccessService.countMatchTimesByMember(startTime, endTime);
        List<String> groupData = sysAccessService.countMatchTimesByGroup(startTime, endTime);
        PrintWriter pw = null;
        try {
			response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
			response.setCharacterEncoding("UTF-8");
            pw = new PrintWriter(response.getOutputStream());
            pw.println("金牌妈妈接收消息数");
            pw.println("openid,手机号,昵称,获得关键字消息数");
            for (String mem : memberData) {
            	pw.println(mem);
			}
            pw.println("群接收关键字消息数");
            pw.println("群Id,群名,接收到关键字消息数");
            for (String group : groupData) {
            	pw.println(group);
			}
            pw.flush();
        }catch(Exception e) {  
            throw new RuntimeException(e);  
        }finally {  
            if(pw != null){
            	pw.close();
            }
        }  
	}
}
