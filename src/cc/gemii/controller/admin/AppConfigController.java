package cc.gemii.controller.admin;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.pojo.CommonResult;

@Controller
@RequestMapping("/admin")
public class AppConfigController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private Properties propertyConfigurer;
	
	/**
	 * @param key 系统配置的key
	 * @param value 系统配置的value
	 * @return 
	 * status -1 配置不存在
	 * status 1 配置更新成功
	 * */
	@RequestMapping(value = "/app/config/update", method = RequestMethod.GET)
	@ResponseBody
	public GeneralContentResult<Map<String,String>> updateAppConfig(@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "value", required = true) String value) {
		log.info("接收到更新配置请求:key="+key+",value="+value);
		GeneralContentResult<Map<String,String>> result = new GeneralContentResult<Map<String,String>>();
		Map<String,String> data = new HashMap<String,String>();
		String soureValue = propertyConfigurer.getProperty(key);
		if(soureValue == null){
			result.setResultCode(ResultCode.OPERATION_FAIL);
			result.setDetailDescription("配置不存在");
			data.put(key, value);
			result.setResultContent(data);
			log.info("接收到更新配置请求 result="+JSONObject.toJSONString(result));
			return result;
		}
		propertyConfigurer.put(key, value);
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setDetailDescription("配置更新成功");
		data.put(key, propertyConfigurer.getProperty(key));
		result.setResultContent(data);
		log.info("接收到更新配置请求 result="+JSONObject.toJSONString(result));
		return result;
	}
	
	/**
	 * 查看配置
	 * */
	@RequestMapping(value = "/app/config/get", method = RequestMethod.GET)
	@ResponseBody
	public GeneralContentResult<String> getAppConfig(@RequestParam(value = "key", required = true) String key) {
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setDetailDescription("操作成功");
		result.setResultContent(propertyConfigurer.getProperty(key));
		return result;
	}
	
	@RequestMapping(value="/app/config/getSwitch",method=RequestMethod.GET)
	@ResponseBody
	public GeneralContentResult<String> getMsgSwitch(@RequestParam(value = "keySwitch", required = true) String keySwitch){
		GeneralContentResult<String> result=new GeneralContentResult<String>();
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setDetailDescription("操作成功");
		result.setResultContent(propertyConfigurer.getProperty(keySwitch));
		return result;
	}
}
