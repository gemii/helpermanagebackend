package cc.gemii.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.component.matchGroup.MatchGroupContext;
import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.mapper.WechatroominfoMapper;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.pojo.RobotResult;
import cc.gemii.pojo.RoomInfoResult;

@Controller
@RequestMapping("/admin")
public class MatchGroupController {
	
	@Autowired
	private MatchGroupContext matchGroupCoontext;
	
	@Autowired
	private WechatroominfoMapper wrMapper;
	
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@RequestMapping(value="/match/info",method=RequestMethod.POST)
	@ResponseBody
	public GeneralContentResult<Map<String,Object>> matchResultInfo(@RequestParam("edc")String edc,@RequestParam("hospital")String hospital,@RequestParam("city")String city,@RequestParam("channel")String channel){
		GeneralContentResult<Map<String, Object>> resultInfo=new GeneralContentResult<>();
		Map<String, Object> result=new HashMap<>();
		Map<String, String> param=new HashMap<>();
		param.put("hospital", hospital);
		param.put("city", city);
		param.put("edc", edc);
		MatchGroupResult resultGroup=matchGroupCoontext.matchGroup(MatchGroupStrategy.valueOf(channel), param);
		if(resultGroup!=null && resultGroup.getMatchGroupId()!=null && "".equals(resultGroup.getMatchGroupId())){
			resultInfo.setResultCode(ResultCode.OPERATION_SUCCESS);
			resultInfo.setDetailDescription("匹配到相应的群");
			
			RoomInfoResult roominfo=wrMapper.selectGroupInfo(resultGroup.getMatchGroupId());
			roominfo.setRoomID(resultGroup.getMatchGroupId());
			result.put("roomInfo", roominfo);
			List<MatchGroup> receives = enterGroupMapper.getMatchGroupByUserInfo(resultGroup.getMatchGroupId(), "1");
			if (receives.size() == 0) {
				resultInfo.setResultContent(result);
				return resultInfo;
			}
			MatchGroup receive = receives.get(new Random().nextInt(receives.size()));	
			RobotResult rResult=wrMapper.selectRobotInfo(receive.getRobotID());
			result.put("robotInfo", rResult);
			resultInfo.setResultContent(result);
			return resultInfo;
		}
		return null;
	}
	
	@RequestMapping(value="/match/strategy",method=RequestMethod.POST)
	@ResponseBody
	public GeneralContentResult<Map<String,String>> selectMatchStrategy(){
		GeneralContentResult<Map<String, String>> resultInfo=new GeneralContentResult<>();
		Map<String, String> result=new HashMap<>();
		result.put("好奇", "HUGGIES_MATCHGROUP_STRATEGY");
		result.put("惠氏卡券", "WYETH_CARD_MATCHGROUP_STRATEGY");
		result.put("好育儿", "GOOD_PARENT_MATCHGROUP_STRATEGY");
		result.put("惠氏药店", "WYETH_PHARMACY_MATCHGROUP_STRATEGY");
		result.put("爱婴岛", "LOVE_BABY_MATCHGROUP_STRATEGY");
		result.put("美素佳儿", "BEAUTY_GOODSON_MATCHGROUP_STRATEGY");
		result.put("惠氏项目合作群", "COOPERATION_MATCHGROUP_STRATEGY");
		result.put("乐茵", "LE_YIN_MATCHGROUP_STRATEGY");
		result.put("亿贝", "YI_BEI_MATCHGROUP_STRATEGY");
		result.put("新的美素佳儿", "NEW_BEAUTY_GOODSON_MATCHGROUP_STRATEGY");
		result.put("对接美素佳儿", "MATCHGROUP_FOR_FRISO_STRATEGY");
		result.put("事儿妈", "WYETH_TROBLEDMUM_MATCHGROUP_STRATEGY");
		result.put("买买买", "GEMII_MATCHGROUP_STRATEGY");
		resultInfo.setResultContent(result);
		resultInfo.setResultCode(ResultCode.OPERATION_SUCCESS);
		return resultInfo;
	}
}
