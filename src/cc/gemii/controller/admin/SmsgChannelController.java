package cc.gemii.controller.admin;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.component.sendMsg.SendMsgContext.SendMsgStrategy;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;

@Controller
@RequestMapping("/admin")
public class SmsgChannelController {
	
	@Autowired
	private SendMsgContext sendmsd;
	@Autowired
	private Properties propertyConfigurer;
	
	@RequestMapping(value="/sms/channel",method=RequestMethod.GET)
	@ResponseBody
	public GeneralContentResult<List<Map<String,String>>> getSmsgChannel(){
		GeneralContentResult<List<Map<String,String>>> resuslt=new GeneralContentResult<>();
		resuslt.setResultCode(ResultCode.OPERATION_SUCCESS);
		resuslt.setDetailDescription("操作成功");
		List<String> paramMsg=new ArrayList<>();
		paramMsg.add("HUANXIN_SENDSMS");
		paramMsg.add("ZHONGLAN_SENDSMS");
		paramMsg.add("MONTNETS_SENDSMS");
		paramMsg.add("NETEASECLOUD_SENDSMS");
		paramMsg.add("SUBMAIL_SENDSMS");
		List<String> param=new ArrayList<>();
		param.add("环信");
		param.add("纵览");
		param.add("梦网");
		param.add("网易云");
		param.add("塞邮");
		List<Map<String,String>> info=new ArrayList();
		Map<String, String> reinfo;
		for (int i = 0; i <param.size() ; i++) {
			reinfo=new HashMap<>();
			reinfo.put("name", param.get(i));
			reinfo.put("value", paramMsg.get(i));
			info.add(reinfo);
		}
		resuslt.setResultContent(info);
		return resuslt;
	}
	
	@RequestMapping(value="/sms/send",method=RequestMethod.GET)
	@ResponseBody
	public GeneralContentResult<String> sendMsg(@RequestParam(value="phone")String phone,@RequestParam(value="channel")String channel){
		GeneralContentResult<String> result = new GeneralContentResult<>();
		String code = phone.substring(phone.length() - 4);
		Formatter fm = new Formatter();
		String firstTemplate = propertyConfigurer.getProperty("send_msg_prompt");
		String content = String.valueOf(fm.format(firstTemplate, code));
		
		ShortMessage shortMessage = new ShortMessage();
		shortMessage.setReciverPhone(phone);
		shortMessage.setContent(content);
		shortMessage.setCode(code);
		result = sendmsd.sendSMSByHtml(shortMessage, SendMsgStrategy.valueOf(channel));
		return result;
	}

}
