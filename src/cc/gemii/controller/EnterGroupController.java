package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.fluent.Request;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.po.BlackWhiteListDO.BlackWhiteListType;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.BlackWhiteListService;
import cc.gemii.service.EnterGroupService;
import cc.gemii.util.RegularUtil;

@Controller
@RequestMapping("/enter")
public class EnterGroupController {

	private Logger logger = Logger.getLogger(this.getClass());
	@Autowired
	private EnterGroupService enterGroupService;
	@Autowired
	private BlackWhiteListService blackWhiteListService;
	
	@RequestMapping("/getCity")
	@ResponseBody
	public CommonResult getCity(@RequestParam String robotID, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.getCity(robotID);
	}

	@RequestMapping("/getCityAndHos")
	@ResponseBody
	public CommonResult getCityAndHos(@RequestParam String robotID, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.getCityAndHost(robotID);
	}

	@RequestMapping("/getAllCity")
	@ResponseBody
	public CommonResult getAllCity(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.getAllCity();
	}

	@RequestMapping("/getHosByCity")
	@ResponseBody
	public CommonResult getHosByCity(@RequestParam(required = false) String robotID, @RequestParam String city,
			HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.getHosByCity(city, robotID);
	}

	@RequestMapping("/submitUserInfo")
	@ResponseBody
	public CommonResult submitUserInfo(Userinfo user, @RequestParam(required = false) String robotID,
			@RequestParam String code, @RequestParam String uid, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if (!RegularUtil.isDate(user.getEdc())) {
			logger.info("日期信息" + user.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		return enterGroupService.submitUserInfo(user, robotID, code, uid);
	}

	@RequestMapping("/modifyH5Status")
	@ResponseBody
	public CommonResult modifyH5Status(@RequestParam String uid, @RequestParam String robotID,
			@RequestParam String code, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.modifyH5Status(robotID, code, uid);
	}

	@RequestMapping("/getCode")
	@ResponseBody
	public CommonResult getCode(@RequestParam String phone, @RequestParam String code, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.getCode(phone, code);
	}

	@RequestMapping(value = "/card/member", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult createMember(UserFriendEnterGroup memberCardInfo, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return enterGroupService.createMember(memberCardInfo);
	}

	/**
	 * 
	 * @param memberReq
	 *            创建会员请求包装类
	 * @return TODO 好奇会员入惠氏群流程
	 */
	@RequestMapping(value = "/huggies/member", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult createHuggiesMember(UserFriendEnterGroup memberReq, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		try {
			return enterGroupService.createHuggiesMember(memberReq);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "服务器繁忙");
		}
	}

	/**
	 * 接受信息匹配群信息
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/receive/match", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult receiveInfoMatchGroup(UserFriendEnterGroup receiveInfo, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			if ("3".equals(receiveInfo.getType()) || "31".equals(receiveInfo.getType())) {
				return enterGroupService.pharmacyMatchGroup(receiveInfo);
			}
			if ("13".equals(receiveInfo.getType())) {
				return this.enterGroupService.fixedMatchGroup(receiveInfo);
			}
			if("9".equals(receiveInfo.getType())){
				return this.enterGroupService.cooperationMatchGroup(receiveInfo);
			}
			return enterGroupService.receiveInfoMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}

	/**
	 * 根据openid查询robotid
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/select/robotid", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult selectRobotId(UserFriendEnterGroup receiveInfo, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		try {
			return enterGroupService.selectRobotId(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}

	/**
	 * 好育儿匹配群信息
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/goodparent/match", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult goodParentMatchGroup(UserFriendEnterGroup receiveInfo, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			return enterGroupService.goodParentMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}

	@RequestMapping(value = "/lovebaby/match", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult loveBabyMatchGroup(UserFriendEnterGroup receiveInfo) {
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			return enterGroupService.loveBabyMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}

	/**
	 * 根据openid查询robotid
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/select/robotUrl", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult selectRobotUrl(UserFriendEnterGroup receiveInfo, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		String funCode = "OPENID_ENTER_GROUP_LIMIT";
		if(BlackWhiteListType.BLACK == blackWhiteListService.getUserType(receiveInfo.getOpenid(),funCode)){
			logger.info("receiveInfo："+JSONObject.toJSONString(receiveInfo));
			logger.info("匹配到黑名单用户入群 openid:"+receiveInfo.getOpenid());
			return CommonResult.ok(null);
		}
		try {
			if(StringUtils.isNotBlank(receiveInfo.getType()) && receiveInfo.getType().length()>1 ){
				receiveInfo.setType(receiveInfo.getType().substring(0, 1));
			}
			return enterGroupService.selectRobotUrl(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}

	/**
	 * 根据院code查询院名
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/select/hospitalName", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult selectHospitalName(String code, String type, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		try {
			return enterGroupService.selectHospitalInfo(code, type);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}

	
	@RequestMapping(value = "/leyin/match", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult leYinMatchGroup(UserFriendEnterGroup receiveInfo) {
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			return enterGroupService.leYinMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	/**
	 * 根据门店code查询门店名称
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/select/LyHospitalInfo", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult selectLyHospitalInfo(String code, String level, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		try {
			return enterGroupService.selectLyHospitalInfo(code, level);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	@RequestMapping(value = "/yibei/match", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult yiBeiMatchGroup(UserFriendEnterGroup receiveInfo) {
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			return enterGroupService.yiBeiMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
//	@RequestMapping(value = "/matchGroupForFriso/match", method = RequestMethod.POST)
//	@ResponseBody
//	public CommonResult matchGroupForFriso(UserFriendEnterGroup receiveInfo) {
//		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
//			logger.info("日期信息" + receiveInfo.getEdc());
//			return CommonResult.build(-3, "日期格式错误");
//		}
//		try {
//			return enterGroupService.matchGroupForFriso(receiveInfo);
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			return CommonResult.build(500, "系统繁忙");
//		}
//	}
//	
//	@RequestMapping(value = "/returnInfoToFriso/return")
//	@ResponseBody
//	public CommonResult returnInfoToFriso(String receiveInfo, HttpServletResponse response) {
//		response.setHeader("Access-Control-Allow-Origin", "*");
//		try {
//			return enterGroupService.returnInfoToFriso(receiveInfo);
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			return CommonResult.build(500, "系统繁忙");
//		}
//	}
	
//	@RequestMapping(value = "/matchGroupForFriso/match", method = RequestMethod.POST)
//	@ResponseBody
//	public CommonResult matchGroupForFriso(UserFriendEnterGroup receiveInfo) {
//		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
//			logger.info("日期信息" + receiveInfo.getEdc());
//			return CommonResult.build(-3, "日期格式错误");
//		}
//		try {
//			return enterGroupService.matchGroupForFriso(receiveInfo);
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			return CommonResult.build(500, "系统繁忙");
//		}
//	}
	
//	@RequestMapping(value = "/newbeautygoodson/match", method = RequestMethod.POST)
//	@ResponseBody
//	public CommonResult newBeautyGoodSonMatchGroup(UserFriendEnterGroup receiveInfo) {
//		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
//			logger.info("日期信息" + receiveInfo.getEdc());
//			return CommonResult.build(-3, "日期格式错误");
//		}
//		try {
//			return enterGroupService.newBeautyGoodSonMatchGroup(receiveInfo);
//		} catch (Exception e) {
//			logger.error(e.getMessage(), e);
//			return CommonResult.build(500, "系统繁忙");
//		}
//	}

}
