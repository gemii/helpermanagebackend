package cc.gemii.controller;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;

import cc.gemii.data.otd.DicNodeData;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.EnterGroupService;
import cc.gemii.service.GemiiMatchGroupService;
import cc.gemii.util.RegularUtil;

@Controller
@RequestMapping("/gemii")
public class GemiiMatchGroupController {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private GemiiMatchGroupService gemiiMatchGroupService;
	
	/**
	 * 景栗匹配群信息
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/selectGroup", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult gemiiMatchGroup(UserFriendEnterGroup receiveInfo) {
		try {
			logger.info("景栗匹配群接受参数  "+JSON.toJSONString(receiveInfo));
			return gemiiMatchGroupService.selectGemiiMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}
	/**
	 * 根据手机号查询userinfo信息
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/selectUser", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult selectUserByPhone(String phone,String type) {
		try {
			logger.info("景栗接受手机号参数 "+phone);
			return gemiiMatchGroupService.selectUserInfo(phone,type);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}

	}
	
	@RequestMapping(value = "/provinces/citys", method = RequestMethod.GET)
	@ResponseBody
	public Collection<DicNodeData> searchAllProvince(String type) {
		return gemiiMatchGroupService.searchProvinceByGemii(type);
	}

//	@RequestMapping(value = "/repeat/info", method = RequestMethod.POST)
//	@ResponseBody
//	public CommonResult selectInfo(UserFriendEnterGroup receiveInfo) {
//		return gemiiMatchGroupService.selectRepeat(receiveInfo);
//	}

}
