package cc.gemii.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.data.otd.EnterGroupInfo;
import cc.gemii.data.otd.FrisoUserInfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.UserFriendEnterGroup;
import cc.gemii.service.FrisoEnterGroupService;
import cc.gemii.util.RegularUtil;

@Controller
@RequestMapping("/enter")
public class FrisoEnterGroupController {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private FrisoEnterGroupService frisoEnterGroupService;
	
	/**
	 * 美素佳儿入群策略
	 * @param receiveInfo
	 * @return
	 */
	@RequestMapping(value = "/beautygoodson/match", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult beautyGoodSonMatchGroup(UserFriendEnterGroup receiveInfo) {
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			if(null != receiveInfo.getHospital() && !receiveInfo.getHospital().equals("") && !receiveInfo.getHospital().equals("其他")){
				return frisoEnterGroupService.newBeautyGoodSonMatchGroup(receiveInfo);
			}
			return frisoEnterGroupService.beautyGoodSonMatchGroup(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	/**
	 * 对接美素匹配群
	 * @param receiveInfo
	 * @return
	 */
	@RequestMapping(value = "/matchGroup", method = RequestMethod.POST)
	@ResponseBody
	public CommonResult matchGroupForFriso(@RequestBody FrisoUserInfo receiveInfo) {
		if (!RegularUtil.isDate(receiveInfo.getEdc())) {
			logger.info("日期信息" + receiveInfo.getEdc());
			return CommonResult.build(-3, "日期格式错误");
		}
		try {
			return frisoEnterGroupService.matchGroupForFriso(receiveInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
	/**
	 * 入群或者退群时回调美素
	 * @param enterGroupInfo
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/returnEnterOrQuitGroupInfoToFriso/return",method = RequestMethod.POST)
	@ResponseBody
	public CommonResult returnEnterOrQuitGroupInfoToFriso(@RequestBody EnterGroupInfo enterGroupInfo,HttpServletRequest request, HttpServletResponse response) {
		try {
			return frisoEnterGroupService.returnEnterOrQuitGroupInfoToFriso(enterGroupInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return CommonResult.build(500, "系统繁忙");
		}
	}
	
}
