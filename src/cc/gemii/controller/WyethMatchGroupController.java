package cc.gemii.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.matchGroup.MatchGroupContext.MatchGroupStrategy;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.otd.UserInfoTroubledMum;
import cc.gemii.po.Userinfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.MatchGroupService;
import cc.gemii.service.WyethMatchGroupService;
import cc.gemii.util.RegularUtil;

/**
 * 惠氏入群逻辑，及相关功能
 * */
@Controller
public class WyethMatchGroupController {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private WyethMatchGroupService wyethMatchGroupController;
	@Autowired
	private MatchGroupService matchGroupService;
	
	/**
	 * 惠氏合作群,自建群，网红群
	 * */
	@RequestMapping("/wyeth/commitUserInfo")
	@ResponseBody
	public CommonResult commitUserInfo(Userinfo user, @RequestParam(required = false) String robotID,
			@RequestParam String code, @RequestParam String uid,@RequestParam String type, HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		if (!RegularUtil.isDate(user.getEdc())) {
			return CommonResult.build(-3, "日期格式错误");
		}
		return wyethMatchGroupController.commitUserInfo(user, robotID, code, uid,type);
	}
	
	@RequestMapping("/wyeth/selectRobotOwner")
	@ResponseBody
	public CommonResult selectRobotOwner(@RequestParam(required = false) String robotID,HttpServletRequest request,HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		return wyethMatchGroupController.selectRobotOwner(robotID);
	}
	
	/**
	 * 事儿妈入群
	 * */
	@RequestMapping("/troubledmum/robot/match")
	@ResponseBody
	public GeneralContentResult<Map<String,String>> matchRobot(UserInfoTroubledMum userInfoTroubledMum) {
		//无需重复入群检查，事儿妈端已经完成openid重复校验
		log.info("troubledmum 接收到参数 userInfoTroubledMum="+JSONObject.toJSONString(userInfoTroubledMum));
		GeneralContentResult<Map<String,String>> result = new  GeneralContentResult<Map<String,String>>();
		if (StringUtils.isBlank(userInfoTroubledMum.getOpenid()) || StringUtils.isBlank(userInfoTroubledMum.getEdc())
				|| StringUtils.isBlank(userInfoTroubledMum.getPhone())
				|| StringUtils.isBlank(String.valueOf(userInfoTroubledMum.getChannel()))) {
			result.setResultCode(ResultCode.ENTER_GROUP_INPUT_ERROR);
			result.setDetailDescription("缺少关键参数");
			return result;
		}
		userInfoTroubledMum.setChannel(Integer.valueOf("16"+userInfoTroubledMum.getChannel()));
		result = matchGroupService.matchByUserInfo(userInfoTroubledMum, MatchGroupStrategy.WYETH_TROBLEDMUM_MATCHGROUP_STRATEGY,"2");
		return result;
	}
	 
}
