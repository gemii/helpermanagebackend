package cc.gemii.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Userstatus;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.StatusService;

@Controller
@RequestMapping("/status")
public class StatusController {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private StatusService statusService;
	
	@RequestMapping("/getRobotByUid")
	@ResponseBody
	public CommonResult getStatus(@RequestParam Integer id,
			HttpServletRequest req,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		logger.debug("getRobotByUid--remoteAddr:"+req.getRemoteAddr());
		return statusService.getRobot(id);
	}
	
	@RequestMapping("/updateH5")
	@ResponseBody
	public CommonResult updateH5(Userstatus userstatus,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return statusService.updateH5(userstatus);
	}
	
	
}
