package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Wechatclerk;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.GroupService;
import cc.gemii.service.KeywordService;

@Controller
@RequestMapping("/group")
public class GroupController {

	@Autowired
	private GroupService groupService;
	
	@Autowired
	private KeywordService keywordService;
	@RequestMapping("/getGroup")
	@ResponseBody
	public CommonResult getAllGroup(@RequestParam Integer page,
			@RequestParam Integer clerkID,
			@RequestParam Integer pageSize,
			@RequestParam(required=false) String groupName,
			HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return groupService.getAllGroup(page, pageSize, groupName, clerkID);
	}
	
	@RequestMapping("/updateGroupInfo")
	@ResponseBody
	public CommonResult updateGroupInfo(Wechatroominfo roomInfo,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return groupService.updateGroupInfo(roomInfo);
	}
	
	@RequestMapping("/getGroupByRobot")
	@ResponseBody
	public CommonResult getGroupByHelper(@RequestParam String robotID,
			@RequestParam Integer page,
			@RequestParam Integer pageSize,
			@RequestParam(required=false) String groupName,
			@RequestParam String type,
			HttpServletResponse response
			, HttpSession session){
		 String groupType = "";
		    Wechatclerk user = (Wechatclerk)session.getAttribute("user");
		    if(user == null){
		    	return CommonResult.build(101, "用户已失效");
		    }
		    if ((user.getType() != null) && (!"".equals(user.getType()))) {
		      groupType = user.getType();
		    }
		response.setHeader("Access-Control-Allow-Origin", "*");
		return groupService.getGroupByHelper(robotID,groupName,page,pageSize,type, groupType);
	}
}
