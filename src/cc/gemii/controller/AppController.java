package cc.gemii.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.pojo.CommonResult;

@Controller
public class AppController {

	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private Properties propertyConfigurer;
	
	/**
	 * @param key 系统配置的key
	 * @param value 系统配置的value
	 * @return 
	 * status -1 配置不存在
	 * status 1 配置更新成功
	 * */
	@RequestMapping(value = "/app/config/update", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult updateAppConfig(@RequestParam(value = "key", required = true) String key,
			@RequestParam(value = "value", required = true) String value) {
		log.info("接收到更新配置请求:key="+key+",value="+value);
		CommonResult result = new CommonResult();
		Map<String,String> data = new HashMap<String,String>();
		String soureValue = propertyConfigurer.getProperty(key);
		if(soureValue == null){
			result.setStatus(-1);
			result.setMsg("配置不存在");
			data.put(key, value);
			result.setData(data);
			log.info("接收到更新配置请求 result="+JSONObject.toJSONString(result));
			return result;
		}
		propertyConfigurer.put(key, value);
		result.setStatus(1);
		result.setMsg("配置更新成功");
		data.put(key, soureValue);
		data.put(key+"_now", propertyConfigurer.getProperty(key));
		result.setData(data);
		log.info("接收到更新配置请求 result="+JSONObject.toJSONString(result));
		return result;
	}
	
	/**
	 * 查看配置
	 * */
	@RequestMapping(value = "/app/config/get", method = RequestMethod.GET)
	@ResponseBody
	public CommonResult getAppConfig(@RequestParam(value = "key", required = true) String key) {
		CommonResult result = new CommonResult();
		Map<String,String> data = new HashMap<String,String>();
		result.setStatus(1);
		data.put(key+"_now", propertyConfigurer.getProperty(key));
		result.setData(data);
		return result;
	}
}
