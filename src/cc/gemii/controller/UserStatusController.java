package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.GeneralPagingResult;
import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserInfoCardVO;
import cc.gemii.data.otd.UserStatusCardVO;
import cc.gemii.po.Userinfo;
import cc.gemii.po.Wechatclerk;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.UserStatusService;

@Controller
public class UserStatusController {

	private Logger log = Logger.getLogger(this.getClass());

	@Autowired
	private UserStatusService userStatusService;

	@RequestMapping(value = "/card/userstatus", method = RequestMethod.POST)
	@ResponseBody
	public GeneralPagingResult<UserStatusCardVO> searchUserStatus(
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "size", required = true) int size, @ModelAttribute UserStatusCardProfile searchInfo,
			HttpSession session) {
		Wechatclerk user = (Wechatclerk) session.getAttribute("user");
		if (user == null) {
			GeneralPagingResult<UserStatusCardVO> result = new GeneralPagingResult<UserStatusCardVO>();
			result.setResultCode(ResultCode.OPERATION_ERROR);
			return result;
		}
		if ((user.getType() != null) && (!"".equals(user.getType()))) {
			searchInfo.setType(user.getType());
		}
		this.log.debug("接收到的参数 page=" + page + "-size=" + size + "-searchInfo" + JSONObject.toJSONString(searchInfo)
				+ "用户信息" + JSONObject.toJSONString(user));
		PageInfo<UserStatusCardVO> pageinfo = userStatusService.searchUserStatusByCardIn(page, size, searchInfo);
		GeneralPagingResult<UserStatusCardVO> result = new GeneralPagingResult<UserStatusCardVO>();
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setPageInfo(pageinfo);
		return result;
	}

	@RequestMapping(value = "/card/userstatus/matchuserinfo", method = RequestMethod.POST)
	@ResponseBody
	public GeneralContentResult<Userinfo> matchuserinfo(@RequestParam("id") int id,
			@RequestParam("phoneNumber") String phoneNumber) {
		log.debug("接收到参数 id=" + id + "-phoneNumber=" + phoneNumber);
		GeneralContentResult<Userinfo> result = new GeneralContentResult<Userinfo>();
		try {
			CommonResult cr = userStatusService.matchuserinfo(id, phoneNumber);
			if (cr != null) {
				Userinfo userinfo = (Userinfo) cr.getData();
				if (cr.getStatus() == 2) {
					result.setResultCode(ResultCode.INFO_EXIST);
					result.setDetailDescription("该手机号已关联");
					return result;
				}
				result.setResultCode(ResultCode.OPERATION_SUCCESS);
				result.setDetailDescription("操作成功");
				result.setResultContent(userinfo);
				return result;
			}
			result.setResultCode(ResultCode.OPERATION_NOT_MATCH_DATA);
			result.setDetailDescription("未找到用户信息");
			return result;
		} catch (Exception e) {
			log.error("接收到参数 id=" + id + "-phoneNumber=" + phoneNumber);
			log.error(e.getMessage(), e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("操作失败");
			return result;
		}
	}
}
