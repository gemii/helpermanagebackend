package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cc.gemii.po.Wechatkeyword;
import cc.gemii.pojo.CommonResult;
import cc.gemii.pojo.SelectKeyword;
import cc.gemii.service.KeywordService;

@Controller
@RequestMapping("/keyword")
public class KeywordController {

	@Autowired
	private KeywordService keywordService;
	
	@RequestMapping("/getKeywordByGid")
	@ResponseBody
	public CommonResult getKeywordByGid(@RequestParam String id,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.getKeywordByGid(id);
	}
	
	@RequestMapping("/insertKeyword")
	@ResponseBody
	public CommonResult insertKeyword(Wechatkeyword keyword,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.insertKeyword(keyword);
	}
	
	@RequestMapping("/updateKeyword")
	@ResponseBody
	public CommonResult updateKeyword(Wechatkeyword keyword,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.updateKeyword(keyword);
	}
	
	@RequestMapping("/selectKeyword")
	@ResponseBody
	public CommonResult selectKeyword(SelectKeyword selectKeyword,HttpServletResponse response){
		response.setHeader("Access-Control-Allow-Origin", "*");
		return keywordService.selectKeyword(selectKeyword);
	}
}
