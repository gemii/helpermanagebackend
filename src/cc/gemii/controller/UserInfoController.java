package cc.gemii.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralPagingResult;
import cc.gemii.data.dto.UserInfoCardProfile;
import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserInfoCardVO;
import cc.gemii.po.Wechatclerk;
import cc.gemii.pojo.CommonResult;
import cc.gemii.service.UserInfoService;

@Controller
public class UserInfoController {

	private Logger log = Logger.getLogger(this.getClass());

	@Autowired
	private UserInfoService userInfoService;
	
	/**
	 * H5信息页面高级查询
	 * */
	@RequestMapping(value="/card/userinfos",method = RequestMethod.POST)
	@ResponseBody
	public GeneralPagingResult<UserInfoCardVO> searchCardUserInfo(
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "size", required = true) int size,
			@ModelAttribute UserInfoCardProfile searchInfo
			, HttpSession session){
		 	Wechatclerk user = (Wechatclerk)session.getAttribute("user");
		 	if(user == null){
		 		GeneralPagingResult<UserInfoCardVO> result = new GeneralPagingResult<UserInfoCardVO>();
				result.setResultCode(ResultCode.OPERATION_ERROR);
				return result;
		    }
		    if ((user.getType() != null) && (!"".equals(user.getType()))) {
		      searchInfo.setType(user.getType());
		    }
		    this.log.info("接收到的参数 page=" + page + "-size=" + size + "-searchInfo" + JSONObject.toJSONString(searchInfo) + "用户信息" + JSONObject.toJSONString(user) + "类型 " + searchInfo.getType());
		PageInfo<UserInfoCardVO> pageinfo = userInfoService.searchUserInfoByCardIn( page, size, searchInfo);
		GeneralPagingResult<UserInfoCardVO> result = new GeneralPagingResult<UserInfoCardVO>();
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setPageInfo(pageinfo);
		return result;
	}
	
}
