package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.GetGroupByHelper;
import cc.gemii.pojo.GroupCustom;
import cc.gemii.pojo.MatchGroup;


public interface WechatroomCustomMapper {
    List<GroupCustom> getAllGroup(Map<String, Object> map);

    List<GroupCustom> getGroupByName(Map<String, Object> map);

	List<GetGroupByHelper> getGroupByHelper(Map<String, Object> map);
	
	int getGroupByHelperCount(Map<String, Object> map);
	
	int getCount(Map<String, Object> map);
	
	List<MatchGroup> getGroupByRoomID(String roomID);

	int getGroupByUidCount(Map<String, Object> param);

	List<GetGroupByHelper> getGroupByUid(Map<String, Object> param);
	
}