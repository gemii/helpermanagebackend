package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.po.WechatrobotinfoExample;

public interface WechatrobotinfoMapper {
    int countByExample(WechatrobotinfoExample example);

    int deleteByExample(WechatrobotinfoExample example);

    int deleteByPrimaryKey(String robotid);

    int insert(Wechatrobotinfo record);

    int insertSelective(Wechatrobotinfo record);

    List<Wechatrobotinfo> selectByExampleWithBLOBs(WechatrobotinfoExample example);

    List<Wechatrobotinfo> selectByExample(WechatrobotinfoExample example);

    Wechatrobotinfo selectByPrimaryKey(String robotid);

    int updateByExampleSelective(@Param("record") Wechatrobotinfo record, @Param("example") WechatrobotinfoExample example);

    int updateByExampleWithBLOBs(@Param("record") Wechatrobotinfo record, @Param("example") WechatrobotinfoExample example);

    int updateByExample(@Param("record") Wechatrobotinfo record, @Param("example") WechatrobotinfoExample example);

    int updateByPrimaryKeySelective(Wechatrobotinfo record);

    int updateByPrimaryKeyWithBLOBs(Wechatrobotinfo record);
}