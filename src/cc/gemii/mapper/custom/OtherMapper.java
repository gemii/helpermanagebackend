package cc.gemii.mapper.custom;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.GetH5Record;

public interface OtherMapper {
	int getTag();

	List<GetH5Record> getH5Record(Map<String, String> map);
}
