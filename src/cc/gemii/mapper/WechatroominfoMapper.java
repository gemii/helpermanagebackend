package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatroominfo;
import cc.gemii.po.WechatroominfoExample;
import cc.gemii.pojo.GroupCustom;
import cc.gemii.pojo.RobotResult;
import cc.gemii.pojo.RoomInfoResult;

public interface WechatroominfoMapper {
    int countByExample(WechatroominfoExample example);

    int deleteByExample(WechatroominfoExample example);

    int deleteByPrimaryKey(String roomid);

    int insert(Wechatroominfo record);

    int insertSelective(Wechatroominfo record);

    List<Wechatroominfo> selectByExample(WechatroominfoExample example);

    Wechatroominfo selectByPrimaryKey(String roomid);

    int updateByExampleSelective(@Param("record") Wechatroominfo record, @Param("example") WechatroominfoExample example);

    int updateByExample(@Param("record") Wechatroominfo record, @Param("example") WechatroominfoExample example);

    int updateByPrimaryKeySelective(Wechatroominfo record);

    int updateByPrimaryKey(Wechatroominfo record);

    
    List<Map<String,String>> selectQRcodePageData(Map<String,String> param);
    
	List<String> selectProvince();

	List<String> selectCity(@Param("provinceName") String provinceName);

	List<String> selectHospitalByCity(@Param("cityName") String cityName,@Param("type")String type);

	List<String> selectCityByArea(@Param("areaName") String areaName);
	
	List<String> selectPharmacyByCity(@Param("cityName") String cityName);
	
	RoomInfoResult selectGroupInfo(String roomID);
	
	RobotResult selectRobotInfo(String robotID);

}