package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatkeyword;
import cc.gemii.po.WechatkeywordExample;

public interface WechatkeywordMapper {
    int countByExample(WechatkeywordExample example);

    int deleteByExample(WechatkeywordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Wechatkeyword record);

    int insertSelective(Wechatkeyword record);

    List<Wechatkeyword> selectByExample(WechatkeywordExample example);

    Wechatkeyword selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Wechatkeyword record, @Param("example") WechatkeywordExample example);

    int updateByExample(@Param("record") Wechatkeyword record, @Param("example") WechatkeywordExample example);

    int updateByPrimaryKeySelective(Wechatkeyword record);

    int updateByPrimaryKey(Wechatkeyword record);
}