package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Robotrole;
import cc.gemii.po.RobotroleExample;

public interface RobotroleMapper {
    int countByExample(RobotroleExample example);

    int deleteByExample(RobotroleExample example);

    int deleteByPrimaryKey(String bottag);

    int insert(Robotrole record);

    int insertSelective(Robotrole record);

    List<Robotrole> selectByExample(RobotroleExample example);

    Robotrole selectByPrimaryKey(String bottag);

    int updateByExampleSelective(@Param("record") Robotrole record, @Param("example") RobotroleExample example);

    int updateByExample(@Param("record") Robotrole record, @Param("example") RobotroleExample example);

    int updateByPrimaryKeySelective(Robotrole record);

    int updateByPrimaryKey(Robotrole record);
    
}