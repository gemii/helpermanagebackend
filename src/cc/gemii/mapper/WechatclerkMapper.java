package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatclerk;
import cc.gemii.po.WechatclerkExample;
import cc.gemii.pojo.RobotResult;

public interface WechatclerkMapper {
    int countByExample(WechatclerkExample example);

    int deleteByExample(WechatclerkExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Wechatclerk record);

    int insertSelective(Wechatclerk record);

    List<Wechatclerk> selectByExample(WechatclerkExample example);

    Wechatclerk selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Wechatclerk record, @Param("example") WechatclerkExample example);

    int updateByExample(@Param("record") Wechatclerk record, @Param("example") WechatclerkExample example);

    int updateByPrimaryKeySelective(Wechatclerk record);

    int updateByPrimaryKey(Wechatclerk record);
    
    RobotResult selectStatus(String robotTag);
    
    String selectRole(String robotTag);
}