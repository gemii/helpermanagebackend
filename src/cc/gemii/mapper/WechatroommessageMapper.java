package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatroommessage;
import cc.gemii.po.WechatroommessageExample;

public interface WechatroommessageMapper {
    int countByExample(WechatroommessageExample example);

    int deleteByExample(WechatroommessageExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Wechatroommessage record);

    int insertSelective(Wechatroommessage record);

    List<Wechatroommessage> selectByExampleWithBLOBs(WechatroommessageExample example);

    List<Wechatroommessage> selectByExample(WechatroommessageExample example);

    Wechatroommessage selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Wechatroommessage record, @Param("example") WechatroommessageExample example);

    int updateByExampleWithBLOBs(@Param("record") Wechatroommessage record, @Param("example") WechatroommessageExample example);

    int updateByExample(@Param("record") Wechatroommessage record, @Param("example") WechatroommessageExample example);

    int updateByPrimaryKeySelective(Wechatroommessage record);

    int updateByPrimaryKeyWithBLOBs(Wechatroommessage record);
}