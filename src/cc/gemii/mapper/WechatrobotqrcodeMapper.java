package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatrobotqrcode;
import cc.gemii.po.WechatrobotqrcodeExample;

public interface WechatrobotqrcodeMapper {
    int countByExample(WechatrobotqrcodeExample example);

    int deleteByExample(WechatrobotqrcodeExample example);

    int deleteByPrimaryKey(String robotid);

    int insert(Wechatrobotqrcode record);

    int insertSelective(Wechatrobotqrcode record);

    List<Wechatrobotqrcode> selectByExample(WechatrobotqrcodeExample example);

    Wechatrobotqrcode selectByPrimaryKey(String robotid);

    int updateByExampleSelective(@Param("record") Wechatrobotqrcode record, @Param("example") WechatrobotqrcodeExample example);

    int updateByExample(@Param("record") Wechatrobotqrcode record, @Param("example") WechatrobotqrcodeExample example);

    int updateByPrimaryKeySelective(Wechatrobotqrcode record);

    int updateByPrimaryKey(Wechatrobotqrcode record);
}