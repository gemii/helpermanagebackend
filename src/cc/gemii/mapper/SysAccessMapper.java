package cc.gemii.mapper;

import java.sql.Timestamp;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.SysAccessDO;

public interface SysAccessMapper {
	
	public int insertSysAccess(SysAccessDO sysAccess);

	/*
	 * count operate
	 * */
	public List<String> countMatchTimesByMember(@Param("startTime") String startTime, @Param("endTime") String endTime);

	public List<String> countMatchTimesByGroup(@Param("startTime") String startTime, @Param("endTime") String endTime);
	
}
