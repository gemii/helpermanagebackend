package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import cc.gemii.pojo.GetHosByCity;

public interface WechatroominfoCustomMapper {
	
	List<GetHosByCity> getHosByCity(Map<String, String> map);
}