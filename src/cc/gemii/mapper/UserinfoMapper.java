package cc.gemii.mapper;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cc.gemii.data.dto.UserInfoCardProfile;
import cc.gemii.data.otd.UserInfoCardVO;
import cc.gemii.po.UserInfoPO;
import cc.gemii.po.Userinfo;
import cc.gemii.po.UserinfoExample;

public interface UserinfoMapper {

    int deleteByExample(UserinfoExample example);

    int insert(Userinfo record);

    Page<UserInfoCardVO> selectCardUserInfos(UserInfoCardProfile searchInfo);

	Userinfo selectByPhoneNumber(String phoneNumber);
	
	/*新方法*/
    int insertUserInfo(UserInfoPO record);

    /**
     * 刷新验证码生效时间
     * */
    int refreshVcodeStartTiem(@Param("id") Integer id);
}