package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import cc.gemii.po.TaskNow;
import cc.gemii.pojo.SendMsgTask;

public interface TaskNowMapper {
	
	/**
	 * 查询手机号与用户状态id
	 * @return
	 */
	public List<SendMsgTask> getPhoneNums();
	
	
	public void insertTaskInfo(List<TaskNow> param);

}
