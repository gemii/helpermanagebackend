package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatuserentergroupstatus;
import cc.gemii.po.WechatuserentergroupstatusExample;
import cc.gemii.po.WechatuserentergroupstatusWithBLOBs;

public interface WechatuserentergroupstatusMapper {
    int countByExample(WechatuserentergroupstatusExample example);

    int deleteByExample(WechatuserentergroupstatusExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WechatuserentergroupstatusWithBLOBs record);

    int insertSelective(WechatuserentergroupstatusWithBLOBs record);

    List<WechatuserentergroupstatusWithBLOBs> selectByExampleWithBLOBs(WechatuserentergroupstatusExample example);

    List<Wechatuserentergroupstatus> selectByExample(WechatuserentergroupstatusExample example);

    WechatuserentergroupstatusWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WechatuserentergroupstatusWithBLOBs record, @Param("example") WechatuserentergroupstatusExample example);

    int updateByExampleWithBLOBs(@Param("record") WechatuserentergroupstatusWithBLOBs record, @Param("example") WechatuserentergroupstatusExample example);

    int updateByExample(@Param("record") Wechatuserentergroupstatus record, @Param("example") WechatuserentergroupstatusExample example);

    int updateByPrimaryKeySelective(WechatuserentergroupstatusWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(WechatuserentergroupstatusWithBLOBs record);

    int updateByPrimaryKey(Wechatuserentergroupstatus record);
}