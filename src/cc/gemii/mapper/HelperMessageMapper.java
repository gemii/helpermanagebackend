package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.HelperMessage;
import cc.gemii.po.HelperMessageExample;

public interface HelperMessageMapper {
    int countByExample(HelperMessageExample example);

    int deleteByExample(HelperMessageExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(HelperMessage record);

    int insertSelective(HelperMessage record);

    List<HelperMessage> selectByExampleWithBLOBs(HelperMessageExample example);

    List<HelperMessage> selectByExample(HelperMessageExample example);

    HelperMessage selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") HelperMessage record, @Param("example") HelperMessageExample example);

    int updateByExampleWithBLOBs(@Param("record") HelperMessage record, @Param("example") HelperMessageExample example);

    int updateByExample(@Param("record") HelperMessage record, @Param("example") HelperMessageExample example);

    int updateByPrimaryKeySelective(HelperMessage record);

    int updateByPrimaryKeyWithBLOBs(HelperMessage record);

    int updateByPrimaryKey(HelperMessage record);
}