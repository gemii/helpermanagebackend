package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import cc.gemii.po.Wechatuserentergroupstatus;
import cc.gemii.pojo.SelectMessage;
import cc.gemii.pojo.SelectMessageAB;
import cc.gemii.pojo.SelectMessageAResult;
import cc.gemii.pojo.SelectMessageBResult;

public interface WechatuserentergroupstatusCustomMapper {
   List<Wechatuserentergroupstatus> selectMessage(SelectMessage selectMessage);
   
   int selectMessageCount(SelectMessage selectMessage);
   
   List<SelectMessageAResult> selectMessageA(SelectMessageAB selectMessage2);
   
   int selectMessageACount(SelectMessageAB selectMessage2);
   
   List<SelectMessageBResult> selectMessageB(SelectMessageAB selectMessageAB);
   
   int selectMessageBCount(SelectMessageAB selectMessageAB);

   void updateMessageStatus(Map<String, String> param);
}