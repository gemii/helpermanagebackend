package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.pojo.GoodSonUserInfo;

public interface GoodSonUserInfoDownloadMapper {

	List<GoodSonUserInfo> goodSonUserInfoQuery(@Param("startTime")String startTime, @Param("endTime") String endTime);

}
