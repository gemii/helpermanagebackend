package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.github.pagehelper.Page;

import cc.gemii.data.dto.UserStatusCardProfile;
import cc.gemii.data.otd.UserStatusCardVO;
import cc.gemii.po.Userinfo;
import cc.gemii.po.Userstatus;
import cc.gemii.po.UserstatusExample;

public interface UserstatusMapper {
    int countByExample(UserstatusExample example);

    int deleteByExample(UserstatusExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Userstatus record);

    int insertSelective(Userstatus record);

    List<Userstatus> selectByExample(UserstatusExample example);

    Userstatus selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Userstatus record, @Param("example") UserstatusExample example);

    int updateByExample(@Param("record") Userstatus record, @Param("example") UserstatusExample example);

    int updateByPrimaryKeySelective(Userstatus record);

    int updateByPrimaryKey(Userstatus record);
    
    Page<UserStatusCardVO> selectCardUserStatus(UserStatusCardProfile userStatusCardProfile);

	int updateFKUserinfoId(@Param("id") int id, @Param("userInfoId")int userInfoId);
	
	List<String> selectInfo(Integer id);

}