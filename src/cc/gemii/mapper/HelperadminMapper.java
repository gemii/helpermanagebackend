package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Helperadmin;
import cc.gemii.po.HelperadminExample;

public interface HelperadminMapper {
    int countByExample(HelperadminExample example);

    int deleteByExample(HelperadminExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Helperadmin record);

    int insertSelective(Helperadmin record);

    List<Helperadmin> selectByExample(HelperadminExample example);

    Helperadmin selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Helperadmin record, @Param("example") HelperadminExample example);

    int updateByExample(@Param("record") Helperadmin record, @Param("example") HelperadminExample example);

    int updateByPrimaryKeySelective(Helperadmin record);

    int updateByPrimaryKey(Helperadmin record);
}