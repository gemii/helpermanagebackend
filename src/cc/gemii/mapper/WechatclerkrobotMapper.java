package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatclerkrobot;
import cc.gemii.po.WechatclerkrobotExample;

public interface WechatclerkrobotMapper {
    int countByExample(WechatclerkrobotExample example);

    int deleteByExample(WechatclerkrobotExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Wechatclerkrobot record);

    int insertSelective(Wechatclerkrobot record);

    List<Wechatclerkrobot> selectByExample(WechatclerkrobotExample example);

    Wechatclerkrobot selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Wechatclerkrobot record, @Param("example") WechatclerkrobotExample example);

    int updateByExample(@Param("record") Wechatclerkrobot record, @Param("example") WechatclerkrobotExample example);

    int updateByPrimaryKeySelective(Wechatclerkrobot record);

    int updateByPrimaryKey(Wechatclerkrobot record);
}