package cc.gemii.mapper;

import java.util.List;

import cc.gemii.po.Wechatrobotinfo;
import cc.gemii.pojo.RepeatRecord;

public interface RobotStatusMapper {

	List<Wechatrobotinfo> getRobot(Integer id);

	List<RepeatRecord> getRepeatRecord(Integer id);

	List<Integer> getRepeatID(RepeatRecord record);
	
	

}
