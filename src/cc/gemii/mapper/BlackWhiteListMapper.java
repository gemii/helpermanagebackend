package cc.gemii.mapper;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.BlackWhiteListDO;

public interface BlackWhiteListMapper {

	public BlackWhiteListDO selectEntityByUserKeyAndFunCode(@Param("userKey") String userKey,@Param("funCode") String funCode);

}
