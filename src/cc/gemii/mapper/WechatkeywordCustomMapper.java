package cc.gemii.mapper;

import java.util.List;

import cc.gemii.pojo.KeywordCustom;
import cc.gemii.pojo.SelectKeyword;


public interface WechatkeywordCustomMapper {

	List<KeywordCustom> selectKeyword(SelectKeyword selectKeyword);
	
	int selectKeywordCount(SelectKeyword selectKeyword);

	int selectKeywordByCidCount(SelectKeyword selectKeyword);

	List<KeywordCustom> selectKeywordByCid(SelectKeyword selectKeyword);
    
}