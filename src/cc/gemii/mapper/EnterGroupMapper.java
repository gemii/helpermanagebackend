package cc.gemii.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import cc.gemii.data.otd.EnterGroupInfo;
import cc.gemii.data.otd.ErrorInfoToFriso;
import cc.gemii.po.Userinfo;
import cc.gemii.po.Userstatus;
import cc.gemii.po.Wechatroominfo;
import cc.gemii.pojo.GetAreaHosNameByCode;
import cc.gemii.pojo.GetRobotIdByU;
import cc.gemii.pojo.MatchGroup;
import cc.gemii.pojo.SelectUser;
import cc.gemii.pojo.UserFriendEnterGroup;

public interface EnterGroupMapper {

	List<String> getCity(String robotID);

	List<String> getAllCity();

	List<String> getHosByCity(Map<String, String> map);

	MatchGroup getMatchGroup(Map<String, String> map);
	
    String MatchGroup(Map<String, String> map);
	
	MatchGroup getCountryGroup(Map<String, String> map);

	MatchGroup getCityGroup(Map<String,String> map);

	Userstatus getLastRecord(Map<String, String> map);

	void updatePhoneCodeSendStatus(Map<String, String> param);

	Integer phoneExists(String phone);

	void insertHuggiesMember(Map<String, Object> param);

	Integer getHuggiesMemberCount();

	Integer openidExists(String openid);
	
	void updateUserRobot(Map<String, Object> param);

	/**
	 * 匹配好奇金牌妈妈要进入的群
	 * */
	List<String> matchGroupByHuggies(Map<String,String> params);
	MatchGroup getMatchGroupByHuggies(String params);
	int getHQUserCount(@Param("isLocal") boolean isLocal,@Param("roomID") String roomID);
	
	/**
	 * 惠氏卡券入群，群匹配sql
	 * */
	List<String> matchGroupByWyeth(Map<String,String> params);
	
	List<String> matchGroupByH5(Map<String,String> params);
	
	
	/**
	 * 
	 * 好育儿入群 爱婴岛入群 美素佳儿入群
	 * @param params
	 * @return
	 */
	List<String> matchGroupByGoodParent(Map<String,String> params);
	
	/**
	 * 药店入群
	 * */
	List<String> pharmacyMatchGroup(Map<String, String> params);
	
	List<String> pharmacyMatchGroupExt(Map<String, String> params);
	/**
	 * 
	 * 查询群信息
	 * @param params
	 * @return
	 */
	GetRobotIdByU matchGroupByLoveBaby(@Param("roomId")String roomId);
	
	
	
	
	void insertMemberCard(Map<String, Object> param);

	/**
	 * 接收信息插入userinfo
	 * @param param
	 */
	void insertUserInfo(Map<String, Object> param);
	
	/**
	 * 惠氏药店插入userinfo
	 * @param param
	 */
	void insertUserInfoParam(Map<String, Object> param);
	
	List<MatchGroup> getMatchGroupByUserInfo(@Param("roomId")String params,@Param("type")String type);
	
	Userinfo selectUserInfo(Map<String, Object> param);
	
	List<Userinfo> selectUser(Map<String, Object> param);
	
	/**
	 * 查询入群状态
	 * @param paramm
	 * @return
	 */
	List<SelectUser> selectStatus(@Param("openid")String paramm,@Param("type") String type);
	
	/**
	 * 根据用户昵称和群id查询群成员
	 * @param param
	 * @return
	 */
	String selectRoomInfo(Map<String,String> param);
	
	List<String> selectRoomSize(String userName);
	
	/**
	 * 根据群id查询u创群id
	 * @param roomId
	 * @return
	 */
	String selectUroomId(String roomId);
	
	/**
	 * 根据院code查询院信息
	 * @param code
	 * @param type
	 * @return
	 */
	String selectHospitalInfo(@Param("code")String code,@Param("type")String type);

	void updateUserInfo(@Param("openid")String openid,@Param("type")String type);
	
	String selectRobotOwner(String robotId);
	
	/**
	 * 根据门店code查询门店信息
	 * @param code
	 * @param level
	 * @return
	 */
	List<GetAreaHosNameByCode>  selectLyHospitalInfo(@Param("code")String code, @Param("level")String level);

	/**
	 * 亿贝入群
	 * @param params
	 * @return
	 */
	List<String> matchGroupByYiBei(Map<String, String> param);

	/**
	 * 乐茵入群
	 * @param params
	 * @return
	 */
	List<String> matchGroupByLeYin(Map<String, String> param);
	

	/**
	 * 根据uRoomID查找群信息
	 * @param params
	 * @return
	 */
	List<Wechatroominfo> selectRoomInfoById(@Param("uRoomID")String uRoomID);
	
	/**
	 * 景栗根据城市匹配群信息
	 * @param param
	 * @return
	 */
	String matchGroupByGemii(Map<String, String> param);
	/**
	 * 惠氏根据城市匹配群信息
	 * @param param
	 * @return
	 */
	String matchGroupByCityWyeth(Map<String, String> param);
	
	/**
	 * 通过手机号查询userinfo
	 * @param phone
	 * @return
	 */
	List<UserFriendEnterGroup> selectUserCity(@Param("phone")String phone,@Param("type")String type);
	/**
	 * 查询省份
	 * @return
	 */
	List<Map<String,String>> selectProvinceByGemii();
	
	/**
	 * 查询惠氏省份
	 * @return
	 */
	List<Map<String,String>> selectProvinceByWyeth();
	
	/**
	 * 接收美素信息插入userInfo
	 * @param param
	 */
	void insertMSUserInfo(Map<String, Object> param);
	
	/**
	 * 入群或退群，推送信息失败，存入数据库
	 * @param param
	 */
	void insertErrorInfo(ErrorInfoToFriso errorInfoToFriso);

	/**
	 * 查询推送失败信息
	 */
	List<ErrorInfoToFriso> selectErrorInfo();

	/**
	 * 更改错误信息status为2
	 */
	void updateErrorInfo(List<ErrorInfoToFriso> selectErrorInfo);

	/**
	 * 查询失败信息条数
	 */
	int countErrorInfo();
	
	/**
	 * 项目合作群
	 * @param param
	 * @return
	 */
	List<String> matchGroupByCooperation(Map<String, String> param);

	/**
	 * 根据openid查询userInfo
	 * @param openID
	 * @return
	 */
	List<Userinfo> selectUserInfoByOpenid(String openID);

	/**
	 * 美素佳儿匹配群
	 * @param param
	 * @return
	 */
	List<String> matchGroupByFriso(Map<String, String> param);

	/**
	 * 根据手机号查询userinfo
	 * @param param
	 * @return
	 */
	List<Userinfo> selectUserInfoByPhone(Map<String, String> param);

	/**
	 * 根据openid查询memberid
	 * @param openid
	 * @return
	 */
	List<String> selectMemberInfoByOpenid(String openid);
	
	SelectUser selectRepeatInfo(@Param("phone")String phone,@Param("type")String type);
	
	/**
	 * 根据群的信息，单表查群
	 * */
	List<String> simpleMatchGroup(Map<String, String> params);
}