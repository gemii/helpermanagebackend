package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatroommember;
import cc.gemii.po.WechatroommemberExample;

public interface WechatroommemberMapper {
    int countByExample(WechatroommemberExample example);

    int deleteByExample(WechatroommemberExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Wechatroommember record);

    int insertSelective(Wechatroommember record);

    List<Wechatroommember> selectByExampleWithBLOBs(WechatroommemberExample example);

    List<Wechatroommember> selectByExample(WechatroommemberExample example);

    Wechatroommember selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Wechatroommember record, @Param("example") WechatroommemberExample example);

    int updateByExampleWithBLOBs(@Param("record") Wechatroommember record, @Param("example") WechatroommemberExample example);

    int updateByExample(@Param("record") Wechatroommember record, @Param("example") WechatroommemberExample example);

    int updateByPrimaryKeySelective(Wechatroommember record);

    int updateByPrimaryKeyWithBLOBs(Wechatroommember record);
}