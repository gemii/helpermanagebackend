package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatroom;
import cc.gemii.po.WechatroomExample;

public interface WechatroomMapper {
    int countByExample(WechatroomExample example);

    int deleteByExample(WechatroomExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Wechatroom record);

    int insertSelective(Wechatroom record);

    List<Wechatroom> selectByExampleWithBLOBs(WechatroomExample example);

    List<Wechatroom> selectByExample(WechatroomExample example);

    Wechatroom selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Wechatroom record, @Param("example") WechatroomExample example);

    int updateByExampleWithBLOBs(@Param("record") Wechatroom record, @Param("example") WechatroomExample example);

    int updateByExample(@Param("record") Wechatroom record, @Param("example") WechatroomExample example);

    int updateByPrimaryKeySelective(Wechatroom record);

    int updateByPrimaryKeyWithBLOBs(Wechatroom record);
}