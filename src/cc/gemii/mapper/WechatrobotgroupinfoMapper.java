package cc.gemii.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cc.gemii.po.Wechatrobotgroupinfo;
import cc.gemii.po.WechatrobotgroupinfoExample;

public interface WechatrobotgroupinfoMapper {
    int countByExample(WechatrobotgroupinfoExample example);

    int deleteByExample(WechatrobotgroupinfoExample example);

    int insert(Wechatrobotgroupinfo record);

    int insertSelective(Wechatrobotgroupinfo record);

    List<Wechatrobotgroupinfo> selectByExampleWithBLOBs(WechatrobotgroupinfoExample example);

    List<Wechatrobotgroupinfo> selectByExample(WechatrobotgroupinfoExample example);

    int updateByExampleSelective(@Param("record") Wechatrobotgroupinfo record, @Param("example") WechatrobotgroupinfoExample example);

    int updateByExampleWithBLOBs(@Param("record") Wechatrobotgroupinfo record, @Param("example") WechatrobotgroupinfoExample example);

    int updateByExample(@Param("record") Wechatrobotgroupinfo record, @Param("example") WechatrobotgroupinfoExample example);
}