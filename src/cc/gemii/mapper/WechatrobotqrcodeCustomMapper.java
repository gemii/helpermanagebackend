package cc.gemii.mapper;

import cc.gemii.pojo.RobotQrcode;

public interface WechatrobotqrcodeCustomMapper {

	RobotQrcode getQrcodeByRid(String robotID);
    
}