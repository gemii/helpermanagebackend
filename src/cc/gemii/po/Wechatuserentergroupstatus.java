package cc.gemii.po;

public class Wechatuserentergroupstatus {
    private Integer id;

    private String grouptime;

    private String matchstatus;

    private String noticestatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGrouptime() {
        return grouptime;
    }

    public void setGrouptime(String grouptime) {
        this.grouptime = grouptime == null ? null : grouptime.trim();
    }

    public String getMatchstatus() {
        return matchstatus;
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus == null ? null : matchstatus.trim();
    }

    public String getNoticestatus() {
        return noticestatus;
    }

    public void setNoticestatus(String noticestatus) {
        this.noticestatus = noticestatus == null ? null : noticestatus.trim();
    }
}