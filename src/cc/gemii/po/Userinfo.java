package cc.gemii.po;

public class Userinfo {
    private Integer id;

    private String openid;

    private String phonenumber;

    private String formtime;

    private String province;

    private String city;

    private String hospitalkeyword;

    private String edc;

    private Integer userstatusid;

    private String srcode;
    
    private String type;
    
    private String matchGroupStatus;
    
    private String matchGroup;
    
    public String getMatchGroup() {
		return matchGroup;
	}

	public void setMatchGroup(String matchGroup) {
		this.matchGroup = matchGroup;
	}

	public String getMatchGroupStatus() {
		return matchGroupStatus;
	}

	public void setMatchGroupStatus(String matchGroupStatus) {
		this.matchGroupStatus = matchGroupStatus;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getMatchrobot() {
		return matchrobot;
	}

	public void setMatchrobot(String matchrobot) {
		this.matchrobot = matchrobot;
	}

	public String getMatchstatus() {
		return matchstatus;
	}

	public void setMatchstatus(String matchstatus) {
		this.matchstatus = matchstatus;
	}

    private String matchrobot;
    
    private String matchstatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber == null ? null : phonenumber.trim();
    }

    public String getFormtime() {
        return formtime;
    }

    public void setFormtime(String formtime) {
        this.formtime = formtime == null ? null : formtime.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getHospitalkeyword() {
        return hospitalkeyword;
    }

    public void setHospitalkeyword(String hospitalkeyword) {
        this.hospitalkeyword = hospitalkeyword == null ? null : hospitalkeyword.trim();
    }

    public String getEdc() {
        return edc;
    }

    public void setEdc(String edc) {
        this.edc = edc == null ? null : edc.trim();
    }

    public Integer getUserstatusid() {
        return userstatusid;
    }

    public void setUserstatusid(Integer userstatusid) {
        this.userstatusid = userstatusid;
    }

    public String getSrcode() {
        return srcode;
    }

    public void setSrcode(String srcode) {
        this.srcode = srcode == null ? null : srcode.trim();
    }
}