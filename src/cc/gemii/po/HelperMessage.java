package cc.gemii.po;

public class HelperMessage {
    private Integer id;

    private Integer helperId;

    private String nickname;

    private String handleStatus;

    private String handleResult;

    private String handleMethod;

    private String remindMessage;

    private String friendMessageTime;

    private Integer sendGroupId;

    private Integer matchGroupId;

    private String sendStatus;

    private String sendTime;

    private String remark;

    private String created;

    private String content;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHelperId() {
        return helperId;
    }

    public void setHelperId(Integer helperId) {
        this.helperId = helperId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(String handleStatus) {
        this.handleStatus = handleStatus == null ? null : handleStatus.trim();
    }

    public String getHandleResult() {
        return handleResult;
    }

    public void setHandleResult(String handleResult) {
        this.handleResult = handleResult == null ? null : handleResult.trim();
    }

    public String getHandleMethod() {
        return handleMethod;
    }

    public void setHandleMethod(String handleMethod) {
        this.handleMethod = handleMethod == null ? null : handleMethod.trim();
    }

    public String getRemindMessage() {
        return remindMessage;
    }

    public void setRemindMessage(String remindMessage) {
        this.remindMessage = remindMessage == null ? null : remindMessage.trim();
    }

    public String getFriendMessageTime() {
        return friendMessageTime;
    }

    public void setFriendMessageTime(String friendMessageTime) {
        this.friendMessageTime = friendMessageTime == null ? null : friendMessageTime.trim();
    }

    public Integer getSendGroupId() {
        return sendGroupId;
    }

    public void setSendGroupId(Integer sendGroupId) {
        this.sendGroupId = sendGroupId;
    }

    public Integer getMatchGroupId() {
        return matchGroupId;
    }

    public void setMatchGroupId(Integer matchGroupId) {
        this.matchGroupId = matchGroupId;
    }

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus == null ? null : sendStatus.trim();
    }

    public String getSendTime() {
        return sendTime;
    }

    public void setSendTime(String sendTime) {
        this.sendTime = sendTime == null ? null : sendTime.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created == null ? null : created.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }
}