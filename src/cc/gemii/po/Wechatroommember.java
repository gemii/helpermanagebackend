package cc.gemii.po;

public class Wechatroommember {
    private Integer id;

    private String roomid;

    private String memberid;

    private String membernickname;

    private String memberdisplayname;

    private String memberattrstatus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid == null ? null : memberid.trim();
    }

    public String getMembernickname() {
        return membernickname;
    }

    public void setMembernickname(String membernickname) {
        this.membernickname = membernickname == null ? null : membernickname.trim();
    }

    public String getMemberdisplayname() {
        return memberdisplayname;
    }

    public void setMemberdisplayname(String memberdisplayname) {
        this.memberdisplayname = memberdisplayname == null ? null : memberdisplayname.trim();
    }

    public String getMemberattrstatus() {
        return memberattrstatus;
    }

    public void setMemberattrstatus(String memberattrstatus) {
        this.memberattrstatus = memberattrstatus == null ? null : memberattrstatus.trim();
    }
}