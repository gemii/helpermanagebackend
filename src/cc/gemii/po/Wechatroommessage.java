package cc.gemii.po;

public class Wechatroommessage {
    private Integer id;

    private String msgid;

    private String roomownerid;

    private String roomname;

    private String usercount;

    private String fromusername;

    private String tousername;

    private String attrstatus;

    private String displayname;

    private String name;

    private String msgtype;

    private String facemsg;

    private String textmsg;

    private String imagemsg;

    private String videomsg;

    private String soundmsg;

    private String linkmsg;

    private String namecardmsg;

    private String locationmsg;

    private String recallmsgid;

    private String sysmsg;

    private String msgtime;

    private String msgtimestamp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid == null ? null : msgid.trim();
    }

    public String getRoomownerid() {
        return roomownerid;
    }

    public void setRoomownerid(String roomownerid) {
        this.roomownerid = roomownerid == null ? null : roomownerid.trim();
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname == null ? null : roomname.trim();
    }

    public String getUsercount() {
        return usercount;
    }

    public void setUsercount(String usercount) {
        this.usercount = usercount == null ? null : usercount.trim();
    }

    public String getFromusername() {
        return fromusername;
    }

    public void setFromusername(String fromusername) {
        this.fromusername = fromusername == null ? null : fromusername.trim();
    }

    public String getTousername() {
        return tousername;
    }

    public void setTousername(String tousername) {
        this.tousername = tousername == null ? null : tousername.trim();
    }

    public String getAttrstatus() {
        return attrstatus;
    }

    public void setAttrstatus(String attrstatus) {
        this.attrstatus = attrstatus == null ? null : attrstatus.trim();
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname == null ? null : displayname.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMsgtype() {
        return msgtype;
    }

    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype == null ? null : msgtype.trim();
    }

    public String getFacemsg() {
        return facemsg;
    }

    public void setFacemsg(String facemsg) {
        this.facemsg = facemsg == null ? null : facemsg.trim();
    }

    public String getTextmsg() {
        return textmsg;
    }

    public void setTextmsg(String textmsg) {
        this.textmsg = textmsg == null ? null : textmsg.trim();
    }

    public String getImagemsg() {
        return imagemsg;
    }

    public void setImagemsg(String imagemsg) {
        this.imagemsg = imagemsg == null ? null : imagemsg.trim();
    }

    public String getVideomsg() {
        return videomsg;
    }

    public void setVideomsg(String videomsg) {
        this.videomsg = videomsg == null ? null : videomsg.trim();
    }

    public String getSoundmsg() {
        return soundmsg;
    }

    public void setSoundmsg(String soundmsg) {
        this.soundmsg = soundmsg == null ? null : soundmsg.trim();
    }

    public String getLinkmsg() {
        return linkmsg;
    }

    public void setLinkmsg(String linkmsg) {
        this.linkmsg = linkmsg == null ? null : linkmsg.trim();
    }

    public String getNamecardmsg() {
        return namecardmsg;
    }

    public void setNamecardmsg(String namecardmsg) {
        this.namecardmsg = namecardmsg == null ? null : namecardmsg.trim();
    }

    public String getLocationmsg() {
        return locationmsg;
    }

    public void setLocationmsg(String locationmsg) {
        this.locationmsg = locationmsg == null ? null : locationmsg.trim();
    }

    public String getRecallmsgid() {
        return recallmsgid;
    }

    public void setRecallmsgid(String recallmsgid) {
        this.recallmsgid = recallmsgid == null ? null : recallmsgid.trim();
    }

    public String getSysmsg() {
        return sysmsg;
    }

    public void setSysmsg(String sysmsg) {
        this.sysmsg = sysmsg == null ? null : sysmsg.trim();
    }

    public String getMsgtime() {
        return msgtime;
    }

    public void setMsgtime(String msgtime) {
        this.msgtime = msgtime == null ? null : msgtime.trim();
    }

    public String getMsgtimestamp() {
        return msgtimestamp;
    }

    public void setMsgtimestamp(String msgtimestamp) {
        this.msgtimestamp = msgtimestamp == null ? null : msgtimestamp.trim();
    }
}