package cc.gemii.po;

public class Wechatclerkrobot {
    private Integer id;

    private Integer clerkid;

    private String robottag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClerkid() {
        return clerkid;
    }

    public void setClerkid(Integer clerkid) {
        this.clerkid = clerkid;
    }

    public String getRobottag() {
        return robottag;
    }

    public void setRobottag(String robottag) {
        this.robottag = robottag == null ? null : robottag.trim();
    }
}