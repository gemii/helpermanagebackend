package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class UserstatusExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserstatusExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andRobotidIsNull() {
            addCriterion("RobotID is null");
            return (Criteria) this;
        }

        public Criteria andRobotidIsNotNull() {
            addCriterion("RobotID is not null");
            return (Criteria) this;
        }

        public Criteria andRobotidEqualTo(String value) {
            addCriterion("RobotID =", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotEqualTo(String value) {
            addCriterion("RobotID <>", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidGreaterThan(String value) {
            addCriterion("RobotID >", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidGreaterThanOrEqualTo(String value) {
            addCriterion("RobotID >=", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLessThan(String value) {
            addCriterion("RobotID <", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLessThanOrEqualTo(String value) {
            addCriterion("RobotID <=", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLike(String value) {
            addCriterion("RobotID like", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotLike(String value) {
            addCriterion("RobotID not like", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidIn(List<String> values) {
            addCriterion("RobotID in", values, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotIn(List<String> values) {
            addCriterion("RobotID not in", values, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidBetween(String value1, String value2) {
            addCriterion("RobotID between", value1, value2, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotBetween(String value1, String value2) {
            addCriterion("RobotID not between", value1, value2, "robotid");
            return (Criteria) this;
        }

        public Criteria andPhonecodeIsNull() {
            addCriterion("PhoneCode is null");
            return (Criteria) this;
        }

        public Criteria andPhonecodeIsNotNull() {
            addCriterion("PhoneCode is not null");
            return (Criteria) this;
        }

        public Criteria andPhonecodeEqualTo(String value) {
            addCriterion("PhoneCode =", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeNotEqualTo(String value) {
            addCriterion("PhoneCode <>", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeGreaterThan(String value) {
            addCriterion("PhoneCode >", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeGreaterThanOrEqualTo(String value) {
            addCriterion("PhoneCode >=", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeLessThan(String value) {
            addCriterion("PhoneCode <", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeLessThanOrEqualTo(String value) {
            addCriterion("PhoneCode <=", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeLike(String value) {
            addCriterion("PhoneCode like", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeNotLike(String value) {
            addCriterion("PhoneCode not like", value, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeIn(List<String> values) {
            addCriterion("PhoneCode in", values, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeNotIn(List<String> values) {
            addCriterion("PhoneCode not in", values, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeBetween(String value1, String value2) {
            addCriterion("PhoneCode between", value1, value2, "phonecode");
            return (Criteria) this;
        }

        public Criteria andPhonecodeNotBetween(String value1, String value2) {
            addCriterion("PhoneCode not between", value1, value2, "phonecode");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("UserName is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("UserName is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("UserName =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("UserName <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("UserName >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("UserName >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("UserName <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("UserName <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("UserName like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("UserName not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("UserName in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("UserName not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("UserName between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("UserName not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("UserID is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("UserID is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(String value) {
            addCriterion("UserID =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(String value) {
            addCriterion("UserID <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(String value) {
            addCriterion("UserID >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(String value) {
            addCriterion("UserID >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(String value) {
            addCriterion("UserID <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(String value) {
            addCriterion("UserID <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLike(String value) {
            addCriterion("UserID like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotLike(String value) {
            addCriterion("UserID not like", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<String> values) {
            addCriterion("UserID in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<String> values) {
            addCriterion("UserID not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(String value1, String value2) {
            addCriterion("UserID between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(String value1, String value2) {
            addCriterion("UserID not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andFriendtimeIsNull() {
            addCriterion("FriendTime is null");
            return (Criteria) this;
        }

        public Criteria andFriendtimeIsNotNull() {
            addCriterion("FriendTime is not null");
            return (Criteria) this;
        }

        public Criteria andFriendtimeEqualTo(String value) {
            addCriterion("FriendTime =", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeNotEqualTo(String value) {
            addCriterion("FriendTime <>", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeGreaterThan(String value) {
            addCriterion("FriendTime >", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeGreaterThanOrEqualTo(String value) {
            addCriterion("FriendTime >=", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeLessThan(String value) {
            addCriterion("FriendTime <", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeLessThanOrEqualTo(String value) {
            addCriterion("FriendTime <=", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeLike(String value) {
            addCriterion("FriendTime like", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeNotLike(String value) {
            addCriterion("FriendTime not like", value, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeIn(List<String> values) {
            addCriterion("FriendTime in", values, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeNotIn(List<String> values) {
            addCriterion("FriendTime not in", values, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeBetween(String value1, String value2) {
            addCriterion("FriendTime between", value1, value2, "friendtime");
            return (Criteria) this;
        }

        public Criteria andFriendtimeNotBetween(String value1, String value2) {
            addCriterion("FriendTime not between", value1, value2, "friendtime");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusIsNull() {
            addCriterion("ConfirmStatus is null");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusIsNotNull() {
            addCriterion("ConfirmStatus is not null");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusEqualTo(String value) {
            addCriterion("ConfirmStatus =", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusNotEqualTo(String value) {
            addCriterion("ConfirmStatus <>", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusGreaterThan(String value) {
            addCriterion("ConfirmStatus >", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusGreaterThanOrEqualTo(String value) {
            addCriterion("ConfirmStatus >=", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusLessThan(String value) {
            addCriterion("ConfirmStatus <", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusLessThanOrEqualTo(String value) {
            addCriterion("ConfirmStatus <=", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusLike(String value) {
            addCriterion("ConfirmStatus like", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusNotLike(String value) {
            addCriterion("ConfirmStatus not like", value, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusIn(List<String> values) {
            addCriterion("ConfirmStatus in", values, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusNotIn(List<String> values) {
            addCriterion("ConfirmStatus not in", values, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusBetween(String value1, String value2) {
            addCriterion("ConfirmStatus between", value1, value2, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andConfirmstatusNotBetween(String value1, String value2) {
            addCriterion("ConfirmStatus not between", value1, value2, "confirmstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusIsNull() {
            addCriterion("MatchStatus is null");
            return (Criteria) this;
        }

        public Criteria andMatchstatusIsNotNull() {
            addCriterion("MatchStatus is not null");
            return (Criteria) this;
        }

        public Criteria andMatchstatusEqualTo(String value) {
            addCriterion("MatchStatus =", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusNotEqualTo(String value) {
            addCriterion("MatchStatus <>", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusGreaterThan(String value) {
            addCriterion("MatchStatus >", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusGreaterThanOrEqualTo(String value) {
            addCriterion("MatchStatus >=", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusLessThan(String value) {
            addCriterion("MatchStatus <", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusLessThanOrEqualTo(String value) {
            addCriterion("MatchStatus <=", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusLike(String value) {
            addCriterion("MatchStatus like", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusNotLike(String value) {
            addCriterion("MatchStatus not like", value, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusIn(List<String> values) {
            addCriterion("MatchStatus in", values, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusNotIn(List<String> values) {
            addCriterion("MatchStatus not in", values, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusBetween(String value1, String value2) {
            addCriterion("MatchStatus between", value1, value2, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andMatchstatusNotBetween(String value1, String value2) {
            addCriterion("MatchStatus not between", value1, value2, "matchstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusIsNull() {
            addCriterion("FriendStatus is null");
            return (Criteria) this;
        }

        public Criteria andFriendstatusIsNotNull() {
            addCriterion("FriendStatus is not null");
            return (Criteria) this;
        }

        public Criteria andFriendstatusEqualTo(String value) {
            addCriterion("FriendStatus =", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusNotEqualTo(String value) {
            addCriterion("FriendStatus <>", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusGreaterThan(String value) {
            addCriterion("FriendStatus >", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusGreaterThanOrEqualTo(String value) {
            addCriterion("FriendStatus >=", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusLessThan(String value) {
            addCriterion("FriendStatus <", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusLessThanOrEqualTo(String value) {
            addCriterion("FriendStatus <=", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusLike(String value) {
            addCriterion("FriendStatus like", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusNotLike(String value) {
            addCriterion("FriendStatus not like", value, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusIn(List<String> values) {
            addCriterion("FriendStatus in", values, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusNotIn(List<String> values) {
            addCriterion("FriendStatus not in", values, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusBetween(String value1, String value2) {
            addCriterion("FriendStatus between", value1, value2, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andFriendstatusNotBetween(String value1, String value2) {
            addCriterion("FriendStatus not between", value1, value2, "friendstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusIsNull() {
            addCriterion("EnterGroupStatus is null");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusIsNotNull() {
            addCriterion("EnterGroupStatus is not null");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusEqualTo(String value) {
            addCriterion("EnterGroupStatus =", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusNotEqualTo(String value) {
            addCriterion("EnterGroupStatus <>", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusGreaterThan(String value) {
            addCriterion("EnterGroupStatus >", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusGreaterThanOrEqualTo(String value) {
            addCriterion("EnterGroupStatus >=", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusLessThan(String value) {
            addCriterion("EnterGroupStatus <", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusLessThanOrEqualTo(String value) {
            addCriterion("EnterGroupStatus <=", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusLike(String value) {
            addCriterion("EnterGroupStatus like", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusNotLike(String value) {
            addCriterion("EnterGroupStatus not like", value, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusIn(List<String> values) {
            addCriterion("EnterGroupStatus in", values, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusNotIn(List<String> values) {
            addCriterion("EnterGroupStatus not in", values, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusBetween(String value1, String value2) {
            addCriterion("EnterGroupStatus between", value1, value2, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andEntergroupstatusNotBetween(String value1, String value2) {
            addCriterion("EnterGroupStatus not between", value1, value2, "entergroupstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusIsNull() {
            addCriterion("UrlStatus is null");
            return (Criteria) this;
        }

        public Criteria andUrlstatusIsNotNull() {
            addCriterion("UrlStatus is not null");
            return (Criteria) this;
        }

        public Criteria andUrlstatusEqualTo(String value) {
            addCriterion("UrlStatus =", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusNotEqualTo(String value) {
            addCriterion("UrlStatus <>", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusGreaterThan(String value) {
            addCriterion("UrlStatus >", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusGreaterThanOrEqualTo(String value) {
            addCriterion("UrlStatus >=", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusLessThan(String value) {
            addCriterion("UrlStatus <", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusLessThanOrEqualTo(String value) {
            addCriterion("UrlStatus <=", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusLike(String value) {
            addCriterion("UrlStatus like", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusNotLike(String value) {
            addCriterion("UrlStatus not like", value, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusIn(List<String> values) {
            addCriterion("UrlStatus in", values, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusNotIn(List<String> values) {
            addCriterion("UrlStatus not in", values, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusBetween(String value1, String value2) {
            addCriterion("UrlStatus between", value1, value2, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andUrlstatusNotBetween(String value1, String value2) {
            addCriterion("UrlStatus not between", value1, value2, "urlstatus");
            return (Criteria) this;
        }

        public Criteria andMatchgroupIsNull() {
            addCriterion("MatchGroup is null");
            return (Criteria) this;
        }

        public Criteria andMatchgroupIsNotNull() {
            addCriterion("MatchGroup is not null");
            return (Criteria) this;
        }

        public Criteria andMatchgroupEqualTo(String value) {
            addCriterion("MatchGroup =", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupNotEqualTo(String value) {
            addCriterion("MatchGroup <>", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupGreaterThan(String value) {
            addCriterion("MatchGroup >", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupGreaterThanOrEqualTo(String value) {
            addCriterion("MatchGroup >=", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupLessThan(String value) {
            addCriterion("MatchGroup <", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupLessThanOrEqualTo(String value) {
            addCriterion("MatchGroup <=", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupLike(String value) {
            addCriterion("MatchGroup like", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupNotLike(String value) {
            addCriterion("MatchGroup not like", value, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupIn(List<String> values) {
            addCriterion("MatchGroup in", values, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupNotIn(List<String> values) {
            addCriterion("MatchGroup not in", values, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupBetween(String value1, String value2) {
            addCriterion("MatchGroup between", value1, value2, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andMatchgroupNotBetween(String value1, String value2) {
            addCriterion("MatchGroup not between", value1, value2, "matchgroup");
            return (Criteria) this;
        }

        public Criteria andH5statusIsNull() {
            addCriterion("H5Status is null");
            return (Criteria) this;
        }

        public Criteria andH5statusIsNotNull() {
            addCriterion("H5Status is not null");
            return (Criteria) this;
        }

        public Criteria andH5statusEqualTo(String value) {
            addCriterion("H5Status =", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusNotEqualTo(String value) {
            addCriterion("H5Status <>", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusGreaterThan(String value) {
            addCriterion("H5Status >", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusGreaterThanOrEqualTo(String value) {
            addCriterion("H5Status >=", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusLessThan(String value) {
            addCriterion("H5Status <", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusLessThanOrEqualTo(String value) {
            addCriterion("H5Status <=", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusLike(String value) {
            addCriterion("H5Status like", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusNotLike(String value) {
            addCriterion("H5Status not like", value, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusIn(List<String> values) {
            addCriterion("H5Status in", values, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusNotIn(List<String> values) {
            addCriterion("H5Status not in", values, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusBetween(String value1, String value2) {
            addCriterion("H5Status between", value1, value2, "h5status");
            return (Criteria) this;
        }

        public Criteria andH5statusNotBetween(String value1, String value2) {
            addCriterion("H5Status not between", value1, value2, "h5status");
            return (Criteria) this;
        }

        public Criteria andGrouptimeIsNull() {
            addCriterion("GroupTime is null");
            return (Criteria) this;
        }

        public Criteria andGrouptimeIsNotNull() {
            addCriterion("GroupTime is not null");
            return (Criteria) this;
        }

        public Criteria andGrouptimeEqualTo(String value) {
            addCriterion("GroupTime =", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeNotEqualTo(String value) {
            addCriterion("GroupTime <>", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeGreaterThan(String value) {
            addCriterion("GroupTime >", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeGreaterThanOrEqualTo(String value) {
            addCriterion("GroupTime >=", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeLessThan(String value) {
            addCriterion("GroupTime <", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeLessThanOrEqualTo(String value) {
            addCriterion("GroupTime <=", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeLike(String value) {
            addCriterion("GroupTime like", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeNotLike(String value) {
            addCriterion("GroupTime not like", value, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeIn(List<String> values) {
            addCriterion("GroupTime in", values, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeNotIn(List<String> values) {
            addCriterion("GroupTime not in", values, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeBetween(String value1, String value2) {
            addCriterion("GroupTime between", value1, value2, "grouptime");
            return (Criteria) this;
        }

        public Criteria andGrouptimeNotBetween(String value1, String value2) {
            addCriterion("GroupTime not between", value1, value2, "grouptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusIsNull() {
            addCriterion("PhoneCodeVerifyStatus is null");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusIsNotNull() {
            addCriterion("PhoneCodeVerifyStatus is not null");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusEqualTo(String value) {
            addCriterion("PhoneCodeVerifyStatus =", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusNotEqualTo(String value) {
            addCriterion("PhoneCodeVerifyStatus <>", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusGreaterThan(String value) {
            addCriterion("PhoneCodeVerifyStatus >", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusGreaterThanOrEqualTo(String value) {
            addCriterion("PhoneCodeVerifyStatus >=", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusLessThan(String value) {
            addCriterion("PhoneCodeVerifyStatus <", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusLessThanOrEqualTo(String value) {
            addCriterion("PhoneCodeVerifyStatus <=", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusLike(String value) {
            addCriterion("PhoneCodeVerifyStatus like", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusNotLike(String value) {
            addCriterion("PhoneCodeVerifyStatus not like", value, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusIn(List<String> values) {
            addCriterion("PhoneCodeVerifyStatus in", values, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusNotIn(List<String> values) {
            addCriterion("PhoneCodeVerifyStatus not in", values, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusBetween(String value1, String value2) {
            addCriterion("PhoneCodeVerifyStatus between", value1, value2, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeverifystatusNotBetween(String value1, String value2) {
            addCriterion("PhoneCodeVerifyStatus not between", value1, value2, "phonecodeverifystatus");
            return (Criteria) this;
        }

        public Criteria andRemindmessageIsNull() {
            addCriterion("RemindMessage is null");
            return (Criteria) this;
        }

        public Criteria andRemindmessageIsNotNull() {
            addCriterion("RemindMessage is not null");
            return (Criteria) this;
        }

        public Criteria andRemindmessageEqualTo(String value) {
            addCriterion("RemindMessage =", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageNotEqualTo(String value) {
            addCriterion("RemindMessage <>", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageGreaterThan(String value) {
            addCriterion("RemindMessage >", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageGreaterThanOrEqualTo(String value) {
            addCriterion("RemindMessage >=", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageLessThan(String value) {
            addCriterion("RemindMessage <", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageLessThanOrEqualTo(String value) {
            addCriterion("RemindMessage <=", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageLike(String value) {
            addCriterion("RemindMessage like", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageNotLike(String value) {
            addCriterion("RemindMessage not like", value, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageIn(List<String> values) {
            addCriterion("RemindMessage in", values, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageNotIn(List<String> values) {
            addCriterion("RemindMessage not in", values, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageBetween(String value1, String value2) {
            addCriterion("RemindMessage between", value1, value2, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andRemindmessageNotBetween(String value1, String value2) {
            addCriterion("RemindMessage not between", value1, value2, "remindmessage");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusIsNull() {
            addCriterion("PhoneCodeSendStatus is null");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusIsNotNull() {
            addCriterion("PhoneCodeSendStatus is not null");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusEqualTo(String value) {
            addCriterion("PhoneCodeSendStatus =", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusNotEqualTo(String value) {
            addCriterion("PhoneCodeSendStatus <>", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusGreaterThan(String value) {
            addCriterion("PhoneCodeSendStatus >", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusGreaterThanOrEqualTo(String value) {
            addCriterion("PhoneCodeSendStatus >=", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusLessThan(String value) {
            addCriterion("PhoneCodeSendStatus <", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusLessThanOrEqualTo(String value) {
            addCriterion("PhoneCodeSendStatus <=", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusLike(String value) {
            addCriterion("PhoneCodeSendStatus like", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusNotLike(String value) {
            addCriterion("PhoneCodeSendStatus not like", value, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusIn(List<String> values) {
            addCriterion("PhoneCodeSendStatus in", values, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusNotIn(List<String> values) {
            addCriterion("PhoneCodeSendStatus not in", values, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusBetween(String value1, String value2) {
            addCriterion("PhoneCodeSendStatus between", value1, value2, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodesendstatusNotBetween(String value1, String value2) {
            addCriterion("PhoneCodeSendStatus not between", value1, value2, "phonecodesendstatus");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeIsNull() {
            addCriterion("PhoneCodeUpTime is null");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeIsNotNull() {
            addCriterion("PhoneCodeUpTime is not null");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeEqualTo(String value) {
            addCriterion("PhoneCodeUpTime =", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeNotEqualTo(String value) {
            addCriterion("PhoneCodeUpTime <>", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeGreaterThan(String value) {
            addCriterion("PhoneCodeUpTime >", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeGreaterThanOrEqualTo(String value) {
            addCriterion("PhoneCodeUpTime >=", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeLessThan(String value) {
            addCriterion("PhoneCodeUpTime <", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeLessThanOrEqualTo(String value) {
            addCriterion("PhoneCodeUpTime <=", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeLike(String value) {
            addCriterion("PhoneCodeUpTime like", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeNotLike(String value) {
            addCriterion("PhoneCodeUpTime not like", value, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeIn(List<String> values) {
            addCriterion("PhoneCodeUpTime in", values, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeNotIn(List<String> values) {
            addCriterion("PhoneCodeUpTime not in", values, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeBetween(String value1, String value2) {
            addCriterion("PhoneCodeUpTime between", value1, value2, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andPhonecodeuptimeNotBetween(String value1, String value2) {
            addCriterion("PhoneCodeUpTime not between", value1, value2, "phonecodeuptime");
            return (Criteria) this;
        }

        public Criteria andNoticestatusIsNull() {
            addCriterion("NoticeStatus is null");
            return (Criteria) this;
        }

        public Criteria andNoticestatusIsNotNull() {
            addCriterion("NoticeStatus is not null");
            return (Criteria) this;
        }

        public Criteria andNoticestatusEqualTo(String value) {
            addCriterion("NoticeStatus =", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusNotEqualTo(String value) {
            addCriterion("NoticeStatus <>", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusGreaterThan(String value) {
            addCriterion("NoticeStatus >", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusGreaterThanOrEqualTo(String value) {
            addCriterion("NoticeStatus >=", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusLessThan(String value) {
            addCriterion("NoticeStatus <", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusLessThanOrEqualTo(String value) {
            addCriterion("NoticeStatus <=", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusLike(String value) {
            addCriterion("NoticeStatus like", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusNotLike(String value) {
            addCriterion("NoticeStatus not like", value, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusIn(List<String> values) {
            addCriterion("NoticeStatus in", values, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusNotIn(List<String> values) {
            addCriterion("NoticeStatus not in", values, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusBetween(String value1, String value2) {
            addCriterion("NoticeStatus between", value1, value2, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andNoticestatusNotBetween(String value1, String value2) {
            addCriterion("NoticeStatus not between", value1, value2, "noticestatus");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("Type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("Type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(String value) {
            addCriterion("Type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(String value) {
            addCriterion("Type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(String value) {
            addCriterion("Type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(String value) {
            addCriterion("Type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(String value) {
            addCriterion("Type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(String value) {
            addCriterion("Type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLike(String value) {
            addCriterion("Type like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotLike(String value) {
            addCriterion("Type not like", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<String> values) {
            addCriterion("Type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<String> values) {
            addCriterion("Type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(String value1, String value2) {
            addCriterion("Type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(String value1, String value2) {
            addCriterion("Type not between", value1, value2, "type");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}