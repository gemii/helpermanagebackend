package cc.gemii.po;

public class Userstatus {
    private Integer id;

    private String robotid;

    private String phonecode;

    private String username;

    private String userid;

    private String friendtime;

    private String confirmstatus;

    private String matchstatus;

    private String friendstatus;

    private String entergroupstatus;

    private String urlstatus;

    private String matchgroup;

    private String h5status;

    private String grouptime;

    private String phonecodeverifystatus;

    private String remindmessage;

    private String phonecodesendstatus;

    private String phonecodeuptime;

    private String noticestatus;

    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRobotid() {
        return robotid;
    }

    public void setRobotid(String robotid) {
        this.robotid = robotid == null ? null : robotid.trim();
    }

    public String getPhonecode() {
        return phonecode;
    }

    public void setPhonecode(String phonecode) {
        this.phonecode = phonecode == null ? null : phonecode.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getFriendtime() {
        return friendtime;
    }

    public void setFriendtime(String friendtime) {
        this.friendtime = friendtime == null ? null : friendtime.trim();
    }

    public String getConfirmstatus() {
        return confirmstatus;
    }

    public void setConfirmstatus(String confirmstatus) {
        this.confirmstatus = confirmstatus == null ? null : confirmstatus.trim();
    }

    public String getMatchstatus() {
        return matchstatus;
    }

    public void setMatchstatus(String matchstatus) {
        this.matchstatus = matchstatus == null ? null : matchstatus.trim();
    }

    public String getFriendstatus() {
        return friendstatus;
    }

    public void setFriendstatus(String friendstatus) {
        this.friendstatus = friendstatus == null ? null : friendstatus.trim();
    }

    public String getEntergroupstatus() {
        return entergroupstatus;
    }

    public void setEntergroupstatus(String entergroupstatus) {
        this.entergroupstatus = entergroupstatus == null ? null : entergroupstatus.trim();
    }

    public String getUrlstatus() {
        return urlstatus;
    }

    public void setUrlstatus(String urlstatus) {
        this.urlstatus = urlstatus == null ? null : urlstatus.trim();
    }

    public String getMatchgroup() {
        return matchgroup;
    }

    public void setMatchgroup(String matchgroup) {
        this.matchgroup = matchgroup == null ? null : matchgroup.trim();
    }

    public String getH5status() {
        return h5status;
    }

    public void setH5status(String h5status) {
        this.h5status = h5status == null ? null : h5status.trim();
    }

    public String getGrouptime() {
        return grouptime;
    }

    public void setGrouptime(String grouptime) {
        this.grouptime = grouptime == null ? null : grouptime.trim();
    }

    public String getPhonecodeverifystatus() {
        return phonecodeverifystatus;
    }

    public void setPhonecodeverifystatus(String phonecodeverifystatus) {
        this.phonecodeverifystatus = phonecodeverifystatus == null ? null : phonecodeverifystatus.trim();
    }

    public String getRemindmessage() {
        return remindmessage;
    }

    public void setRemindmessage(String remindmessage) {
        this.remindmessage = remindmessage == null ? null : remindmessage.trim();
    }

    public String getPhonecodesendstatus() {
        return phonecodesendstatus;
    }

    public void setPhonecodesendstatus(String phonecodesendstatus) {
        this.phonecodesendstatus = phonecodesendstatus == null ? null : phonecodesendstatus.trim();
    }

    public String getPhonecodeuptime() {
        return phonecodeuptime;
    }

    public void setPhonecodeuptime(String phonecodeuptime) {
        this.phonecodeuptime = phonecodeuptime == null ? null : phonecodeuptime.trim();
    }

    public String getNoticestatus() {
        return noticestatus;
    }

    public void setNoticestatus(String noticestatus) {
        this.noticestatus = noticestatus == null ? null : noticestatus.trim();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }
}