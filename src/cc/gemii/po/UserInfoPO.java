package cc.gemii.po;

import java.io.Serializable;

public class UserInfoPO implements Serializable{
	private Integer id;
	private String openId;
	private String phoneNumber;
	private String formTime;
	private String province;
	private String city;
	private String hospitalKeyword;
	private String edc;
	private String userStatusID;
	private String srCode;
	private String type;
	private String matchGroup;
	private String matchRobot;
	private String matchStatus;
	private String userName;
	private String uUserID;
	private String name;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOpenId() {
		return openId;
	}
	public void setOpenId(String openId) {
		this.openId = openId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getFormTime() {
		return formTime;
	}
	public void setFormTime(String formTime) {
		this.formTime = formTime;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getHospitalKeyword() {
		return hospitalKeyword;
	}
	public void setHospitalKeyword(String hospitalKeyword) {
		this.hospitalKeyword = hospitalKeyword;
	}
	public String getEdc() {
		return edc;
	}
	public void setEdc(String edc) {
		this.edc = edc;
	}
	public String getUserStatusID() {
		return userStatusID;
	}
	public void setUserStatusID(String userStatusID) {
		this.userStatusID = userStatusID;
	}
	public String getSrCode() {
		return srCode;
	}
	public void setSrCode(String srCode) {
		this.srCode = srCode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMatchGroup() {
		return matchGroup;
	}
	public void setMatchGroup(String matchGroup) {
		this.matchGroup = matchGroup;
	}
	public String getMatchRobot() {
		return matchRobot;
	}
	public void setMatchRobot(String matchRobot) {
		this.matchRobot = matchRobot;
	}
	public String getMatchStatus() {
		return matchStatus;
	}
	public void setMatchStatus(String matchStatus) {
		this.matchStatus = matchStatus;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getuUserID() {
		return uUserID;
	}
	public void setuUserID(String uUserID) {
		this.uUserID = uUserID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
