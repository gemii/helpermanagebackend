package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class WechatroominfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public WechatroominfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRoomidIsNull() {
            addCriterion("RoomID is null");
            return (Criteria) this;
        }

        public Criteria andRoomidIsNotNull() {
            addCriterion("RoomID is not null");
            return (Criteria) this;
        }

        public Criteria andRoomidEqualTo(String value) {
            addCriterion("RoomID =", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotEqualTo(String value) {
            addCriterion("RoomID <>", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThan(String value) {
            addCriterion("RoomID >", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidGreaterThanOrEqualTo(String value) {
            addCriterion("RoomID >=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThan(String value) {
            addCriterion("RoomID <", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLessThanOrEqualTo(String value) {
            addCriterion("RoomID <=", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidLike(String value) {
            addCriterion("RoomID like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotLike(String value) {
            addCriterion("RoomID not like", value, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidIn(List<String> values) {
            addCriterion("RoomID in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotIn(List<String> values) {
            addCriterion("RoomID not in", values, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidBetween(String value1, String value2) {
            addCriterion("RoomID between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomidNotBetween(String value1, String value2) {
            addCriterion("RoomID not between", value1, value2, "roomid");
            return (Criteria) this;
        }

        public Criteria andRoomnameIsNull() {
            addCriterion("RoomName is null");
            return (Criteria) this;
        }

        public Criteria andRoomnameIsNotNull() {
            addCriterion("RoomName is not null");
            return (Criteria) this;
        }

        public Criteria andRoomnameEqualTo(String value) {
            addCriterion("RoomName =", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameNotEqualTo(String value) {
            addCriterion("RoomName <>", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameGreaterThan(String value) {
            addCriterion("RoomName >", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameGreaterThanOrEqualTo(String value) {
            addCriterion("RoomName >=", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameLessThan(String value) {
            addCriterion("RoomName <", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameLessThanOrEqualTo(String value) {
            addCriterion("RoomName <=", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameLike(String value) {
            addCriterion("RoomName like", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameNotLike(String value) {
            addCriterion("RoomName not like", value, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameIn(List<String> values) {
            addCriterion("RoomName in", values, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameNotIn(List<String> values) {
            addCriterion("RoomName not in", values, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameBetween(String value1, String value2) {
            addCriterion("RoomName between", value1, value2, "roomname");
            return (Criteria) this;
        }

        public Criteria andRoomnameNotBetween(String value1, String value2) {
            addCriterion("RoomName not between", value1, value2, "roomname");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("city is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("city is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("city =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("city <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("city >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("city >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("city <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("city <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("city like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("city not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("city in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("city not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("city between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("city not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andDepartmentIsNull() {
            addCriterion("department is null");
            return (Criteria) this;
        }

        public Criteria andDepartmentIsNotNull() {
            addCriterion("department is not null");
            return (Criteria) this;
        }

        public Criteria andDepartmentEqualTo(String value) {
            addCriterion("department =", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotEqualTo(String value) {
            addCriterion("department <>", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentGreaterThan(String value) {
            addCriterion("department >", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentGreaterThanOrEqualTo(String value) {
            addCriterion("department >=", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLessThan(String value) {
            addCriterion("department <", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLessThanOrEqualTo(String value) {
            addCriterion("department <=", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentLike(String value) {
            addCriterion("department like", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotLike(String value) {
            addCriterion("department not like", value, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentIn(List<String> values) {
            addCriterion("department in", values, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotIn(List<String> values) {
            addCriterion("department not in", values, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentBetween(String value1, String value2) {
            addCriterion("department between", value1, value2, "department");
            return (Criteria) this;
        }

        public Criteria andDepartmentNotBetween(String value1, String value2) {
            addCriterion("department not between", value1, value2, "department");
            return (Criteria) this;
        }

        public Criteria andSubnameIsNull() {
            addCriterion("subName is null");
            return (Criteria) this;
        }

        public Criteria andSubnameIsNotNull() {
            addCriterion("subName is not null");
            return (Criteria) this;
        }

        public Criteria andSubnameEqualTo(String value) {
            addCriterion("subName =", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameNotEqualTo(String value) {
            addCriterion("subName <>", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameGreaterThan(String value) {
            addCriterion("subName >", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameGreaterThanOrEqualTo(String value) {
            addCriterion("subName >=", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameLessThan(String value) {
            addCriterion("subName <", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameLessThanOrEqualTo(String value) {
            addCriterion("subName <=", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameLike(String value) {
            addCriterion("subName like", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameNotLike(String value) {
            addCriterion("subName not like", value, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameIn(List<String> values) {
            addCriterion("subName in", values, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameNotIn(List<String> values) {
            addCriterion("subName not in", values, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameBetween(String value1, String value2) {
            addCriterion("subName between", value1, value2, "subname");
            return (Criteria) this;
        }

        public Criteria andSubnameNotBetween(String value1, String value2) {
            addCriterion("subName not between", value1, value2, "subname");
            return (Criteria) this;
        }

        public Criteria andChannelIsNull() {
            addCriterion("channel is null");
            return (Criteria) this;
        }

        public Criteria andChannelIsNotNull() {
            addCriterion("channel is not null");
            return (Criteria) this;
        }

        public Criteria andChannelEqualTo(String value) {
            addCriterion("channel =", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotEqualTo(String value) {
            addCriterion("channel <>", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThan(String value) {
            addCriterion("channel >", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThanOrEqualTo(String value) {
            addCriterion("channel >=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThan(String value) {
            addCriterion("channel <", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThanOrEqualTo(String value) {
            addCriterion("channel <=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLike(String value) {
            addCriterion("channel like", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotLike(String value) {
            addCriterion("channel not like", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelIn(List<String> values) {
            addCriterion("channel in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotIn(List<String> values) {
            addCriterion("channel not in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelBetween(String value1, String value2) {
            addCriterion("channel between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotBetween(String value1, String value2) {
            addCriterion("channel not between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andAliasIsNull() {
            addCriterion("alias is null");
            return (Criteria) this;
        }

        public Criteria andAliasIsNotNull() {
            addCriterion("alias is not null");
            return (Criteria) this;
        }

        public Criteria andAliasEqualTo(String value) {
            addCriterion("alias =", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotEqualTo(String value) {
            addCriterion("alias <>", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThan(String value) {
            addCriterion("alias >", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThanOrEqualTo(String value) {
            addCriterion("alias >=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThan(String value) {
            addCriterion("alias <", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThanOrEqualTo(String value) {
            addCriterion("alias <=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLike(String value) {
            addCriterion("alias like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotLike(String value) {
            addCriterion("alias not like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasIn(List<String> values) {
            addCriterion("alias in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotIn(List<String> values) {
            addCriterion("alias not in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasBetween(String value1, String value2) {
            addCriterion("alias between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotBetween(String value1, String value2) {
            addCriterion("alias not between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andGroupattrIsNull() {
            addCriterion("GroupAttr is null");
            return (Criteria) this;
        }

        public Criteria andGroupattrIsNotNull() {
            addCriterion("GroupAttr is not null");
            return (Criteria) this;
        }

        public Criteria andGroupattrEqualTo(String value) {
            addCriterion("GroupAttr =", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrNotEqualTo(String value) {
            addCriterion("GroupAttr <>", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrGreaterThan(String value) {
            addCriterion("GroupAttr >", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrGreaterThanOrEqualTo(String value) {
            addCriterion("GroupAttr >=", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrLessThan(String value) {
            addCriterion("GroupAttr <", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrLessThanOrEqualTo(String value) {
            addCriterion("GroupAttr <=", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrLike(String value) {
            addCriterion("GroupAttr like", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrNotLike(String value) {
            addCriterion("GroupAttr not like", value, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrIn(List<String> values) {
            addCriterion("GroupAttr in", values, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrNotIn(List<String> values) {
            addCriterion("GroupAttr not in", values, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrBetween(String value1, String value2) {
            addCriterion("GroupAttr between", value1, value2, "groupattr");
            return (Criteria) this;
        }

        public Criteria andGroupattrNotBetween(String value1, String value2) {
            addCriterion("GroupAttr not between", value1, value2, "groupattr");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeIsNull() {
            addCriterion("HospitalCode is null");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeIsNotNull() {
            addCriterion("HospitalCode is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeEqualTo(String value) {
            addCriterion("HospitalCode =", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeNotEqualTo(String value) {
            addCriterion("HospitalCode <>", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeGreaterThan(String value) {
            addCriterion("HospitalCode >", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeGreaterThanOrEqualTo(String value) {
            addCriterion("HospitalCode >=", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeLessThan(String value) {
            addCriterion("HospitalCode <", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeLessThanOrEqualTo(String value) {
            addCriterion("HospitalCode <=", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeLike(String value) {
            addCriterion("HospitalCode like", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeNotLike(String value) {
            addCriterion("HospitalCode not like", value, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeIn(List<String> values) {
            addCriterion("HospitalCode in", values, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeNotIn(List<String> values) {
            addCriterion("HospitalCode not in", values, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeBetween(String value1, String value2) {
            addCriterion("HospitalCode between", value1, value2, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalcodeNotBetween(String value1, String value2) {
            addCriterion("HospitalCode not between", value1, value2, "hospitalcode");
            return (Criteria) this;
        }

        public Criteria andHospitalnameIsNull() {
            addCriterion("HospitalName is null");
            return (Criteria) this;
        }

        public Criteria andHospitalnameIsNotNull() {
            addCriterion("HospitalName is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalnameEqualTo(String value) {
            addCriterion("HospitalName =", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameNotEqualTo(String value) {
            addCriterion("HospitalName <>", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameGreaterThan(String value) {
            addCriterion("HospitalName >", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameGreaterThanOrEqualTo(String value) {
            addCriterion("HospitalName >=", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameLessThan(String value) {
            addCriterion("HospitalName <", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameLessThanOrEqualTo(String value) {
            addCriterion("HospitalName <=", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameLike(String value) {
            addCriterion("HospitalName like", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameNotLike(String value) {
            addCriterion("HospitalName not like", value, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameIn(List<String> values) {
            addCriterion("HospitalName in", values, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameNotIn(List<String> values) {
            addCriterion("HospitalName not in", values, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameBetween(String value1, String value2) {
            addCriterion("HospitalName between", value1, value2, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andHospitalnameNotBetween(String value1, String value2) {
            addCriterion("HospitalName not between", value1, value2, "hospitalname");
            return (Criteria) this;
        }

        public Criteria andUptimeIsNull() {
            addCriterion("upTime is null");
            return (Criteria) this;
        }

        public Criteria andUptimeIsNotNull() {
            addCriterion("upTime is not null");
            return (Criteria) this;
        }

        public Criteria andUptimeEqualTo(String value) {
            addCriterion("upTime =", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeNotEqualTo(String value) {
            addCriterion("upTime <>", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeGreaterThan(String value) {
            addCriterion("upTime >", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeGreaterThanOrEqualTo(String value) {
            addCriterion("upTime >=", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeLessThan(String value) {
            addCriterion("upTime <", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeLessThanOrEqualTo(String value) {
            addCriterion("upTime <=", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeLike(String value) {
            addCriterion("upTime like", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeNotLike(String value) {
            addCriterion("upTime not like", value, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeIn(List<String> values) {
            addCriterion("upTime in", values, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeNotIn(List<String> values) {
            addCriterion("upTime not in", values, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeBetween(String value1, String value2) {
            addCriterion("upTime between", value1, value2, "uptime");
            return (Criteria) this;
        }

        public Criteria andUptimeNotBetween(String value1, String value2) {
            addCriterion("upTime not between", value1, value2, "uptime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNull() {
            addCriterion("startTime is null");
            return (Criteria) this;
        }

        public Criteria andStarttimeIsNotNull() {
            addCriterion("startTime is not null");
            return (Criteria) this;
        }

        public Criteria andStarttimeEqualTo(String value) {
            addCriterion("startTime =", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotEqualTo(String value) {
            addCriterion("startTime <>", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThan(String value) {
            addCriterion("startTime >", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeGreaterThanOrEqualTo(String value) {
            addCriterion("startTime >=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThan(String value) {
            addCriterion("startTime <", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLessThanOrEqualTo(String value) {
            addCriterion("startTime <=", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeLike(String value) {
            addCriterion("startTime like", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotLike(String value) {
            addCriterion("startTime not like", value, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeIn(List<String> values) {
            addCriterion("startTime in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotIn(List<String> values) {
            addCriterion("startTime not in", values, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeBetween(String value1, String value2) {
            addCriterion("startTime between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andStarttimeNotBetween(String value1, String value2) {
            addCriterion("startTime not between", value1, value2, "starttime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNull() {
            addCriterion("endTime is null");
            return (Criteria) this;
        }

        public Criteria andEndtimeIsNotNull() {
            addCriterion("endTime is not null");
            return (Criteria) this;
        }

        public Criteria andEndtimeEqualTo(String value) {
            addCriterion("endTime =", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotEqualTo(String value) {
            addCriterion("endTime <>", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThan(String value) {
            addCriterion("endTime >", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeGreaterThanOrEqualTo(String value) {
            addCriterion("endTime >=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThan(String value) {
            addCriterion("endTime <", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLessThanOrEqualTo(String value) {
            addCriterion("endTime <=", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeLike(String value) {
            addCriterion("endTime like", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotLike(String value) {
            addCriterion("endTime not like", value, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeIn(List<String> values) {
            addCriterion("endTime in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotIn(List<String> values) {
            addCriterion("endTime not in", values, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeBetween(String value1, String value2) {
            addCriterion("endTime between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andEndtimeNotBetween(String value1, String value2) {
            addCriterion("endTime not between", value1, value2, "endtime");
            return (Criteria) this;
        }

        public Criteria andMonitorcountIsNull() {
            addCriterion("monitorCount is null");
            return (Criteria) this;
        }

        public Criteria andMonitorcountIsNotNull() {
            addCriterion("monitorCount is not null");
            return (Criteria) this;
        }

        public Criteria andMonitorcountEqualTo(Integer value) {
            addCriterion("monitorCount =", value, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountNotEqualTo(Integer value) {
            addCriterion("monitorCount <>", value, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountGreaterThan(Integer value) {
            addCriterion("monitorCount >", value, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("monitorCount >=", value, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountLessThan(Integer value) {
            addCriterion("monitorCount <", value, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountLessThanOrEqualTo(Integer value) {
            addCriterion("monitorCount <=", value, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountIn(List<Integer> values) {
            addCriterion("monitorCount in", values, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountNotIn(List<Integer> values) {
            addCriterion("monitorCount not in", values, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountBetween(Integer value1, Integer value2) {
            addCriterion("monitorCount between", value1, value2, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andMonitorcountNotBetween(Integer value1, Integer value2) {
            addCriterion("monitorCount not between", value1, value2, "monitorcount");
            return (Criteria) this;
        }

        public Criteria andHelpercountIsNull() {
            addCriterion("helperCount is null");
            return (Criteria) this;
        }

        public Criteria andHelpercountIsNotNull() {
            addCriterion("helperCount is not null");
            return (Criteria) this;
        }

        public Criteria andHelpercountEqualTo(Integer value) {
            addCriterion("helperCount =", value, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountNotEqualTo(Integer value) {
            addCriterion("helperCount <>", value, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountGreaterThan(Integer value) {
            addCriterion("helperCount >", value, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountGreaterThanOrEqualTo(Integer value) {
            addCriterion("helperCount >=", value, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountLessThan(Integer value) {
            addCriterion("helperCount <", value, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountLessThanOrEqualTo(Integer value) {
            addCriterion("helperCount <=", value, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountIn(List<Integer> values) {
            addCriterion("helperCount in", values, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountNotIn(List<Integer> values) {
            addCriterion("helperCount not in", values, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountBetween(Integer value1, Integer value2) {
            addCriterion("helperCount between", value1, value2, "helpercount");
            return (Criteria) this;
        }

        public Criteria andHelpercountNotBetween(Integer value1, Integer value2) {
            addCriterion("helperCount not between", value1, value2, "helpercount");
            return (Criteria) this;
        }

        public Criteria andRobotcountIsNull() {
            addCriterion("robotCount is null");
            return (Criteria) this;
        }

        public Criteria andRobotcountIsNotNull() {
            addCriterion("robotCount is not null");
            return (Criteria) this;
        }

        public Criteria andRobotcountEqualTo(Integer value) {
            addCriterion("robotCount =", value, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountNotEqualTo(Integer value) {
            addCriterion("robotCount <>", value, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountGreaterThan(Integer value) {
            addCriterion("robotCount >", value, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("robotCount >=", value, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountLessThan(Integer value) {
            addCriterion("robotCount <", value, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountLessThanOrEqualTo(Integer value) {
            addCriterion("robotCount <=", value, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountIn(List<Integer> values) {
            addCriterion("robotCount in", values, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountNotIn(List<Integer> values) {
            addCriterion("robotCount not in", values, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountBetween(Integer value1, Integer value2) {
            addCriterion("robotCount between", value1, value2, "robotcount");
            return (Criteria) this;
        }

        public Criteria andRobotcountNotBetween(Integer value1, Integer value2) {
            addCriterion("robotCount not between", value1, value2, "robotcount");
            return (Criteria) this;
        }

        public Criteria andWhitelistIsNull() {
            addCriterion("whiteList is null");
            return (Criteria) this;
        }

        public Criteria andWhitelistIsNotNull() {
            addCriterion("whiteList is not null");
            return (Criteria) this;
        }

        public Criteria andWhitelistEqualTo(Integer value) {
            addCriterion("whiteList =", value, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistNotEqualTo(Integer value) {
            addCriterion("whiteList <>", value, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistGreaterThan(Integer value) {
            addCriterion("whiteList >", value, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistGreaterThanOrEqualTo(Integer value) {
            addCriterion("whiteList >=", value, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistLessThan(Integer value) {
            addCriterion("whiteList <", value, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistLessThanOrEqualTo(Integer value) {
            addCriterion("whiteList <=", value, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistIn(List<Integer> values) {
            addCriterion("whiteList in", values, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistNotIn(List<Integer> values) {
            addCriterion("whiteList not in", values, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistBetween(Integer value1, Integer value2) {
            addCriterion("whiteList between", value1, value2, "whitelist");
            return (Criteria) this;
        }

        public Criteria andWhitelistNotBetween(Integer value1, Integer value2) {
            addCriterion("whiteList not between", value1, value2, "whitelist");
            return (Criteria) this;
        }

        public Criteria andKomcountIsNull() {
            addCriterion("komCount is null");
            return (Criteria) this;
        }

        public Criteria andKomcountIsNotNull() {
            addCriterion("komCount is not null");
            return (Criteria) this;
        }

        public Criteria andKomcountEqualTo(Integer value) {
            addCriterion("komCount =", value, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountNotEqualTo(Integer value) {
            addCriterion("komCount <>", value, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountGreaterThan(Integer value) {
            addCriterion("komCount >", value, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("komCount >=", value, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountLessThan(Integer value) {
            addCriterion("komCount <", value, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountLessThanOrEqualTo(Integer value) {
            addCriterion("komCount <=", value, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountIn(List<Integer> values) {
            addCriterion("komCount in", values, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountNotIn(List<Integer> values) {
            addCriterion("komCount not in", values, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountBetween(Integer value1, Integer value2) {
            addCriterion("komCount between", value1, value2, "komcount");
            return (Criteria) this;
        }

        public Criteria andKomcountNotBetween(Integer value1, Integer value2) {
            addCriterion("komCount not between", value1, value2, "komcount");
            return (Criteria) this;
        }

        public Criteria andAddcountIsNull() {
            addCriterion("addCount is null");
            return (Criteria) this;
        }

        public Criteria andAddcountIsNotNull() {
            addCriterion("addCount is not null");
            return (Criteria) this;
        }

        public Criteria andAddcountEqualTo(Integer value) {
            addCriterion("addCount =", value, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountNotEqualTo(Integer value) {
            addCriterion("addCount <>", value, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountGreaterThan(Integer value) {
            addCriterion("addCount >", value, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("addCount >=", value, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountLessThan(Integer value) {
            addCriterion("addCount <", value, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountLessThanOrEqualTo(Integer value) {
            addCriterion("addCount <=", value, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountIn(List<Integer> values) {
            addCriterion("addCount in", values, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountNotIn(List<Integer> values) {
            addCriterion("addCount not in", values, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountBetween(Integer value1, Integer value2) {
            addCriterion("addCount between", value1, value2, "addcount");
            return (Criteria) this;
        }

        public Criteria andAddcountNotBetween(Integer value1, Integer value2) {
            addCriterion("addCount not between", value1, value2, "addcount");
            return (Criteria) this;
        }

        public Criteria andBackcountIsNull() {
            addCriterion("backCount is null");
            return (Criteria) this;
        }

        public Criteria andBackcountIsNotNull() {
            addCriterion("backCount is not null");
            return (Criteria) this;
        }

        public Criteria andBackcountEqualTo(Integer value) {
            addCriterion("backCount =", value, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountNotEqualTo(Integer value) {
            addCriterion("backCount <>", value, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountGreaterThan(Integer value) {
            addCriterion("backCount >", value, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("backCount >=", value, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountLessThan(Integer value) {
            addCriterion("backCount <", value, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountLessThanOrEqualTo(Integer value) {
            addCriterion("backCount <=", value, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountIn(List<Integer> values) {
            addCriterion("backCount in", values, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountNotIn(List<Integer> values) {
            addCriterion("backCount not in", values, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountBetween(Integer value1, Integer value2) {
            addCriterion("backCount between", value1, value2, "backcount");
            return (Criteria) this;
        }

        public Criteria andBackcountNotBetween(Integer value1, Integer value2) {
            addCriterion("backCount not between", value1, value2, "backcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountIsNull() {
            addCriterion("currentCount is null");
            return (Criteria) this;
        }

        public Criteria andCurrentcountIsNotNull() {
            addCriterion("currentCount is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentcountEqualTo(Integer value) {
            addCriterion("currentCount =", value, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountNotEqualTo(Integer value) {
            addCriterion("currentCount <>", value, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountGreaterThan(Integer value) {
            addCriterion("currentCount >", value, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("currentCount >=", value, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountLessThan(Integer value) {
            addCriterion("currentCount <", value, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountLessThanOrEqualTo(Integer value) {
            addCriterion("currentCount <=", value, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountIn(List<Integer> values) {
            addCriterion("currentCount in", values, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountNotIn(List<Integer> values) {
            addCriterion("currentCount not in", values, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountBetween(Integer value1, Integer value2) {
            addCriterion("currentCount between", value1, value2, "currentcount");
            return (Criteria) this;
        }

        public Criteria andCurrentcountNotBetween(Integer value1, Integer value2) {
            addCriterion("currentCount not between", value1, value2, "currentcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountIsNull() {
            addCriterion("totalCount is null");
            return (Criteria) this;
        }

        public Criteria andTotalcountIsNotNull() {
            addCriterion("totalCount is not null");
            return (Criteria) this;
        }

        public Criteria andTotalcountEqualTo(Integer value) {
            addCriterion("totalCount =", value, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountNotEqualTo(Integer value) {
            addCriterion("totalCount <>", value, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountGreaterThan(Integer value) {
            addCriterion("totalCount >", value, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountGreaterThanOrEqualTo(Integer value) {
            addCriterion("totalCount >=", value, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountLessThan(Integer value) {
            addCriterion("totalCount <", value, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountLessThanOrEqualTo(Integer value) {
            addCriterion("totalCount <=", value, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountIn(List<Integer> values) {
            addCriterion("totalCount in", values, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountNotIn(List<Integer> values) {
            addCriterion("totalCount not in", values, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountBetween(Integer value1, Integer value2) {
            addCriterion("totalCount between", value1, value2, "totalcount");
            return (Criteria) this;
        }

        public Criteria andTotalcountNotBetween(Integer value1, Integer value2) {
            addCriterion("totalCount not between", value1, value2, "totalcount");
            return (Criteria) this;
        }

        public Criteria andPasscountIsNull() {
            addCriterion("passCount is null");
            return (Criteria) this;
        }

        public Criteria andPasscountIsNotNull() {
            addCriterion("passCount is not null");
            return (Criteria) this;
        }

        public Criteria andPasscountEqualTo(Integer value) {
            addCriterion("passCount =", value, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountNotEqualTo(Integer value) {
            addCriterion("passCount <>", value, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountGreaterThan(Integer value) {
            addCriterion("passCount >", value, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountGreaterThanOrEqualTo(Integer value) {
            addCriterion("passCount >=", value, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountLessThan(Integer value) {
            addCriterion("passCount <", value, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountLessThanOrEqualTo(Integer value) {
            addCriterion("passCount <=", value, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountIn(List<Integer> values) {
            addCriterion("passCount in", values, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountNotIn(List<Integer> values) {
            addCriterion("passCount not in", values, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountBetween(Integer value1, Integer value2) {
            addCriterion("passCount between", value1, value2, "passcount");
            return (Criteria) this;
        }

        public Criteria andPasscountNotBetween(Integer value1, Integer value2) {
            addCriterion("passCount not between", value1, value2, "passcount");
            return (Criteria) this;
        }

        public Criteria andNowroomnameIsNull() {
            addCriterion("NowRoomName is null");
            return (Criteria) this;
        }

        public Criteria andNowroomnameIsNotNull() {
            addCriterion("NowRoomName is not null");
            return (Criteria) this;
        }

        public Criteria andNowroomnameEqualTo(String value) {
            addCriterion("NowRoomName =", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameNotEqualTo(String value) {
            addCriterion("NowRoomName <>", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameGreaterThan(String value) {
            addCriterion("NowRoomName >", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameGreaterThanOrEqualTo(String value) {
            addCriterion("NowRoomName >=", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameLessThan(String value) {
            addCriterion("NowRoomName <", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameLessThanOrEqualTo(String value) {
            addCriterion("NowRoomName <=", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameLike(String value) {
            addCriterion("NowRoomName like", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameNotLike(String value) {
            addCriterion("NowRoomName not like", value, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameIn(List<String> values) {
            addCriterion("NowRoomName in", values, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameNotIn(List<String> values) {
            addCriterion("NowRoomName not in", values, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameBetween(String value1, String value2) {
            addCriterion("NowRoomName between", value1, value2, "nowroomname");
            return (Criteria) this;
        }

        public Criteria andNowroomnameNotBetween(String value1, String value2) {
            addCriterion("NowRoomName not between", value1, value2, "nowroomname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}