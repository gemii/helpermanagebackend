package cc.gemii.po;

public class TaskNow {
	
	private int id;
	private String type;
	private String status;
	private String creatTime;
	private String updateTime;
	private String taskName;
	private int FKUserStatusId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreatTime() {
		return creatTime;
	}
	public void setCreatTime(String creatTime) {
		this.creatTime = creatTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public int getFKUserStatusId() {
		return FKUserStatusId;
	}
	public void setFKUserStatusId(int fKUserStatusId) {
		FKUserStatusId = fKUserStatusId;
	}

}
