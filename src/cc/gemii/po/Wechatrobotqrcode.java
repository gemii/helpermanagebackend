package cc.gemii.po;

public class Wechatrobotqrcode {
    private String robotid;

    private String qrimage;

    private String h5url;

    public String getRobotid() {
        return robotid;
    }

    public void setRobotid(String robotid) {
        this.robotid = robotid == null ? null : robotid.trim();
    }

    public String getQrimage() {
        return qrimage;
    }

    public void setQrimage(String qrimage) {
        this.qrimage = qrimage == null ? null : qrimage.trim();
    }

    public String getH5url() {
        return h5url;
    }

    public void setH5url(String h5url) {
        this.h5url = h5url == null ? null : h5url.trim();
    }
}