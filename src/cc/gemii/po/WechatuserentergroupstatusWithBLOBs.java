package cc.gemii.po;

public class WechatuserentergroupstatusWithBLOBs extends Wechatuserentergroupstatus {
    private String robotid;

    private String upmessage;

    private String messageprocessstatus;

    private String remindmessage;

    private String username;

    private String userid;

    private String messagestatus;

    private String confirmstatus;

    private String messagingtime;

    private String roomid;

    public String getRobotid() {
        return robotid;
    }

    public void setRobotid(String robotid) {
        this.robotid = robotid == null ? null : robotid.trim();
    }

    public String getUpmessage() {
        return upmessage;
    }

    public void setUpmessage(String upmessage) {
        this.upmessage = upmessage == null ? null : upmessage.trim();
    }

    public String getMessageprocessstatus() {
        return messageprocessstatus;
    }

    public void setMessageprocessstatus(String messageprocessstatus) {
        this.messageprocessstatus = messageprocessstatus == null ? null : messageprocessstatus.trim();
    }

    public String getRemindmessage() {
        return remindmessage;
    }

    public void setRemindmessage(String remindmessage) {
        this.remindmessage = remindmessage == null ? null : remindmessage.trim();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid == null ? null : userid.trim();
    }

    public String getMessagestatus() {
        return messagestatus;
    }

    public void setMessagestatus(String messagestatus) {
        this.messagestatus = messagestatus == null ? null : messagestatus.trim();
    }

    public String getConfirmstatus() {
        return confirmstatus;
    }

    public void setConfirmstatus(String confirmstatus) {
        this.confirmstatus = confirmstatus == null ? null : confirmstatus.trim();
    }

    public String getMessagingtime() {
        return messagingtime;
    }

    public void setMessagingtime(String messagingtime) {
        this.messagingtime = messagingtime == null ? null : messagingtime.trim();
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }
}