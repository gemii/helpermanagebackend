package cc.gemii.po;

public class Wechatroom {
    private Integer id;

    private String roomname;

    private String roomid;

    private String roomownerid;

    private String usercount;

    private String roomicon;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname == null ? null : roomname.trim();
    }

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getRoomownerid() {
        return roomownerid;
    }

    public void setRoomownerid(String roomownerid) {
        this.roomownerid = roomownerid == null ? null : roomownerid.trim();
    }

    public String getUsercount() {
        return usercount;
    }

    public void setUsercount(String usercount) {
        this.usercount = usercount == null ? null : usercount.trim();
    }

    public String getRoomicon() {
        return roomicon;
    }

    public void setRoomicon(String roomicon) {
        this.roomicon = roomicon == null ? null : roomicon.trim();
    }
}