package cc.gemii.po;

public class Wechatroominfo {
    private String roomid;

    private String roomname;

    private String city;

    private String area;

    private String province;

    private String department;

    private String subname;

    private String channel;

    private String alias;

    private String groupattr;

    private String hospitalcode;

    private String hospitalname;

    private String uptime;

    private String starttime;

    private String endtime;

    private Integer monitorcount;

    private Integer helpercount;

    private Integer robotcount;

    private Integer whitelist;

    private Integer komcount;

    private Integer addcount;

    private Integer backcount;

    private Integer currentcount;

    private Integer totalcount;

    private Integer passcount;

    private String nowroomname;
    
    private String groupTag;

    public String getRoomid() {
        return roomid;
    }

    public void setRoomid(String roomid) {
        this.roomid = roomid == null ? null : roomid.trim();
    }

    public String getRoomname() {
        return roomname;
    }

    public void setRoomname(String roomname) {
        this.roomname = roomname == null ? null : roomname.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname == null ? null : subname.trim();
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel == null ? null : channel.trim();
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    public String getGroupattr() {
        return groupattr;
    }

    public void setGroupattr(String groupattr) {
        this.groupattr = groupattr == null ? null : groupattr.trim();
    }

    public String getHospitalcode() {
        return hospitalcode;
    }

    public void setHospitalcode(String hospitalcode) {
        this.hospitalcode = hospitalcode == null ? null : hospitalcode.trim();
    }

    public String getHospitalname() {
        return hospitalname;
    }

    public void setHospitalname(String hospitalname) {
        this.hospitalname = hospitalname == null ? null : hospitalname.trim();
    }

    public String getUptime() {
        return uptime;
    }

    public void setUptime(String uptime) {
        this.uptime = uptime == null ? null : uptime.trim();
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime == null ? null : starttime.trim();
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime == null ? null : endtime.trim();
    }

    public Integer getMonitorcount() {
        return monitorcount;
    }

    public void setMonitorcount(Integer monitorcount) {
        this.monitorcount = monitorcount;
    }

    public Integer getHelpercount() {
        return helpercount;
    }

    public void setHelpercount(Integer helpercount) {
        this.helpercount = helpercount;
    }

    public Integer getRobotcount() {
        return robotcount;
    }

    public void setRobotcount(Integer robotcount) {
        this.robotcount = robotcount;
    }

    public Integer getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(Integer whitelist) {
        this.whitelist = whitelist;
    }

    public Integer getKomcount() {
        return komcount;
    }

    public void setKomcount(Integer komcount) {
        this.komcount = komcount;
    }

    public Integer getAddcount() {
        return addcount;
    }

    public void setAddcount(Integer addcount) {
        this.addcount = addcount;
    }

    public Integer getBackcount() {
        return backcount;
    }

    public void setBackcount(Integer backcount) {
        this.backcount = backcount;
    }

    public Integer getCurrentcount() {
        return currentcount;
    }

    public void setCurrentcount(Integer currentcount) {
        this.currentcount = currentcount;
    }

    public Integer getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(Integer totalcount) {
        this.totalcount = totalcount;
    }

    public Integer getPasscount() {
        return passcount;
    }

    public void setPasscount(Integer passcount) {
        this.passcount = passcount;
    }

    public String getNowroomname() {
        return nowroomname;
    }

    public void setNowroomname(String nowroomname) {
        this.nowroomname = nowroomname == null ? null : nowroomname.trim();
    }

	public String getGroupTag() {
		return groupTag;
	}

	public void setGroupTag(String groupTag) {
		this.groupTag = groupTag;
	}
    
    
}