package cc.gemii.po;

import java.util.ArrayList;
import java.util.List;

public class UserinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public UserinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNull() {
            addCriterion("Openid is null");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNotNull() {
            addCriterion("Openid is not null");
            return (Criteria) this;
        }

        public Criteria andOpenidEqualTo(String value) {
            addCriterion("Openid =", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotEqualTo(String value) {
            addCriterion("Openid <>", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThan(String value) {
            addCriterion("Openid >", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThanOrEqualTo(String value) {
            addCriterion("Openid >=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThan(String value) {
            addCriterion("Openid <", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThanOrEqualTo(String value) {
            addCriterion("Openid <=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLike(String value) {
            addCriterion("Openid like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotLike(String value) {
            addCriterion("Openid not like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidIn(List<String> values) {
            addCriterion("Openid in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotIn(List<String> values) {
            addCriterion("Openid not in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidBetween(String value1, String value2) {
            addCriterion("Openid between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotBetween(String value1, String value2) {
            addCriterion("Openid not between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andPhonenumberIsNull() {
            addCriterion("PhoneNumber is null");
            return (Criteria) this;
        }

        public Criteria andPhonenumberIsNotNull() {
            addCriterion("PhoneNumber is not null");
            return (Criteria) this;
        }

        public Criteria andPhonenumberEqualTo(String value) {
            addCriterion("PhoneNumber =", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotEqualTo(String value) {
            addCriterion("PhoneNumber <>", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberGreaterThan(String value) {
            addCriterion("PhoneNumber >", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberGreaterThanOrEqualTo(String value) {
            addCriterion("PhoneNumber >=", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberLessThan(String value) {
            addCriterion("PhoneNumber <", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberLessThanOrEqualTo(String value) {
            addCriterion("PhoneNumber <=", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberLike(String value) {
            addCriterion("PhoneNumber like", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotLike(String value) {
            addCriterion("PhoneNumber not like", value, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberIn(List<String> values) {
            addCriterion("PhoneNumber in", values, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotIn(List<String> values) {
            addCriterion("PhoneNumber not in", values, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberBetween(String value1, String value2) {
            addCriterion("PhoneNumber between", value1, value2, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andPhonenumberNotBetween(String value1, String value2) {
            addCriterion("PhoneNumber not between", value1, value2, "phonenumber");
            return (Criteria) this;
        }

        public Criteria andFormtimeIsNull() {
            addCriterion("FormTime is null");
            return (Criteria) this;
        }

        public Criteria andFormtimeIsNotNull() {
            addCriterion("FormTime is not null");
            return (Criteria) this;
        }

        public Criteria andFormtimeEqualTo(String value) {
            addCriterion("FormTime =", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeNotEqualTo(String value) {
            addCriterion("FormTime <>", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeGreaterThan(String value) {
            addCriterion("FormTime >", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeGreaterThanOrEqualTo(String value) {
            addCriterion("FormTime >=", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeLessThan(String value) {
            addCriterion("FormTime <", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeLessThanOrEqualTo(String value) {
            addCriterion("FormTime <=", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeLike(String value) {
            addCriterion("FormTime like", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeNotLike(String value) {
            addCriterion("FormTime not like", value, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeIn(List<String> values) {
            addCriterion("FormTime in", values, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeNotIn(List<String> values) {
            addCriterion("FormTime not in", values, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeBetween(String value1, String value2) {
            addCriterion("FormTime between", value1, value2, "formtime");
            return (Criteria) this;
        }

        public Criteria andFormtimeNotBetween(String value1, String value2) {
            addCriterion("FormTime not between", value1, value2, "formtime");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("Province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("Province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("Province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("Province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("Province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("Province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("Province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("Province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("Province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("Province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("Province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("Province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("Province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("Province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andCityIsNull() {
            addCriterion("City is null");
            return (Criteria) this;
        }

        public Criteria andCityIsNotNull() {
            addCriterion("City is not null");
            return (Criteria) this;
        }

        public Criteria andCityEqualTo(String value) {
            addCriterion("City =", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotEqualTo(String value) {
            addCriterion("City <>", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThan(String value) {
            addCriterion("City >", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityGreaterThanOrEqualTo(String value) {
            addCriterion("City >=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThan(String value) {
            addCriterion("City <", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLessThanOrEqualTo(String value) {
            addCriterion("City <=", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityLike(String value) {
            addCriterion("City like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotLike(String value) {
            addCriterion("City not like", value, "city");
            return (Criteria) this;
        }

        public Criteria andCityIn(List<String> values) {
            addCriterion("City in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotIn(List<String> values) {
            addCriterion("City not in", values, "city");
            return (Criteria) this;
        }

        public Criteria andCityBetween(String value1, String value2) {
            addCriterion("City between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andCityNotBetween(String value1, String value2) {
            addCriterion("City not between", value1, value2, "city");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordIsNull() {
            addCriterion("HospitalKeyword is null");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordIsNotNull() {
            addCriterion("HospitalKeyword is not null");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordEqualTo(String value) {
            addCriterion("HospitalKeyword =", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordNotEqualTo(String value) {
            addCriterion("HospitalKeyword <>", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordGreaterThan(String value) {
            addCriterion("HospitalKeyword >", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordGreaterThanOrEqualTo(String value) {
            addCriterion("HospitalKeyword >=", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordLessThan(String value) {
            addCriterion("HospitalKeyword <", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordLessThanOrEqualTo(String value) {
            addCriterion("HospitalKeyword <=", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordLike(String value) {
            addCriterion("HospitalKeyword like", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordNotLike(String value) {
            addCriterion("HospitalKeyword not like", value, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordIn(List<String> values) {
            addCriterion("HospitalKeyword in", values, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordNotIn(List<String> values) {
            addCriterion("HospitalKeyword not in", values, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordBetween(String value1, String value2) {
            addCriterion("HospitalKeyword between", value1, value2, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andHospitalkeywordNotBetween(String value1, String value2) {
            addCriterion("HospitalKeyword not between", value1, value2, "hospitalkeyword");
            return (Criteria) this;
        }

        public Criteria andEdcIsNull() {
            addCriterion("Edc is null");
            return (Criteria) this;
        }

        public Criteria andEdcIsNotNull() {
            addCriterion("Edc is not null");
            return (Criteria) this;
        }

        public Criteria andEdcEqualTo(String value) {
            addCriterion("Edc =", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcNotEqualTo(String value) {
            addCriterion("Edc <>", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcGreaterThan(String value) {
            addCriterion("Edc >", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcGreaterThanOrEqualTo(String value) {
            addCriterion("Edc >=", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcLessThan(String value) {
            addCriterion("Edc <", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcLessThanOrEqualTo(String value) {
            addCriterion("Edc <=", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcLike(String value) {
            addCriterion("Edc like", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcNotLike(String value) {
            addCriterion("Edc not like", value, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcIn(List<String> values) {
            addCriterion("Edc in", values, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcNotIn(List<String> values) {
            addCriterion("Edc not in", values, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcBetween(String value1, String value2) {
            addCriterion("Edc between", value1, value2, "edc");
            return (Criteria) this;
        }

        public Criteria andEdcNotBetween(String value1, String value2) {
            addCriterion("Edc not between", value1, value2, "edc");
            return (Criteria) this;
        }

        public Criteria andUserstatusidIsNull() {
            addCriterion("UserStatusID is null");
            return (Criteria) this;
        }

        public Criteria andUserstatusidIsNotNull() {
            addCriterion("UserStatusID is not null");
            return (Criteria) this;
        }

        public Criteria andUserstatusidEqualTo(Integer value) {
            addCriterion("UserStatusID =", value, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidNotEqualTo(Integer value) {
            addCriterion("UserStatusID <>", value, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidGreaterThan(Integer value) {
            addCriterion("UserStatusID >", value, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidGreaterThanOrEqualTo(Integer value) {
            addCriterion("UserStatusID >=", value, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidLessThan(Integer value) {
            addCriterion("UserStatusID <", value, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidLessThanOrEqualTo(Integer value) {
            addCriterion("UserStatusID <=", value, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidIn(List<Integer> values) {
            addCriterion("UserStatusID in", values, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidNotIn(List<Integer> values) {
            addCriterion("UserStatusID not in", values, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidBetween(Integer value1, Integer value2) {
            addCriterion("UserStatusID between", value1, value2, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andUserstatusidNotBetween(Integer value1, Integer value2) {
            addCriterion("UserStatusID not between", value1, value2, "userstatusid");
            return (Criteria) this;
        }

        public Criteria andSrcodeIsNull() {
            addCriterion("SRCode is null");
            return (Criteria) this;
        }

        public Criteria andSrcodeIsNotNull() {
            addCriterion("SRCode is not null");
            return (Criteria) this;
        }

        public Criteria andSrcodeEqualTo(String value) {
            addCriterion("SRCode =", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeNotEqualTo(String value) {
            addCriterion("SRCode <>", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeGreaterThan(String value) {
            addCriterion("SRCode >", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeGreaterThanOrEqualTo(String value) {
            addCriterion("SRCode >=", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeLessThan(String value) {
            addCriterion("SRCode <", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeLessThanOrEqualTo(String value) {
            addCriterion("SRCode <=", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeLike(String value) {
            addCriterion("SRCode like", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeNotLike(String value) {
            addCriterion("SRCode not like", value, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeIn(List<String> values) {
            addCriterion("SRCode in", values, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeNotIn(List<String> values) {
            addCriterion("SRCode not in", values, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeBetween(String value1, String value2) {
            addCriterion("SRCode between", value1, value2, "srcode");
            return (Criteria) this;
        }

        public Criteria andSrcodeNotBetween(String value1, String value2) {
            addCriterion("SRCode not between", value1, value2, "srcode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}