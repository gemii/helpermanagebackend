package cc.gemii.pojo;

import cc.gemii.po.Userinfo;

public class UserInfoCustom extends Userinfo{

	private String code;

	private String srcode;
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getSrcode() {
		return srcode;
	}

	public void setSrcode(String srcode) {
		this.srcode = srcode;
	}
}
