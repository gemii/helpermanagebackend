package cc.gemii.pojo;

public class GetHosByCity {

	private String RoomID;
	
	private String NowRoomName;

	private String HospitalName;
	
	public String getHospitalName() {
		return HospitalName;
	}

	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}

	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getNowRoomName() {
		return NowRoomName;
	}

	public void setNowRoomName(String nowRoomName) {
		NowRoomName = nowRoomName;
	}
	
}
