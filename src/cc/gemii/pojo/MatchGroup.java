package cc.gemii.pojo;

public class MatchGroup {

	private String roomName;
	
	private String roomID;
	
	private String robotID;

	private String status;
	
	private Integer userCount;
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomID() {
		return roomID;
	}

	public void setRoomID(String roomID) {
		this.roomID = roomID;
	}

	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getUserCount() {
		return userCount;
	}

	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	@Override
	public String toString() {
		return "MatchGroup [roomName=" + roomName + ", roomID=" + roomID
				+ ", robotID=" + robotID + ", status=" + status
				+ ", userCount=" + userCount + "]";
	}
	
	
}
