package cc.gemii.pojo;

public class SelectMessageBResult {

	private String City;
	
	private String HospitalKeyword;
	
	private String Edc;
	
	private String MatchStatus;
	
	private String UserName;
	
	private String FormTime;
	
	private String EnterGroupStatus;
	
	private String FriendStatus;
	
	private String NowRoomName;
	
	private String QrStatus;

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getHospitalKeyword() {
		return HospitalKeyword;
	}

	public void setHospitalKeyword(String hospitalKeyword) {
		HospitalKeyword = hospitalKeyword;
	}

	public String getEdc() {
		return Edc;
	}

	public void setEdc(String edc) {
		Edc = edc;
	}

	public String getMatchStatus() {
		return MatchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		MatchStatus = matchStatus;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getFormTime() {
		return FormTime;
	}

	public void setFormTime(String formTime) {
		FormTime = formTime;
	}

	public String getEnterGroupStatus() {
		return EnterGroupStatus;
	}

	public void setEnterGroupStatus(String enterGroupStatus) {
		EnterGroupStatus = enterGroupStatus;
	}

	public String getFriendStatus() {
		return FriendStatus;
	}

	public void setFriendStatus(String friendStatus) {
		FriendStatus = friendStatus;
	}

	public String getNowRoomName() {
		return NowRoomName;
	}

	public void setNowRoomName(String nowRoomName) {
		NowRoomName = nowRoomName;
	}

	public String getQrStatus() {
		return QrStatus;
	}

	public void setQrStatus(String qrStatus) {
		QrStatus = qrStatus;
	}
	
}
