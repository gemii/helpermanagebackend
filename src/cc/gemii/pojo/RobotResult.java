package cc.gemii.pojo;

public class RobotResult {
	
	private String status;
	
	private String robotId;
	
	private String robotName;
	
	private String robotTag;
	
	private String startTime;
	

	public String getRobotName() {
		return robotName;
	}

	public void setRobotName(String robotName) {
		this.robotName = robotName;
	}

	public String getRobotTag() {
		return robotTag;
	}

	public void setRobotTag(String robotTag) {
		this.robotTag = robotTag;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRobotId() {
		return robotId;
	}

	public void setRobotId(String robotId) {
		this.robotId = robotId;
	}

}
