package cc.gemii.pojo;
/**
 * ClassName:MemberCardReq.java Function: Reason:
 * @author zili.jin
 * @version
 * @since
 * @Date Apr 13, 2017
 *
 * @see
 */
public class UserFriendEnterGroup {

	private String openid;
	
	private String fullName;
	
	private String nickname;
	
	private String headimgurl;
	
	private String phone;
	
	private String city;
	
	private String hospital;
	
	private String edc;
	
	private String type;
	
	private String province;
	
	private String username;
	
	private String area;
	
	private String name;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMatchgroup() {
		return matchgroup;
	}

	public void setMatchgroup(String matchgroup) {
		this.matchgroup = matchgroup;
	}

	public String getMatchrobot() {
		return matchrobot;
	}

	public void setMatchrobot(String matchrobot) {
		this.matchrobot = matchrobot;
	}

	public String getMatchstatus() {
		return matchstatus;
	}

	public void setMatchstatus(String matchstatus) {
		this.matchstatus = matchstatus;
	}

	private String matchgroup;
	
	private String matchrobot;
	
	private String matchstatus; 

	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getHeadimgurl() {
		return headimgurl;
	}

	public void setHeadimgurl(String headimgurl) {
		this.headimgurl = headimgurl;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHospital() {
		return hospital;
	}

	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEdc() {
		return edc;
	}

	public void setEdc(String edc) {
		this.edc = edc;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
