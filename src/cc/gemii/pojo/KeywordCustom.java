package cc.gemii.pojo;

import cc.gemii.po.Wechatkeyword;

public class KeywordCustom extends Wechatkeyword{

	private String roomName;

	private String robotName;
	
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRobotName() {
		return robotName;
	}

	public void setRobotName(String robotName) {
		this.robotName = robotName;
	}
}
