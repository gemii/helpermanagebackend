package cc.gemii.pojo;

public class SelectMessageAResult {

	private Integer id;
	
	private String City;
	
	private String HospitalKeyword;
	
	private String Edc;
	
	private String MatchStatus;
	
	private String UserName;
	
	private String UrlStatus;
	
	private String FormTime;
	
	private String EnterGroupStatus;
	
	private String ConfirmStatus;
	
	private String NowRoomName;
	
	private String PhoneCode;

	private String H5Status;
	
	private String SRCode;
	
	private String PhoneCodeVerifyStatus;
	
	private String RemindMessage;
	
	private String PhoneCodeSendStatus;
	
	private String PhoneCodeUpTime;
	
	private String NoticeStatus;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getHospitalKeyword() {
		return HospitalKeyword;
	}

	public void setHospitalKeyword(String hospitalKeyword) {
		HospitalKeyword = hospitalKeyword;
	}

	public String getEdc() {
		return Edc;
	}

	public void setEdc(String edc) {
		Edc = edc;
	}

	public String getMatchStatus() {
		return MatchStatus;
	}

	public void setMatchStatus(String matchStatus) {
		MatchStatus = matchStatus;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getUrlStatus() {
		return UrlStatus;
	}

	public void setUrlStatus(String urlStatus) {
		UrlStatus = urlStatus;
	}

	public String getFormTime() {
		return FormTime;
	}

	public void setFormTime(String formTime) {
		FormTime = formTime;
	}

	public String getEnterGroupStatus() {
		return EnterGroupStatus;
	}

	public void setEnterGroupStatus(String enterGroupStatus) {
		EnterGroupStatus = enterGroupStatus;
	}

	public String getConfirmStatus() {
		return ConfirmStatus;
	}

	public void setConfirmStatus(String confirmStatus) {
		ConfirmStatus = confirmStatus;
	}

	public String getNowRoomName() {
		return NowRoomName;
	}

	public void setNowRoomName(String nowRoomName) {
		NowRoomName = nowRoomName;
	}


	public String getPhoneCode() {
		return PhoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		PhoneCode = phoneCode;
	}

	public String getH5Status() {
		return H5Status;
	}

	public void setH5Status(String h5Status) {
		H5Status = h5Status;
	}

	public String getSRCode() {
		return SRCode;
	}

	public void setSRCode(String sRCode) {
		SRCode = sRCode;
	}

	public String getPhoneCodeVerifyStatus() {
		return PhoneCodeVerifyStatus;
	}

	public void setPhoneCodeVerifyStatus(String phoneCodeVerifyStatus) {
		PhoneCodeVerifyStatus = phoneCodeVerifyStatus;
	}

	public String getRemindMessage() {
		return RemindMessage;
	}

	public void setRemindMessage(String remindMessage) {
		RemindMessage = remindMessage;
	}

	public String getPhoneCodeSendStatus() {
		return PhoneCodeSendStatus;
	}

	public void setPhoneCodeSendStatus(String phoneCodeSendStatus) {
		PhoneCodeSendStatus = phoneCodeSendStatus;
	}

	public String getPhoneCodeUpTime() {
		return PhoneCodeUpTime;
	}

	public void setPhoneCodeUpTime(String phoneCodeUpTime) {
		PhoneCodeUpTime = phoneCodeUpTime;
	}

	public String getNoticeStatus() {
		return NoticeStatus;
	}

	public void setNoticeStatus(String noticeStatus) {
		NoticeStatus = noticeStatus;
	}
	
	
}
