package cc.gemii.pojo;

import cc.gemii.po.HelperMessage;

public class HelperMessageCustom extends HelperMessage{

	private String sendGroupName;
	
	private String matchGroupName;

	public String getSendGroupName() {
		return sendGroupName;
	}

	public void setSendGroupName(String sendGroupName) {
		this.sendGroupName = sendGroupName;
	}

	public String getMatchGroupName() {
		return matchGroupName;
	}

	public void setMatchGroupName(String matchGroupName) {
		this.matchGroupName = matchGroupName;
	}
	
	
}
