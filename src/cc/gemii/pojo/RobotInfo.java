package cc.gemii.pojo;

import cc.gemii.po.Wechatrobotinfo;

public class RobotInfo extends Wechatrobotinfo{

	private String H5Url;

	public String getH5Url() {
		return H5Url;
	}

	public void setH5Url(String h5Url) {
		H5Url = h5Url;
	}
}
