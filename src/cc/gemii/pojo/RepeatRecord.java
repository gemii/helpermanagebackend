package cc.gemii.pojo;

public class RepeatRecord {

	private Integer ClerkID;
	
	private String RobotTag;
	
	private String RobotID;
	
	private Integer maxid;

	public Integer getClerkID() {
		return ClerkID;
	}

	public void setClerkID(Integer clerkID) {
		ClerkID = clerkID;
	}

	public String getRobotTag() {
		return RobotTag;
	}

	public void setRobotTag(String robotTag) {
		RobotTag = robotTag;
	}

	public String getRobotID() {
		return RobotID;
	}

	public void setRobotID(String robotID) {
		RobotID = robotID;
	}

	public Integer getMaxid() {
		return maxid;
	}

	public void setMaxid(Integer maxid) {
		this.maxid = maxid;
	}
	
}
