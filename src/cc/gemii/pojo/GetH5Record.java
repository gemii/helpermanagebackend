package cc.gemii.pojo;

public class GetH5Record {

	private String openid;
	
	private String username;
	
	private String city;
	
	private String keyword;
	
	private String edc;
	
	private String phone;

	private String srcode;
	
	private String formTime;

	private String roomName;
	
	private String area;
	
	private String province;
	
	private String cityLocation;
	
	private String RoomID;
	
	private String GroupAttr;
	
	private String department;
	
	private String relatedCity;
	
	private String hospitalName;
	
	private String hospitalCode;
	
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getEdc() {
		return edc;
	}

	public void setEdc(String edc) {
		this.edc = edc;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSrcode() {
		return srcode;
	}

	public void setSrcode(String srcode) {
		this.srcode = srcode;
	}

	public String getFormTime() {
		return formTime;
	}

	public void setFormTime(String formTime) {
		this.formTime = formTime;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getGroupAttr() {
		return GroupAttr;
	}

	public void setGroupAttr(String groupAttr) {
		GroupAttr = groupAttr;
	}

	public String getCityLocation() {
		return cityLocation;
	}

	public void setCityLocation(String cityLocation) {
		this.cityLocation = cityLocation;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getRelatedCity() {
		return relatedCity;
	}

	public void setRelatedCity(String relatedCity) {
		this.relatedCity = relatedCity;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getHospitalCode() {
		return hospitalCode;
	}

	public void setHospitalCode(String hospitalCode) {
		this.hospitalCode = hospitalCode;
	}
	
}
