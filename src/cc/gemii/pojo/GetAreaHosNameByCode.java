package cc.gemii.pojo;

public class GetAreaHosNameByCode {

	private String AreaName;
	
	private String HospitalName;

	public String getAreaName() {
		return AreaName;
	}

	public void setAreaName(String areaName) {
		AreaName = areaName;
	}

	public String getHospitalName() {
		return HospitalName;
	}

	public void setHospitalName(String hospitalName) {
		HospitalName = hospitalName;
	}
	
	
}
