package cc.gemii.pojo;

public class GroupCustom {

	private String RoomID;
	
	private String RoomName;
	
	private String UserCount;
	
	private String area;
	
	private String province;
	
	private String city;
	
	private String department;
	
	private String subName;
	
	private String channel;
	
	private String alias;

	private String groupAttr;
	
	public String getRoomID() {
		return RoomID;
	}

	public void setRoomID(String roomID) {
		RoomID = roomID;
	}

	public String getRoomName() {
		return RoomName;
	}

	public void setRoomName(String roomName) {
		RoomName = roomName;
	}

	public String getUserCount() {
		return UserCount;
	}

	public void setUserCount(String userCount) {
		UserCount = userCount;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getGroupAttr() {
		return groupAttr;
	}

	public void setGroupAttr(String groupAttr) {
		this.groupAttr = groupAttr;
	}
	
}
