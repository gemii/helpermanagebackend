package cc.gemii.pojo;

public class SelectMessageAB {

	private String robotID;
	
	private String username;
	
	private String roomName;
	
	private String urlStatus;
	
	private String enterRoomStatus;

	private String code;
	
	private Integer page;
	
	private Integer pageSize;
	
	private Integer start;
	
	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getUrlStatus() {
		return urlStatus;
	}

	public void setUrlStatus(String urlStatus) {
		this.urlStatus = urlStatus;
	}

	public String getEnterRoomStatus() {
		return enterRoomStatus;
	}

	public void setEnterRoomStatus(String enterRoomStatus) {
		this.enterRoomStatus = enterRoomStatus;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "SelectMessageAB [robotID=" + robotID + ", username=" + username
				+ ", roomName=" + roomName + ", urlStatus=" + urlStatus
				+ ", enterRoomStatus=" + enterRoomStatus + ", code=" + code
				+ ", page=" + page + ", pageSize=" + pageSize + ", start="
				+ start + "]";
	}
	
}
