package cc.gemii.pojo;

public class GetRobotIdByU {

	private String roomName;

	private String taskId;

	private String uRoomId;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getuRoomId() {
		return uRoomId;
	}

	public void setuRoomId(String uRoomId) {
		this.uRoomId = uRoomId;
	}

}
