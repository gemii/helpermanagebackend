package cc.gemii.pojo;

public class SelectMessage{

	private String robotID;
	
	private String upMessage;
	
	private String username;
	
	private String status;
	
	private Integer page;
	
	private Integer pageSize;

	private Integer start;
	

	public String getRobotID() {
		return robotID;
	}

	public void setRobotID(String robotID) {
		this.robotID = robotID;
	}

	public String getUpMessage() {
		return upMessage;
	}

	public void setUpMessage(String upMessage) {
		this.upMessage = upMessage;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}
}
