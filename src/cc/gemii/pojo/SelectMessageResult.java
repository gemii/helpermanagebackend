package cc.gemii.pojo;

import cc.gemii.po.WechatuserentergroupstatusWithBLOBs;

public class SelectMessageResult extends WechatuserentergroupstatusWithBLOBs{

	private String roomName;

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
}
