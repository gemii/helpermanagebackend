package cc.gemii.pojo;

public class RobotQrcode {

	private byte[] QrcodeImg;

	public byte[] getQrcodeImg() {
		return QrcodeImg;
	}

	public void setQrcodeImg(byte[] qrcodeImg) {
		QrcodeImg = qrcodeImg;
	}
}
