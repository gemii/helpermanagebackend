package cc.gemii.pojo;

public class SelectUser {
	
	private String userName;
	
	private String matchGroup;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMatchGroup() {
		return matchGroup;
	}

	public void setMatchGroup(String matchGroup) {
		this.matchGroup = matchGroup;
	}

}
