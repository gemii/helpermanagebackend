package cc.gemii.task;

import org.springframework.stereotype.Component;

@Component("testDemoTask")
public class TestDemoTask {

	public void test() {
		System.out.println(System.currentTimeMillis());
	}
}
