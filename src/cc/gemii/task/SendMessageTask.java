package cc.gemii.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.po.TaskNow;
import cc.gemii.pojo.SendMsgTask;
import cc.gemii.service.TaskNowService;

/**
 * 定时发送短信
 * 
 * @author gemii
 *
 */
@Component("sendMessageTask")
public class SendMessageTask {

	private Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private TaskNowService taskNowService;

	@Autowired
	private Properties propertyConfigurer;

	@Autowired
	private SendMsgContext sendMsgContext;

	public void sendMsg() {
		logger.info("发送定时短信开始 ");
		List<SendMsgTask> sendmsg = taskNowService.getPhones();
		if (sendmsg != null && sendmsg.size() > 0) {
			logger.info("可以发送短信 ");
			StringBuffer sb = new StringBuffer();
			for (SendMsgTask sendMsgTask : sendmsg) {
				sb.append(sendMsgTask.getPhoneNumber()).append(",");
			}
			String phone = sb.substring(0, sb.length() - 1);
			GeneralContentResult<String> result = sendMsg(phone);
			List<TaskNow> taskNows=new ArrayList<>();
			if (ResultCode.OPERATION_SUCCESS.equals(result.getResultCode())) {
				for (SendMsgTask sendMsgTask : sendmsg) {
					TaskNow	task = new TaskNow();
					task.setFKUserStatusId(sendMsgTask.getId());
					task.setType("1");
					task.setTaskName("定时发送短信");
					taskNows.add(task);
				}
				taskNowService.insertTaskInfo(taskNows);
				logger.info("一共发送了 "+sendmsg.size()+"条短信");
			}
		}
		logger.info("发送定时短信结束 ");
	}

	/**
	 * 发送短信
	 * 
	 * @param phone
	 * @return
	 */
	private GeneralContentResult<String> sendMsg(String phone) {
		ShortMessage _shortMessage = new ShortMessage();
		_shortMessage.setReciverPhone(phone);
		String firstTemplate = propertyConfigurer.getProperty("send_msg_task");
		_shortMessage.setContent(firstTemplate);
		return sendMsgContext.sendSMS(_shortMessage);
	}

}
