package cc.gemii.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import cc.gemii.data.otd.ErrorInfoToFriso;
import cc.gemii.service.FrisoEnterGroupService;
import cc.gemii.util.HttpClientUtil;

/**
 * 定时推送白天推送失败的消息
 * 
 * @author gemii
 *
 */
@Component("sendErrorInfoToFriso")
public class SendErrorInfoToFriso {

	private Logger logger = Logger.getLogger(this.getClass());
	
	private static String DEFAULT_CHARSET = "UTF-8";
	
	@Autowired
	private FrisoEnterGroupService frisoEnterGroupService;
	
	@Autowired
	private Properties propertyConfigurer;
	
	public void sendErrorInfoToFriso() throws Exception{
		logger.info("定时任务开始批量推送白天推送失败的消息");
		//查询错误信息条数，确定循环次数
		int count = frisoEnterGroupService.countErrorInfo();
		if(count > 0){
			int num = count/100 + 1;//循环次数
			logger.info("预计推送次数 ：" + num);
			//获取access_token
			logger.info("获取access_token");
			String param = this.propertyConfigurer.getProperty("APPID_APPSECRET");
			String url_getAccessToken = this.propertyConfigurer.getProperty("GET_ACCESS_TOKEN");
			String accessResult = HttpClientUtil.request(url_getAccessToken, param,"GET", DEFAULT_CHARSET, true, null);
			Map<String,Object> token = JSON.parseObject(accessResult);
			String access_token = (String) token.get("data");
			logger.info("定时任务获取到access_token的值为 ：" + access_token);
			//将access_token置于请求头
			Map<String,String> header = new HashMap<>();
			header.put("Authorization","bearer "+ access_token);
			header.put("Content-Type", "application/json");
			String url_returnInfo = this.propertyConfigurer.getProperty("RETURN_INFO_TO_FRISO");
			//循环推送失败消息
			for(int i = 0; i < num; i++){
				//查询近一个星期推送失败的消息(100条)
				List<ErrorInfoToFriso> selectErrorInfo = frisoEnterGroupService.selectErrorInfo();
				String json = JSON.toJSON(selectErrorInfo).toString();
				//推送美素
//				String resultByFriso = HttpClientUtil.doPostJsonSetHeader(this.propertyConfigurer.getProperty("RETURN_INFO_TO_FRISO"), json,header);
				String resultByFriso = HttpClientUtil.request(url_returnInfo, json, "POST", DEFAULT_CHARSET, true, header);
				Map<String,Object> result = JSON.parseObject(resultByFriso);
				//返回成功，更新status为2
				if(result.get("errcode").equals(0)){
					frisoEnterGroupService.updateErrorInfo(selectErrorInfo);
					logger.info("推送消息成功");
				}else{
					logger.info("推送消息失败");
				}
			}
		}

	}
	
//	public static void main(String[] args) {
//		ArrayList<Object> list = new ArrayList<Object>();
//		
//		ErrorInfoToFriso errorInfoToFriso = new ErrorInfoToFriso();
//		errorInfoToFriso.setEventTime("11");
//		errorInfoToFriso.setGroupName("你好");
//		list.add(errorInfoToFriso);
//		System.out.println(JSON.toJSON(list).toString());
//	}
	
	
}
