package cc.gemii.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.web.context.support.WebApplicationContextUtils;

import cc.gemii.mapper.custom.OtherMapper;
import cc.gemii.util.RedisUtil;
import cc.gemii.util.TagUtil;

public class GetTag implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		//destroy jedis pool
		RedisUtil.destroyJedisPool();
	}

	@Override
	public void contextInitialized(ServletContextEvent sc) {
		OtherMapper otherMapper = WebApplicationContextUtils
				.getWebApplicationContext(sc.getServletContext()).getBean(OtherMapper.class);
		int tag = otherMapper.getTag() + 1;
		TagUtil.setTag(tag);
	}
}