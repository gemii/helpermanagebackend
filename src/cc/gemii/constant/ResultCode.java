package cc.gemii.constant;

public class ResultCode {
	/*系统级 返回 code码*/
	/**操作成功*/
	public final static String OPERATION_SUCCESS = "100";
	/**操作异常*/
	public final static String OPERATION_ERROR = "101";
	/**操作失败*/
	public final static String OPERATION_FAIL = "102";
	/**操作未登录*/
	public final static String OPERATION_NOT_LOGGEDIN = "103";
	/**操作访问受限*/
	public final static String OPERATION_ACCESS_REFUSED = "104";
	/**操作失败*/
	public final static String OPERATION_NOT_MATCH_DATA = "201";
	/**已存在**/
	public final static String INFO_EXIST="300";
	
	/**返回成功**/
	public final static Integer RESULT_SUCCESS = 200;
	
	/**返回失败**/
	public final static Integer RESULT_FAIL = -100;
	
	/**返回其他信息**/
	public final static Integer RESULT_OTHER = 100;

	/*
	 * 功能级 返回code码
	 * 20100  
	 * 第1位    2表示功能级code码，
	 * 第2～3位 01表示匹配小助手功能
	 * 第4～5位 00表示具体的错误码值
	 * */
	/**缺少关键参数*/
	public final static String ENTER_GROUP_INPUT_ERROR = "20100";
	/**已入群*/
	public final static String ENTER_GROUP_REPEAT = "20101";
	/**未找到合适的群*/
	public final static String ENTER_GROUP_NOT_MATCH_ROOM = "20102";
	/**未找到合适的小助手*/
	public final static String ENTER_GROUP_NOT_MATCH_ROBOT = "20103";

}
