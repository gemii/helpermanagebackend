package cc.gemii.component.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.po.Wechatclerk;

public class AdminAccessFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletResponse response = (HttpServletResponse) resp;
		HttpServletRequest request = (HttpServletRequest) req;
		Wechatclerk user = (Wechatclerk)request.getSession().getAttribute("user");
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		if(user == null){
			result.setResultCode(ResultCode.OPERATION_NOT_LOGGEDIN);
			result.setDetailDescription("未登录");
			response.setContentType("application/json;charset=UTF-8");
			response.getOutputStream().write(JSONObject.toJSONString(result).getBytes());
			return;
		}
		if(!"sadmin".equals(user.getType())){
			response.setContentType("application/json;charset=UTF-8");
			result.setResultCode(ResultCode.OPERATION_ACCESS_REFUSED);
			result.setDetailDescription("权限不足");
			response.getOutputStream().write(JSONObject.toJSONString(result).getBytes());
			return;
		}
		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
