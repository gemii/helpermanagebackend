package cc.gemii.component.log;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;


/**
 * 统一异常拦截类
 * @author xdli
 *
 */
@Component
@Aspect
public class LogInterceptor{
	
	private Logger logger = Logger.getLogger(this.getClass());

	private final static long EXPECTED_METHOD_TIME = 2000;
	
	@Pointcut("execution(* cc.gemii..*(..))")
    private void anyMethod(){}//定义一个切入点
	
	
	@Around("anyMethod()")
	public Object aronud(ProceedingJoinPoint  call) throws Throwable{
		String methodName = "";
		try {
			long startTime = System.currentTimeMillis();
			Object proceedReturn =  call.proceed();
			long endTime = System.currentTimeMillis();
			long useTime = endTime - startTime;
			if(useTime > EXPECTED_METHOD_TIME){
				try {
					MethodSignature signature =  (MethodSignature)call.getSignature();
					methodName =call.getTarget().getClass().getName()+"."+signature.getMethod().getName();
					StringBuffer sb = new StringBuffer();
					for (Object arg : call.getArgs()) {
						sb.append(arg.getClass().getSimpleName()).append("=").append(JSONObject.toJSONString(arg)).append("|");
					}
					logger.info("logInterceptor 方法执行时间超强期望时间 耗时："+useTime+"ms-methodName="+methodName+"-args="+sb);
				} catch (Exception e) {
					logger.info("logInterceptor 方法执行时间超强期望时间 耗时："+useTime+"ms-methodName="+methodName+"-error");
				}
			}
			return proceedReturn;
		} catch (Exception e) {
			logger.info(e.getMessage(),e);
			throw e;
		}
	}
}
