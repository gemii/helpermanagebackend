package cc.gemii.component.matchGroup;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.util.RegularUtil;
import net.sf.json.JSONObject;

/**
 * 根据给定条件匹配合适的群,匹配策略选择容器
 */
@Component
public class MatchGroupContext {

	private Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private IMatchGroupStrategy huggiesMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy wyethCardMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy goodParentMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy wyethPharmacyMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy loveBabyMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy beautyGoodSonMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy wyethThreeMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy leYinMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy yiBeiMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy newBeautyGoodSonMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy matchGroupForFrisoStrategy;
	@Autowired
	private IMatchGroupStrategy gemiiMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy wyethTrobledMumMatchGroupStrategy;
	@Autowired
	private IMatchGroupStrategy cooperationMatchGroupStrategy;
	
	
	/**
	 * 容器匹配策略枚举
	 */
	public enum MatchGroupStrategy {
		/**
		 * 好奇入群策略
		 */
		HUGGIES_MATCHGROUP_STRATEGY,
		/**
		 * 惠氏卡券入群策略
		 */
		WYETH_CARD_MATCHGROUP_STRATEGY,

		/**
		 * 好育儿入群策略
		 */
		GOOD_PARENT_MATCHGROUP_STRATEGY,
		
		/**
		 * 药店入群
		 */
		WYETH_PHARMACY_MATCHGROUP_STRATEGY,
		/**
		 * 爱婴岛入群策略
		 */
		LOVE_BABY_MATCHGROUP_STRATEGY, 
		/**
		 * 美素佳儿入群策略
		 */
		BEAUTY_GOODSON_MATCHGROUP_STRATEGY,
		/**
		 * 惠氏匹配合作群,自建群，网红群
		 */
		WYETH_THREE_MATCHGROUP_STRATEGY,
		/**
		 * 乐茵入群策略
		 */
		LE_YIN_MATCHGROUP_STRATEGY,
		/**
		 * 亿贝入群策略
		 */
		YI_BEI_MATCHGROUP_STRATEGY,
		/**
		 * 新的美素佳儿入群策略
		 */
		NEW_BEAUTY_GOODSON_MATCHGROUP_STRATEGY,
		/**
		 * 对接美素佳儿入群策略
		 */
		MATCHGROUP_FOR_FRISO_STRATEGY,
		/**
		 * 景栗入群匹配
		 */
		GEMII_MATCHGROUP_STRATEGY,
		/**
		 * 事儿妈渠道入群
		 */
		WYETH_TROBLEDMUM_MATCHGROUP_STRATEGY,
		/**
		 * 惠氏项目合作群入群
		 */
		COOPERATION_MATCHGROUP_STRATEGY
	}

	/**
	 * 匹配群
	 */
	public MatchGroupResult matchGroup(MatchGroupStrategy _strategy, Map<String, String> _params) {
		logger.info("matchGroup-start--strategy=" + _strategy + "--params=" + JSONObject.fromObject(_params));
		MatchGroupResult matchGroupResult = null;
		switch (_strategy) {
		case HUGGIES_MATCHGROUP_STRATEGY:
			matchGroupResult = huggiesMatchGroupStrategy.matchGroup(_params);
			break;
		case WYETH_CARD_MATCHGROUP_STRATEGY:
			matchGroupResult = wyethCardMatchGroupStrategy.matchGroup(_params);
			break;
		case GOOD_PARENT_MATCHGROUP_STRATEGY:
			matchGroupResult = goodParentMatchGroupStrategy.matchGroup(_params);
			break;
		case WYETH_PHARMACY_MATCHGROUP_STRATEGY:
			matchGroupResult = wyethPharmacyMatchGroupStrategy.matchGroup(_params);
			break;
		case LOVE_BABY_MATCHGROUP_STRATEGY:
			matchGroupResult = loveBabyMatchGroupStrategy.matchGroup(_params);
			break;
		case BEAUTY_GOODSON_MATCHGROUP_STRATEGY:
			matchGroupResult = beautyGoodSonMatchGroupStrategy.matchGroup(_params);
			break;
		case WYETH_THREE_MATCHGROUP_STRATEGY:
			matchGroupResult = wyethThreeMatchGroupStrategy.matchGroup(_params);
			break;
		case LE_YIN_MATCHGROUP_STRATEGY:
			matchGroupResult = leYinMatchGroupStrategy.matchGroup(_params);
			break;
		case YI_BEI_MATCHGROUP_STRATEGY:
			matchGroupResult = yiBeiMatchGroupStrategy.matchGroup(_params);
			break;
		case NEW_BEAUTY_GOODSON_MATCHGROUP_STRATEGY:
			matchGroupResult = newBeautyGoodSonMatchGroupStrategy.matchGroup(_params);
			break;
		case MATCHGROUP_FOR_FRISO_STRATEGY:
			matchGroupResult = matchGroupForFrisoStrategy.matchGroup(_params);
			break;
		case GEMII_MATCHGROUP_STRATEGY:
			matchGroupResult = gemiiMatchGroupStrategy.matchGroup(_params);
			break;
		case WYETH_TROBLEDMUM_MATCHGROUP_STRATEGY:
			matchGroupResult = wyethTrobledMumMatchGroupStrategy.matchGroup(_params);
			break;
		case COOPERATION_MATCHGROUP_STRATEGY:
			matchGroupResult=cooperationMatchGroupStrategy.matchGroup(_params);
			break;
		default:
			break;
		}
		logger.info("matchGroup-end--strategy=" + _strategy + "--matchGroupResult="
				+ JSONObject.fromObject(matchGroupResult));
		return matchGroupResult;
	}
}
