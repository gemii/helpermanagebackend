package cc.gemii.component.matchGroup.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;

/**
 * 惠氏-事儿妈 入群
 * */
@Component("wyethTrobledMumMatchGroupStrategy")
public class WyethTrobledMumMatchGroupStrategy implements IMatchGroupStrategy {
	
	private Logger logger = Logger.getLogger(this.getClass());
	private final static String GROUPTAG = "16";
	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}

	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		String groupid=matchGroupBy(_matchingconds);
		if(null==groupid){
			return buildMatchGroupResult(-1, null);
		}
		return buildMatchGroupResult(1,groupid);
	}
	
	private String matchGroupBy(Map<String, String> _matchingconds){
		Map<String, String> param = new HashMap<>();
		//匹配全国群
		param.put("groupTag", GROUPTAG); //城市群与全国群不需要指定groupTag
		param.put("GroupAttr", "事儿妈群");
		param.put("city", _matchingconds.get("city"));
		List<String> groupIds = enterGroupMapper.simpleMatchGroup(param); 
		if(groupIds !=null && groupIds.size()>0){
			return groupIds.get(0);
		}
		return null;
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}	
}
