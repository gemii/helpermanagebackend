package cc.gemii.component.matchGroup.strategy;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;

/**
 * 惠氏药店入群
 * @author gemii
 *
 */
@Component("wyethPharmacyMatchGroupStrategy")
public class WyethPharmacyMatchGroupStrategy implements IMatchGroupStrategy{

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}

	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		String groupid=matchGroupBy(_matchingconds);
		if(null==groupid){
			return buildMatchGroupResult(-1, null);
		}
		return buildMatchGroupResult(1,groupid);
	}
	
	private String matchGroupBy(Map<String, String> _matchingconds){
		Map<String, String> param = new HashMap<>();
		if(!"其他".equals(_matchingconds.get("hospital")) && StringUtils.isNotBlank(_matchingconds.get("hospital"))){
			param.put("city", _matchingconds.get("city"));
			param.put("hospitalName", _matchingconds.get("hospital"));
			param.put("edc", _matchingconds.get("edc"));
			param.put("groupTag", "3");
			param.put("GroupAttr", "药店群");
			List<String> groupIds = enterGroupMapper.pharmacyMatchGroup(param); 
			if(groupIds !=null && groupIds.size()>0){
				return groupIds.get(0);
			}
		}
		//匹配城市群
		if(!"其他".equals(_matchingconds.get("city")) && StringUtils.isNotBlank(_matchingconds.get("city"))){
			param = new HashMap<>();
			param.put("city", _matchingconds.get("city"));
			param.put("edc", _matchingconds.get("edc"));
//			param.put("groupTag", "3");   城市群与全国群不需要指定groupTag
			param.put("GroupAttr", "城市群");
			List<String> groupIds = enterGroupMapper.pharmacyMatchGroupExt(param); 
			if(groupIds !=null && groupIds.size()>0){
				return groupIds.get(0);
			}
		}
		//匹配全国群
		param = new HashMap<>();
//		param.put("groupTag", "3");   城市群与全国群不需要指定groupTag
		param.put("GroupAttr", "全国群");
		param.put("roomName", "新妈");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date edcDate = sdf.parse(_matchingconds.get("edc"));
			if(edcDate.getTime() > new Date().getTime()){
				param.put("roomName", "孕妈");
			}
		} catch (ParseException e) {
			logger.error(e.getMessage(),e);
		}
		List<String> groupIds = enterGroupMapper.pharmacyMatchGroupExt(param); 
		if(groupIds !=null && groupIds.size()>0){
			return groupIds.get(0);
		}
		return null;
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}

}
