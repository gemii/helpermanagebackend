package cc.gemii.component.matchGroup.strategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.util.RegularUtil;
import cc.gemii.util.TimeUtils;

@Component("wyethCardMatchGroupStrategy")
public class WyethCardMatchGroupStrategy implements IMatchGroupStrategy {

	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}

	@Autowired
	private EnterGroupMapper enterGroupMapper;

	/**
	 * TODO 匹配群逻辑待修改
	 */
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {

		Map<String, String> groupid=matchGroupBy(_matchingconds);
		if (groupid != null) {
			if("匹配医院群成功".equals(groupid.get("returntype"))){
				return buildMatchGroupResult(1, groupid.get("groupids"));
			}else if("匹配城市群成功".equals(groupid.get("returntype"))){
				return buildMatchGroupResult(2, groupid.get("groupids"));
			}else{
				return buildMatchGroupResult(3, groupid.get("groupids"));
			}
			
		}
		return buildMatchGroupResult(-1, null);
	}

	private Map<String, String> matchGroupBy(Map<String, String> _matchingconds) {
		
		Map<String, String> param = new HashMap<>();
		Map<String, String> returninfo=new HashMap<>();
		// 匹配三个条件
		if(!"其他".equals(_matchingconds.get("hospital"))){
			param.put("city", _matchingconds.get("city"));
			param.put("hospitalName", _matchingconds.get("hospital"));
			param.put("edc", _matchingconds.get("edc"));
			param.put("type", "1");
			List<String> groupIds = enterGroupMapper.matchGroupByWyeth(param); 
			if (groupIds != null && groupIds.size() > 0) {
				returninfo.put("returntype", "匹配医院群成功");
				returninfo.put("groupids", groupIds.get(0));
				return returninfo;
			}
		}
		 if (!"其他".equals(_matchingconds.get("city")))
		{
			// 匹配两个条件
			param = new HashMap<>();
			param.put("city", _matchingconds.get("city"));
			param.put("edc", _matchingconds.get("edc"));
			param.put("type", "2");
			List<String> groupIds2 = enterGroupMapper.matchGroupByWyeth(param); 
			if (groupIds2 != null && groupIds2.size() > 0) {
				returninfo.put("returntype", "匹配城市群成功");
				returninfo.put("groupids", groupIds2.get(0));
				return returninfo;
			}
		}
		// 匹配一个条件
		param = new HashMap<>();
		param.put("edc", validateEdc(_matchingconds.get("edc")));
		param.put("type", "3");
		List<String> groupIds3 = enterGroupMapper.matchGroupByWyeth(param);
		if (groupIds3 != null && groupIds3.size() > 0) {
			returninfo.put("returntype", "匹配全国群成功");
			returninfo.put("groupids", groupIds3.get(0));
			return returninfo;
		}
		return null;
	}

	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}
	
	private String validateEdc(String edc) {
		// 验证预产期,根据edc与当前日期进行比较,edc早于当前日期365天入 %新妈%群,晚于当前日期280天入 %孕妈%群
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		long day;
		try {
			day = TimeUtils.dateDiff(sdf.parse(edc));
			if (day < -365 || day > 280) {
				return "无";
			} else if (day >= -365 && day <= 0) {
				return "%新妈%";
			} else {
				return "%孕妈%";
			}
		} catch (ParseException e) {
			return "无";
		}

	}

}
