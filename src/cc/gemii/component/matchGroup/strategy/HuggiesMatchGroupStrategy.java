package cc.gemii.component.matchGroup.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.po.BlackWhiteListDO.BlackWhiteListType;
import cc.gemii.service.BlackWhiteListService;
import cc.gemii.util.RedisUtil;

/**
 * 好奇入群匹配策略
 * */
@Component("huggiesMatchGroupStrategy")
public class HuggiesMatchGroupStrategy implements IMatchGroupStrategy {
	
	private Logger logger = Logger.getLogger(this.getClass());
	
	/**
	 * 好奇入群黑白名单 functionCode
	 * */
	private static final String ENTERGROUP_HAOQI_WHITELIST="ENTERGROUP_HAOQI";
	/**
	 * 好奇入群每个群 金牌妈妈最大数量限制
	 * */
	private static final int HQ_USER_COUNT_LIMIT = 3;
	/**
	 * 好奇入群每个群 本地金牌妈妈最小数量限制
	 * */
	private static final int LOCAL_USER_COUNT_MIN_LIMIT = 1;

	/**
	 * 匹配群结果映射
	 * */
	private static Map<Integer,String> resultCodeMap = new HashMap<Integer,String>();
	static{
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "不是金牌妈妈");
		resultCodeMap.put(-2, "名额有限");
		resultCodeMap.put(-3, "重复申请");
		resultCodeMap.put(-4, "群匹配失败");
	}
	
	@Autowired
	private BlackWhiteListService blackWhiteListService;
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	/**
	 * 根据条件匹配合适群
	 * @param _matchingconds 匹配群的传人条件
	 * @return 匹配到的群
	 * */
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		String phone = _matchingconds.get("phone");
		String openid = _matchingconds.get("openid");
		String city = _matchingconds.get("city");
		
		int checkResult = checkBeforeMatchGroup(phone,openid);
		if(checkResult != 1){
			return buildMatchGroupResult(checkResult,null);
		}
		//根据用户选择的城市匹配群
		String matchedGroupID = matchGroupByCity(city);

		if(matchedGroupID == null){
			return buildMatchGroupResult(-4,null);
		}
		return buildMatchGroupResult(1,matchedGroupID);
	}

	/**
	 * 根据城市名匹配群
	 * @param _city 城市名
	 * @return 匹配到的群ID，为null表示没有匹配到合适的群
	 * */
	private String matchGroupByCity(String _city){
		Map<String,String> params = new HashMap<String,String>();
		//1,先匹配城市中的群
		params.put("city",_city);
		params.put("searchCity",_city);
		params.put("GroupAttr","'医院群','城市群'");
		params.put("memberCountLimit",String.valueOf(HQ_USER_COUNT_LIMIT));
		List<String> groupIDs = enterGroupMapper.matchGroupByHuggies(params);
		if(groupIDs != null && groupIDs.size() > 0){
			logger.info("matchGroupByCity-匹配到该城市的群-groupId="+groupIDs.get(0));
			return groupIDs.get(0);
		}
		//2,再匹配兄弟城市的群
		params = new HashMap<String,String>();
		params.put("city",_city);
		params.put("GroupAttr","'医院群','城市群'");
		params.put("memberCountLimit",String.valueOf(HQ_USER_COUNT_LIMIT));
		groupIDs = enterGroupMapper.matchGroupByHuggies(params);
		if(groupIDs != null && groupIDs.size() > 0){
			for (String groupID : groupIDs) {
				int localUserCount = enterGroupMapper.getHQUserCount(true,groupID);
				if(localUserCount >= LOCAL_USER_COUNT_MIN_LIMIT){
					logger.info("matchGroupByCity-匹配到兄弟城市的群-groupId="+groupID);
					return groupID;
				}
				if(localUserCount < LOCAL_USER_COUNT_MIN_LIMIT){
					int userCount = enterGroupMapper.getHQUserCount(false,groupID);
					if(userCount < (HQ_USER_COUNT_LIMIT - LOCAL_USER_COUNT_MIN_LIMIT) ){
						logger.info("matchGroupByCity-匹配到兄弟城市的群-groupId="+groupID);
						return groupID;
					}
				}
			}
		}
		//3,再匹配全国群
		params = new HashMap<String,String>();
		params.put("city",_city);
		params.put("GroupAttr","'全国群'");
		params.put("memberCountLimit",String.valueOf(HQ_USER_COUNT_LIMIT));
		groupIDs = enterGroupMapper.matchGroupByHuggies(params);
		if(groupIDs != null && groupIDs.size() > 0){
			logger.info("matchGroupByCity-匹配到全国的群-groupId="+groupIDs.get(0));
			return groupIDs.get(0);
		}	
		//4,若全国群还匹配不了，随机选取一个其他省的群（预留一个名额给本城市的金牌妈妈）
		params = new HashMap<String,String>();
		params.put("GroupAttr","'医院群','城市群'");
		params.put("memberCountLimit",String.valueOf(HQ_USER_COUNT_LIMIT));
		groupIDs = enterGroupMapper.matchGroupByHuggies(params);
		if(groupIDs != null && groupIDs.size() > 0){
			for (String groupID : groupIDs) {
				int localUserCount = enterGroupMapper.getHQUserCount(true,groupID);
				if(localUserCount >= LOCAL_USER_COUNT_MIN_LIMIT){
					logger.info("matchGroupByCity-匹配到其他省份的群-groupId="+groupID);
					return groupID;
				}
				if(localUserCount < LOCAL_USER_COUNT_MIN_LIMIT){
					int userCount = enterGroupMapper.getHQUserCount(false,groupID);
					if(userCount < (HQ_USER_COUNT_LIMIT - LOCAL_USER_COUNT_MIN_LIMIT) ){
						logger.info("matchGroupByCity-匹配到其他省份的群-groupId="+groupID);
						return groupID;
					}
				}
			}
		}
		logger.info("matchGroupByCity-没有匹配到合适的群-groupId=null");
		return null;
	}
	
	/**
	 * 匹配前对传入信息校验
	 * @param _phone 手机号
	 * @param _openid 微信号openid
	 * @return 校验结果，结果参照<code>resultCodeMap</code>
	 * */
	private int checkBeforeMatchGroup(String _phone,String _openid){
		BlackWhiteListType blackWhiteListType = blackWhiteListService.getUserType(_phone, ENTERGROUP_HAOQI_WHITELIST);
		if(BlackWhiteListType.WHITE != blackWhiteListType){
			return -1;
		}
		Integer maxMemberCount = Integer.valueOf(RedisUtil.get("huggies_max_member"));
		Integer currentMemberCount = enterGroupMapper.getHuggiesMemberCount();
		if(currentMemberCount >= maxMemberCount){
			return -2;
		}
		Integer id = enterGroupMapper.openidExists(_openid);
		if(id != null ){
			return -3;
		}
		return 1;
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode,String _matchGroupId){
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get( _resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}
	
}
