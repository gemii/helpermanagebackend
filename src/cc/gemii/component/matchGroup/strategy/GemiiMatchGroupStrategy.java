package cc.gemii.component.matchGroup.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;

@Component("gemiiMatchGroupStrategy")
public class GemiiMatchGroupStrategy implements IMatchGroupStrategy{
	
	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}

	@Autowired
	private EnterGroupMapper enterGroupMapper;

	/**
	 * TODO 匹配群逻辑待修改
	 */
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {

		Map<String, String> groupid=matchGroupBy(_matchingconds);
		if (groupid != null) {
				return buildMatchGroupResult(1, groupid.get("groupids"));
			
		}
		return buildMatchGroupResult(-1, null);
	}

	private Map<String, String> matchGroupBy(Map<String, String> _matchingconds) {
		Map<String, String> param = new HashMap<>();
		param.put("city", _matchingconds.get("city"));
		param.put("groupTag", _matchingconds.get("type"));
		String groupId="";
		if("8".equals(_matchingconds.get("type"))){
			groupId = enterGroupMapper.matchGroupByGemii(param); 
		}else if("15".equals(_matchingconds.get("type"))){
			groupId= enterGroupMapper.matchGroupByCityWyeth(param);
		}
		Map<String, String> returninfo=new HashMap<>();
		if (groupId != null && !"".equals(groupId)) {
			returninfo.put("groupids", groupId);
			return returninfo;
		}
		return null;
	}

	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}



}
