package cc.gemii.component.matchGroup.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.po.Userinfo;
import cc.gemii.po.Wechatroommember;

/**
 * 美素佳儿入群匹配策略
 * @author gemii
 *
 */
@Component("beautyGoodSonMatchGroupStrategy")
public class BeautyGoodSonMatchGroupStrategy implements IMatchGroupStrategy {

	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
		resultCodeMap.put(2, "重复入群");
	}
	
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		Boolean b = checkRepeatEnter(_matchingconds);
		if(b){
			//重复入群
			return buildMatchGroupResult(2, null);
		}
		String groupid=matchGroupBy(_matchingconds);
		if(groupid != null){
			return buildMatchGroupResult(1,groupid);
		}
		return buildMatchGroupResult(-1, null);
	}
	
	/**
	 * 判断是否重复入群
	 * @param _matchingconds
	 * @return 
	 */
	private Boolean checkRepeatEnter(Map<String, String> _matchingconds) {
		Map<String, String> param = new HashMap<>();
		param.put("phone", _matchingconds.get("phone"));
		param.put("type", _matchingconds.get("type"));
		//根据手机号校验重复入群
		List<Userinfo> userinfoList = enterGroupMapper.selectUserInfoByPhone(param);
		if(null != userinfoList && userinfoList.size() > 0){
			String openid = userinfoList.get(0).getOpenid();
			List<String> memberlist = enterGroupMapper.selectMemberInfoByOpenid(openid);
			if(null != memberlist && memberlist.size() > 0){
				//重复入群
				return true;
			}
		}
		return false;
	}

	private String matchGroupBy(Map<String, String> _matchingconds){
		Map<String, String> param = new HashMap<>();
		param.put("city", _matchingconds.get("city"));
		param.put("edc", _matchingconds.get("edc"));
		param.put("groupTag", "5");
		param.put("GroupAttr", "城市群");
		List<String> groupIds = enterGroupMapper.matchGroupByFriso(param); 
		if(groupIds !=null && groupIds.size()>0){
			return groupIds.get(0);
		}
		return null;
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}

}
