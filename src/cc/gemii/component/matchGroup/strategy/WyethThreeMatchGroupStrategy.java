package cc.gemii.component.matchGroup.strategy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.util.TimeUtils;

/**
 * 惠氏入合作群,自建群，网红群
 * 
 * @author gemii
 *
 */
@Component("wyethThreeMatchGroupStrategy")
public class WyethThreeMatchGroupStrategy implements IMatchGroupStrategy {
	
	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}

	@Autowired
	private EnterGroupMapper enterGroupMapper;

	/**
	 * TODO 匹配群逻辑
	 */
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {

		Map<String, String> groupid = matchGroupBy(_matchingconds);
		if (groupid != null) {
			if ("1".equals(groupid.get("returntype"))) {
				return buildMatchGroupResult(1, groupid.get("groupids"));
			} else if ("2".equals(groupid.get("returntype"))) {
				return buildMatchGroupResult(2, groupid.get("groupids"));
			} else {
				return buildMatchGroupResult(3, groupid.get("groupids"));
			}

		}
		return buildMatchGroupResult(-1, null);
	}

	private Map<String, String> matchGroupBy(Map<String, String> _matchingconds) {

		Map<String, String> param = new HashMap<>();
		Map<String, String> returninfo = new HashMap<>();
		// 匹配三个条件
		if(!"其他".equals(_matchingconds.get("hospital"))){
			param.put("city", _matchingconds.get("city"));
			param.put("hospitalName", _matchingconds.get("hospital"));
			param.put("edc", _matchingconds.get("edc"));
			param.put("robotID", _matchingconds.get("robotID"));
			if("2".equals(_matchingconds.get("type")) || "3".equals(_matchingconds.get("type")) || "5".equals(_matchingconds.get("type"))){
				param.put("type", "4");
			}
			List<String> groupIds = enterGroupMapper.matchGroupByH5(param);
			if (groupIds != null && groupIds.size() > 0) {
				returninfo.put("returntype", "1");
				returninfo.put("groupids", groupIds.get(0));
				return returninfo;
			}
		}
		if (!"其他".equals(_matchingconds.get("city"))){
			// 匹配两个条件
			param = new HashMap<>();
			param.put("city", _matchingconds.get("city"));
			param.put("edc", _matchingconds.get("edc"));
			param.put("robotID", _matchingconds.get("robotID"));
			param.put("type", "2");
			List<String> groupIds2 = enterGroupMapper.matchGroupByH5(param);
			if (groupIds2 != null && groupIds2.size() > 0) {
				returninfo.put("returntype", "2");
				returninfo.put("groupids", groupIds2.get(0));
				return returninfo;
			}
		}
		// 匹配一个条件
		param = new HashMap<>();
		param.put("type", "3");
		param.put("roomName", "新妈");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date edcDate = sdf.parse(_matchingconds.get("edc"));
			if(edcDate.getTime() > new Date().getTime()){
				param.put("roomName", "孕妈");
			}
		} catch (ParseException e) {
			logger.error(e.getMessage(),e);
		}
		param.put("robotID", _matchingconds.get("robotID"));
		List<String> groupIds3 = enterGroupMapper.matchGroupByH5(param);
		if (groupIds3 != null && groupIds3.size() > 0) {
			returninfo.put("returntype", "3");
			returninfo.put("groupids", groupIds3.get(0));
			return returninfo;
		}
		return null;
	}

	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}
	
	
}
