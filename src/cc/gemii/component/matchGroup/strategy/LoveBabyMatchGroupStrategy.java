package cc.gemii.component.matchGroup.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;

/**
 * 爱婴岛入群匹配策略
 * @author gemii
 *
 */
@Component("loveBabyMatchGroupStrategy")
public class LoveBabyMatchGroupStrategy implements IMatchGroupStrategy{
	
	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}

	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		String groupid=matchGroupBy(_matchingconds);
		if(!"".equals(groupid) && groupid != null){
			return buildMatchGroupResult(1,groupid);
		}
		return buildMatchGroupResult(-1, null);
	}
	
	private String matchGroupBy(Map<String, String> _matchingconds){
		Map<String, String> param = new HashMap<>();
		param.put("city", _matchingconds.get("city"));
		param.put("hospitalName", _matchingconds.get("hospital"));
		param.put("edc", _matchingconds.get("edc"));
		param.put("groupTag", "4");
		param.put("GroupAttr", "门店群");
		List<String> groupIds = enterGroupMapper.matchGroupByGoodParent(param); 
		if(groupIds !=null && groupIds.size()>0){
			return groupIds.get(0);
		}
		return null;
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}

}
