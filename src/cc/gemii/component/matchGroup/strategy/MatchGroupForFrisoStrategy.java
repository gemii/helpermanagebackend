package cc.gemii.component.matchGroup.strategy;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cc.gemii.component.matchGroup.IMatchGroupStrategy;
import cc.gemii.component.matchGroup.MatchGroupResult;
import cc.gemii.mapper.EnterGroupMapper;
import cc.gemii.util.TimeUtils;

/**
 * 对接美素佳儿入群匹配策略
 * @author gemii
 *
 */
@Component("matchGroupForFrisoStrategy")
public class MatchGroupForFrisoStrategy implements IMatchGroupStrategy {

	/**
	 * 匹配群结果映射
	 */
	private static Map<Integer, String> resultCodeMap = new HashMap<Integer, String>();
	static {
		resultCodeMap.put(1, "匹配成功");
		resultCodeMap.put(-1, "群匹配失败");
	}
	
	@Autowired
	private EnterGroupMapper enterGroupMapper;
	
	
	@Override
	public MatchGroupResult matchGroup(Map<String, String> _matchingconds) {
		String groupid=matchGroupBy(_matchingconds);
		if(groupid != null){
			return buildMatchGroupResult(1,groupid);
		}
		return buildMatchGroupResult(-1, null);
	}
	
	@SuppressWarnings("deprecation")
	private String matchGroupBy(Map<String, String> _matchingconds){
		Map<String, String> param = new HashMap<>();
//		param.put("province", _matchingconds.get("province"));
//		param.put("hospitalName", _matchingconds.get("store"));
		param.put("city", _matchingconds.get("city"));
		String edc_str = _matchingconds.get("edc");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");//TODO 日期转换
		param.put("edc", format.format(new Date(edc_str)));
		param.put("groupTag", "5");//TODO
		param.put("GroupAttr", "城市群");//TODO
		List<String> groupIds = enterGroupMapper.matchGroupByFriso(param); 
		if(groupIds != null && groupIds.size() > 0){
			return groupIds.get(0);
		}
		return null;
	}
	
	private MatchGroupResult buildMatchGroupResult(int _resultCode, String _matchGroupId) {
		MatchGroupResult result = new MatchGroupResult();
		result.setResultCode(_resultCode);
		result.setMsg(resultCodeMap.get(_resultCode));
		result.setMatchGroupId(_matchGroupId);
		return result;
	}

}
