package cc.gemii.component.listener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import cc.gemii.component.sendMsg.SendMsgContext;
import cc.gemii.data.ShortMessage;

/**
 * 项目启动成功后对 短信发送功能进行自动验证
 * 
 * */
@Component
public class SendSMSVerfifyProcessor implements ApplicationListener<ContextRefreshedEvent> {

	private static final String SEND_CONTENT_TEMPLATE = "Hi,你所维护的系统(HelperManage)已于%s发布成功，可以开始进行验证啦,记得提交代码！";
	
	@Autowired
	private SendMsgContext sendMsgContext;
	@Autowired
	private Properties propertyConfigurer;
	/**
	 * 项目启动提醒，兼顾短信验证
	 * */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		if(event.getApplicationContext().getParent() == null && isProductEnv()){
			SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			String dateStr = df.format(new Date());
			String[] values = new String[]{dateStr};
			
			Formatter fm = new Formatter();
			String content = String.valueOf(fm.format(SEND_CONTENT_TEMPLATE,values));
			
			String phones = propertyConfigurer.getProperty("developer_phones");
			ShortMessage _shortMessage = new ShortMessage();
			_shortMessage.setContent(content);
			_shortMessage.setReciverPhone(phones);
			sendMsgContext.sendSMS(_shortMessage);
		}
		
	}

	/**
	 * 判断是否是生产环境
	 * */
	private boolean isProductEnv(){
		String env = propertyConfigurer.getProperty("run.env");
		String os = System.getProperty("os.name");
		return "product".equals(env) && os.contains("Linux");
	}
	
}
