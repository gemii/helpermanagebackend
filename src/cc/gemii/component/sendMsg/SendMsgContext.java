/**
 * Project Name:liz-mgadapter
 * File Name:SendMsgContext.java
 * Package Name:com.gemii.lizcloud.core.mgadapter.service.sendMsg
 * Date:2017年5月17日上午11:57:41
 * Copyright (c) 2017, yukang All Rights Reserved.
 *
*/

package cc.gemii.component.sendMsg;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;

/**
 * ClassName:SendMsgContext <br/>
 * 负责短信和邮件的发送网关的策略选择
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2017年5月17日 上午11:57:41 <br/>
 * @author   yk
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
@Component
public class SendMsgContext {
	
	private Logger log = Logger.getLogger(this.getClass());
	
	public enum SendMsgStrategy{
		/**
		 * 通过环信 发送短信，腾讯短信，梦网短信
		 * */
		HUANXIN_SENDSMS,ZHONGLAN_SENDSMS,TENCENT_SENDSMS,
		/**梦网短信*/
		MONTNETS_SENDSMS,
		/**网易云短信*/
		NETEASECLOUD_SENDSMS,
		/**塞邮发送短信*/
		SUBMAIL_SENDSMS
	}
	@Autowired
	private ISendSMS huanXinSendSMS;
	@Autowired
	private ISendSMS zhongLanSendSMS;
	@Autowired
	private ISendSMS tencentSendSMS;
	@Autowired
	private ISendSMS montnetsSendSMS;
	@Autowired
	private ISendSMS neteaseCloudSendSMS;
	@Autowired
	private ISendSMS subMailSendSMS;
	@Autowired
	private Properties propertyConfigurer;

	/**
	 * 发送短信<br/>
	 * 负责发送短信及短信发送网关的选择
	 * @param _sendMsgStrategy 发送短信的策略
	 * @param _shortMessage 发送的短信
	 * @return 发送结果
	 * */
	public GeneralContentResult<String>  sendSMS(ShortMessage _shortMessage){
		log.info("sendSMS--_shortMessage="+JSONObject.toJSONString(_shortMessage));
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		String switchInfo = propertyConfigurer.getProperty("send_msg_switch");
		if("true".equals(switchInfo)){
			SendMsgStrategy sendMsgStrategy = SendMsgStrategy.HUANXIN_SENDSMS;
			String channel = propertyConfigurer.getProperty("send_sms_channel");
			if(channel != null){
				sendMsgStrategy = SendMsgStrategy.valueOf(channel);
			}
			result = sendSMSByChannl(_shortMessage,sendMsgStrategy);
			log.info("sendSMS--sendResult="+JSONObject.toJSONString(result));
			return result;
		}else{
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			return result;
		}
	}
	
	private GeneralContentResult<String> sendSMSByChannl(ShortMessage _shortMessage,SendMsgStrategy sendMsgStrategy){
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		switch (sendMsgStrategy) {
			case HUANXIN_SENDSMS:
				result =  huanXinSendSMS.sendSMS(_shortMessage);
				break;
			case ZHONGLAN_SENDSMS:
				result =  zhongLanSendSMS.sendSMS(_shortMessage);
				break;
			case TENCENT_SENDSMS:
				result = tencentSendSMS.sendSMS(_shortMessage);
				break;
			case MONTNETS_SENDSMS:
				result = montnetsSendSMS.sendSMS(_shortMessage);
				break;
			case NETEASECLOUD_SENDSMS:
				result = neteaseCloudSendSMS.sendSMS(_shortMessage);
				break;
			case SUBMAIL_SENDSMS:
				result = subMailSendSMS.sendSMS(_shortMessage);
				break;
			default:
				break;
		}
		return result;
	}
	
	public GeneralContentResult<String>  sendSMSByHtml(ShortMessage _shortMessage,SendMsgStrategy sendMsgStrategy){
		log.info("sendSMS--_shortMessage="+JSONObject.toJSONString(_shortMessage));
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		String switchInfo = propertyConfigurer.getProperty("send_msg_switch");
		if("true".equals(switchInfo)){
			result = sendSMSByChannl(_shortMessage,sendMsgStrategy);
			log.info("sendSMS--sendResult="+JSONObject.toJSONString(result));
			return result;
		}else{
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			return result;
		}
	}
}

