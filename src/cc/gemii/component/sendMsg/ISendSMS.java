/**
 * Project Name:liz-mgadapter
 * File Name:ISendSMS.java
 * Package Name:com.gemii.lizcloud.core.mgadapter.service.sendMsg
 * Date:2017年5月17日上午11:57:17
 * Copyright (c) 2017, yukang All Rights Reserved.
 *
*/

package cc.gemii.component.sendMsg;

import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;

/**
 * ClassName:ISendSMS <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2017年5月17日 上午11:57:17 <br/>
 * @author   yk
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
public interface ISendSMS {
	
	/**
	 * 发送短信
	 * @param _shortMessage 短信对象
	 * @return 
	 * 	发送结果对象
	 * */
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage);
	
}

