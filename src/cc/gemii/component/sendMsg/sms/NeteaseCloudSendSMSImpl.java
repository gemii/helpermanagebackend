package cc.gemii.component.sendMsg.sms;

import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.sendMsg.ISendSMS;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.XMLUtil;

@Component("neteaseCloudSendSMS")
public class NeteaseCloudSendSMSImpl implements ISendSMS {

	private Logger log = Logger.getLogger(this.getClass());

	private static final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5',
	            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
	 
	private static final String URL = "https://api.netease.im/sms/sendtemplate.action";
	private static final String APPKEY = "96d597a574f9cc252711bd5943a512ef";
	private	static final String SECRET = "f0de03f81869";
	
	private static final Map<String,String> resultCodeMap = new HashMap<String,String>();
	static{
		resultCodeMap.put("200","操作成功");
		resultCodeMap.put("315","IP限制");
		resultCodeMap.put("403","非法操作或没有权限");
		resultCodeMap.put("414","参数错误");
		resultCodeMap.put("416","频率控制");
		resultCodeMap.put("500","服务器内部错误");
	}
	
	@Override
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage) {
		String nonce = UUID.randomUUID().toString();
		String curTime = String.valueOf(new Date().getTime());
		String checksum = getCheckSum(SECRET,nonce,curTime);
		
		Map<String,String> headers = new HashMap<String,String>();
		headers.put("AppKey", APPKEY);
		headers.put("CheckSum", checksum);
		headers.put("Nonce", nonce);
		headers.put("CurTime", curTime);
		headers.put("charset", "utf-8");
		headers.put("Content-Type", "application/x-www-form-urlencoded");
		Map<String,String> data = new HashMap<String,String>();
		if(StringUtils.isBlank(_shortMessage.getCode())){
			//TODO 通知类ID
			data.put("templateid", _shortMessage.getTemplateid());
		}else{
			//TODO 验证码类短信
			data.put("templateid","3053562");
			data.put("params", JSONObject.toJSONString(new String[]{_shortMessage.getCode()}));
		}
		
		data.put("mobiles",JSONObject.toJSONString(_shortMessage.getReciverPhone().split(",")));
		
		try {
			String response = HttpClientUtil.doPost(URL, data, headers);
			return buildSendResultByResonse(response);
		} catch (Exception e) {
			log.error(JSONObject.toJSONString(_shortMessage));
			log.error(e.getMessage(),e);
			return buildSendResultByResonse("error");
		}
	}

	private GeneralContentResult<String> buildSendResultByResonse(String _response){
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultContent(_response);
		if("error".equals(_response)){
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
		Map<String, Object> map = null;
		JSONObject obj = JSONObject.parseObject(_response);
		String code = String.valueOf(obj.getInteger("code"));
		if("200".equals(code)){
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			result.setDetailDescription("send success");
			return result;
		}else{
			result.setResultCode(ResultCode.OPERATION_FAIL);
			result.setDetailDescription("send fail"+resultCodeMap.get("code"));
			return result;
		}
	}

	private static String getCheckSum(String appSecret, String nonce, String curTime) {
        return encode("sha1", appSecret + nonce + curTime);
    }
	
	private static String encode(String algorithm, String value) {
        if (value == null) {
            return null;
        }
        try {
            MessageDigest messageDigest
                    = MessageDigest.getInstance(algorithm);
            messageDigest.update(value.getBytes());
            return getFormattedText(messageDigest.digest());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
	
    private static String getFormattedText(byte[] bytes) {
        int len = bytes.length;
        StringBuilder buf = new StringBuilder(len * 2);
        for (int j = 0; j < len; j++) {
            buf.append(HEX_DIGITS[(bytes[j] >> 4) & 0x0f]);
            buf.append(HEX_DIGITS[bytes[j] & 0x0f]);
        }
        return buf.toString();
    }
   
}
