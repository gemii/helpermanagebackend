package cc.gemii.component.sendMsg.sms;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.sendMsg.ISendSMS;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;

@Component("montnetsSendSMS")
public class MontnetsSendSMSImpl implements ISendSMS {

	private Logger log = Logger.getLogger(this.getClass());
	
	private final static String ip = "122.144.173.37";
	private final static String port = "8901";
	private final static String strUserId = "JJ0505";
	private final static String strPwd = "128488";

	@Override
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage) {
		try {
			String phone = _shortMessage.getReciverPhone();// 手机号码，用英文逗号,分隔，最大1000个号码。
			String strMessage = _shortMessage.getContent();// 短信内容
			String strSubPort = "*";// 扩展子号 （不带请填星号*，长度不大于6位）;
			String strUserMsgId = "0";// 用户自定义流水号，不带请输入0（流水号范围-（2^63）……2^63-1）
			StringBuffer strPtMsgId = new StringBuffer("");// 如果成功，存流水号。失败，存错误码。
			int result = this.SendSms(strPtMsgId, ip, port, strUserId, strPwd, phone, strMessage, strSubPort,
					strUserMsgId);// 短信息发送接口（相同内容群发，可自定义流水号）POST请求。

			return buildSendResultByResonse(String.valueOf(result));
		} catch (Exception e) {
			log.error(JSONObject.toJSONString(_shortMessage));
			log.error(e.getMessage(), e);
			return buildSendResultByResonse("error");
		}
	}

	private GeneralContentResult<String> buildSendResultByResonse(String _response) {
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultContent(_response);
		if ("error".equals(_response)) {
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
		if ("0".equals(_response)) {
			result.setResultCode(ResultCode.OPERATION_SUCCESS);
			result.setDetailDescription("send success");
			return result;
		} else {// 返回值为非0，代表失败
			result.setResultCode(ResultCode.OPERATION_FAIL);
			result.setDetailDescription("send fail");
			return result;
		}
	}

	private int SendSms(StringBuffer strPtMsgId, String ip, String port, String strUserId, String strPwd,
			String strMobiles, String strMessage, String strSubPort, String strUserMsgId) {
		int returnInt = -200;// 定义返回值变量
		try {

			String result = null;// 存放解析后的返回值
			Params p = new Params();
			p.setUserId(strUserId);// 设置账号
			p.setPassword(strPwd);// 设置密码
			p.setPszMobis(strMobiles);// 设置手机号码
			p.setPszMsg(strMessage);// 设置短信内容
			p.setIMobiCount(String.valueOf(strMobiles.split(",").length));// 设置手机号码个数
			p.setPszSubPort(strSubPort);// 设置扩展子号
			p.setMsgId(strUserMsgId);// 设置流水号
			String Message = executePost(p, "http://" + ip + ":" + port + "/MWGate/wmgw.asmx/" + "MongateSendSubmit");// 调用底层POST方法提交
			// 请求返回值不为空，则解析返回值
			if (Message != null && Message != "") {
				Document doc = DocumentHelper.parseText(Message);
				Element el = doc.getRootElement();
				result = el.getText();// 解析返回值
			} // 处理返回结果
			if (result != null && !"".equals(result) && result.length() > 10) {
				// 解析后的返回值不为空且长度大于10，则是提交成功
				returnInt = 0;
				strPtMsgId.append(result);
			} else if (result == null || "".equals(result)) {// 解析后的返回值为空，则提交失败
				strPtMsgId.append(returnInt);
			} else {// 解析后的返回值不为空且长度不大于10，则提交失败，返回错误码
					// returnInt=Integer.parseInt(result);
				strPtMsgId.append(returnInt);
			}
		} catch (Exception e) {
			returnInt = -200;
			strPtMsgId.append(returnInt);
			e.printStackTrace();// 异常处理
		}
		return returnInt;// 返回值返回
	}

	private String executePost(Object obj, String httpUrl) throws Exception {
		String result = "";
		Class cls = obj.getClass();
		Field[] fields = cls.getDeclaredFields();
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		// 设置请求参数
		String fieldName = null;
		String fieldNameUpper = null;
		Method getMethod = null;
		String value = null;
		for (int i = 0; i < fields.length; i++) {// 循环设置请求参数
			fieldName = fields[i].getName();
			fieldNameUpper = Character.toUpperCase(fieldName.charAt(0)) + fieldName.substring(1);
			if("This$0".equals(fieldNameUpper)){
				continue;
			}
			getMethod = cls.getMethod("get" + fieldNameUpper);// 通过反射获取get方法
			value = (String) getMethod.invoke(obj);// 通过反射调用get方法
			if (value != null) {// 请求参数值不为空，才设置
				params.add(new BasicNameValuePair(fieldName, value));
			}
		}
		HttpPost httppost = new HttpPost(httpUrl);// 设置HttpPost
		httppost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8)); // 设置参数的编码UTF-8
		HttpClient httpclient = new DefaultHttpClient();// 创建HttpClient
		HttpEntity entity = httpclient.execute(httppost).getEntity();// Http请求网关
		if (entity != null && entity.getContentLength() > 0) {// 返回值不为空，且长度大于0
			result = EntityUtils.toString(entity);// 将返回值转换成字符串
		} // 处理返回结果
		httpclient.getConnectionManager().shutdown();// 关闭连接
		return result;// 返回返回值
	}

	private class Params {
		private String userId; // 用户账号
		private String password; // 用户密码
		private String pszMobis; // 目标号码，用英文逗号(,)分隔，最大1000个号码。
		// 一次提交的号码类型不受限制，但手机会做验证，若有不合法的手机号将会被退回。
		// 号码段类型分为：移动、联通、电信手机 注意：请不要使用中文的逗号
		private String pszMsg; // 短信内容， 内容长度不大于350个汉字
		private String iMobiCount;// 号码个数（最大1000个手机）
		private String pszSubPort; // 子端口号码，不带请填星号{*}
									// 长度由账号类型定4-6位，通道号总长度不能超过20位。
		// 如：10657****主通道号，3321绑定的扩展端口，主+扩展+子端口总长度不能超过20位。
		private String MsgId; // 一个8字节64位的大整型（INT64），格式化成的字符串。
		// 因此该字段必须为纯数字，且范围不能超过INT64的取值范围（-(2^63）……2^63-1）
		// 即-9223372036854775807……9223372036854775808。
		// 格式化成字符串后最大长度不超过20个字节。
		private String iReqType; // 请求类型(0: 上行&状态报告 1:上行 2: 状态报告)
		private String Sa; // 扩展号
		private String multixmt;// 批量短信请求包。该字段中包含N个短信包结构体。每个结构体间用固定的分隔符隔开。
		// N的范围为1~200.
		// 分隔符为英文逗号(,).
		// 单个短信包结构体的内容见下表.

		public String getUserId() {
			return userId;
		}

		public void setUserId(String userId) {
			this.userId = userId;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getPszMobis() {
			return pszMobis;
		}

		public void setPszMobis(String pszMobis) {
			this.pszMobis = pszMobis;
		}

		public String getPszMsg() {
			return pszMsg;
		}

		public void setPszMsg(String pszMsg) {
			this.pszMsg = pszMsg;
		}

		public String getIMobiCount() {
			return iMobiCount;
		}

		public void setIMobiCount(String iMobiCount) {
			this.iMobiCount = iMobiCount;
		}

		public String getPszSubPort() {
			return pszSubPort;
		}

		public void setPszSubPort(String pszSubPort) {
			this.pszSubPort = pszSubPort;
		}

		public String getMsgId() {
			return MsgId;
		}

		public void setMsgId(String MsgId) {
			this.MsgId = MsgId;
		}

		public void setSa(String sa) {
			this.Sa = sa;
		}

		public String getSa() {
			return this.Sa;
		}

		public void setMultixmt(String multix_mt) {
			this.multixmt = multix_mt;
		}

		public String getMultixmt() {
			return multixmt;
		}

		public String getIReqType() {
			return iReqType;
		}

		public void setIReqType(String iReqType) {
			this.iReqType = iReqType;
		}
	}

}
