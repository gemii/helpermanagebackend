/**
 * Project Name:liz-mgadapter
 * File Name:HuanXinSendSMSImpl.java
 * Package Name:com.gemii.lizcloud.core.mgadapter.service.sendMsg.sms
 * Date:2017年5月17日下午2:36:13
 * Copyright (c) 2017, yukang All Rights Reserved.
 *
*/

package cc.gemii.component.sendMsg.sms;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.sendMsg.ISendSMS;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.XMLUtil;

/**
 * ClassName:HuanXinSendSMSImpl <br/>
 * 环信 接口发送短信
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2017年5月17日 下午2:36:13 <br/>
 * @author   yk
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
@Component("huanXinSendSMS")
public class HuanXinSendSMSImpl implements ISendSMS {

	private Logger log = Logger.getLogger(this.getClass());

//	private final static String Huanxin_URL = "http://123.56.206.101:8082/MWGate/wmgw.asmx/MongateCsSpSendSmsNew";
	//20170804 环信域名切换
	private final static String Huanxin_URL = "http://smp-emp.easemob.com:8082/MWGate/wmgw.asmx/MongateCsSpSendSmsNew";
	private final static String userId = "SHJLSC";
	private final static String password = "SHJLSC1";
	
	@Override
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage) {
		String[] mobiles = _shortMessage.getReciverPhone().split(",");
		String content = _shortMessage.getContent();
		try {
			Map<String, String> param = new LinkedHashMap<String, String>();
			param.put("userId", userId);
			param.put("password", password);
			param.put("pszMobis", StringUtils.join(mobiles, ","));
			param.put("pszMsg", content);
			param.put("iMobiCount", String.valueOf(mobiles.length));
			param.put("pszSubPort", "*");
			String response = HttpClientUtil.doPost(Huanxin_URL, param);
			log.info("真实一共发送了 "+mobiles.length+"条短信");
			return buildSendResultByResonse(response);
		} catch (Exception e) {
			log.error(JSONObject.toJSONString(_shortMessage));
			log.error(e.getMessage(),e);
			return buildSendResultByResonse("error");
		}
	}

	private GeneralContentResult<String> buildSendResultByResonse(String _response){
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultContent(_response);
		if("error".equals(_response)){
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
		Map<String, Object> map = null;
		try {
			map = XMLUtil.xml2map(_response);
			String responseStr = String.valueOf(map.get("string"));
			if(responseStr.length() > 10){
				result.setResultCode(ResultCode.OPERATION_SUCCESS);
				result.setDetailDescription("send success");
				return result;
			}else{
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("send fail");
				return result;
			}
		} catch (DocumentException e) {
			log.error(_response);
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
	}
}

