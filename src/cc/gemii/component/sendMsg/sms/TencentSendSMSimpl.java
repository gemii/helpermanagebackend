package cc.gemii.component.sendMsg.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import cc.gemii.component.sendMsg.ISendSMS;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.SHA1Util;
import cc.gemii.util.XMLUtil;

/**
 * 腾讯发送短信
 * @author gemii
 *
 */
@Component("tencentSendSMS")
public class TencentSendSMSimpl implements ISendSMS{
	
	
	private Logger log = Logger.getLogger(this.getClass());

	private final static String Tencent_URL = "https://yun.tim.qq.com/v5/tlssmssvr/sendsms?sdkappid=APPID&random=RANDOM";
	private static String AppId = "1400014204";
	private static String AppKey = "249bbc86b90f9eec2805fe048956557d";
	private static String SIG = "appkey=APPKEY&random=RANDOM&time=TIMESTAMP&mobile=MOBILE";


	@Override
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage) {
		String random = String.valueOf((int) (Math.random() * 10000));
		String timestamp = String.valueOf((System.currentTimeMillis()/1000));
		String url = Tencent_URL.replace("APPID", AppId).replace("RANDOM",random);
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, String> telMap = new HashMap<String, String>();
		telMap.put("nationcode", "86");
		telMap.put("mobile", _shortMessage.getReciverPhone());
		map.put("tel", telMap);
		map.put("type", "0");
		map.put("msg", _shortMessage.getContent());
		map.put("sig", SHA1Util.Encrypt(SIG.replace("APPKEY", AppKey).replace("RANDOM", random).replace("TIMESTAMP", timestamp).replace("MOBILE", _shortMessage.getReciverPhone()), "SHA-256"));
		map.put("time", timestamp);
		map.put("extend", "");
		map.put("ext", "");
		Map<String, Object> resultMap = (Map<String, Object>) JSON.parse(HttpClientUtil.doPostJson(url, JSON.toJSONString(map)));
		String result = String.valueOf(resultMap.get("result"));
		if("0".equals(result)){
			return buildSendResultByResonse("success");
		}else{
			return buildSendResultByResonse("error");
		}
	}
	
	
	private GeneralContentResult<String> buildSendResultByResonse(String _response){
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultContent(_response);
		if("error".equals(_response)){
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
		Map<String, Object> map = null;
		try {
			map = XMLUtil.xml2map(_response);
			String responseStr = String.valueOf(map.get("string"));
			if(responseStr.length() > 10){
				result.setResultCode(ResultCode.OPERATION_SUCCESS);
				result.setDetailDescription("send success");
				return result;
			}else{
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("send fail");
				return result;
			}
		} catch (DocumentException e) {
			log.error(_response);
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
	}

}
