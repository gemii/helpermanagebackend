/**
 * Project Name:liz-mgadapter
 * File Name:ZhongLanSendSMSImpl.java
 * Package Name:com.gemii.lizcloud.core.mgadapter.service.sendMsg.sms
 * Date:2017年6月13日上午11:05:43
 * Copyright (c) 2017, yukang All Rights Reserved.
 *
*/

package cc.gemii.component.sendMsg.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.sendMsg.ISendSMS;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.util.HttpClientUtil;
import cc.gemii.util.XMLUtil;

/**
 * ClassName:ZhongLanSendSMSImpl <br/>
 * 众览SMS 不支持群发
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2017年6月13日 上午11:05:43 <br/>
 * @author   yk
 * @version  
 * @since    JDK 1.8
 * @see 	 
 */
@Component("zhongLanSendSMS")
public class ZhongLanSendSMSImpl implements ISendSMS {

	private Logger log = Logger.getLogger(this.getClass());

	private static final String ZhongLan_URL = "http://www.wemediacn.net/api/smsservice.asmx/SendSMS";
	private static final String TokenID = "611408984465753557160497";
	
	@Override
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage) {
		try {
			String content = _shortMessage.getContent();
			String phone = _shortMessage.getReciverPhone();
			Map<String,String> paramMap = new HashMap<String,String>();
			paramMap.put("TokenID", TokenID);
			paramMap.put("Content", content);
			if(StringUtils.isNotBlank(_shortMessage.getCode())){
				content = "尊敬的用户,您的验证码为:"+_shortMessage.getCode()+",有效期60分钟";
				log.info("短信内容替换为："+content);
				paramMap.put("Content", content);
			}
			paramMap.put("mobile", phone);
			String response = "";
			//短信群发
			if(phone.contains(",")){
				for (String p : phone.split(",")) {
					paramMap.put("mobile", p);
					log.debug("返回值：request="+JSONObject.toJSONString(paramMap));
					response = HttpClientUtil.doGet(ZhongLan_URL, paramMap);
					log.debug("返回值：response="+response);
				}
			}else{
				log.debug("返回值：request="+JSONObject.toJSONString(paramMap));
				response = HttpClientUtil.doGet(ZhongLan_URL, paramMap);
				log.debug("返回值：response="+response);
			}
			return buildSendResultByResonse(response);
		} catch (Exception e) {
			log.error(JSONObject.toJSONString(_shortMessage));
			log.error(e.getMessage(),e);
			return buildSendResultByResonse("error");
		}
	}
	
	private GeneralContentResult<String> buildSendResultByResonse(String _response){
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultContent(_response);
		if("error".equals(_response)){
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
		Map<String, Object> map = null;
		try {
			map = XMLUtil.xml2map(_response);
			String responseStr = String.valueOf(map.get("string"));
			if(responseStr.startsWith("OK")){
				result.setResultCode(ResultCode.OPERATION_SUCCESS);
				result.setDetailDescription("send success");
				return result;
			}else{
				result.setResultCode(ResultCode.OPERATION_FAIL);
				result.setDetailDescription("send fail");
				return result;
			}
		} catch (DocumentException e) {
			log.error(_response);
			log.error(e.getMessage(),e);
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
	}

}

