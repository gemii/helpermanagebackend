package cc.gemii.component.sendMsg.sms;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cc.gemii.component.sendMsg.ISendSMS;
import cc.gemii.constant.ResultCode;
import cc.gemii.data.GeneralContentResult;
import cc.gemii.data.ShortMessage;
import cc.gemii.util.HttpClientUtil;

@Component("subMailSendSMS")
public class SubMailSendSMSImpl implements ISendSMS{

	private Logger log = Logger.getLogger(this.getClass());

	private final static String	url = "https://api.mysubmail.com/message/multixsend.json";
	private final static String APPID = "14993";
	private final static String SIGNATURE = "61170d2d31c9dd76e38ee82133f5c051";
	
	@Override
	public GeneralContentResult<String> sendSMS(ShortMessage _shortMessage) {
		try {
			Map<String,String> reqMap = buildReqMap(_shortMessage);
			String resp = HttpClientUtil.doPost(url, reqMap);
			return buildSendResultByResonse(resp);
		}catch (Exception e) {
			log.error(JSONObject.toJSONString(_shortMessage));
			log.error(e.getMessage(),e);
			return buildSendResultByResonse("error");
		}
	}

	private Map<String,String> buildReqMap(ShortMessage _shortMessage){
		Map<String,String> result = new HashMap<String,String>();
		result.put("appid", APPID);
		if(StringUtils.isBlank(_shortMessage.getCode())){
			result.put("project", "PGl2G2");
		}else{
			result.put("project", "2cvo54");
		}
		result.put("signature", SIGNATURE);
		JSONArray arr = new JSONArray();
		for (String phone : _shortMessage.getReciverPhone().split(",")) {
			JSONObject obj = new JSONObject();
			obj.put("to",phone);
			if(StringUtils.isNotBlank(_shortMessage.getCode())){
				JSONObject vars = new JSONObject();
				vars.put("code", _shortMessage.getCode());
				vars.put("time", "60分钟");
				obj.put("vars", vars);
			}
			arr.add(obj);
		}
		result.put("multi", arr.toJSONString());
		return result;
	}
	
	private GeneralContentResult<String> buildSendResultByResonse(String _response){
		GeneralContentResult<String> result = new GeneralContentResult<String>();
		result.setResultContent(_response);
		if("error".equals(_response)){
			result.setResultCode(ResultCode.OPERATION_ERROR);
			result.setDetailDescription("send error");
			return result;
		}
		JSONArray arr = JSONArray.parseArray(_response);
		int successCount = 0;
		int errorCount = 0;
		for (int i = 0;i < arr.size();i++) {
			JSONObject obj = arr.getJSONObject(i);
			if("success".equals(obj.get("status"))){
				successCount++;
			}else{
				errorCount++;
			}
		}
		result.setResultCode(ResultCode.OPERATION_SUCCESS);
		result.setDetailDescription("send success="+successCount+"-error="+errorCount);
		return result;
	}
}
