package cc.gemii.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisUtil {
	// Redis服务器IP
	private static String ADDRESS;

	// Redis的端口号
	private static int PORT;

	// 访问密码
	private static String AUTH;

	// 可用连接实例的最大数目，默认值为8；
	// 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
	private static int MAX_ACTIVE;

	// 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
	private static int MAX_IDLE;

	// 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
	private static int MAX_WAIT;

	// 超时时间
	private static int TIMEOUT;

	private static JedisPool jedisPool = null;

	/**
	 * redis过期时间,以秒为单位
	 */
	public final static int EXRP_HOUR = 60 * 60; // 一小时
	public final static int EXRP_DAY = 60 * 60 * 24; // 一天
	public final static int EXRP_MONTH = 60 * 60 * 24 * 30; // 一个月

	public RedisUtil(){}
	
	public RedisUtil(String address,int port,String password,int maxActive,int maxIdle,int maxWait,int timeout) {
		this.ADDRESS = address;
		this.PORT = port;
		this.AUTH = password;
		this.MAX_ACTIVE = maxActive;
		this.MAX_IDLE = maxIdle;
		this.MAX_WAIT = maxWait;
		this.TIMEOUT = timeout;
		getJedisPool();
	}
	
	public synchronized static JedisPool getJedisPool() {
		if (jedisPool == null) {
			JedisPoolConfig config = new JedisPoolConfig();
			config.setMaxTotal(MAX_ACTIVE);
			config.setMaxIdle(MAX_IDLE);
			config.setMaxWaitMillis(MAX_WAIT);
			if("".equals(AUTH)||null == AUTH){
				jedisPool = new JedisPool(config, ADDRESS, PORT, TIMEOUT);
			}else{
				jedisPool = new JedisPool(config, ADDRESS, PORT, TIMEOUT,AUTH);
			}
			return jedisPool;
		}
		return jedisPool;
	}

	public static void destroyJedisPool(){
		jedisPool.destroy();
	}
	
	public synchronized static Jedis getResource() {
		Jedis jedis = null;
		jedis = getJedisPool().getResource();
		return jedis;
	}

	public static void returnResource(Jedis jedis) {
		if (jedis != null) {
			jedisPool.returnResource(jedis);
		}
	}

	public static void publishMessage(String channel, String message) {
		Jedis jedis = getResource();
		jedis.publish(channel, message);
		returnResource(jedis);
	}
	
	public static String getIP(){
		return ADDRESS + "_";
	}

	public static void sendSubscribeMessage(List<String> channels, String type){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("MsgType", type);
		Map<String, Object> Msg = new HashMap<String, Object>();
		Msg.put("Uin", "");
		Msg.put("RoomIDs", null);
		Msg.put("Content", channels);
		Msg.put("FileName", "");
		map.put("Msg", Msg);
		publishMessage(getIP(), JSON.toJSONString(map));
	}
	
	
	public static String set(String key, String value) {
		Jedis jedis = getResource();
		String result = jedis.set(key, value);
		returnResource(jedis);
		return result;
	}
	
	public static String set(String key, String value,int seconds) {
		Jedis jedis = getResource();
		String result = jedis.set(key, value);
		jedis.expire(key, seconds);
		returnResource(jedis);
		return result;
	}
	
	public static String get(String key) {
		Jedis jedis = getResource();
		String result = jedis.get(key);
		returnResource(jedis);
		return result;
	}

	
	public static Long hset(String key, String item, String value) {
		Jedis jedis = getResource();
		Long result = jedis.hset(key, item, value);
		returnResource(jedis);
		return result;
	}

	
	public static String hget(String key, String item) {
		Jedis jedis = getResource();
		String result = jedis.hget(key, item);
		returnResource(jedis);
		return result;
	}

	
	public static Long incr(String key) {
		Jedis jedis = getResource();
		Long result = jedis.incr(key);
		returnResource(jedis);
		return result;
	}

	
	public static Long decr(String key) {
		Jedis jedis = getResource();
		Long result = jedis.decr(key);
		returnResource(jedis);
		return result;
	}

	
	public static Long expire(String key, int second) {
		Jedis jedis = getResource();
		Long result = jedis.expire(key,second);
		returnResource(jedis);
		return result;
	}

	
	public static Long ttl(String key) {
		Jedis jedis = getResource();
		Long result = jedis.ttl(key);
		returnResource(jedis);
		return result;
	}

	
	public static Long del(String key) {
		Jedis jedis = getResource();
		Long result = jedis.del(key);
		returnResource(jedis);
		return result;
	}

	
	public static Long hdel(String key, String item) {
		Jedis jedis = getResource();
		Long result = jedis.hdel(key, item);
		returnResource(jedis);
		return result;
	}

	
	public static Long hincyby(String key, String item, Long value) {
		Jedis jedis = getResource();
		Long result = jedis.hincrBy(key, item, value);
		returnResource(jedis);
		return result;
	}
	
}
