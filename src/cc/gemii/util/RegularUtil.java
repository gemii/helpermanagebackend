package cc.gemii.util;

import java.util.regex.Pattern;

/**
 * 正则表达式验证工具类
 * 
 * @author gemii
 *
 */
public class RegularUtil {

	/**
	 * 验证日期格式是否为yyyy-MM-dd(简单验证)
	 * @param str
	 * @return
	 */
	public static boolean isDate(String str) {
		Pattern p = Pattern.compile("^\\d{4}(\\-|\\/|\\.)\\d{2}\\1\\d{1,2}$");
		boolean m = p.matcher(str).matches();
		return m;
	}

}
