package cc.gemii.util;

public class TagUtil {

	private static int tag = 0;
	
	public synchronized static int getTag(){
		int temp = tag;
		tag ++;
		return temp;
	}
	
	public static void setTag(int compute){
		tag = compute;
	}
}
