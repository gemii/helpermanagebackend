package cc.gemii.util;

public class ConfigUtil {
	
	// 入群状态
	public static final String NOT_INTO_ROOM = "未入群";

	public static final String ENTER_ROOM_STATUS_SUCCESS = "已入群";
		
	public static final String ENTER_ROOM_STATUS_INVITE = "已邀请";
	
	public static final String ENTER_ROOM_STATUS_HM = "手动邀请";
	
	// 小助手在线描述
	public static final String ROBOT_ONLINE = "online";

	// 小助手未在线描述
	public static final String ROBOT_OFFLINE = "offline";

	// 群匹配 状态
	public static final String MATCH_GROUP_FAILED_COUNTRY = "匹配全国群";
	
	public static final String MATCH_GROUP_FAILED_CITY = "匹配城市群";
	
	public static final String MATCH_GROUP_FAILED_NONE = "未匹配到群";
	
	public static final String MATCH_GROUP_SUCCESS = "群匹配成功";

	// 因没有城市群或城市群人满,群匹配失败
	public static final String MATCH_GROUP_FAILED_FULL = "群匹配失败(城市群人满)";
	
	// 二维码发送状态
	public static final String SEND_QRCODE_SUCCESS = "二维码发送成功";
	
	// 发送H5状态
	public static final String SEND_H5_SUCCESS = "发送成功";

	public static final String SEND_H5_CLICK = "已点击";
	
	public static final String SEND_H5_UPFORM = "发送成功(提交表单)";
	
	// 未确认状态
	public static final String NOT_CONFIRM = "未确认";

	// 已确认好友
	public static final String ADDED_FRIEND = "已确认好友";
	
	// 未确认好友
	public static final String NOT_ADDED_FRIEND = "未确认好友";
	
	//H5 Plan A:先加小助手再填H5
	public static final String H5_PLAN_A = "A";
	
	//H5 Plan B:先填H5再加小助手
	public static final String H5_PLAN_B = "B";
	
	//不存在的城市  医院
	public static final String OTHERS = "其他";

	//邀请码状态
	public static final String PHONE_CODE_SEND_SUCCESS = "已发送";
	
	public static final String PHONE_CODE_SEND_FAILED = "未发送";
	
	public static final String PHONE_CODE_RESEND = "重新发送";
	
	//确认状态
	public static final String CONFIRM_STATUS = "人工确认";
}
