package cc.gemii.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.jets3t.service.S3Service;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;

public class FileUtil {

	private static String awsAccessKey;
	private static String awsSecreyKey;
	private static String defaultBucketName = "nfs.gemii.cc";
	private static String url_prex = "http://nfs.gemii.cc/";
	
	public FileUtil(){}
	public FileUtil(String accessKey,String secreyKey){
		this.awsAccessKey = accessKey;
		this.awsSecreyKey = secreyKey;
	}
	
	public static String uploadFile(String dir, String filename, byte[] bytes){
		return uploadFile(null, dir, filename, bytes);
	}
	
	public static String uploadFile(String dir, String filename, InputStream in) throws IOException{
		byte[] byt = input2byte(in);
		return uploadFile(null, dir, filename, byt);
	}
	
	/**
	 * 
	 * @param bytes
	 * @return
	 * TODO 上传图片
	 */
	private static String uploadFile(String bucketName, String dir, String filename, byte[] bytes) {
		try {
			String dir_filename = (dir == null?"":(dir + "/")) + filename;
			AWSCredentials awsCredentials = new AWSCredentials(awsAccessKey,awsSecreyKey);
			S3Service s3Service = new RestS3Service(awsCredentials);
			S3Bucket s3Bucket = s3Service.getBucket(bucketName == null?defaultBucketName : bucketName);
			S3Object s3Object = new S3Object(dir_filename);
			ByteArrayInputStream greetingIS = new ByteArrayInputStream(bytes);
			s3Object.setDataInputStream(greetingIS);
			s3Object.setContentLength(bytes.length);
			s3Service.putObject(s3Bucket, s3Object);
			// return url
			return url_prex + dir_filename;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static final byte[] input2byte(InputStream inStream)  
            throws IOException {  
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();  
        byte[] buff = new byte[500];  
        int rc = 0;  
        while ((rc = inStream.read(buff, 0, 100)) > 0) {  
            swapStream.write(buff, 0, rc);  
        }  
        byte[] in2b = swapStream.toByteArray();  
        return in2b;  
    }  
	
}
