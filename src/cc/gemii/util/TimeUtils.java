package cc.gemii.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * class_name：TimeUtils description： 用于返回时间值以及截取时间等操作 editor：yunfei.han date：Jul
 * 27, 2016 1:50:02 PM
 * 
 * @version V1.0
 *
 */
public class TimeUtils {

	/**
	 * 
	 * @return 纯数字类型的当前时间
	 * @throws ParseException
	 */
	public static String getTime() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");// 设置日期格式
		return df.format(new Date());
	}

	public static Date getDate() {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");// 设置日期格式
		try {
			return df.parse(df.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param 时间类型
	 *            如：yyyy-mm-dd HH:mm:ss
	 * @return 格式化后的当前时间
	 */
	public static String getTime(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		return df.format(new Date());
	}

	public static Date getDate(String type) {
		SimpleDateFormat df = new SimpleDateFormat(type);// 设置日期格式
		try {
			return df.parse(df.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @Title: 日期类型的转换
	 * @Description: TODO
	 * @param @param 要转化的美式日期 如：Tue Aug 02 11:00:18 CST 2016
	 * @return 转化后的日期 如2016-08-02 11:00:18
	 * @throws
	 */
	public static String changeDate(Date date) {
		try {
			/*
			 * if (date.matches(".*[a-zA-Z]+.*") == true) { SimpleDateFormat sdf
			 * = new SimpleDateFormat( "EEE MMM dd HH:mm:ss z yyyy", Locale.US);
			 * Date d = sdf.parse(date);
			 */
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date.toString();
	}

	/**
	 * 
	 * @Title 获取时间差
	 * @param 要比较的时间
	 * @return 该时间距离今天还有多少天 负数代表该天已经过去
	 * @throws
	 */
	public static long dateDiff(Date date) {
		Date date1 = TimeUtils.getDate();
		long l = date.getTime() - date1.getTime();
		long day = l / (24 * 60 * 60 * 1000);
		return day;
	}

	/**
	 * 
	 * @Title 判断当前时间是否在周一零点到周二上午12点之间
	 * @return True or False
	 */
	public static boolean isMonOrThu() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 1) // 星期一直接返回true
			return true;
		if (week == 2) { // 星期二判断是否在十二点之前
			int hour = cal.get(Calendar.HOUR_OF_DAY);
			if (hour < 12)
				return true;
		}
		return false;
	}

	/**
	 * 
	 * @Title 字符串转化为日期类型
	 * @param 日期类型
	 * @return yyyy-MM-dd类型的日期
	 * @throws
	 */
	public static Date StringToDate(String string) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		try {
			date = sdf.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 
	 * @Title 字符串转化为日期类型
	 * @param 日期类型
	 * @return 全类型的日期
	 * @throws
	 */
	public static Date StringToDateAll(String string) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		try {
			date = sdf.parse(string);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 
	 * @param date_string
	 * @return
	 * TODO yyyyMMddHH:mm:ss to yyyy-MM-dd HH:mm:ss
	 */
	public static String patternConvert(String date_string){
		try {
			SimpleDateFormat sdf_start = new SimpleDateFormat("yyyyMMddHHmmss");
			SimpleDateFormat sdf_end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return sdf_end.format(sdf_start.parse(date_string));
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
}
