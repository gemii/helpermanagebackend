package cc.gemii.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParamUtil {

	//api 白名单
	private static Map<String, String> ips = null;
	
	public static Map<String, String> getAcceptIP(){
		if(ips == null){
			ips = new HashMap<String, String>();
			ips.put("127.0.0.1", "");
			ips.put("0:0:0:0:0:0:0:1", "");
		}
		return ips;
	}
	
	//add robot url 配置文件已配置
//	public static final String START_ROBOT_URL = "http://localhost:8082/cmd?act=start&bot=BOT&new=NEW";
	
	//get user info from wechat
	public static final String GET_USERINFO_BY_OPENID = "http://wx.gemii.cc/wx/getUserinfoByFW?openid=OPEN_ID";
	
	//add robot response : start success
	public static final String START_ROBOT_SUCCESS = "0";
	
	public static final String UNKOWN_ROBOT_TAG = "1";
	
	//redis : login
	public static final String ITEM_LOGIN = "login";
	
	//not found QR code url
	public static final String NOT_FOUND_QRCODE = "未获取到二维码URL";
	
	//redis NextTag
    public static final String ROBOT_NEXT_TAG = "NextTag";
    
 // 扫描二维码
 	public static final String SCAN_QRCODE = "1";
 	
 	private static List<String> chong_qing_wanhe = null;
 	
 	public static List<String> get_chong_qing_wanhe(){
 		if(chong_qing_wanhe == null){
 			chong_qing_wanhe = new ArrayList<String>();
 			chong_qing_wanhe.add("渝中");
 			chong_qing_wanhe.add("巴南");
 			chong_qing_wanhe.add("南岸");
 			chong_qing_wanhe.add("江北");
 			chong_qing_wanhe.add("渝北");
 			chong_qing_wanhe.add("九龙坡");
 			chong_qing_wanhe.add("大渡口");
 			chong_qing_wanhe.add("沙坪坝");
 			chong_qing_wanhe.add("璧山");
 			chong_qing_wanhe.add("彭水");
 			chong_qing_wanhe.add("黔江");
 			chong_qing_wanhe.add("綦江");
 			chong_qing_wanhe.add("南川");
 			chong_qing_wanhe.add("荣昌");
 			chong_qing_wanhe.add("大足");
 			chong_qing_wanhe.add("铜梁");
 			chong_qing_wanhe.add("北碚");
 			chong_qing_wanhe.add("长寿");
 			chong_qing_wanhe.add("合川");
 			chong_qing_wanhe.add("梁平");
 			chong_qing_wanhe.add("万开区");
 			chong_qing_wanhe.add("江津");
 			chong_qing_wanhe.add("潼南");
 			chong_qing_wanhe.add("秀山");
 			chong_qing_wanhe.add("永川");
 			chong_qing_wanhe.add("石柱");
 			chong_qing_wanhe.add("武隆");
 		}
 		return chong_qing_wanhe;
 	}
 	
}
